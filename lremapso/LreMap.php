<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<?php
$paperId_1 = $_GET['paperId'];
$first = $_POST['first'];
//$paperId = $_POST['paperId'];
$paperId = (isset($_POST['paperId']) ? $_POST['paperId'] : $_GET['paperId']);
//$the_conf = (isset($_POST['conf']) ? $_POST['conf'] : $_GET['conf']);
$the_conf="THE_CONF";
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE>The LreMap Stand-Off Version</TITLE>
<STYLE>

<!--
  @import url(https://www.softconf.com/lrec2016/css/start.css); /* start CSS */
  @import url(https://www.softconf.com/lrec2016/css/topmenu.css); /* topmenu CSS */
  @import url(https://www.softconf.com/lrec2016/css/schedulemaker.css); /* schedulemaker CSS */
  @import url(https://www.softconf.com/lrec2016/css/submitPaper.css); /* submitpaper CSS */
  @import url(https://www.softconf.com/lrec2016/css/tab.css); /* tab CSS */

-->

</STYLE>


<style>
      .ui-autocomplete {
      max-height: 200px;
      overflow-y: auto;
      /* prevent horizontal scrollbar */
      overflow-x: hidden;
      /* add padding to account for vertical scrollbar */
      padding-right: 20px;
      }
      /* IE 6 doesn't support max-height
      * we use height instead, but this forces the menu to always be this tall
      */
      * html .ui-autocomplete {
      height: 100px;
      }

</style>



<script type="text/javascript" src="https://www.softconf.com/lrec2016/javascript/XulMenu.js"></script>
<script type="text/javascript" src="https://www.softconf.com/lrec2016/javascript/utils.js"></script>
<script type="text/javascript" src="https://www.softconf.com/lrec2016/javascript/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="https://www.softconf.com/lrec2016/javascript/jquery-migrate-1.2.1.js"></script>
<script type="text/javascript" src="https://www.softconf.com/lrec2016/javascript/jquery-ui.min.js"></script>
<script type="text/javascript" src="https://www.softconf.com/lrec2016/javascript/jquery.dialogextend.min.js"></script>
<script type="text/javascript" src="https://www.softconf.com/lrec2016/javascript/jquery.form.js"></script> 
<script type="text/javascript" src="https://www.softconf.com/lrec2016/javascript/jquery.jeditable.js"></script> 
<script type="text/javascript" src="https://www.softconf.com/lrec2016/javascript/jquery.startutils.js"></script> 
<link type="text/css" href="https://www.softconf.com/lrec2016/css/jquery-ui.css" rel="stylesheet" />

<script type="text/javascript">
     jQuery.noConflict();
</script>
</HEAD>
<BODY BGCOLOR='#ffffff'>


<script type="text/javascript">
  jQuery(function() {
    
    setToolTip();
  });
</script>
<?php
include("./lremapso_files/custom.php");
?>




<div>
<?php
include("./lremapso_files/addinfo.php");
?>
  
</div>



<div id="bar">
  <table cellspacing="0" cellpadding="0" id="menu1" class="XulMenu">
    <tr>
    <!--
      <td id="topMenuAjax">
	<script type="text/javascript">
  jQuery(document).ready(function() {
    jQuery.ajax({
      url: 'https://www.softconf.com/lrec2016/main/user/scmd.cgi?scmd=topMenu_Ajax',
      type: "post",
      dataType: 'json',
      data: {
        "mytime" : new Date().getTime()
      },
      success: function(returnData){
        jQuery("#topMenuAjax").html(returnData.thehtml);
        topMenuLoaded();
      },
      error: function(e) { ; }
    });
  });
</script>

      </td>
-->
      <td width="100%">
	&nbsp;
      </td>
      <td><a href="http://www.ilc.cnr.it" target="_blank">
		  <img src="./lremapso_files/logos/CNR-ILC_Logo_icon.png" border=0 title='Visit CNR-ILC' style="padding: 0px;">
		</a>
        </td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td><a href="http://www.elda.org" target="_blank">
		  <img src="./lremapso_files/logos/elra-elda-logo_icon.png" border=0 title='Visit ELRA/ELDA' style="padding: 0px;">
		</a>
        </td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>


      <td>
	<span id="warningdialog_menuheader" style="display:none; float: right" style="padding: 0px;">
	  <table border=3 style="padding: 0px;">
	    <tr>
	      <td style="padding: 0px;">
		<a href="javascript:showWarning()">
		  <img src="https://www.softconf.com/lrec2016/images/exclamationmark14.png" border=0 title='Click to show the Warnings window.' style="padding: 0px;">
		</a>
	      </td>
	      <td style="padding: 0px;" nowrap>
		<a href="javascript:showWarning();" title='Click to show the Warnings window.'>
		  &nbsp;<span id="warningdialog_text"></span>
		</a>
	      </td>
	    </tr>
	  </table>
	</span>

	<div style="display:none">
	  <div id="warningdialog" title="Warnings">
	    <div style="overflow: auto; height:100%; width:100%; font-size: 12px;">
	      <div id='warningdialog_content'>Loading...
	      </div>
	    </div>
	  </div>
	</div>


	<script type="text/javascript">
	  function showWarning() {
	    jQuery('#warningdialog').dialog('open');
	  }

	 

	</script>

      </td>


      <td nowrap>
	<span id="warningdialog_separator" style="display:none">
	  &nbsp;&nbsp;&nbsp;
	</span>
      </td> </tr>
  </table>
</div>

<script type="text/javascript">
  var topMenuCounter = 0;
  function topMenuLoaded() {
    topMenuCounter++;
    if (topMenuCounter == 2) {
      var menu1 = new XulMenu("menu1");
      menu1.arrow1 = "https://www.softconf.com/lrec2016/images/arrow1.gif";
      menu1.arrow2 = "https://www.softconf.com/lrec2016/images/arrow2.gif";
      menu1.init();
    }
  }
</script>


<!--
The following is not an error.  It's to outsmart IE7 to presenting the
dimensions as it should.
// -->

<div style="width=100%; COLOR: #000000; BACKGROUND-COLOR: #ffffff; padding-left: 10px; padding-right: 10px; padding-top: 15px; padding-bottom: 5px">

<form  ENCTYPE="multipart/form-data"

       ACTION="result.php" 

       METHOD=POST
       name='myForm'
       accept-charset='UTF-8'> 

<input type='hidden' id="theaction" name='page_theaction' value="">
<input type='hidden' id="pageid" name='pageid' value="0">
<input type='hidden' id="customsubmissionpage_id" name='customsubmissionpage_id' value="0">
<input type='hidden' id="paperId" name='paperId' value="<?php echo $paperId;?>">
<input type='hidden' id="conf" name='conf' value="<?php echo $the_conf;?>">
<input type='hidden' id="pageid" name='pageid' value="0">
<input type='hidden' id="numofres" name='numofres' value="0">


<link rel="stylesheet" type="text/css" href="https://www.softconf.com/lrec2016/css/table.css" media="all">
<script type="text/javascript" src="https://www.softconf.com/lrec2016/javascript/table.js"></script>
<?php
include("./lremapso_files/confinfo.php");
?>
<style>
  .ui-autocomplete {
  max-height: 100px;
  overflow-y: auto;
  /* prevent horizontal scrollbar */
  overflow-x: hidden;
  /* add padding to account for vertical scrollbar */
  padding-right: 20px;
  }
  /* IE 6 doesn't support max-height
  * we use height instead, but this forces the menu to always be this tall
  */
  * html .ui-autocomplete {
  height: 100px;
  }
</style>

<p>
 
<b>LRE Map</b></br></br>
The LRE Map, launched at LREC2010, sheds light on the vast amount of resources that
represent the background of the research presented at LREC and other conferences.


<p>
It is accessible at <a href="http://www.resourcebook.eu/"
target='_blank'>http://www.resourcebook.eu</a>.
</br>
</br>
The purpose of the following section is to discover, collect and 
document the use and creation of language resources (datasets, tools, 
etc.). We want to collect information on both existing resources that you used
for your research, and on resources that you have newly created. The resource entry form is
very simple to make this process as easy as possible for you. To help normalizing language
resource metadata we have introduced new features (such as autocompletion) that take into
account metadata inserted so far by all of you.
</br>
</br>
In order to allow accurate citations of the LR(s) cited in your LREC 
paper, you will be able to enter the ISLRN (International Standard 
Language Resource Number) of each resource you are entering in the LRE-Map.
If the resource does not have an ISLRN (make sure to tick the box "No 
ISLRN/I don't know"), ELRA will handle the assignment of this 
identifier for you.
</br>



If you <b>do not have</b> any resources to describe please close the page and exit. We kindly thank for not messing up our data.
<p>
<!--
<blockquote>
   <input type=checkbox 
          id=noResources
          name='OmitResources'
          onclick="noresources()"
          value="yes"
          > No resources to report with this submission.


</blockquote>
-->
<div id=resourceBlock>

<span id='database_0' style='display:none'>PDTB</span>
<span id='database_1' style='display:none'>Sample annotated text for definiteness annotations</span>
<span id='database_2' style='display:none'>A Library for Large Linear Classification (LIBLINEAR 1.51)</span>
<span id='database_3' style='display:none'>Sportic</span>
<span id='database_4' style='display:none'>mwetoolkit</span>
<span id='database_5' style='display:none'>MERLOT Corpus</span>
<span id='database_6' style='display:none'>GIVE-2 Corpus Collection Software</span>
<span id='database_7' style='display:none'>Sentence Re-ranker based on Information Extraction</span>
<span id='database_8' style='display:none'>KinOath</span>
<span id='database_9' style='display:none'>FrameNet</span>
<span id='database_10' style='display:none'>a Beautiful Anaphora Resolution Toolkit (BART)</span>
<span id='database_11' style='display:none'>Penn Discourse Treebank</span>
<span id='database_12' style='display:none'>A hybrid System for Evaluations Analysis</span>
<span id='database_13' style='display:none'>DanNet</span>
<span id='database_14' style='display:none'>SALTO</span>
<span id='database_15' style='display:none'>JOS ToTaLe text analyser</span>
<span id='database_16' style='display:none'>CoNLL 2006 Shared Task Data</span>
<span id='database_17' style='display:none'>Twitter Dataset</span>
<span id='database_18' style='display:none'>Data repository of spontaneous spoken Czech</span>
<span id='database_19' style='display:none'>TreeTagger</span>
<span id='database_20' style='display:none'>The Eurogene corpus</span>
<span id='database_21' style='display:none'>Celebrity</span>
<span id='database_22' style='display:none'>Syntactic Annotation Guidelines for the Quranic Arabic Dependency Treebank</span>
<span id='database_23' style='display:none'>Hunalign</span>
<span id='database_24' style='display:none'>IMDB actors</span>
<span id='database_25' style='display:none'>Sinica Corpus of Modern Chinese</span>
<span id='database_26' style='display:none'>WASP and WASP^{-1}</span>
<span id='database_27' style='display:none'>OntoNotes 4.0</span>
<span id='database_28' style='display:none'>RWTH Phoenix Weather Forecast</span>
<span id='database_29' style='display:none'>Web Service</span>
<span id='database_30' style='display:none'>ERG</span>
<span id='database_31' style='display:none'>ZAYA</span>
<span id='database_32' style='display:none'>Serbian morphological electronic dictionary (SrpRec)</span>
<span id='database_33' style='display:none'>TxtCeram</span>
<span id='database_34' style='display:none'>An unsupervised incremental parser (CCL)</span>
<span id='database_35' style='display:none'>British National Corpus (BNC)</span>
<span id='database_36' style='display:none'>Petit Larousse Illustré 1905</span>
<span id='database_37' style='display:none'>GRASS - The Grammar Structure Scenario</span>
<span id='database_38' style='display:none'>GeolISSTerm</span>
<span id='database_39' style='display:none'>Paragraph Clustering List (LINA-PCL-1.0)</span>
<span id='database_40' style='display:none'>JWPL</span>
<span id='database_41' style='display:none'>Bochum English Countability Lexicon</span>
<span id='database_42' style='display:none'>ReVerb</span>
<span id='database_43' style='display:none'>Jane</span>
<span id='database_44' style='display:none'>Catib version of the Penn Arabic Treebank part 3 v3.1</span>
<span id='database_45' style='display:none'>SRI Language Modeling Toolkit</span>
<span id='database_46' style='display:none'>AuCoPro - Splitting</span>
<span id='database_47' style='display:none'>GX-LEAF</span>
<span id='database_48' style='display:none'>Virtual Language World</span>
<span id='database_49' style='display:none'>Rascalli Gossip DB</span>
<span id='database_50' style='display:none'>Arabic Treebank</span>
<span id='database_51' style='display:none'>Auslan Corpus</span>
<span id='database_52' style='display:none'>KDD-D / KDD-T Datasets</span>
<span id='database_53' style='display:none'>English-Bengali Transliteration</span>
<span id='database_54' style='display:none'>The GSL Lexicon Database</span>
<span id='database_55' style='display:none'>ELAN annotation tool</span>
<span id='database_56' style='display:none'>Romanian Anonymous Speech Corpus</span>
<span id='database_57' style='display:none'>EGL database</span>
<span id='database_58' style='display:none'>SUC</span>
<span id='database_59' style='display:none'>Feature-Grouping</span>
<span id='database_60' style='display:none'>OLARE</span>
<span id='database_61' style='display:none'>Bilingual Twitter corpus</span>
<span id='database_62' style='display:none'>WordAligner</span>
<span id='database_63' style='display:none'>CREAGEST</span>
<span id='database_64' style='display:none'>Chinese and English Parallel Corpus Extracted from Comparable Patents</span>
<span id='database_65' style='display:none'>Linked Hypernyms Dataset</span>
<span id='database_66' style='display:none'>Moses</span>
<span id='database_67' style='display:none'>OpenCalais</span>
<span id='database_68' style='display:none'>CELEX Lexical Database</span>
<span id='database_69' style='display:none'>Wordnet</span>
<span id='database_70' style='display:none'>The Kalashnikov 2K dependency bank</span>
<span id='database_71' style='display:none'>Arabic Tree Bank</span>
<span id='database_72' style='display:none'>DramaBank corpus and Scheherazade annotation tool</span>
<span id='database_73' style='display:none'>TRIS Corpus</span>
<span id='database_74' style='display:none'>Other-Repetitions Automatic Detection Tool</span>
<span id='database_75' style='display:none'>Emotion Blog Corpus</span>
<span id='database_76' style='display:none'>TweetCaT</span>
<span id='database_77' style='display:none'>Machine-translated newspaper articles annotated for translation quality</span>
<span id='database_78' style='display:none'>Evaluation of BioExcom on the BioScope corpus</span>
<span id='database_79' style='display:none'>Penn Discourse Treebank 2.0</span>
<span id='database_80' style='display:none'>Digi-Sami corpus</span>
<span id='database_81' style='display:none'>Resumes_epidemies</span>
<span id='database_82' style='display:none'>Bulgarian National Reference Corpus</span>
<span id='database_83' style='display:none'>PathoJen</span>
<span id='database_84' style='display:none'>Slovene–Croatian parallel corpus</span>
<span id='database_85' style='display:none'>Alignment of FrameNet and WordNet</span>
<span id='database_86' style='display:none'>DerivBase.hr</span>
<span id='database_87' style='display:none'>GIZA++</span>
<span id='database_88' style='display:none'>ILSP Term Extractor</span>
<span id='database_89' style='display:none'>Rembrandt framework</span>
<span id='database_90' style='display:none'>Austrian Academy Corpus (AAC)</span>
<span id='database_91' style='display:none'>Latent Semantic Analysis Website</span>
<span id='database_92' style='display:none'>Russian Automotive Corpus</span>
<span id='database_93' style='display:none'>20-NewsGroups</span>
<span id='database_94' style='display:none'>Italian Sign Language Corpus</span>
<span id='database_95' style='display:none'>POS-Tagged New Testament in Wolof (Matthew gospel)</span>
<span id='database_96' style='display:none'>British National Corpus</span>
<span id='database_97' style='display:none'>AnCora-ES</span>
<span id='database_98' style='display:none'>The Norwegian Dependency Treebank</span>
<span id='database_99' style='display:none'>Dictionnaire de valence des verbes français (Dicovalence 2)</span>
<span id='database_100' style='display:none'>A Georgian-Russian-English-German Valency Lexicon for Natural Language Processing</span>
<span id='database_101' style='display:none'>Chinese-English Parallel Aligned Treebanks</span>
<span id='database_102' style='display:none'>HFST finite state speller evaluation</span>
<span id='database_103' style='display:none'>SemiNER</span>
<span id='database_104' style='display:none'>DIINAR</span>
<span id='database_105' style='display:none'>Arabic Treebank Part 3 v 3.2</span>
<span id='database_106' style='display:none'>Charniak parser</span>
<span id='database_107' style='display:none'>Boston Radio News Corpus</span>
<span id='database_108' style='display:none'>MMAX</span>
<span id='database_109' style='display:none'>DBLP</span>
<span id='database_110' style='display:none'>SPECIALIST lexicon</span>
<span id='database_111' style='display:none'>Semeval-3 Task 6</span>
<span id='database_112' style='display:none'>MALT parser</span>
<span id='database_113' style='display:none'>Ontology Library for Intelligence Domain</span>
<span id='database_114' style='display:none'>IFrame</span>
<span id='database_115' style='display:none'>Coreference Resolution Engine (Reconcile)</span>
<span id='database_116' style='display:none'>RWTH-PHOENIX</span>
<span id='database_117' style='display:none'>Excite query log</span>
<span id='database_118' style='display:none'>Quaero Terminology Extraction Evaluation Abstracts Corpus</span>
<span id='database_119' style='display:none'>The Source Project</span>
<span id='database_120' style='display:none'>Multimodal SSI based on Video, Depth, Ultrasound and sEMG</span>
<span id='database_121' style='display:none'>C&C toolkit</span>
<span id='database_122' style='display:none'>Cornetto</span>
<span id='database_123' style='display:none'>GENIA treebank</span>
<span id='database_124' style='display:none'>Stockholm EPR Clinical Entity Corpus</span>
<span id='database_125' style='display:none'>AmazonCNProductReviews</span>
<span id='database_126' style='display:none'>Microsoft Research IME Corpus</span>
<span id='database_127' style='display:none'>Aligned Annotation Tool</span>
<span id='database_128' style='display:none'>Topic Modeling Toolkit</span>
<span id='database_129' style='display:none'>GATE</span>
<span id='database_130' style='display:none'>Lemur Toolkit</span>
<span id='database_131' style='display:none'>Event Annotated Carbon Sequestration data</span>
<span id='database_132' style='display:none'>Charniak Parser</span>
<span id='database_133' style='display:none'>Brown Corpus</span>
<span id='database_134' style='display:none'>Toponym Ontology Geocoding Service (TOGS)</span>
<span id='database_135' style='display:none'>Cornerstone</span>
<span id='database_136' style='display:none'>Cross-lingual relatedness thesaurus</span>
<span id='database_137' style='display:none'>ACE</span>
<span id='database_138' style='display:none'>LuxId</span>
<span id='database_139' style='display:none'>CARTOLA</span>
<span id='database_140' style='display:none'>NIdent-EN</span>
<span id='database_141' style='display:none'>Wikipedia</span>
<span id='database_142' style='display:none'>Hamburg Dependency Treebank</span>
<span id='database_143' style='display:none'>Word Sketch Grammar for Russian</span>
<span id='database_144' style='display:none'>Korean Case Frames</span>
<span id='database_145' style='display:none'>Glossary of International Relations (GLOSSIR)</span>
<span id='database_146' style='display:none'>GIRA</span>
<span id='database_147' style='display:none'>Enabling Minority Language Engineering</span>
<span id='database_148' style='display:none'>Customer Review Dataset</span>
<span id='database_149' style='display:none'>test set for QA candidate ranking</span>
<span id='database_150' style='display:none'>Crisis Management Events Annotation Guidelines</span>
<span id='database_151' style='display:none'>PAN-PC-09</span>
<span id='database_152' style='display:none'>BAS Bavarian Archive for Speech Signals Pronunciation Lexicon PHONOLEX</span>
<span id='database_153' style='display:none'>WMT13 Data</span>
<span id='database_154' style='display:none'>Open-Content Text Corpus</span>
<span id='database_155' style='display:none'>Stockholm-Umeå corpus</span>
<span id='database_156' style='display:none'>Prop Bank</span>
<span id='database_157' style='display:none'>Association Norms</span>
<span id='database_158' style='display:none'>Species 2000</span>
<span id='database_159' style='display:none'>Ontologies of Linguistic Annotation (OLiA ontologies)</span>
<span id='database_160' style='display:none'>RelaxCor</span>
<span id='database_161' style='display:none'>Capek: an annotation editor for schoolchildren</span>
<span id='database_162' style='display:none'>JWI (the MIT Java Wordnet Interface)</span>
<span id='database_163' style='display:none'>Twitter Corpus  (TC)</span>
<span id='database_164' style='display:none'>Semeval 2007 English Lexical Sample corpus</span>
<span id='database_165' style='display:none'>the open parallel corpus (OPUS)</span>
<span id='database_166' style='display:none'>Weighted Lexicon of Event Nouns</span>
<span id='database_167' style='display:none'>GramTrans</span>
<span id='database_168' style='display:none'>PolNet-Polish Wordnet</span>
<span id='database_169' style='display:none'>Arabic-English parallel corpora</span>
<span id='database_170' style='display:none'>IXA Pipeline</span>
<span id='database_171' style='display:none'>Unified Medical Language System (UMLS)</span>
<span id='database_172' style='display:none'>IntelliText</span>
<span id='database_173' style='display:none'>Corpus of Pronominal Anaphora of the Quran</span>
<span id='database_174' style='display:none'>NTCIR Patent Corpus</span>
<span id='database_175' style='display:none'>MUC6</span>
<span id='database_176' style='display:none'>MAATS</span>
<span id='database_177' style='display:none'>Basque Dependency Tree Bank (BDT)</span>
<span id='database_178' style='display:none'>OntoNotes</span>
<span id='database_179' style='display:none'>MRBA Hungarian Reference Speech Database</span>
<span id='database_180' style='display:none'>Greek MWE dictionary</span>
<span id='database_181' style='display:none'>ukWaC</span>
<span id='database_182' style='display:none'>Simultaneous Translation Corpus (STC)</span>
<span id='database_183' style='display:none'>Legal Case Factors Extraction</span>
<span id='database_184' style='display:none'>IEPA</span>
<span id='database_185' style='display:none'>User generated content out of automotive forums</span>
<span id='database_186' style='display:none'>Chinese Treebank 5.0</span>
<span id='database_187' style='display:none'>Stanford English grammatical relation extraction utility</span>
<span id='database_188' style='display:none'>Comparable Corpora for EU Languages</span>
<span id='database_189' style='display:none'>ENOVA</span>
<span id='database_190' style='display:none'>Simple English Wikipedia</span>
<span id='database_191' style='display:none'>PerTreebank</span>
<span id='database_192' style='display:none'>Sogou Query Log</span>
<span id='database_193' style='display:none'>Uppsala Persian Dependency Treebank</span>
<span id='database_194' style='display:none'>PAROLE-SIMPLE-CLIPS</span>
<span id='database_195' style='display:none'>500M Japanese Sentences on the Web</span>
<span id='database_196' style='display:none'>grande grammaire du français</span>
<span id='database_197' style='display:none'>Lexicon Enhancement via the GOLD Ontology (LEGO)</span>
<span id='database_198' style='display:none'>NooJ</span>
<span id='database_199' style='display:none'>ICWSM 2009 Spinn3r Dataset</span>
<span id='database_200' style='display:none'>Augmented Multi-party Interaction (AMI) meeting corpus</span>
<span id='database_201' style='display:none'>World Wide English corpus</span>
<span id='database_202' style='display:none'>KEA</span>
<span id='database_203' style='display:none'>DeSR</span>
<span id='database_204' style='display:none'>FreeLing</span>
<span id='database_205' style='display:none'>Sketch Engine</span>
<span id='database_206' style='display:none'>English-Sindhi Parallel corpora</span>
<span id='database_207' style='display:none'>morphdb.hu</span>
<span id='database_208' style='display:none'>A Turkish morphological analyzer (TRmorph)</span>
<span id='database_209' style='display:none'>Maltese-English bilingual lexicon</span>
<span id='database_210' style='display:none'>Oldpress Corpus</span>
<span id='database_211' style='display:none'>DANTE</span>
<span id='database_212' style='display:none'>DELiC4MT</span>
<span id='database_213' style='display:none'>maf-en</span>
<span id='database_214' style='display:none'>Clause boundary annotation program (CBAP)</span>
<span id='database_215' style='display:none'>Multilingual Turin University Treebank (ParTUT)</span>
<span id='database_216' style='display:none'>Xip</span>
<span id='database_217' style='display:none'>NERanka</span>
<span id='database_218' style='display:none'>Textual Entailment Specialized Dataset</span>
<span id='database_219' style='display:none'>Tsumugi-1.0.1</span>
<span id='database_220' style='display:none'>GARD Question Type Corpus</span>
<span id='database_221' style='display:none'>IRNA</span>
<span id='database_222' style='display:none'>Electronic Orange Book</span>
<span id='database_223' style='display:none'>opinion holder predicates</span>
<span id='database_224' style='display:none'>Heart of Gold</span>
<span id='database_225' style='display:none'>wikAPIdia</span>
<span id='database_226' style='display:none'>The LarKC life sciences dataset</span>
<span id='database_227' style='display:none'>SwissAdmin</span>
<span id='database_228' style='display:none'>SRILM - The SRI Language Modeling Toolkit</span>
<span id='database_229' style='display:none'>EuroWordNet</span>
<span id='database_230' style='display:none'>English-Bulgarian Translation Lexicon</span>
<span id='database_231' style='display:none'>CSJ</span>
<span id='database_232' style='display:none'>Lignocellulose Corpus</span>
<span id='database_233' style='display:none'>RoMorphoDict</span>
<span id='database_234' style='display:none'>HyREX</span>
<span id='database_235' style='display:none'>Sense Tagged Corpus</span>
<span id='database_236' style='display:none'>Lucon</span>
<span id='database_237' style='display:none'>DeWaC</span>
<span id='database_238' style='display:none'>Praat version 5.1</span>
<span id='database_239' style='display:none'>Morfeus</span>
<span id='database_240' style='display:none'>Domain product features</span>
<span id='database_241' style='display:none'>Estonian WordNet</span>
<span id='database_242' style='display:none'>Collective Named Entities</span>
<span id='database_243' style='display:none'>Child Language Data Exchange System (CHILDES)</span>
<span id='database_244' style='display:none'>Word Relatedness Datasets</span>
<span id='database_245' style='display:none'>PurePos</span>
<span id='database_246' style='display:none'>ACE-2 data set</span>
<span id='database_247' style='display:none'>C-ORAL-JAPON</span>
<span id='database_248' style='display:none'>IMAGACT</span>
<span id='database_249' style='display:none'>The ICSI Meeting Corpus</span>
<span id='database_250' style='display:none'>English-Myanmar Parallel Text aligner</span>
<span id='database_251' style='display:none'>Fodina Patent Texts</span>
<span id='database_252' style='display:none'>DMV parser</span>
<span id='database_253' style='display:none'>Mainichi Shimbun Corpus</span>
<span id='database_254' style='display:none'>QWN-PPV</span>
<span id='database_255' style='display:none'>Gold Standard for English abstract nouns</span>
<span id='database_256' style='display:none'>Mediatory Summary Corpus</span>
<span id='database_257' style='display:none'>The Weltmodell</span>
<span id='database_258' style='display:none'>ORTOFON</span>
<span id='database_259' style='display:none'>NomLex</span>
<span id='database_260' style='display:none'>Emotion Cause Event Corpus</span>
<span id='database_261' style='display:none'>Russian Positional Tagset</span>
<span id='database_262' style='display:none'>Passage</span>
<span id='database_263' style='display:none'>Chinese Sentiment Lexicon</span>
<span id='database_264' style='display:none'>EVNE2013</span>
<span id='database_265' style='display:none'>ROUGE</span>
<span id='database_266' style='display:none'>LinGO Grammar Matrix</span>
<span id='database_267' style='display:none'>MASC word sense corpus</span>
<span id='database_268' style='display:none'>DanPASS dialogues</span>
<span id='database_269' style='display:none'>Semantic Parser (no specific name)</span>
<span id='database_270' style='display:none'>CMT Corpus of Maritime terminology</span>
<span id='database_271' style='display:none'>RASP</span>
<span id='database_272' style='display:none'>mma</span>
<span id='database_273' style='display:none'>The Online Linguistic Database (version 1.0)</span>
<span id='database_274' style='display:none'>TokensRegex</span>
<span id='database_275' style='display:none'>LDC transaltion guidelines</span>
<span id='database_276' style='display:none'>OrganicLingua_CLIR_Evaluation</span>
<span id='database_277' style='display:none'>TPR-DB</span>
<span id='database_278' style='display:none'>weka</span>
<span id='database_279' style='display:none'>Catalan Version of a Subset of the EuroVoc</span>
<span id='database_280' style='display:none'>French and English Translation Databases</span>
<span id='database_281' style='display:none'>MIDT++</span>
<span id='database_282' style='display:none'>Finite state spelling model for Lithuanian</span>
<span id='database_283' style='display:none'>CTexT Alignment Interface</span>
<span id='database_284' style='display:none'>Svenskt associationslexikon (SALDO)</span>
<span id='database_285' style='display:none'>Piithie Corpus</span>
<span id='database_286' style='display:none'>IREX data set for NE recognition</span>
<span id='database_287' style='display:none'>D-Coi corpus</span>
<span id='database_288' style='display:none'>International Corpus of Learner English version 2</span>
<span id='database_289' style='display:none'>Mapudungun-Spanish MT test suite</span>
<span id='database_290' style='display:none'>Ontology Library for Financial Domain</span>
<span id='database_291' style='display:none'>DV:41640,41650;TLexpédier</span>
<span id='database_292' style='display:none'>WePS benchmark data</span>
<span id='database_293' style='display:none'>Corpus Based Opinion Words</span>
<span id='database_294' style='display:none'>FBIS corpus</span>
<span id='database_295' style='display:none'>Ontonotes 4.0 newswire section</span>
<span id='database_296' style='display:none'>The bilingual deaf children RU-Kentalis database</span>
<span id='database_297' style='display:none'>ISOLET</span>
<span id='database_298' style='display:none'>i2b2 2009 shared task on medication extraction</span>
<span id='database_299' style='display:none'>MG4J</span>
<span id='database_300' style='display:none'>The Portuguese Regional Accent Databank</span>
<span id='database_301' style='display:none'>Consumer</span>
<span id='database_302' style='display:none'>Witchcraft Workbench</span>
<span id='database_303' style='display:none'>Europarl</span>
<span id='database_304' style='display:none'>CMU Sphinx</span>
<span id='database_305' style='display:none'>a User-Extensible Morphological Analyzer for Japanese (JUMAN)</span>
<span id='database_306' style='display:none'>Irish Sign Language</span>
<span id='database_307' style='display:none'>Research Artcle Corpus</span>
<span id='database_308' style='display:none'>Automotive Repair Orders</span>
<span id='database_309' style='display:none'>treetagger</span>
<span id='database_310' style='display:none'>UniDic for Early Middle Japanese</span>
<span id='database_311' style='display:none'>News Tweets For SRL</span>
<span id='database_312' style='display:none'>Poster Conversation Corpus</span>
<span id='database_313' style='display:none'>Kawakib</span>
<span id='database_314' style='display:none'>Uppsala Student English Corpus (USE)</span>
<span id='database_315' style='display:none'>Estonian Morphological Disambiguator</span>
<span id='database_316' style='display:none'>Granska tagger</span>
<span id='database_317' style='display:none'>Dutch Biographical Portal</span>
<span id='database_318' style='display:none'>Frequentielijst 27 Miljoen Woorden Krantencorpus 1995</span>
<span id='database_319' style='display:none'>HuComTech Multimodal (Audio-visual) Database</span>
<span id='database_320' style='display:none'>CLASSYN text type-specific corpora</span>
<span id='database_321' style='display:none'>DanNet , Arab WordNet</span>
<span id='database_322' style='display:none'>non-native speaking data data</span>
<span id='database_323' style='display:none'>Morphological Analyser for South Asian LAnguages</span>
<span id='database_324' style='display:none'>Technical domain lexicon</span>
<span id='database_325' style='display:none'>English-Thai Relaxed Amphigram Bank</span>
<span id='database_326' style='display:none'>ETAP-3</span>
<span id='database_327' style='display:none'>Evaluation Tool for Subjective Loudness Perception</span>
<span id='database_328' style='display:none'>Criminality Corpus for Text Categorization</span>
<span id='database_329' style='display:none'>Chinese Language Technology Platform</span>
<span id='database_330' style='display:none'>BLEU</span>
<span id='database_331' style='display:none'>BalkaNet</span>
<span id='database_332' style='display:none'>Wordnet 3.0</span>
<span id='database_333' style='display:none'>MUC Coreference Data Set</span>
<span id='database_334' style='display:none'>Course material, writing manual and evaluation techniques</span>
<span id='database_335' style='display:none'>Lingenio Corpus Tool</span>
<span id='database_336' style='display:none'>UW parallel meeting corpus</span>
<span id='database_337' style='display:none'>ProPOSEC: a Prosody and POS annotated Spoken English Corpus</span>
<span id='database_338' style='display:none'>genchal-repository</span>
<span id='database_339' style='display:none'>Stanford parser</span>
<span id='database_340' style='display:none'>RWTH-BOSTON-400</span>
<span id='database_341' style='display:none'>Slashdot Comments Corpus</span>
<span id='database_342' style='display:none'>NTU sentiment dictionary</span>
<span id='database_343' style='display:none'>Bible Bilingual Lexicons</span>
<span id='database_344' style='display:none'>Harvard Uncertainty Speech Corpus</span>
<span id='database_345' style='display:none'>Tagged Medical Forum Data</span>
<span id='database_346' style='display:none'>National Corpus of Polish</span>
<span id='database_347' style='display:none'>NST acoustic and language models</span>
<span id='database_348' style='display:none'>Spanish NumExp Grammars JAPE</span>
<span id='database_349' style='display:none'>JWNL</span>
<span id='database_350' style='display:none'>Evaluation des Systèmes de Transcription enrichie d'Émissions Radiophoniques (ESTER)</span>
<span id='database_351' style='display:none'>PPDB</span>
<span id='database_352' style='display:none'>BCCWJ</span>
<span id='database_353' style='display:none'>English version of National Center Test for University Admission in Japan</span>
<span id='database_354' style='display:none'>AMI corpus</span>
<span id='database_355' style='display:none'>MultiLingSemSim</span>
<span id='database_356' style='display:none'>RIKEN Japanese Mother-Infant Conversation Corpus</span>
<span id='database_357' style='display:none'>SYNC3 Collaborative Annotation Tool</span>
<span id='database_358' style='display:none'>Erlangen Corpus of Speech Recognition Transcripts</span>
<span id='database_359' style='display:none'>EVT.INF GOLD STANDARD</span>
<span id='database_360' style='display:none'>DIRNDL_anaphora</span>
<span id='database_361' style='display:none'>Dot object predication gold standard</span>
<span id='database_362' style='display:none'>iLex</span>
<span id='database_363' style='display:none'>CTB6</span>
<span id='database_364' style='display:none'>DGS-Corpus</span>
<span id='database_365' style='display:none'>SenseLearner 2.0</span>
<span id='database_366' style='display:none'>xLiD-Lexica: Cross-lingual Linked Data Lexica</span>
<span id='database_367' style='display:none'>Aalto RadioCon FI</span>
<span id='database_368' style='display:none'>Entity Mention Classifier</span>
<span id='database_369' style='display:none'>EMO_Events</span>
<span id='database_370' style='display:none'>Tools for querying an N-gram database</span>
<span id='database_371' style='display:none'>Penn WSJ Treebank v.3</span>
<span id='database_372' style='display:none'>MMA</span>
<span id='database_373' style='display:none'>Engineering Lecture Corpus (ELC)</span>
<span id='database_374' style='display:none'>TimeBank</span>
<span id='database_375' style='display:none'>International Corpus of Learner English</span>
<span id='database_376' style='display:none'>PerDiPa Collection</span>
<span id='database_377' style='display:none'>CORPRES</span>
<span id='database_378' style='display:none'>Google Web1T corpus (LDC2006T13)</span>
<span id='database_379' style='display:none'>Providence Corpus</span>
<span id='database_380' style='display:none'>Parallel Corpora Collector (PaCo2)</span>
<span id='database_381' style='display:none'>SubCAT</span>
<span id='database_382' style='display:none'>AAC corpusBrowser</span>
<span id='database_383' style='display:none'>SELFEH</span>
<span id='database_384' style='display:none'>Stanford dcoref</span>
<span id='database_385' style='display:none'>Privacy</span>
<span id='database_386' style='display:none'>Apertium Catalan Monolingual Dictionary from Spanish-Catalan language pair</span>
<span id='database_387' style='display:none'>Gold Standard for Spanish concrete nouns</span>
<span id='database_388' style='display:none'>PEXACC</span>
<span id='database_389' style='display:none'>Ergbio</span>
<span id='database_390' style='display:none'>Tajik Corpus</span>
<span id='database_391' style='display:none'>english to hindi dictionary shabdakoSha</span>
<span id='database_392' style='display:none'>trec_eval</span>
<span id='database_393' style='display:none'>Wikipedia Miner</span>
<span id='database_394' style='display:none'>SYN2013PUB</span>
<span id='database_395' style='display:none'>A Universal Part-of-Speech Tagset</span>
<span id='database_396' style='display:none'>newstest2008</span>
<span id='database_397' style='display:none'>Turkish Treebank</span>
<span id='database_398' style='display:none'>Alpaco alignment editor</span>
<span id='database_399' style='display:none'>ClueWeb TREC Category B</span>
<span id='database_400' style='display:none'>BAWE</span>
<span id='database_401' style='display:none'>Lexical Markup Framework (LMF)</span>
<span id='database_402' style='display:none'>Obesity dataset</span>
<span id='database_403' style='display:none'>The EASR Corpus of Hungarian Elderly Speech</span>
<span id='database_404' style='display:none'>Penn Tree Bank</span>
<span id='database_405' style='display:none'>Lefff</span>
<span id='database_406' style='display:none'>Chicago Fingerspelling Corpus</span>
<span id='database_407' style='display:none'>Digg dataset</span>
<span id='database_408' style='display:none'>A Python Toolkit for Universal Transliteration</span>
<span id='database_409' style='display:none'>Universal Declaration of Human Rights</span>
<span id='database_410' style='display:none'>The deaf adults RU database</span>
<span id='database_411' style='display:none'>MARITERM</span>
<span id='database_412' style='display:none'>DISCO</span>
<span id='database_413' style='display:none'>Corpus of Ambiguous Abbreviations and Gene Names in the Biomedical Domain</span>
<span id='database_414' style='display:none'>Stanford's NLP Parser</span>
<span id='database_415' style='display:none'>TAC 2010 KBP Training Entity Linking V2.0</span>
<span id='database_416' style='display:none'>Unsupervised incremental parser</span>
<span id='database_417' style='display:none'>WaCkypedia_en</span>
<span id='database_418' style='display:none'>WikiWoods</span>
<span id='database_419' style='display:none'>DFKI Citation Classification Dataset</span>
<span id='database_420' style='display:none'>CRPC</span>
<span id='database_421' style='display:none'>WMT 2011</span>
<span id='database_422' style='display:none'>International Corpus of Learner English (ICLE)</span>
<span id='database_423' style='display:none'>RELcat a Relation Registry</span>
<span id='database_424' style='display:none'>Augmented RTE-5 Search Data Set</span>
<span id='database_425' style='display:none'>PIITHIE corpus</span>
<span id='database_426' style='display:none'>Eihera</span>
<span id='database_427' style='display:none'>Russian Emotional Corpus (REC)</span>
<span id='database_428' style='display:none'>BioNLP'09 shared task data set</span>
<span id='database_429' style='display:none'>Index Thomisticus Treebank</span>
<span id='database_430' style='display:none'>Prague Dependency Treebank 3.0 (PDT 3.0)</span>
<span id='database_431' style='display:none'>FEMA: Help After a Disaster</span>
<span id='database_432' style='display:none'>SANTINIS</span>
<span id='database_433' style='display:none'>NotaBen RDF Annotation Tool</span>
<span id='database_434' style='display:none'>Acromine Disambiguator</span>
<span id='database_435' style='display:none'>BNC</span>
<span id='database_436' style='display:none'>Quaero Extended Named Entities annotation guide</span>
<span id='database_437' style='display:none'>ClearTK</span>
<span id='database_438' style='display:none'>Netlog Corpus and Chatty Subcorpus</span>
<span id='database_439' style='display:none'>Subjectivity Lexicon</span>
<span id='database_440' style='display:none'>Simple Rule Language Global Health Rulebook</span>
<span id='database_441' style='display:none'>American National Corpus (ANC) Manually Annotated Sub-Corpus (MASC)</span>
<span id='database_442' style='display:none'>Explanatory sentence data set</span>
<span id='database_443' style='display:none'>SIGHAN Bakeoff 2005</span>
<span id='database_444' style='display:none'>Corpus on Debate and Deliberation</span>
<span id='database_445' style='display:none'>PDT 2.0 annotation tools</span>
<span id='database_446' style='display:none'>GenSem</span>
<span id='database_447' style='display:none'>ULex</span>
<span id='database_448' style='display:none'>DragonToolkit</span>
<span id='database_449' style='display:none'>Nomco</span>
<span id='database_450' style='display:none'>Votter Corpus</span>
<span id='database_451' style='display:none'>WordNet Sense Relate</span>
<span id='database_452' style='display:none'>SentiWordNet</span>
<span id='database_453' style='display:none'>Annotation guidelines for event nominal tagging</span>
<span id='database_454' style='display:none'>Arvid</span>
<span id='database_455' style='display:none'>texto4science</span>
<span id='database_456' style='display:none'>NERsuite (A Named Entity Recognition Toolkit)</span>
<span id='database_457' style='display:none'>CINTIL Dependency Bank</span>
<span id='database_458' style='display:none'>General Inquirer</span>
<span id='database_459' style='display:none'>English to UNL Corpus</span>
<span id='database_460' style='display:none'>ICLE-NLI</span>
<span id='database_461' style='display:none'>WordNet</span>
<span id='database_462' style='display:none'>HindEnCorp 0.5</span>
<span id='database_463' style='display:none'>Temporal Relations for Clinical Notes</span>
<span id='database_464' style='display:none'>Non-canonical constructions in oral discourse: a crosslinguistic perspective (NOCANDO)</span>
<span id='database_465' style='display:none'>An Arabic Twitter Corpus for Subjectivity and Sentiment Analysis</span>
<span id='database_466' style='display:none'>NonDir Parser</span>
<span id='database_467' style='display:none'>HPRD50</span>
<span id='database_468' style='display:none'>EuroParl</span>
<span id='database_469' style='display:none'>CINTIL DeepGrambank</span>
<span id='database_470' style='display:none'>CorpusCine</span>
<span id='database_471' style='display:none'>SentiWordNet 3.0</span>
<span id='database_472' style='display:none'>A new French Meta Grammar (frmg)</span>
<span id='database_473' style='display:none'>DoubleTreeJS</span>
<span id='database_474' style='display:none'>BART: Baltimore Anaphora Resolution Toolkit</span>
<span id='database_475' style='display:none'>CSTNews corpus</span>
<span id='database_476' style='display:none'>OpenNLP Sentence FraMed Model</span>
<span id='database_477' style='display:none'>Arabic Treebank part 3 version 3.2 LDC Catalog Number: LDC2010T08</span>
<span id='database_478' style='display:none'>OntoValence Dictionary</span>
<span id='database_479' style='display:none'>ALGASD</span>
<span id='database_480' style='display:none'>Shabdanjali</span>
<span id='database_481' style='display:none'>Portuguese corpus sample with modal information</span>
<span id='database_482' style='display:none'>Italian Cooking Recipes Dataset</span>
<span id='database_483' style='display:none'>Wall Street Journal (WSJ) Corpus</span>
<span id='database_484' style='display:none'>Term Union</span>
<span id='database_485' style='display:none'>LingNet</span>
<span id='database_486' style='display:none'>STeP-1-POS Tagger</span>
<span id='database_487' style='display:none'>apertium-kir</span>
<span id='database_488' style='display:none'>DomainEditor</span>
<span id='database_489' style='display:none'>NaviTexte</span>
<span id='database_490' style='display:none'>RFTagger</span>
<span id='database_491' style='display:none'>DutchSemCor</span>
<span id='database_492' style='display:none'>Meaning-Text Theory (MTT)</span>
<span id='database_493' style='display:none'>Elicited Imitation Test Item Development Tool</span>
<span id='database_494' style='display:none'>Kalashnikov 2K dependency bank</span>
<span id='database_495' style='display:none'>LingPipe</span>
<span id='database_496' style='display:none'>Persian WordNet</span>
<span id='database_497' style='display:none'>Hungarian-Lithuanian parallel corpus</span>
<span id='database_498' style='display:none'>Parallel text-image french news corpus</span>
<span id='database_499' style='display:none'>CLIC Corpus della Lingua Italiana Contemporanea</span>
<span id='database_500' style='display:none'>QA examples for consumer troubles</span>
<span id='database_501' style='display:none'>TIDES Extraction (ACE) 2003 Multilingual Training Data</span>
<span id='database_502' style='display:none'>Timebank</span>
<span id='database_503' style='display:none'>TIMIT</span>
<span id='database_504' style='display:none'>Speeral</span>
<span id='database_505' style='display:none'>Buckwalter Arabic Morphological Analyzer</span>
<span id='database_506' style='display:none'>Corpus del español</span>
<span id='database_507' style='display:none'>Minefield</span>
<span id='database_508' style='display:none'>Estonian TimeML annotated corpus</span>
<span id='database_509' style='display:none'>Topic Related Review Sentences</span>
<span id='database_510' style='display:none'>Arabic WordNet</span>
<span id='database_511' style='display:none'>Stanford POS Tagger</span>
<span id='database_512' style='display:none'>Hercules Dalianis</span>
<span id='database_513' style='display:none'>Persian Syntactic Verb Valency Lexicon</span>
<span id='database_514' style='display:none'>JDM colors</span>
<span id='database_515' style='display:none'>Sonar</span>
<span id='database_516' style='display:none'>METEOR for Indian Languages</span>
<span id='database_517' style='display:none'>SWIFT Aligner</span>
<span id='database_518' style='display:none'>Normalization Guidelines</span>
<span id='database_519' style='display:none'>MGC</span>
<span id='database_520' style='display:none'>Spanish Aragonese DBpedia Abstract Corpus</span>
<span id='database_521' style='display:none'>ProppOnto</span>
<span id='database_522' style='display:none'>DELA</span>
<span id='database_523' style='display:none'>Polish WordNet (plWordNet)</span>
<span id='database_524' style='display:none'>Beijing language acquisition corpus</span>
<span id='database_525' style='display:none'>Talbanken</span>
<span id='database_526' style='display:none'>GermaNet</span>
<span id='database_527' style='display:none'>PropBank</span>
<span id='database_528' style='display:none'>Stanford Parser</span>
<span id='database_529' style='display:none'>Carafe</span>
<span id='database_530' style='display:none'>LINA-msc</span>
<span id='database_531' style='display:none'>PubMed</span>
<span id='database_532' style='display:none'>Syntactic lexicon for French (Lefff)</span>
<span id='database_533' style='display:none'>Illinois Coreference Resolver</span>
<span id='database_534' style='display:none'>A Multimodal Corpus of Rapid Dialogue Games</span>
<span id='database_535' style='display:none'>Emotional Narratives  Corpus</span>
<span id='database_536' style='display:none'>pn-filter</span>
<span id='database_537' style='display:none'>New York Times Corpus</span>
<span id='database_538' style='display:none'>DeepBank</span>
<span id='database_539' style='display:none'>Japanese Event Database</span>
<span id='database_540' style='display:none'>Google N-gram Corpus V1</span>
<span id='database_541' style='display:none'>WS4LR</span>
<span id='database_542' style='display:none'>CoNLL 2000 shallow parsing data set</span>
<span id='database_543' style='display:none'>Meta-Knowledge Annotation Scheme for Bio-Events (MeKASBE)</span>
<span id='database_544' style='display:none'>Newspaper articles and technical texts (HT, MT, PE) annotated for translation quality</span>
<span id='database_545' style='display:none'>Keywords of Russian movie descriptions</span>
<span id='database_546' style='display:none'>NLGbAse</span>
<span id='database_547' style='display:none'>TKK</span>
<span id='database_548' style='display:none'>PML-TQ</span>
<span id='database_549' style='display:none'>mOLIFde</span>
<span id='database_550' style='display:none'>NTCIR-1 Test Collection</span>
<span id='database_551' style='display:none'>Tartu Multimodal Data</span>
<span id='database_552' style='display:none'>TTSlabCS1</span>
<span id='database_553' style='display:none'>Unitex</span>
<span id='database_554' style='display:none'>Colecçao Dourada do HAREM</span>
<span id='database_555' style='display:none'>Penn TreeBank 3, Switchboard corpus part</span>
<span id='database_556' style='display:none'>The Icelandic Frequency Dictionary</span>
<span id='database_557' style='display:none'>NegDDI-DrugBank</span>
<span id='database_558' style='display:none'>Reference Corpus of Contemporary Portuguese (CRPC)</span>
<span id='database_559' style='display:none'>A library for extracting Power Normalized Cepstral Coefficients (libpncc)</span>
<span id='database_560' style='display:none'>German court decision corpus</span>
<span id='database_561' style='display:none'>POLYMOTS</span>
<span id='database_562' style='display:none'>Bermuda Phonetic Transcription</span>
<span id='database_563' style='display:none'>People’s Daily from 1993-1997</span>
<span id='database_564' style='display:none'>LADL tables</span>
<span id='database_565' style='display:none'>Learner Corpus of Hungarian</span>
<span id='database_566' style='display:none'>AnHitzDlg</span>
<span id='database_567' style='display:none'>PT-EN /EN-PT translation lexicon</span>
<span id='database_568' style='display:none'>FreeLang</span>
<span id='database_569' style='display:none'>English Read by Japanese (ERJ) database</span>
<span id='database_570' style='display:none'>SMORE</span>
<span id='database_571' style='display:none'>Malt Parser</span>
<span id='database_572' style='display:none'>STEVIN Nederlandstalig Referentiecorpus (SoNaR)</span>
<span id='database_573' style='display:none'>Fips Web Service</span>
<span id='database_574' style='display:none'>SPECIALIST dTagger</span>
<span id='database_575' style='display:none'>Ariadne Corpus Management System</span>
<span id='database_576' style='display:none'>MAZEA-Web</span>
<span id='database_577' style='display:none'>LexPar</span>
<span id='database_578' style='display:none'>The EASR Corpus of Polish Elderly Speech</span>
<span id='database_579' style='display:none'>CALBC (Collaborative Annotation of a Large Biomedical Corpus) corpora</span>
<span id='database_580' style='display:none'>GlobalPhone</span>
<span id='database_581' style='display:none'>EcoLexicon</span>
<span id='database_582' style='display:none'>UIMAST</span>
<span id='database_583' style='display:none'>Aesop’s fables timeline annotations</span>
<span id='database_584' style='display:none'>Ren-CECps</span>
<span id='database_585' style='display:none'>NKI-CCRT Corpus</span>
<span id='database_586' style='display:none'>CICC Indonesian Basic Dictionary</span>
<span id='database_587' style='display:none'>MRP Readability Corpus</span>
<span id='database_588' style='display:none'>RITEL</span>
<span id='database_589' style='display:none'>NTCIR patent corpus</span>
<span id='database_590' style='display:none'>Dot type corpus with experts</span>
<span id='database_591' style='display:none'>TAC 2010 KBP Evaluation Entity Linking Gold Standard V1.0</span>
<span id='database_592' style='display:none'>STeP-1- morpho analyszer</span>
<span id='database_593' style='display:none'>AcadOnto</span>
<span id='database_594' style='display:none'>iMAP referring expression corpus</span>
<span id='database_595' style='display:none'>MIT FlightBrowser Corpus</span>
<span id='database_596' style='display:none'>PersPred</span>
<span id='database_597' style='display:none'>Tu&Roth</span>
<span id='database_598' style='display:none'>Science Magazine Podcast Transcript Corpus</span>
<span id='database_599' style='display:none'>NIST score (script: mteval-v13a.pl)</span>
<span id='database_600' style='display:none'>ASC-IT</span>
<span id='database_601' style='display:none'>Anotatornia</span>
<span id='database_602' style='display:none'>MapTask</span>
<span id='database_603' style='display:none'>An Open Toolkit for Automatic Machine Translation (Meta-)Evaluation (Asiya)</span>
<span id='database_604' style='display:none'>PiTu</span>
<span id='database_605' style='display:none'>The Quran and Tafsir Corpus</span>
<span id='database_606' style='display:none'>Headline Paraphrase Corpus Englisg</span>
<span id='database_607' style='display:none'>World Atlas of Language Structures (WALS)</span>
<span id='database_608' style='display:none'>Stanford Core NLP (NER)</span>
<span id='database_609' style='display:none'>Kamusi Project</span>
<span id='database_610' style='display:none'>Normalization datasets</span>
<span id='database_611' style='display:none'>ChaSen</span>
<span id='database_612' style='display:none'>English Wikipedia</span>
<span id='database_613' style='display:none'>Relevant Term extractor</span>
<span id='database_614' style='display:none'>OAL</span>
<span id='database_615' style='display:none'>PR2</span>
<span id='database_616' style='display:none'>SaGA</span>
<span id='database_617' style='display:none'>Calendar Expression Semantic Tagger</span>
<span id='database_618' style='display:none'>EDRlexicon</span>
<span id='database_619' style='display:none'>AURORA Project Database 2.0 - Evaluation Package</span>
<span id='database_620' style='display:none'>Czech National Corpus - SYN</span>
<span id='database_621' style='display:none'>BT Correspondence Corpus</span>
<span id='database_622' style='display:none'>PropBank (Proposition Bank)</span>
<span id='database_623' style='display:none'>NED</span>
<span id='database_624' style='display:none'>MetaMap</span>
<span id='database_625' style='display:none'>VoxRoCorpus</span>
<span id='database_626' style='display:none'>WF text dependent speaker verificaition</span>
<span id='database_627' style='display:none'>LRE MAP</span>
<span id='database_628' style='display:none'>Spanish Learner Oral Corpus</span>
<span id='database_629' style='display:none'>Proppian fairy tale Markup Language (PftML)</span>
<span id='database_630' style='display:none'>Florianópolis</span>
<span id='database_631' style='display:none'>QASTLE (Question-Answering Systems TooL for Evaluation)</span>
<span id='database_632' style='display:none'>Wikipedia English version</span>
<span id='database_633' style='display:none'>Gold Standard for Spanish non-deverbal eventive nouns</span>
<span id='database_634' style='display:none'>Nomage Corpus</span>
<span id='database_635' style='display:none'>Phonetisaurus G2P</span>
<span id='database_636' style='display:none'>AncientGreekWordNet</span>
<span id='database_637' style='display:none'>Europarl v6 French-English</span>
<span id='database_638' style='display:none'>Difficult Speech Corpus (DiSCo)</span>
<span id='database_639' style='display:none'>CASIA-CASSIL</span>
<span id='database_640' style='display:none'>SignCom Project</span>
<span id='database_641' style='display:none'>TAC 2010 KBP Evaluation Source Data</span>
<span id='database_642' style='display:none'>Freebase</span>
<span id='database_643' style='display:none'>SAL</span>
<span id='database_644' style='display:none'>EmotiNet</span>
<span id='database_645' style='display:none'>pair-hmm-translit</span>
<span id='database_646' style='display:none'>CWB</span>
<span id='database_647' style='display:none'>ETAPE</span>
<span id='database_648' style='display:none'>CaboCha</span>
<span id='database_649' style='display:none'>Dan Bikelâ€™s randomized parsing evaluation comparator</span>
<span id='database_650' style='display:none'>GenI</span>
<span id='database_651' style='display:none'>Corpus CINTIL - Corpus Internacional do Português</span>
<span id='database_652' style='display:none'>Japanese BRP corpus</span>
<span id='database_653' style='display:none'>Myanmar Word Segmentation</span>
<span id='database_654' style='display:none'>Joint Research Centre (JRC) Quotes Collection for Sentiment Analysis</span>
<span id='database_655' style='display:none'>Q-WordNet</span>
<span id='database_656' style='display:none'>Aikuma</span>
<span id='database_657' style='display:none'>ptbconv-3.0</span>
<span id='database_658' style='display:none'>Arabic Treebank Part 1 v 4.0</span>
<span id='database_659' style='display:none'>Large Bilingual Speech Database for Synthesis (Ahosyn)</span>
<span id='database_660' style='display:none'>TiGer2Dep</span>
<span id='database_661' style='display:none'>Scale dataset</span>
<span id='database_662' style='display:none'>Stanford part-of-speech tagger</span>
<span id='database_663' style='display:none'>AZ-II corpus</span>
<span id='database_664' style='display:none'>Chinese Whispers Paraphrase Corpus (CWPC)</span>
<span id='database_665' style='display:none'>HOO Evaluation Framework</span>
<span id='database_666' style='display:none'>Croatian Morphological Lexicon</span>
<span id='database_667' style='display:none'>Varro Toolkit</span>
<span id='database_668' style='display:none'>WSJCAM0 British English speech database</span>
<span id='database_669' style='display:none'>Chinese Open Wordnet</span>
<span id='database_670' style='display:none'>T2T3</span>
<span id='database_671' style='display:none'>JUMAN</span>
<span id='database_672' style='display:none'>MALLET</span>
<span id='database_673' style='display:none'>TER</span>
<span id='database_674' style='display:none'>Mozilla Semantic Desktop</span>
<span id='database_675' style='display:none'>Translation Based Opinion Words</span>
<span id='database_676' style='display:none'>Twitter corpus</span>
<span id='database_677' style='display:none'>Multiword Expression lexicons</span>
<span id='database_678' style='display:none'>Core WordNet</span>
<span id='database_679' style='display:none'>Tartu First Encounters Corpus</span>
<span id='database_680' style='display:none'>Parallel Corpus of US English and Shanghainese Child Vocalizations</span>
<span id='database_681' style='display:none'>Crisis Management Corpus</span>
<span id='database_682' style='display:none'>ILSP Chunker</span>
<span id='database_683' style='display:none'>Label Tool</span>
<span id='database_684' style='display:none'>Standards for Controlled Languages</span>
<span id='database_685' style='display:none'>FidaPlus</span>
<span id='database_686' style='display:none'>Anvil</span>
<span id='database_687' style='display:none'>Bengali Blog Opinion Corpus</span>
<span id='database_688' style='display:none'>CoNLL-2011 shared task data</span>
<span id='database_689' style='display:none'>HGC</span>
<span id='database_690' style='display:none'>SPAS (Structure and Point Annotation of Stories)</span>
<span id='database_691' style='display:none'>Polarity lexicons in Spanish</span>
<span id='database_692' style='display:none'>Corpus of Japanese Predicate Phrases for Synonym/Antonym Relations</span>
<span id='database_693' style='display:none'>Crowdsourced translations, edits, and rankings for the 2009 NIST Urdu-to-English dataset</span>
<span id='database_694' style='display:none'>Arabic Discourse Annotation Tool (ADA tool)</span>
<span id='database_695' style='display:none'>LPCC - a large parallel corpus of clefts</span>
<span id='database_696' style='display:none'>CorpusPedia</span>
<span id='database_697' style='display:none'>NPCEditor</span>
<span id='database_698' style='display:none'>Port4NooJ</span>
<span id='database_699' style='display:none'>Snowball</span>
<span id='database_700' style='display:none'>Auditory Classification Image (Matlab toolbox)</span>
<span id='database_701' style='display:none'>POS Tagger</span>
<span id='database_702' style='display:none'>WikiWeasel</span>
<span id='database_703' style='display:none'>Hungarian Webcorpus</span>
<span id='database_704' style='display:none'>Text::Perfide::BookSync (Perl module)</span>
<span id='database_705' style='display:none'>AyDA</span>
<span id='database_706' style='display:none'>CAMELEON comparable corpus</span>
<span id='database_707' style='display:none'>Treebank</span>
<span id='database_708' style='display:none'>TCF</span>
<span id='database_709' style='display:none'>METHONTOLOGY</span>
<span id='database_710' style='display:none'>MXPOST</span>
<span id='database_711' style='display:none'>Doshisha eye-gaze dialogue data</span>
<span id='database_712' style='display:none'>Training set 5(5500 labeled questions)</span>
<span id='database_713' style='display:none'>On-line Heritage Database for Poland’s Endangered Languages1: Design and Implementation</span>
<span id='database_714' style='display:none'>dasher for sign writing</span>
<span id='database_715' style='display:none'>Europarl with multilingual synsets</span>
<span id='database_716' style='display:none'>GF Open-Domain Translation System</span>
<span id='database_717' style='display:none'>KALAKA-2</span>
<span id='database_718' style='display:none'>Visible Objects Algorithm</span>
<span id='database_719' style='display:none'>Europarl and News-Commentary corpora</span>
<span id='database_720' style='display:none'>NoClone</span>
<span id='database_721' style='display:none'>Speech Database in Basque for Synthesis and Voice Conversion (AhoSpeakers)</span>
<span id='database_722' style='display:none'>WikiLex</span>
<span id='database_723' style='display:none'>Emotional Speech Database for Basque (Ahoemo3)</span>
<span id='database_724' style='display:none'>Mate tagger</span>
<span id='database_725' style='display:none'>Nomage lexicon</span>
<span id='database_726' style='display:none'>PCEDT 2.0</span>
<span id='database_727' style='display:none'>Fuzzy V Measure</span>
<span id='database_728' style='display:none'>WordNet Domains</span>
<span id='database_729' style='display:none'>Senseval-3: English Lexical Sample</span>
<span id='database_730' style='display:none'>A Collection of Russian Corpora (University of Leeds)</span>
<span id='database_731' style='display:none'>ELRA-M0033</span>
<span id='database_732' style='display:none'>Stephen Jay Gould ''Leonardo's Mountain of Clams and the diet of Worms.'</span>
<span id='database_733' style='display:none'>Supervised Latent Dirichlet Allocation for Classification</span>
<span id='database_734' style='display:none'>Spanish SpeechDat(II)</span>
<span id='database_735' style='display:none'>Turkish NER Tagger</span>
<span id='database_736' style='display:none'>Stanford Log-linear Part-Of-Speech Tagger</span>
<span id='database_737' style='display:none'>FOLKER</span>
<span id='database_738' style='display:none'>TIMIT Acoustic-Phonetic Continuous Speech Corpus</span>
<span id='database_739' style='display:none'>Anymalign</span>
<span id='database_740' style='display:none'>EDR</span>
<span id='database_741' style='display:none'>BRATEval</span>
<span id='database_742' style='display:none'>KI-04</span>
<span id='database_743' style='display:none'>WordNet Content Management System</span>
<span id='database_744' style='display:none'>CMU_ARCTIC speech synthesis databases</span>
<span id='database_745' style='display:none'>Wan's keyword extraction dataset</span>
<span id='database_746' style='display:none'>A2ST-COMP</span>
<span id='database_747' style='display:none'>NEGRA</span>
<span id='database_748' style='display:none'>PAROLE sottinsieme</span>
<span id='database_749' style='display:none'>Discourse Treebank for Chinese</span>
<span id='database_750' style='display:none'>The UU SLI-Dyslexia project data base</span>
<span id='database_751' style='display:none'>FSPAR</span>
<span id='database_752' style='display:none'>EMMA - Evaluation metric for morphological analysis</span>
<span id='database_753' style='display:none'>Hierarchical Aligner for Alignment Validation</span>
<span id='database_754' style='display:none'>The Quranic Arabic Corpus</span>
<span id='database_755' style='display:none'>EPIC Twitter NLP Development Dataset</span>
<span id='database_756' style='display:none'>CSTM toolkit</span>
<span id='database_757' style='display:none'>The Halliday Centre Tagger</span>
<span id='database_758' style='display:none'>Controlled Language in Crisis Management (CLCM)</span>
<span id='database_759' style='display:none'>InterText</span>
<span id='database_760' style='display:none'>AhoVoicedDB</span>
<span id='database_761' style='display:none'>VOLEM</span>
<span id='database_762' style='display:none'>Parallel Corpus</span>
<span id='database_763' style='display:none'>AUTONOMATA-g2p-toolkit</span>
<span id='database_764' style='display:none'>ETAPE Corpus</span>
<span id='database_765' style='display:none'>Boston University Radio News Corpus</span>
<span id='database_766' style='display:none'>Twitter Emotion Corpus</span>
<span id='database_767' style='display:none'>Medical Subject Headings</span>
<span id='database_768' style='display:none'>A2ST</span>
<span id='database_769' style='display:none'>Formalism for Morphological and Phonological Grammar</span>
<span id='database_770' style='display:none'>Annotations at analytical level: Instructions for annotators</span>
<span id='database_771' style='display:none'>WordNet Domains Specific corpora from Wikipedia</span>
<span id='database_772' style='display:none'>LUNA.PL</span>
<span id='database_773' style='display:none'>EU Green Papers</span>
<span id='database_774' style='display:none'>The Helsinki Finite-State Transducer toolkit</span>
<span id='database_775' style='display:none'>Nordisk Språkteknologi (NST) Corpus</span>
<span id='database_776' style='display:none'>Damascene Colloquial Arabic Speech</span>
<span id='database_777' style='display:none'>Wikipedia Edit Category Corpus</span>
<span id='database_778' style='display:none'>XTag</span>
<span id='database_779' style='display:none'>USENET corpus</span>
<span id='database_780' style='display:none'>Lancaster Contemporary Mandarin Corpus</span>
<span id='database_781' style='display:none'>FunGramKB Morphicon</span>
<span id='database_782' style='display:none'>CoPEF</span>
<span id='database_783' style='display:none'>GigaWord</span>
<span id='database_784' style='display:none'>Transcriber</span>
<span id='database_785' style='display:none'>SAVAS META-SHARE repository</span>
<span id='database_786' style='display:none'>OCELLES</span>
<span id='database_787' style='display:none'>WeSearch Data Collection (WDC)</span>
<span id='database_788' style='display:none'>Penn Chinese Treebank 6.0</span>
<span id='database_789' style='display:none'>Diabetes and cancer monolingual comparable corpora</span>
<span id='database_790' style='display:none'>ELRA-W0051</span>
<span id='database_791' style='display:none'>Extented DOLCE Ontology</span>
<span id='database_792' style='display:none'>LeXimir</span>
<span id='database_793' style='display:none'>Semeval-2010 Japanese WSD task dataset</span>
<span id='database_794' style='display:none'>Dbt & Faccette</span>
<span id='database_795' style='display:none'>Eustagger: lemmatizer/tagger for Basque</span>
<span id='database_796' style='display:none'>Google Translate</span>
<span id='database_797' style='display:none'>Fonema TTS front end</span>
<span id='database_798' style='display:none'>Arabic Syntactic Dependency Parser Model</span>
<span id='database_799' style='display:none'>Conversational Arabic Long Audio Aligner</span>
<span id='database_800' style='display:none'>DOOM, Romanian Lexical Data Bases: Inflected and Syllabic Forms Dictionaries</span>
<span id='database_801' style='display:none'>Kachna L1/L2 Picture Replication Corpus</span>
<span id='database_802' style='display:none'>CorAl</span>
<span id='database_803' style='display:none'>Prague Dependency Treebank 2.0 (PDT 2.0)</span>
<span id='database_804' style='display:none'>KPWr</span>
<span id='database_805' style='display:none'>Ngram Search Engine</span>
<span id='database_806' style='display:none'>Cantonese-Mandarin Parallel Corpus</span>
<span id='database_807' style='display:none'>SRILM</span>
<span id='database_808' style='display:none'>Near-Identity Relations for Coreference (NIdent) CA</span>
<span id='database_809' style='display:none'>ARALEX</span>
<span id='database_810' style='display:none'>GALE LDC Parallel Data</span>
<span id='database_811' style='display:none'>NTCIR-8 Patent Translation data</span>
<span id='database_812' style='display:none'>SEMAFOR 2.0</span>
<span id='database_813' style='display:none'>Lithuanian stemmer</span>
<span id='database_814' style='display:none'>reconcile</span>
<span id='database_815' style='display:none'>OntoTag's ontologies</span>
<span id='database_816' style='display:none'>GARD Question Decomposition Corpus</span>
<span id='database_817' style='display:none'>BabelMASC</span>
<span id='database_818' style='display:none'>C&C Tools</span>
<span id='database_819' style='display:none'>AncientTimes Corpus</span>
<span id='database_820' style='display:none'>Opinosis dataset</span>
<span id='database_821' style='display:none'>NomBank</span>
<span id='database_822' style='display:none'>The English-Swedish-Turkish Parallel Treebank</span>
<span id='database_823' style='display:none'>Nganasan Lexicon</span>
<span id='database_824' style='display:none'>Web Dataset for Text-based Image Annotation Development</span>
<span id='database_825' style='display:none'>maui-indexer</span>
<span id='database_826' style='display:none'>Berkeley Parser</span>
<span id='database_827' style='display:none'>SogouT</span>
<span id='database_828' style='display:none'>BioCauseMK</span>
<span id='database_829' style='display:none'>Corpus of Spontaneous Japanese</span>
<span id='database_830' style='display:none'>SpLaSH</span>
<span id='database_831' style='display:none'>Disease and Adverse Effect Corpus</span>
<span id='database_832' style='display:none'>LX-Parser</span>
<span id='database_833' style='display:none'>BioCaster Ontology</span>
<span id='database_834' style='display:none'>Guidelines for Evaluators for MT output errors</span>
<span id='database_835' style='display:none'>Jena</span>
<span id='database_836' style='display:none'>ConVote</span>
<span id='database_837' style='display:none'>Edinburgh alignment corpus</span>
<span id='database_838' style='display:none'>AnCora-Nom-Es</span>
<span id='database_839' style='display:none'>Stockholm-Umeå Corpus (SUC) 2.0</span>
<span id='database_840' style='display:none'>http://nif4oggd.aksw.org/</span>
<span id='database_841' style='display:none'>SwitchBoard</span>
<span id='database_842' style='display:none'>NKI TE-VOICE Analysis Tool</span>
<span id='database_843' style='display:none'>National Center for Sign Language and Gesture Rescoures (NCSLGR) corpus, Boston Univeristy</span>
<span id='database_844' style='display:none'>Elissa</span>
<span id='database_845' style='display:none'>Principles of Part-of-Speech (POS) Tagging of Indian Language Corpora</span>
<span id='database_846' style='display:none'>Polish LFG Grammar</span>
<span id='database_847' style='display:none'>Iraqi Arabic Did You Mean...?</span>
<span id='database_848' style='display:none'>Accentuator</span>
<span id='database_849' style='display:none'>Web-Harvested Corpus Annotated with GermaNet Senses (WebCAGe)</span>
<span id='database_850' style='display:none'>Tokenizer</span>
<span id='database_851' style='display:none'>IceTagger</span>
<span id='database_852' style='display:none'>PDT-VALLEX</span>
<span id='database_853' style='display:none'>DUC 2007 Summaries and Pyramid annotations</span>
<span id='database_854' style='display:none'>MaNaLa</span>
<span id='database_855' style='display:none'>Language Models</span>
<span id='database_856' style='display:none'>ENCORE</span>
<span id='database_857' style='display:none'>JCore Token FraMed Model</span>
<span id='database_858' style='display:none'>Connective annotation over EN/FR Europarl</span>
<span id='database_859' style='display:none'>JRC-Acquis partitioned in domains</span>
<span id='database_860' style='display:none'>Chinese Treebank 6.0</span>
<span id='database_861' style='display:none'>Senso Comune</span>
<span id='database_862' style='display:none'>Modeling linguistic corpora in OWL/DL (POWLA)</span>
<span id='database_863' style='display:none'>Wiktionary Morphology Data</span>
<span id='database_864' style='display:none'>Baidu Zhidao Corpus</span>
<span id='database_865' style='display:none'>GeneralLexicon_Fr-En</span>
<span id='database_866' style='display:none'>SemLinker</span>
<span id='database_867' style='display:none'>MASC</span>
<span id='database_868' style='display:none'>EcoLexicon Corpus</span>
<span id='database_869' style='display:none'>MorphoAdorner name recognizer</span>
<span id='database_870' style='display:none'>SNOMED International</span>
<span id='database_871' style='display:none'>Base de datos de verbos, alternancias de diátesis y esquemas sintácticos del español (ADESSE)</span>
<span id='database_872' style='display:none'>x</span>
<span id='database_873' style='display:none'>Russian Sign Language Explanatory Dictionary</span>
<span id='database_874' style='display:none'>Corpus for Verbal Intelligence Estimation</span>
<span id='database_875' style='display:none'>Kyoto Text Corpus version 4.0</span>
<span id='database_876' style='display:none'>BioInfer</span>
<span id='database_877' style='display:none'>Morphadorner</span>
<span id='database_878' style='display:none'>MOSES</span>
<span id='database_879' style='display:none'>Multilingual sentiment dictionaries</span>
<span id='database_880' style='display:none'>Rhythm Book Corpus</span>
<span id='database_881' style='display:none'>Persian Part of Speech Tagger</span>
<span id='database_882' style='display:none'>Connexor parser</span>
<span id='database_883' style='display:none'>Attribution Database</span>
<span id='database_884' style='display:none'>CC-CEDICT</span>
<span id='database_885' style='display:none'>SALT</span>
<span id='database_886' style='display:none'>MST parser (maximum spanning tree parser)</span>
<span id='database_887' style='display:none'>OpeNER-sentiment-lexicons</span>
<span id='database_888' style='display:none'>COST: Corpus Of Spanish Tweets</span>
<span id='database_889' style='display:none'>English Gigaword Fourth Edition</span>
<span id='database_890' style='display:none'>ANNODIS corpus</span>
<span id='database_891' style='display:none'>Chasen Japanese Language parser</span>
<span id='database_892' style='display:none'>DKPro</span>
<span id='database_893' style='display:none'>G-Flow summaries</span>
<span id='database_894' style='display:none'>Stanford Deterministic Coreference Resolution System</span>
<span id='database_895' style='display:none'>TreeEditor</span>
<span id='database_896' style='display:none'>Sp_ToBI annotated Glissando Corpus</span>
<span id='database_897' style='display:none'>Glossa</span>
<span id='database_898' style='display:none'>Hedging Annotation Guidelines</span>
<span id='database_899' style='display:none'>Data Collection Platform</span>
<span id='database_900' style='display:none'>MATE</span>
<span id='database_901' style='display:none'>Romanian and English corpora</span>
<span id='database_902' style='display:none'>Service-Finder Automatic Semantic Annotator</span>
<span id='database_903' style='display:none'>WordFreak</span>
<span id='database_904' style='display:none'>WOLF</span>
<span id='database_905' style='display:none'>Drugs@FDA Data Files</span>
<span id='database_906' style='display:none'>IULA Catalan LSP Treebank</span>
<span id='database_907' style='display:none'>Web Service Architecture for Aligners</span>
<span id='database_908' style='display:none'>LOC.ORG GOLD STANDARD</span>
<span id='database_909' style='display:none'>QuEst</span>
<span id='database_910' style='display:none'>Corpus Search</span>
<span id='database_911' style='display:none'>Spanish Resource Grammar</span>
<span id='database_912' style='display:none'>KPG English Learner Corpus</span>
<span id='database_913' style='display:none'>AC/DC</span>
<span id='database_914' style='display:none'>WMT 2010 system combination task corpus</span>
<span id='database_915' style='display:none'>MElt (Maximum Entropy Lexicon-enriched Tagger)</span>
<span id='database_916' style='display:none'>Cambridge HTK, HDecode</span>
<span id='database_917' style='display:none'>kdd09cma1</span>
<span id='database_918' style='display:none'>Named Entity Recogniser for Manipuri using SVM</span>
<span id='database_919' style='display:none'>jWordSplitter</span>
<span id='database_920' style='display:none'>Atlante Sintattico d'Italia, Syntactic Atlas of Italy (ASIt)</span>
<span id='database_921' style='display:none'>Wikipedia Revision Preposition Error Corpus</span>
<span id='database_922' style='display:none'>WordSim - 353</span>
<span id='database_923' style='display:none'>FunGramKB Grammaticon</span>
<span id='database_924' style='display:none'>R</span>
<span id='database_925' style='display:none'>Multilingual corpus for Opinion Mining</span>
<span id='database_926' style='display:none'>Multi Layered Hindi Dependency Treebank</span>
<span id='database_927' style='display:none'>Affect database</span>
<span id='database_928' style='display:none'>TAC 2009 KBP Evaluation Source Data</span>
<span id='database_929' style='display:none'>The CELEX Lexical Database</span>
<span id='database_930' style='display:none'>Chinese Opinion Treebank</span>
<span id='database_931' style='display:none'>Twitter Adverse Drug Reaction Mentions - ASU DIEGO Lab</span>
<span id='database_932' style='display:none'>TweetNLP</span>
<span id='database_933' style='display:none'>Revista Pesquisa FAPESP Parallel Corpora</span>
<span id='database_934' style='display:none'>IDIX</span>
<span id='database_935' style='display:none'>PrimeCoef</span>
<span id='database_936' style='display:none'>African WordNet</span>
<span id='database_937' style='display:none'>Quaero named entity corpus</span>
<span id='database_938' style='display:none'>pre-test, training, post-test experimental design</span>
<span id='database_939' style='display:none'>Service-Finder data</span>
<span id='database_940' style='display:none'>DELAF</span>
<span id='database_941' style='display:none'>Legal</span>
<span id='database_942' style='display:none'>Tree tagger</span>
<span id='database_943' style='display:none'>KALAKA-3</span>
<span id='database_944' style='display:none'>DBnary</span>
<span id='database_945' style='display:none'>Fips</span>
<span id='database_946' style='display:none'>FinINTAS</span>
<span id='database_947' style='display:none'>TIGER Treebank</span>
<span id='database_948' style='display:none'>Intra Chunk Dependency Parser</span>
<span id='database_949' style='display:none'>kddo1</span>
<span id='database_950' style='display:none'>MIX1</span>
<span id='database_951' style='display:none'>40 brèves 2012</span>
<span id='database_952' style='display:none'>OpenNLP Toolkit</span>
<span id='database_953' style='display:none'>The Survey Zone (SurZe)</span>
<span id='database_954' style='display:none'>RWTH-PHOENIX-Weather</span>
<span id='database_955' style='display:none'>AhoDiarize</span>
<span id='database_956' style='display:none'>Aesop’s fables and Andrew Lang fairy tales collection</span>
<span id='database_957' style='display:none'>dict.cc</span>
<span id='database_958' style='display:none'>AVATecH BAtch eXecutor ABAX</span>
<span id='database_959' style='display:none'>Swiss-French SpeechDat(II)</span>
<span id='database_960' style='display:none'>SweetOnionCCG2PTBConverter</span>
<span id='database_961' style='display:none'>Hyderabad Dependency Treebank (HyDT)</span>
<span id='database_962' style='display:none'>English Gigaword Corpus Fourth Edition</span>
<span id='database_963' style='display:none'>EU Bookshop parallel corpus</span>
<span id='database_964' style='display:none'>Giza++</span>
<span id='database_965' style='display:none'>Chronolines Corpus</span>
<span id='database_966' style='display:none'>FBIS Corpus</span>
<span id='database_967' style='display:none'>Suggested Upper Merged Ontology (SUMO)</span>
<span id='database_968' style='display:none'>TüBa-D/Z connective annotation</span>
<span id='database_969' style='display:none'>BabelNet</span>
<span id='database_970' style='display:none'>Morphind</span>
<span id='database_971' style='display:none'>A Library for Support Vector Machines (LibSVM)</span>
<span id='database_972' style='display:none'>Document Understanding Conference (DUC) Corpus</span>
<span id='database_973' style='display:none'>Reuters</span>
<span id='database_974' style='display:none'>The Herme Database of Spontaneous Multimodal Human-Robot Dialogues</span>
<span id='database_975' style='display:none'>GATE Crowdsourcing Plugin</span>
<span id='database_976' style='display:none'>Dependency Part of BulTreeBank (BulTreeBank-DP)</span>
<span id='database_977' style='display:none'>CroDeriV</span>
<span id='database_978' style='display:none'>2008 NIST Speaker Recognition Evaluation Test Set</span>
<span id='database_979' style='display:none'>ILex</span>
<span id='database_980' style='display:none'>South-East European Times</span>
<span id='database_981' style='display:none'>ParCor 1.0</span>
<span id='database_982' style='display:none'>Balanced Corpus of Contemporary Written Japanese (BCCWJ)</span>
<span id='database_983' style='display:none'>Kanji Tester</span>
<span id='database_984' style='display:none'>Erlangen-CLP</span>
<span id='database_985' style='display:none'>Prague Czech-English Dependency Treebank</span>
<span id='database_986' style='display:none'>Qalqalah Pronunication Guide</span>
<span id='database_987' style='display:none'>Annotation guidelines for Dutch-English word alignment</span>
<span id='database_988' style='display:none'>ILSP Text Simplification tool</span>
<span id='database_989' style='display:none'>Automatic Punjabi Text Summarization System</span>
<span id='database_990' style='display:none'>NIST LRE 2007</span>
<span id='database_991' style='display:none'>LAST MINUTE Corpus</span>
<span id='database_992' style='display:none'>ComputerWorld</span>
<span id='database_993' style='display:none'>NLP Resource Metadata Questions Treebank (NLP-QT)</span>
<span id='database_994' style='display:none'>Lextec</span>
<span id='database_995' style='display:none'>Person Name Recognition for Alpine Texts</span>
<span id='database_996' style='display:none'>MERLOT Guidelines</span>
<span id='database_997' style='display:none'>n-gram-FeatureSet</span>
<span id='database_998' style='display:none'>Italian Corpus for Aspect Based Sentiment Analysis of Movie Reviews</span>
<span id='database_999' style='display:none'>CoNLL’2000 shared task dataset</span>
<span id='database_1000' style='display:none'>BabyExp</span>
<span id='database_1001' style='display:none'>AEGIR lexicon</span>
<span id='database_1002' style='display:none'>Kurzprotokolle des deutschen Bundeskabinetts</span>
<span id='database_1003' style='display:none'>MWE Focused Crawler</span>
<span id='database_1004' style='display:none'>WordNet 3.0</span>
<span id='database_1005' style='display:none'>Hindi Projected Treebank from English-Hindi Tides Parallel Corpus</span>
<span id='database_1006' style='display:none'>books, DVDs, electronics, and kitchen appliances</span>
<span id='database_1007' style='display:none'>Chinese CCGbank</span>
<span id='database_1008' style='display:none'>PUMA</span>
<span id='database_1009' style='display:none'>Spontal-N</span>
<span id='database_1010' style='display:none'>CCGBank</span>
<span id='database_1011' style='display:none'>Multi-purpose Annotation Environmen (MAE)</span>
<span id='database_1012' style='display:none'>Glottolog/Langdoc</span>
<span id='database_1013' style='display:none'>Sprout</span>
<span id='database_1014' style='display:none'>Czech Multi-channel Speech Database of DSP Lectures</span>
<span id='database_1015' style='display:none'>ePetals</span>
<span id='database_1016' style='display:none'>Fangorn</span>
<span id='database_1017' style='display:none'>Illinois Part Of Speech Tagger</span>
<span id='database_1018' style='display:none'>broad-coverage lexical resource of Arabic</span>
<span id='database_1019' style='display:none'>Anonimised patient records</span>
<span id='database_1020' style='display:none'>Mooney dataset: geoquery data</span>
<span id='database_1021' style='display:none'>named entity corpus</span>
<span id='database_1022' style='display:none'>Apertium linguistic data for the Spanish--English language pair</span>
<span id='database_1023' style='display:none'>TimeBankPT</span>
<span id='database_1024' style='display:none'>Maximum Likelihood Linear Regression (MLLR)</span>
<span id='database_1025' style='display:none'>Alpino</span>
<span id='database_1026' style='display:none'>WordNetBR or TEP</span>
<span id='database_1027' style='display:none'>ItalWordNet</span>
<span id='database_1028' style='display:none'>Maritime Insurance Terminology</span>
<span id='database_1029' style='display:none'>WebODE</span>
<span id='database_1030' style='display:none'>Hungarian WordNet</span>
<span id='database_1031' style='display:none'>Porter Stemmer</span>
<span id='database_1032' style='display:none'>Tsinghua Chinese Treebank</span>
<span id='database_1033' style='display:none'>Multicultural Romanized Name Matches</span>
<span id='database_1034' style='display:none'>Google Books Distributional Thesaurus</span>
<span id='database_1035' style='display:none'>WMT 2010 Translation Task Data</span>
<span id='database_1036' style='display:none'>KiDKo</span>
<span id='database_1037' style='display:none'>ELAN</span>
<span id='database_1038' style='display:none'>FrameNet-Wordnet Detour</span>
<span id='database_1039' style='display:none'>Internal</span>
<span id='database_1040' style='display:none'>Corpus of harvested CMDI data</span>
<span id='database_1041' style='display:none'>Eurogene system</span>
<span id='database_1042' style='display:none'>FNE dataset</span>
<span id='database_1043' style='display:none'>TeP</span>
<span id='database_1044' style='display:none'>RuThes thesaurus</span>
<span id='database_1045' style='display:none'>HaGenLex</span>
<span id='database_1046' style='display:none'>Multidomain Uncertainty Corpus, Bioscope</span>
<span id='database_1047' style='display:none'>Kadokawa Ruigo Shin Jiten</span>
<span id='database_1048' style='display:none'>Corpus DEFT (DÉfi Fouille de Textes)</span>
<span id='database_1049' style='display:none'>Cooperative Remote Search Task (CReST) corpus</span>
<span id='database_1050' style='display:none'>MultiWordNet</span>
<span id='database_1051' style='display:none'>Movie Review Data</span>
<span id='database_1052' style='display:none'>test set containing non-compositional and compositional phrases</span>
<span id='database_1053' style='display:none'>MaltParser</span>
<span id='database_1054' style='display:none'>Twitter Datasets</span>
<span id='database_1055' style='display:none'>Estonian Foreign Accent Corpus</span>
<span id='database_1056' style='display:none'>Corpus de Référence de Français Parlé</span>
<span id='database_1057' style='display:none'>NTU Sentiment Dictionary (NTUSD)</span>
<span id='database_1058' style='display:none'>NELL Ontology and Knowledge Base</span>
<span id='database_1059' style='display:none'>Nijmegen Corpus of Casual Spanish (NCCSp)</span>
<span id='database_1060' style='display:none'>Penn Treebank NP annotation</span>
<span id='database_1061' style='display:none'>The Web Col Framework</span>
<span id='database_1062' style='display:none'>MLTagger</span>
<span id='database_1063' style='display:none'>Czech Meteor tables</span>
<span id='database_1064' style='display:none'>GerManC</span>
<span id='database_1065' style='display:none'>RestQueries250ext</span>
<span id='database_1066' style='display:none'>Personal Name Treebank</span>
<span id='database_1067' style='display:none'>Romanian Generative Lexicon</span>
<span id='database_1068' style='display:none'>Crowdsourced Annotation of Metadiscourse for English</span>
<span id='database_1069' style='display:none'>Automatic Text Labelling of Topics</span>
<span id='database_1070' style='display:none'>DBpedia</span>
<span id='database_1071' style='display:none'>CorpusSVCs</span>
<span id='database_1072' style='display:none'>Norwegian Dependency Treebank</span>
<span id='database_1073' style='display:none'>Granularity ontology</span>
<span id='database_1074' style='display:none'>Rainbow</span>
<span id='database_1075' style='display:none'>DeCo</span>
<span id='database_1076' style='display:none'>Diversity in Collective Discourse</span>
<span id='database_1077' style='display:none'>Open Information Extraction</span>
<span id='database_1078' style='display:none'>ComLex</span>
<span id='database_1079' style='display:none'>Five PPI Corpora</span>
<span id='database_1080' style='display:none'>Controlled Language for Crisis Management (CLCM)</span>
<span id='database_1081' style='display:none'>Multi-Domain Sentiment Dataset</span>
<span id='database_1082' style='display:none'>Stanford NLP POS tagger</span>
<span id='database_1083' style='display:none'>DUC 2002 Summarization Corpus</span>
<span id='database_1084' style='display:none'>RoZP</span>
<span id='database_1085' style='display:none'>SVMTool</span>
<span id='database_1086' style='display:none'>Speech & Prosody Segmentation</span>
<span id='database_1087' style='display:none'>Potential opinion bearing bi-grams from our corpus of editorials and opinionated texts</span>
<span id='database_1088' style='display:none'>VALLEX 2.5</span>
<span id='database_1089' style='display:none'>Multilingual UN Parallel Text 2000—2009 (MultiUN)</span>
<span id='database_1090' style='display:none'>Feature Norms of German Noun Compounds</span>
<span id='database_1091' style='display:none'>Representing/Manipulating multimodal annotated data</span>
<span id='database_1092' style='display:none'>N-code</span>
<span id='database_1093' style='display:none'>Wiktionary</span>
<span id='database_1094' style='display:none'>KorAP</span>
<span id='database_1095' style='display:none'>Apertium linguistic data for the Breton--French language pair</span>
<span id='database_1096' style='display:none'>PyPLN</span>
<span id='database_1097' style='display:none'>SIGNUM Database</span>
<span id='database_1098' style='display:none'>Frown corpus</span>
<span id='database_1099' style='display:none'>Callisto</span>
<span id='database_1100' style='display:none'>Corpus CINTIL-PREPLEXOS</span>
<span id='database_1101' style='display:none'>Cambridge Hidden Markov Model Toolkit (HTK)</span>
<span id='database_1102' style='display:none'>Legal texts</span>
<span id='database_1103' style='display:none'>SNOMED CT</span>
<span id='database_1104' style='display:none'>Sclera2cornetto</span>
<span id='database_1105' style='display:none'>Linguistic Data Consortium</span>
<span id='database_1106' style='display:none'>Unicode Character Database</span>
<span id='database_1107' style='display:none'>FipsCo</span>
<span id='database_1108' style='display:none'>JRC-ACQFrRo</span>
<span id='database_1109' style='display:none'>Prague Dependency Treebank (PDT) 2.0</span>
<span id='database_1110' style='display:none'>The Berkeley Word Aligner</span>
<span id='database_1111' style='display:none'>Switchboard Corpus</span>
<span id='database_1112' style='display:none'>MPQA Opinion Corpus</span>
<span id='database_1113' style='display:none'>Doors</span>
<span id='database_1114' style='display:none'>LAST MINUTE</span>
<span id='database_1115' style='display:none'>ANNALIST: ANNotation ALIgnment and Scoring Tool</span>
<span id='database_1116' style='display:none'>Web 1T 5-gram corpus</span>
<span id='database_1117' style='display:none'>OpenEditionReviews</span>
<span id='database_1118' style='display:none'>n-gram List</span>
<span id='database_1119' style='display:none'>AAC-Austrian Academy Corpus</span>
<span id='database_1120' style='display:none'>ImagiWeb Political Dataset</span>
<span id='database_1121' style='display:none'>Text::NSP</span>
<span id='database_1122' style='display:none'>Praaline</span>
<span id='database_1123' style='display:none'>A Colloquial Corpus of Japanese Sign Language</span>
<span id='database_1124' style='display:none'>pepr Framework - Process Engine for Pattern Recognition</span>
<span id='database_1125' style='display:none'>furniture ontology</span>
<span id='database_1126' style='display:none'>VerbNet</span>
<span id='database_1127' style='display:none'>The IBM Sales Question Corpus</span>
<span id='database_1128' style='display:none'>N2 Corpus - Referent Properties</span>
<span id='database_1129' style='display:none'>GenitivDB</span>
<span id='database_1130' style='display:none'>Dragon Naturally Speaking</span>
<span id='database_1131' style='display:none'>WSJCAM0 Cambridge Read News</span>
<span id='database_1132' style='display:none'>Message Understanding Conference (MUC) 4</span>
<span id='database_1133' style='display:none'>Lwazi TTS corpora (v Sep 2009)</span>
<span id='database_1134' style='display:none'>SUMO</span>
<span id='database_1135' style='display:none'>Multilingual MPQA</span>
<span id='database_1136' style='display:none'>Hindi/Urdu Treebank</span>
<span id='database_1137' style='display:none'>Object-Based Seech Recognizer</span>
<span id='database_1138' style='display:none'>French and English Synonym Databases</span>
<span id='database_1139' style='display:none'>FunGramKB Onomasticon</span>
<span id='database_1140' style='display:none'>ICTCLAS</span>
<span id='database_1141' style='display:none'>Bikel parser</span>
<span id='database_1142' style='display:none'>CROVALLEX</span>
<span id='database_1143' style='display:none'>Graded sense and usage annotation</span>
<span id='database_1144' style='display:none'>Wikipedia (Turkish section)</span>
<span id='database_1145' style='display:none'>Xeros Incremental Parser XIP</span>
<span id='database_1146' style='display:none'>Aligned ConceptNets</span>
<span id='database_1147' style='display:none'>VoxFactory</span>
<span id='database_1148' style='display:none'>Balanced Corpus of Contemporary Written Japanese</span>
<span id='database_1149' style='display:none'>Annnotation Ontology</span>
<span id='database_1150' style='display:none'>TrEd</span>
<span id='database_1151' style='display:none'>WAPUSK20</span>
<span id='database_1152' style='display:none'>Not Applicable</span>
<span id='database_1153' style='display:none'>ERG: LinGO English Resource Grammar</span>
<span id='database_1154' style='display:none'>Spanish-LSE corpus</span>
<span id='database_1155' style='display:none'>slTwitterCorpus</span>
<span id='database_1156' style='display:none'>Corpus of Interactional Data (CID)</span>
<span id='database_1157' style='display:none'>Ground truth word-to-word translations</span>
<span id='database_1158' style='display:none'>Multi-perspective question answering (MPQA) sentiment lexicon</span>
<span id='database_1159' style='display:none'>Semantic Annotation Tool (SAT)</span>
<span id='database_1160' style='display:none'>MPI/DOBES Language Resource Archive</span>
<span id='database_1161' style='display:none'>GrETEL (Greedy Extraction of Trees for Empirical Linguistics)</span>
<span id='database_1162' style='display:none'>The EASR Corpus of European Portuguese Elderly Speech</span>
<span id='database_1163' style='display:none'>Modern Greek Spontaneous Essay Text</span>
<span id='database_1164' style='display:none'>WordNet Similarity</span>
<span id='database_1165' style='display:none'>ISABASE - 2</span>
<span id='database_1166' style='display:none'>UMLS Specialist Lexicon</span>
<span id='database_1167' style='display:none'>North American News Text Corpus (LDC95T21)</span>
<span id='database_1168' style='display:none'>Lexique et grammaire de dérivation</span>
<span id='database_1169' style='display:none'>Associative Concept Dictionary (ACD)</span>
<span id='database_1170' style='display:none'>Turkish Word Sketches</span>
<span id='database_1171' style='display:none'>TOEFL11</span>
<span id='database_1172' style='display:none'>Protégé</span>
<span id='database_1173' style='display:none'>Utool</span>
<span id='database_1174' style='display:none'>Stockholm Umeå Corpus</span>
<span id='database_1175' style='display:none'>GER-TV1000h</span>
<span id='database_1176' style='display:none'>COREL: Conceptual Representation Language</span>
<span id='database_1177' style='display:none'>CFINSLs</span>
<span id='database_1178' style='display:none'>Marathi Wordnet</span>
<span id='database_1179' style='display:none'>LDC User Interface</span>
<span id='database_1180' style='display:none'>anonymized-for-blind-review</span>
<span id='database_1181' style='display:none'>Onomastica interlanguage pronunciation lexicon</span>
<span id='database_1182' style='display:none'>6-monthly snapshots of Wikipedia</span>
<span id='database_1183' style='display:none'>LAF/GrAF</span>
<span id='database_1184' style='display:none'>PORTMEDIA Lang</span>
<span id='database_1185' style='display:none'>Standford Parser</span>
<span id='database_1186' style='display:none'>Cultural Heritage item - Wikipedia match</span>
<span id='database_1187' style='display:none'>Illinois Chunker</span>
<span id='database_1188' style='display:none'>Women's Studies International Forum</span>
<span id='database_1189' style='display:none'>CAREGIVER</span>
<span id='database_1190' style='display:none'>Annotation Schema for Collocation Errors in Learner Corpora</span>
<span id='database_1191' style='display:none'>AV-WordProminence</span>
<span id='database_1192' style='display:none'>PROMETHEUS DATABASE</span>
<span id='database_1193' style='display:none'>Phrase Structure Grammar</span>
<span id='database_1194' style='display:none'>LIPS: Lexical Isolation Point Software</span>
<span id='database_1195' style='display:none'>ElixirFM</span>
<span id='database_1196' style='display:none'>StackQA</span>
<span id='database_1197' style='display:none'>Mutation Miner Ontology</span>
<span id='database_1198' style='display:none'>Lwazi ASR Corpora</span>
<span id='database_1199' style='display:none'>Multi-Perspective Question Answering (MPQA) Opinion Corpus</span>
<span id='database_1200' style='display:none'>EXMARaLDA</span>
<span id='database_1201' style='display:none'>Penn Treebank</span>
<span id='database_1202' style='display:none'>BTEC</span>
<span id='database_1203' style='display:none'>Kyoto Text Corpus</span>
<span id='database_1204' style='display:none'>Preposition Noun Combinations</span>
<span id='database_1205' style='display:none'>MetaphorDeco</span>
<span id='database_1206' style='display:none'>GRANSKA tagger</span>
<span id='database_1207' style='display:none'>Dense TimeBank</span>
<span id='database_1208' style='display:none'>The Specialist Lexicon</span>
<span id='database_1209' style='display:none'>English–Croatian tourism parallel corpus (high-quality)</span>
<span id='database_1210' style='display:none'>URL dataset</span>
<span id='database_1211' style='display:none'>HeidelTime</span>
<span id='database_1212' style='display:none'>Chinese-Japanese Quasi-parallel corpus</span>
<span id='database_1213' style='display:none'>Myanmar Word Tokenizer</span>
<span id='database_1214' style='display:none'>Hal Scientific Paper Corpora</span>
<span id='database_1215' style='display:none'>The Freiburg - LOB Corpus of British English (FLOB)</span>
<span id='database_1216' style='display:none'>FIRE 2010 data</span>
<span id='database_1217' style='display:none'>QTag</span>
<span id='database_1218' style='display:none'>Wikipedia Edit Chains</span>
<span id='database_1219' style='display:none'>https://github.com/aksw/nif4oggd</span>
<span id='database_1220' style='display:none'>English (P)LTAG treebank</span>
<span id='database_1221' style='display:none'>Query Dataset for Email Search</span>
<span id='database_1222' style='display:none'>MICA</span>
<span id='database_1223' style='display:none'>Appraise</span>
<span id='database_1224' style='display:none'><oXygen/> XML editor</span>
<span id='database_1225' style='display:none'>Hebrew-English transliteration dictionary</span>
<span id='database_1226' style='display:none'>ACE (Automatic Content Extraction) 2005 Corpus</span>
<span id='database_1227' style='display:none'>LQVSumm</span>
<span id='database_1228' style='display:none'>A Dataset for Extracting Korean Keywords from National Assembly Transcripts</span>
<span id='database_1229' style='display:none'>Arabic word list</span>
<span id='database_1230' style='display:none'>TnT Tagger</span>
<span id='database_1231' style='display:none'>TSUBAKI Corpus</span>
<span id='database_1232' style='display:none'>TaKIPI</span>
<span id='database_1233' style='display:none'>Prague Dependency Treebank 2.0</span>
<span id='database_1234' style='display:none'>DUC 2007 Dataset</span>
<span id='database_1235' style='display:none'>passage cpcv3</span>
<span id='database_1236' style='display:none'>Felicittà + TW-FELICITTA</span>
<span id='database_1237' style='display:none'>Manhattan</span>
<span id='database_1238' style='display:none'>Multilingual corpora with coreferential annotation of person entities</span>
<span id='database_1239' style='display:none'>Colloquial Egyptian Arabic</span>
<span id='database_1240' style='display:none'>Human Annotation for Machine Translation</span>
<span id='database_1241' style='display:none'>KByte  </span>
<span id='database_1242' style='display:none'>ROUGE-1.5.5</span>
<span id='database_1243' style='display:none'>French Lexical Network</span>
<span id='database_1244' style='display:none'>Bilingual lexicon</span>
<span id='database_1245' style='display:none'>Propbank frameset files</span>
<span id='database_1246' style='display:none'>IULA Spanish LSP Treebank</span>
<span id='database_1247' style='display:none'>metu sabancı Turkish Dependency Treebank - addtional annotation</span>
<span id='database_1248' style='display:none'>VerbOcean</span>
<span id='database_1249' style='display:none'>Alborada-I3A corpus of disordered speech</span>
<span id='database_1250' style='display:none'>UNT Computer Science Short Answer Dataset v 2.0</span>
<span id='database_1251' style='display:none'>Japanese WordNet</span>
<span id='database_1252' style='display:none'>Peykare Or Textual Corpus of Persian Language</span>
<span id='database_1253' style='display:none'>MaryTTS</span>
<span id='database_1254' style='display:none'>Penn Chinese Treebank</span>
<span id='database_1255' style='display:none'>YouTube Educational Videos</span>
<span id='database_1256' style='display:none'>Annotated UGC corpus for normalization</span>
<span id='database_1257' style='display:none'>New Stuttgart Radio News Corpus</span>
<span id='database_1258' style='display:none'>Croatian Inflectional Lexicon (MOLEX)</span>
<span id='database_1259' style='display:none'>C-3 (Coherence and Coreference Corpus)</span>
<span id='database_1260' style='display:none'>ILCI corpora</span>
<span id='database_1261' style='display:none'>Nihongo Goi Taikei</span>
<span id='database_1262' style='display:none'>Term Extractor</span>
<span id='database_1263' style='display:none'>ISOcat Data Category Registry (DCR)</span>
<span id='database_1264' style='display:none'>Croatian Valency Lexicon of Verbs (CROVALLEX)</span>
<span id='database_1265' style='display:none'>InSight Interaction</span>
<span id='database_1266' style='display:none'>Myanmar-English-Myanmar Dictionary</span>
<span id='database_1267' style='display:none'>Multitrad</span>
<span id='database_1268' style='display:none'>Java WordNet::Similarity</span>
<span id='database_1269' style='display:none'>Romanian Wordnet</span>
<span id='database_1270' style='display:none'>Standard Arabic Morphological Analyzer (SAMA)</span>
<span id='database_1271' style='display:none'>DIRT</span>
<span id='database_1272' style='display:none'>wordnet</span>
<span id='database_1273' style='display:none'>Szeged Corpus</span>
<span id='database_1274' style='display:none'>PACE Corpus</span>
<span id='database_1275' style='display:none'>Knowtator</span>
<span id='database_1276' style='display:none'>eXtended WordFrameNet</span>
<span id='database_1277' style='display:none'>DE-EN long-range pre-ordering development and test set</span>
<span id='database_1278' style='display:none'>Open Corpus of Everyday Documents for Simplification Tasks</span>
<span id='database_1279' style='display:none'>Collection of newspaper articles</span>
<span id='database_1280' style='display:none'>TimeML</span>
<span id='database_1281' style='display:none'>Benchmark Database of Phonetic Alignments</span>
<span id='database_1282' style='display:none'>news articles on epidemic</span>
<span id='database_1283' style='display:none'>Ester2</span>
<span id='database_1284' style='display:none'>Czech National Corpus</span>
<span id='database_1285' style='display:none'>Transcriber SK scripts modification</span>
<span id='database_1286' style='display:none'>Systran</span>
<span id='database_1287' style='display:none'>Annotated Celebrity Corpus with semantic people relation mentions</span>
<span id='database_1288' style='display:none'>FSPar</span>
<span id='database_1289' style='display:none'>MSTParser</span>
<span id='database_1290' style='display:none'>OMProDat - Open Multilingual Prosodic Database</span>
<span id='database_1291' style='display:none'>CKIP</span>
<span id='database_1292' style='display:none'>LVF+1</span>
<span id='database_1293' style='display:none'>JRC-Acquis</span>
<span id='database_1294' style='display:none'>Dictionary of Affect in Language</span>
<span id='database_1295' style='display:none'>Tree-Alignment Visualizer</span>
<span id='database_1296' style='display:none'>Chinese to Uighur, Kazakh and Kyrgiz Parallel Dictionary Database</span>
<span id='database_1297' style='display:none'>JCore Sentence FraMed Model</span>
<span id='database_1298' style='display:none'>Mapudungun-Spanish AVENUE Machine Translation Grammar</span>
<span id='database_1299' style='display:none'>TimeBank Corpus</span>
<span id='database_1300' style='display:none'>YAGO2</span>
<span id='database_1301' style='display:none'>Le Petit Prince in UNL</span>
<span id='database_1302' style='display:none'>Rules for annotating VPs of Northern Sotho</span>
<span id='database_1303' style='display:none'>Sighan 2005 bakeoff data</span>
<span id='database_1304' style='display:none'>http://mlode.nlp2rdf.org/sparql</span>
<span id='database_1305' style='display:none'>RCV1</span>
<span id='database_1306' style='display:none'>CELEX2</span>
<span id='database_1307' style='display:none'>A ECA-MSA Lexicon</span>
<span id='database_1308' style='display:none'>NTCIR 2008 Training Data</span>
<span id='database_1309' style='display:none'>Gulf of Guinea creole corpora</span>
<span id='database_1310' style='display:none'>Project documents ontology</span>
<span id='database_1311' style='display:none'>ISCA Archive</span>
<span id='database_1312' style='display:none'>The 4 Universities' dataset</span>
<span id='database_1313' style='display:none'>French Wikipedia</span>
<span id='database_1314' style='display:none'>Twente Debate Corpus</span>
<span id='database_1315' style='display:none'>Turkish Dependency Parser</span>
<span id='database_1316' style='display:none'>OpenNLP</span>
<span id='database_1317' style='display:none'>Hindi WordNet</span>
<span id='database_1318' style='display:none'>IAC</span>
<span id='database_1319' style='display:none'>Hamshahri</span>
<span id='database_1320' style='display:none'>Indic NLP toolkit</span>
<span id='database_1321' style='display:none'>Dicovalence</span>
<span id='database_1322' style='display:none'>Asymmetric Threat Response and Analysis Program (ATRAP)</span>
<span id='database_1323' style='display:none'>Part-of-speech tagger for Norwegian</span>
<span id='database_1324' style='display:none'>Berkeley Word Aligner</span>
<span id='database_1325' style='display:none'>Wikipedia dump</span>
<span id='database_1326' style='display:none'>hrsrTwitterCorpus</span>
<span id='database_1327' style='display:none'>Health-related sentiments and opinions</span>
<span id='database_1328' style='display:none'>Corpus of naturally-occurring corrections and paraphrases from Wikipedia's revision history</span>
<span id='database_1329' style='display:none'>arXMLiv</span>
<span id='database_1330' style='display:none'>Penn Chinese Treebank 5.0</span>
<span id='database_1331' style='display:none'>CRF++</span>
<span id='database_1332' style='display:none'>People's Daily</span>
<span id='database_1333' style='display:none'>Czech Web Corpus 2011</span>
<span id='database_1334' style='display:none'>Propbank</span>
<span id='database_1335' style='display:none'>ISO DIS 24612 Language resource management - Lingusitic annotation framework (LAF) and other ISO standards</span>
<span id='database_1336' style='display:none'>Prosomarker</span>
<span id='database_1337' style='display:none'>Annotation Guidelines for English-Dutch Translation Quality Assessment, version 1.1</span>
<span id='database_1338' style='display:none'>A list of confusion sets</span>
<span id='database_1339' style='display:none'>europarl</span>
<span id='database_1340' style='display:none'>Self-Annotation Tool</span>
<span id='database_1341' style='display:none'>Dependency-Parsed FrameNet Corpus</span>
<span id='database_1342' style='display:none'>LDC Arabic-English parallel corpus</span>
<span id='database_1343' style='display:none'>MeSH</span>
<span id='database_1344' style='display:none'>Morphological lexicon for Estonian in GF</span>
<span id='database_1345' style='display:none'>Usability Guidelines for Annotation Tools</span>
<span id='database_1346' style='display:none'>Google V2 and n-gram tools</span>
<span id='database_1347' style='display:none'>The basic dictionary of FinSL example text corpus (Suvi)</span>
<span id='database_1348' style='display:none'>ad hoc tasks of TREC</span>
<span id='database_1349' style='display:none'>Multilingual LUNA Corpus</span>
<span id='database_1350' style='display:none'>The EDR Electronic Dictionary</span>
<span id='database_1351' style='display:none'>Named Entity Annotated Tweet Corpus in Turkish</span>
<span id='database_1352' style='display:none'>CoNLL 2005 Dataset</span>
<span id='database_1353' style='display:none'>Document Understanding Conference (DUC) 2004 Dataset</span>
<span id='database_1354' style='display:none'>dict.cc lexicon</span>
<span id='database_1355' style='display:none'>LDC Word Alignment Tool</span>
<span id='database_1356' style='display:none'>TAUS Data Association TM Pool</span>
<span id='database_1357' style='display:none'>MPQA Opinion Corpus version 1.2 with additional judgments</span>
<span id='database_1358' style='display:none'>WordNet RDF</span>
<span id='database_1359' style='display:none'>Galician speech corpus</span>
<span id='database_1360' style='display:none'>European Medicines Agency (EMEA) documents</span>
<span id='database_1361' style='display:none'>OntoLing annotation model</span>
<span id='database_1362' style='display:none'>A thesaurus of argument structure for Japanese verbs</span>
<span id='database_1363' style='display:none'>UCS toolkit</span>
<span id='database_1364' style='display:none'>Annotated Film Dialogue Corpus</span>
<span id='database_1365' style='display:none'>SMI Remote Eye Tracking Device</span>
<span id='database_1366' style='display:none'>IARPA Babel Language Pack Zulu (training set)</span>
<span id='database_1367' style='display:none'>Core Code</span>
<span id='database_1368' style='display:none'>Attribution Corpus of Italian</span>
<span id='database_1369' style='display:none'>IITKGP Text Emotion Corpus</span>
<span id='database_1370' style='display:none'>Semisupervised Named Entity Recognizer (SemiNER)</span>
<span id='database_1371' style='display:none'>Reference Corpus of Contemporary Portuguese (CRPC) Modality Sample</span>
<span id='database_1372' style='display:none'>TAC 2011 KBP English Evaluation Entity Linking Annotation v1.1</span>
<span id='database_1373' style='display:none'>National Center for Sign Language and Gesture Resources (NCSLGR) corpus, Boston University</span>
<span id='database_1374' style='display:none'>MusicNavi2 database</span>
<span id='database_1375' style='display:none'>Dutch Speech-Text Aligner</span>
<span id='database_1376' style='display:none'>Spanish2MSL</span>
<span id='database_1377' style='display:none'>IJCNLP-08 NERSSEAL Shared Task Data</span>
<span id='database_1378' style='display:none'>CMDI assessment score</span>
<span id='database_1379' style='display:none'>Manipuri Stemmer</span>
<span id='database_1380' style='display:none'>Mixed-Language Document Corpus</span>
<span id='database_1381' style='display:none'>LibSVM</span>
<span id='database_1382' style='display:none'>ACE 2005</span>
<span id='database_1383' style='display:none'>SLMotion</span>
<span id='database_1384' style='display:none'>3D Face Tracker</span>
<span id='database_1385' style='display:none'>Asian WordNet</span>
<span id='database_1386' style='display:none'>Transcoding Sanskrit Formats</span>
<span id='database_1387' style='display:none'>AutoTutor</span>
<span id='database_1388' style='display:none'>Collapsed Gibbs Sampling Methods for Topic Models (lda)</span>
<span id='database_1389' style='display:none'>MaltParser for Spanish</span>
<span id='database_1390' style='display:none'>HMM-based dialogue annotation</span>
<span id='database_1391' style='display:none'>Simcoach speech synthesis evaluation corpus</span>
<span id='database_1392' style='display:none'>German Medical Terms</span>
<span id='database_1393' style='display:none'>MMI Facial Expression Database</span>
<span id='database_1394' style='display:none'>Helsinki Corpus of Swahili</span>
<span id='database_1395' style='display:none'>WordNet-Affect-OCC</span>
<span id='database_1396' style='display:none'>DicoInfo</span>
<span id='database_1397' style='display:none'>Dutch Sentiment Lexicon (adjectives)</span>
<span id='database_1398' style='display:none'>Pragmatic Resources of Old Indo-European Languages</span>
<span id='database_1399' style='display:none'>Annotated Webclopedia question collection</span>
<span id='database_1400' style='display:none'>ukb</span>
<span id='database_1401' style='display:none'>FunGramKB Lexicon</span>
<span id='database_1402' style='display:none'>20-Newsgroup</span>
<span id='database_1403' style='display:none'>TREC Million Query Track</span>
<span id='database_1404' style='display:none'>BulTreebank</span>
<span id='database_1405' style='display:none'>TextPro</span>
<span id='database_1406' style='display:none'>enju HPSG parser</span>
<span id='database_1407' style='display:none'>Appraisal Lexicon</span>
<span id='database_1408' style='display:none'>Enju</span>
<span id='database_1409' style='display:none'>Matxin</span>
<span id='database_1410' style='display:none'>Urdu Web-based Corpus</span>
<span id='database_1411' style='display:none'>Library of Natural-Language Representations of Formal Relations</span>
<span id='database_1412' style='display:none'>The GENIA corpus</span>
<span id='database_1413' style='display:none'>Elhuyar Basque-Chinese predictionary</span>
<span id='database_1414' style='display:none'>GG</span>
<span id='database_1415' style='display:none'>Worldmapper</span>
<span id='database_1416' style='display:none'>Fictional Character Names</span>
<span id='database_1417' style='display:none'>pecco</span>
<span id='database_1418' style='display:none'>Iban-English Lexicon</span>
<span id='database_1419' style='display:none'>Integrated Reference Corpora for Spoken Romance Languages (C-ORAL-ROM)</span>
<span id='database_1420' style='display:none'>HowNet Knowledge Database</span>
<span id='database_1421' style='display:none'>LVCoref - Coreference resolution for Latvian</span>
<span id='database_1422' style='display:none'>NUs Corpus of Learner English (NUCLE)</span>
<span id='database_1423' style='display:none'>Documents from I and II Vatican Councils</span>
<span id='database_1424' style='display:none'>Dutch corpus for abbreviation detection and resolution</span>
<span id='database_1425' style='display:none'>TWA sense tagged data</span>
<span id='database_1426' style='display:none'>Syntax in Elements of Text (SET)</span>
<span id='database_1427' style='display:none'>GIZA#</span>
<span id='database_1428' style='display:none'>TexAFon 2.0</span>
<span id='database_1429' style='display:none'>ee</span>
<span id='database_1430' style='display:none'>Customized Europarl corpus</span>
<span id='database_1431' style='display:none'>Italian Framenet</span>
<span id='database_1432' style='display:none'>ir4qa_eval</span>
<span id='database_1433' style='display:none'>Georgian Ontological Semantics Lexicon</span>
<span id='database_1434' style='display:none'>Corpus de Extractos de Textos Electrónicos NILC/Folha de S. Paulo - CETENFolha</span>
<span id='database_1435' style='display:none'>Kyrgyz to Chinese Dictionary Database</span>
<span id='database_1436' style='display:none'>SweVoc</span>
<span id='database_1437' style='display:none'>DériF</span>
<span id='database_1438' style='display:none'>The NumGen (Generating Numerical Expressions) Corpus</span>
<span id='database_1439' style='display:none'>n-gram Index</span>
<span id='database_1440' style='display:none'>The Digital Library of Polish and Poland-related Ephemeral Prints from the 16th, 17th and 18th Centuries</span>
<span id='database_1441' style='display:none'>ParTes</span>
<span id='database_1442' style='display:none'>Language identifier for Bosnian, Croatian and Serbian</span>
<span id='database_1443' style='display:none'>CCG SRL tool</span>
<span id='database_1444' style='display:none'>CINTIL Propbank</span>
<span id='database_1445' style='display:none'>ColLex.en</span>
<span id='database_1446' style='display:none'>Inforex</span>
<span id='database_1447' style='display:none'>Google N-gram corpus Web 1T (2006)</span>
<span id='database_1448' style='display:none'>SemEval 2010 Coreference Resolution Task Corpus</span>
<span id='database_1449' style='display:none'>EvalLex</span>
<span id='database_1450' style='display:none'>Bengali Speech Corpus</span>
<span id='database_1451' style='display:none'>Delicious dataset</span>
<span id='database_1452' style='display:none'>ABaC:us</span>
<span id='database_1453' style='display:none'>ICTCLAS(Institute of Computing Technology, Chinese Lexical Analysis System)</span>
<span id='database_1454' style='display:none'>La Repubblica</span>
<span id='database_1455' style='display:none'>Email Summarization and Keyword Extraction</span>
<span id='database_1456' style='display:none'>Synthetic English-Croatian Europarl</span>
<span id='database_1457' style='display:none'>Magahi Morphological Analyser</span>
<span id='database_1458' style='display:none'>Test suite for biomedical ontology concept recognition systems</span>
<span id='database_1459' style='display:none'>Europarl Parallel Corpus</span>
<span id='database_1460' style='display:none'>SentiWS</span>
<span id='database_1461' style='display:none'>Canola Corpus</span>
<span id='database_1462' style='display:none'>MASKKOT</span>
<span id='database_1463' style='display:none'>Quaero QA corpus</span>
<span id='database_1464' style='display:none'>SAMT extension</span>
<span id='database_1465' style='display:none'>LIBSVM</span>
<span id='database_1466' style='display:none'>SSPNet Vocalisation Corpus (SVC)</span>
<span id='database_1467' style='display:none'>Event levels</span>
<span id='database_1468' style='display:none'>Afrikaans Word Lists</span>
<span id='database_1469' style='display:none'>Maximum Entropy Modeling Toolkit</span>
<span id='database_1470' style='display:none'>POV differences</span>
<span id='database_1471' style='display:none'>The Database of Icelandic Inflection [Beygingarlýsing íslensks nútímamáls]</span>
<span id='database_1472' style='display:none'>Collocation database for corpus evaluation</span>
<span id='database_1473' style='display:none'>UIUC Question Classification Data (Training set 5)</span>
<span id='database_1474' style='display:none'>Djangology: A Light-weight Web-based Tool for Distributed Collaborative Text Annotation</span>
<span id='database_1475' style='display:none'>Srebrenica corpus</span>
<span id='database_1476' style='display:none'>Annotated corpus</span>
<span id='database_1477' style='display:none'>ACCURAT Initial Comparable Corpora</span>
<span id='database_1478' style='display:none'>Pashto monolingual corpus</span>
<span id='database_1479' style='display:none'>TRIS Corpus v.03</span>
<span id='database_1480' style='display:none'>Stanford Dependency Annotation Manual for French</span>
<span id='database_1481' style='display:none'>TimeEL corpus</span>
<span id='database_1482' style='display:none'>DAD</span>
<span id='database_1483' style='display:none'>PoliMorf</span>
<span id='database_1484' style='display:none'>Spanish C-ORAL-ROM-ELE</span>
<span id='database_1485' style='display:none'>Semaine Database</span>
<span id='database_1486' style='display:none'>Chinese-English Patent MT preprocessing toolkit</span>
<span id='database_1487' style='display:none'>Add MS Kit</span>
<span id='database_1488' style='display:none'>Academia Sinica Balanced Corpus of Modern Chinese (Sinica Corpus)</span>
<span id='database_1489' style='display:none'>Gossip ontology and celebrity DB</span>
<span id='database_1490' style='display:none'>SignCom corpus</span>
<span id='database_1491' style='display:none'>Urdu data annotated for Emotions</span>
<span id='database_1492' style='display:none'>Sweet-Home speech and multimodal corpus</span>
<span id='database_1493' style='display:none'>DK-CLARIN Movin Corpus</span>
<span id='database_1494' style='display:none'>Language Grid</span>
<span id='database_1495' style='display:none'>TREC Questions</span>
<span id='database_1496' style='display:none'>The Wenzhou Spoken Corpus</span>
<span id='database_1497' style='display:none'>Simplify_Data_For_Parsers_Cz</span>
<span id='database_1498' style='display:none'>SETimes.HR</span>
<span id='database_1499' style='display:none'>ISST</span>
<span id='database_1500' style='display:none'>Dictionary of Italian Collocations</span>
<span id='database_1501' style='display:none'>MeCab</span>
<span id='database_1502' style='display:none'>TREC Category B</span>
<span id='database_1503' style='display:none'>Virtual Language Observatory</span>
<span id='database_1504' style='display:none'>MoKi</span>
<span id='database_1505' style='display:none'>C99</span>
<span id='database_1506' style='display:none'>Buddhism clause-based mapping dictionary</span>
<span id='database_1507' style='display:none'>English-Latvian comparable corpus extracted from Wikipedia</span>
<span id='database_1508' style='display:none'>Appraisal Lexicon (lexique de l'évaluation)</span>
<span id='database_1509' style='display:none'>The KPG English Corpus</span>
<span id='database_1510' style='display:none'>BioScope</span>
<span id='database_1511' style='display:none'>KNP</span>
<span id='database_1512' style='display:none'>Corpus LSFB</span>
<span id='database_1513' style='display:none'>TREC AP corpus</span>
<span id='database_1514' style='display:none'>Uyghur to Chinese MT corpus (UCC)</span>
<span id='database_1515' style='display:none'>ISOcat Data Category Registry</span>
<span id='database_1516' style='display:none'>DUC-2006, DUC-2007 data</span>
<span id='database_1517' style='display:none'>STEx</span>
<span id='database_1518' style='display:none'>Polish Summaries Corpus</span>
<span id='database_1519' style='display:none'>EPSILON</span>
<span id='database_1520' style='display:none'>ILCI Phase 1 corpora</span>
<span id='database_1521' style='display:none'>EMEA - European Medicines Agency documents</span>
<span id='database_1522' style='display:none'>MC4WEPS</span>
<span id='database_1523' style='display:none'>Morfessor</span>
<span id='database_1524' style='display:none'>ImagAct-ItalWorNet-Mapping</span>
<span id='database_1525' style='display:none'>SxPipe/NP2eval</span>
<span id='database_1526' style='display:none'>Gromoteur</span>
<span id='database_1527' style='display:none'>Vocal Expressions of Nineteen Emotions across Cultures (VENEC)</span>
<span id='database_1528' style='display:none'>PAROLE</span>
<span id='database_1529' style='display:none'>OpenNLP Tools</span>
<span id='database_1530' style='display:none'>Palula Corpus</span>
<span id='database_1531' style='display:none'>AWAdb</span>
<span id='database_1532' style='display:none'>POS.LM</span>
<span id='database_1533' style='display:none'>LUCID</span>
<span id='database_1534' style='display:none'>Joint Research Centre JRC-Acquis German-English</span>
<span id='database_1535' style='display:none'>Hebrew-English parallel corpus</span>
<span id='database_1536' style='display:none'>NOISEX-92</span>
<span id='database_1537' style='display:none'>Online Database of Interlinear text (ODIN)</span>
<span id='database_1538' style='display:none'>Official Europarl test set from WMT 2008</span>
<span id='database_1539' style='display:none'>CFT Corpus of Fiscal Terminology</span>
<span id='database_1540' style='display:none'>SCD-based Dictionary Entry Parser</span>
<span id='database_1541' style='display:none'>SoraLex</span>
<span id='database_1542' style='display:none'>Aspect and Domain Sensitive Sentiment Lexicon</span>
<span id='database_1543' style='display:none'>GermanPolarityClues</span>
<span id='database_1544' style='display:none'>CCGbank</span>
<span id='database_1545' style='display:none'>LDC Machine Reading Annotation Tool</span>
<span id='database_1546' style='display:none'>Russian sentiment lexicon for movie reviews</span>
<span id='database_1547' style='display:none'>GCIDE</span>
<span id='database_1548' style='display:none'>The Switchboard Corpus</span>
<span id='database_1549' style='display:none'>Ripper</span>
<span id='database_1550' style='display:none'>Project ETAPE corpus</span>
<span id='database_1551' style='display:none'>SentiML</span>
<span id='database_1552' style='display:none'>NLTK POS FraMed Model</span>
<span id='database_1553' style='display:none'>Term Grammar</span>
<span id='database_1554' style='display:none'>SpaPD</span>
<span id='database_1555' style='display:none'>Stanford Names Entity Recogniser 1.1</span>
<span id='database_1556' style='display:none'>AVLaughterCycle database</span>
<span id='database_1557' style='display:none'>The Twins Corpus of Museum Visitor Questions</span>
<span id='database_1558' style='display:none'>Sentiment Quiz</span>
<span id='database_1559' style='display:none'>Roots</span>
<span id='database_1560' style='display:none'>movie review dataset</span>
<span id='database_1561' style='display:none'>Chinese Character Data (Hanzi Data)</span>
<span id='database_1562' style='display:none'>Korean emotional speech corpus</span>
<span id='database_1563' style='display:none'>ConanDoyle-neg corpus</span>
<span id='database_1564' style='display:none'>Sample of ANC Annotated for Idioms</span>
<span id='database_1565' style='display:none'>DECODA</span>
<span id='database_1566' style='display:none'>DUC 2002 Dataset</span>
<span id='database_1567' style='display:none'>NOMCO multimodal Nordic corpus</span>
<span id='database_1568' style='display:none'>A Database for Measuring Linguistic Information Content</span>
<span id='database_1569' style='display:none'>Gold Standard for Spanish Mass Nouns</span>
<span id='database_1570' style='display:none'>L3Morpho</span>
<span id='database_1571' style='display:none'>Aix Map Task</span>
<span id='database_1572' style='display:none'>Dependency based Transfer rules</span>
<span id='database_1573' style='display:none'>1984-EH-NP</span>
<span id='database_1574' style='display:none'>Biomedical why-question answering corpus</span>
<span id='database_1575' style='display:none'>AAC - Austrian Academ Corpus</span>
<span id='database_1576' style='display:none'>Corpus de Français Parlé Parisien</span>
<span id='database_1577' style='display:none'>multi-X</span>
<span id='database_1578' style='display:none'>Eng4NooJ</span>
<span id='database_1579' style='display:none'>Message Understanding Conference (MUC) 6</span>
<span id='database_1580' style='display:none'>Diachronic Corpus of Spanish</span>
<span id='database_1581' style='display:none'>GALE Phase 4 DevTest Chinese Word Alignment and Tagging</span>
<span id='database_1582' style='display:none'>English - Bengali Parallel Corpus</span>
<span id='database_1583' style='display:none'>Reuters RCV1</span>
<span id='database_1584' style='display:none'>Gold Standard for English non-deverbal eventive nouns</span>
<span id='database_1585' style='display:none'>AKUEM</span>
<span id='database_1586' style='display:none'>To be defined</span>
<span id='database_1587' style='display:none'>BAMA and SAMA modified agglutinative and fusional affix lexicon</span>
<span id='database_1588' style='display:none'>NKI TES /a/ corpus</span>
<span id='database_1589' style='display:none'>OpLexicon</span>
<span id='database_1590' style='display:none'>Steady Selling Product Review Dataset</span>
<span id='database_1591' style='display:none'>Greek Dependency Treebank</span>
<span id='database_1592' style='display:none'>MPI Language Resource Archive</span>
<span id='database_1593' style='display:none'>TARSQI Toolkit</span>
<span id='database_1594' style='display:none'>Multilingual Statistical Parsing Engine</span>
<span id='database_1595' style='display:none'>PoliTa</span>
<span id='database_1596' style='display:none'>Pr.A.Ti.D.</span>
<span id='database_1597' style='display:none'>FrameNet transformer</span>
<span id='database_1598' style='display:none'>Lingua::TreeTagger</span>
<span id='database_1599' style='display:none'>Utility Evaluation for Information Extraction</span>
<span id='database_1600' style='display:none'>Joint Research Centre JRC-Acquis</span>
<span id='database_1601' style='display:none'>Lingua::JA::Summarize::Extract</span>
<span id='database_1602' style='display:none'>ANVIL</span>
<span id='database_1603' style='display:none'>Translational Word Lists</span>
<span id='database_1604' style='display:none'>deepKnowNet</span>
<span id='database_1605' style='display:none'>Multilingual Named Enity Annotated Corpora</span>
<span id='database_1606' style='display:none'>Czech Morphological Analysis in PDT 2.0</span>
<span id='database_1607' style='display:none'>ETS Textual Entailment Test Suite for the Evaluation of Automatic Content Scoring Technologies</span>
<span id='database_1608' style='display:none'>SrpRec - Serbian morphological electronic dictionary (SrpRec)</span>
<span id='database_1609' style='display:none'>MyTerMS</span>
<span id='database_1610' style='display:none'>Woordenboek van de Drentse dialecten</span>
<span id='database_1611' style='display:none'>Knowledge Base Population Corpus (TAC KBP)</span>
<span id='database_1612' style='display:none'>Slovak tokenizer and sentence boundary detector</span>
<span id='database_1613' style='display:none'>A Uyghur Tokenizer and part-of-speech tagger</span>
<span id='database_1614' style='display:none'>Code-Mixing Social Media Text</span>
<span id='database_1615' style='display:none'>Tree Tagger</span>
<span id='database_1616' style='display:none'>Machine Learning for Language Toolkit (MALLET)</span>
<span id='database_1617' style='display:none'>Euconst</span>
<span id='database_1618' style='display:none'>TectoMT</span>
<span id='database_1619' style='display:none'>Digital Replay System (DRS)</span>
<span id='database_1620' style='display:none'>Large Movie Review Dataset</span>
<span id='database_1621' style='display:none'>FR.FrameNet</span>
<span id='database_1622' style='display:none'>Hyponym detection and generation</span>
<span id='database_1623' style='display:none'>University of Maryland Parallel Corpus Project: The Bible</span>
<span id='database_1624' style='display:none'>Discourse Graph Bank</span>
<span id='database_1625' style='display:none'>SIGA</span>
<span id='database_1626' style='display:none'>The Prague Dependency Treebank 2.0 (PDT 2.0)</span>
<span id='database_1627' style='display:none'>10-K Corpus</span>
<span id='database_1628' style='display:none'>TTL</span>
<span id='database_1629' style='display:none'>Wiki50</span>
<span id='database_1630' style='display:none'>RIDIRE-CPI</span>
<span id='database_1631' style='display:none'>Event Lexicon</span>
<span id='database_1632' style='display:none'>CHKD biomedical Dictionary of English/Chinese Named Entities</span>
<span id='database_1633' style='display:none'>Grammatical Framework (GF) Resource Grammar Library</span>
<span id='database_1634' style='display:none'>Automatic Content Extraction</span>
<span id='database_1635' style='display:none'>ARTiFactO</span>
<span id='database_1636' style='display:none'>Uyghur Encyclopedia (UE)</span>
<span id='database_1637' style='display:none'>EILMT parallel corpus</span>
<span id='database_1638' style='display:none'>Feature Rich Clustering</span>
<span id='database_1639' style='display:none'>ding</span>
<span id='database_1640' style='display:none'>English Web as Corpus (ukWaC)</span>
<span id='database_1641' style='display:none'>CID</span>
<span id='database_1642' style='display:none'>Sylli</span>
<span id='database_1643' style='display:none'>FLELLex</span>
<span id='database_1644' style='display:none'>CroTag Morphosyntactic Tagger</span>
<span id='database_1645' style='display:none'>T-PAS</span>
<span id='database_1646' style='display:none'>Multiword expression dictionaries</span>
<span id='database_1647' style='display:none'>the BiLingual Annotator/Annotation/Analysis Support Tool (Blast)</span>
<span id='database_1648' style='display:none'>Suicide risk corpus</span>
<span id='database_1649' style='display:none'>Logoscope</span>
<span id='database_1650' style='display:none'>de-news</span>
<span id='database_1651' style='display:none'>Human Language Tehcnology Virtual Organization</span>
<span id='database_1652' style='display:none'>CESTA Evaluation Package</span>
<span id='database_1653' style='display:none'>UTD-MotionEvent</span>
<span id='database_1654' style='display:none'>DOLCE</span>
<span id='database_1655' style='display:none'>GeoNames</span>
<span id='database_1656' style='display:none'>NTUSD</span>
<span id='database_1657' style='display:none'>WIT3</span>
<span id='database_1658' style='display:none'>Document-level aligned corpora</span>
<span id='database_1659' style='display:none'>DAIC</span>
<span id='database_1660' style='display:none'>Fine-Grain Morphological Analyzer and Part-of-Speech Tagger for Arabic Text</span>
<span id='database_1661' style='display:none'>Twente Newspaper Corpus</span>
<span id='database_1662' style='display:none'>Spanish Wikipedia</span>
<span id='database_1663' style='display:none'>BAStat</span>
<span id='database_1664' style='display:none'>Automatic Syntactic Analysis for Polish Language (ASA-PL)</span>
<span id='database_1665' style='display:none'>ACL RD-TEC: A Reference Dataset for Terminology Extraction and Classification Research in Computational Linguistics</span>
<span id='database_1666' style='display:none'>EUROPARL</span>
<span id='database_1667' style='display:none'>SUTime</span>
<span id='database_1668' style='display:none'>Timeline</span>
<span id='database_1669' style='display:none'>PUNKT</span>
<span id='database_1670' style='display:none'>Jezikovne tehnologije</span>
<span id='database_1671' style='display:none'>Stars corpus</span>
<span id='database_1672' style='display:none'>Apertium Spanish Monolingual Dictionary from Spanish-Catalan language pair</span>
<span id='database_1673' style='display:none'>Social Constructs - Pursuit of power</span>
<span id='database_1674' style='display:none'>Unicode CBETA Archives</span>
<span id='database_1675' style='display:none'>NgramQuery</span>
<span id='database_1676' style='display:none'>Glozz</span>
<span id='database_1677' style='display:none'>Associative Concept Dictionary for Verbs</span>
<span id='database_1678' style='display:none'>BAQ Corpus version 2.0</span>
<span id='database_1679' style='display:none'>MSR Syntactic Analogy Questions</span>
<span id='database_1680' style='display:none'>WTIMIT 1.0</span>
<span id='database_1681' style='display:none'>SxPipe/NP</span>
<span id='database_1682' style='display:none'>Yahoo Search API</span>
<span id='database_1683' style='display:none'>Elan</span>
<span id='database_1684' style='display:none'>TimeBank 1.2</span>
<span id='database_1685' style='display:none'>Icelandic-Bulgarian Edda</span>
<span id='database_1686' style='display:none'>Emo</span>
<span id='database_1687' style='display:none'>المناهج الجديدة</span>
<span id='database_1688' style='display:none'>TIGR Annotation Guidelines for Entity Extraction and Information Retrieval Ground Truth Creation</span>
<span id='database_1689' style='display:none'>TRECEVAL</span>
<span id='database_1690' style='display:none'>TREC corpus Disk4&5</span>
<span id='database_1691' style='display:none'>Goldstandard of German morphological analysis</span>
<span id='database_1692' style='display:none'>GALE 5W Distillation Evaluation</span>
<span id='database_1693' style='display:none'>Arabic Treebank Part3 - Version 3.1</span>
<span id='database_1694' style='display:none'>RWTH-BOSTON-50</span>
<span id='database_1695' style='display:none'>French Event Nominals Annotated corpus</span>
<span id='database_1696' style='display:none'>C&C tools</span>
<span id='database_1697' style='display:none'>CAMOMILE Collaborative Annotation Framework</span>
<span id='database_1698' style='display:none'>ReWriter</span>
<span id='database_1699' style='display:none'>English Gigaword</span>
<span id='database_1700' style='display:none'>Chamber debates</span>
<span id='database_1701' style='display:none'>Lwazi ASR corpus</span>
<span id='database_1702' style='display:none'>Vocon3200 Basque</span>
<span id='database_1703' style='display:none'>Latvian resource grammar in Grammatical Framework</span>
<span id='database_1704' style='display:none'>Lexical Normalisation Annotations for Short Text Messages (LexNorm)</span>
<span id='database_1705' style='display:none'>MISTRAL ALIZE</span>
<span id='database_1706' style='display:none'>Wikipedia Talk Pages Comments</span>
<span id='database_1707' style='display:none'>Greinir skáldskapar</span>
<span id='database_1708' style='display:none'>Corpus of Semantic Graphs with associated English strings</span>
<span id='database_1709' style='display:none'>Trecvid</span>
<span id='database_1710' style='display:none'>TANDEM-STRAIGHT</span>
<span id='database_1711' style='display:none'>ASKNet</span>
<span id='database_1712' style='display:none'>PAX</span>
<span id='database_1713' style='display:none'>GALE Phase 4 Chinese Parallel Word Alignment and Tagging Part 1</span>
<span id='database_1714' style='display:none'>Librivox</span>
<span id='database_1715' style='display:none'>InterLaugh</span>
<span id='database_1716' style='display:none'>Multilingual Central Repository version 3.0</span>
<span id='database_1717' style='display:none'>Annotated Tweets</span>
<span id='database_1718' style='display:none'>Simple Rule Language Editor</span>
<span id='database_1719' style='display:none'>AUDIMUS</span>
<span id='database_1720' style='display:none'>Subtitle Parallel Data in English-Chinese</span>
<span id='database_1721' style='display:none'>Aurora-2</span>
<span id='database_1722' style='display:none'>MADA+TOKAN</span>
<span id='database_1723' style='display:none'>CWB-treebank</span>
<span id='database_1724' style='display:none'>GENIA corpus</span>
<span id='database_1725' style='display:none'>TREE TAGER</span>
<span id='database_1726' style='display:none'>CroDep Dependency Parser</span>
<span id='database_1727' style='display:none'>Arabic-English Parallel Word Aligned Treebank Corpus</span>
<span id='database_1728' style='display:none'>Belgisch Staatsblad corpus</span>
<span id='database_1729' style='display:none'>German Grammar Terms Index (Unofficial)</span>
<span id='database_1730' style='display:none'>Kyoto University Text Corpus</span>
<span id='database_1731' style='display:none'>Stanford Named Entity Recognizer</span>
<span id='database_1732' style='display:none'>Wikipedia (German)</span>
<span id='database_1733' style='display:none'>Machine Reading P1 IC Training Data (LDC2010E07)</span>
<span id='database_1734' style='display:none'>Persian Linguistic Database (PLDB)</span>
<span id='database_1735' style='display:none'>Annotation tool for bilingual aligned corpora</span>
<span id='database_1736' style='display:none'>OpenLogos</span>
<span id='database_1737' style='display:none'>The World Atlas of Language Structures Online</span>
<span id='database_1738' style='display:none'>Xerox Incremental Parser (XIP)</span>
<span id='database_1739' style='display:none'>Redwoods</span>
<span id='database_1740' style='display:none'>TagNText</span>
<span id='database_1741' style='display:none'>Treebank.info</span>
<span id='database_1742' style='display:none'>CHILDES</span>
<span id='database_1743' style='display:none'>PAN Plagiarism Corpus PAN-PC-11</span>
<span id='database_1744' style='display:none'>MultiVal</span>
<span id='database_1745' style='display:none'>Corpus VALIBEL</span>
<span id='database_1746' style='display:none'>Corpus of temporal-causal structure</span>
<span id='database_1747' style='display:none'>Text handler</span>
<span id='database_1748' style='display:none'>JournalisticNL11</span>
<span id='database_1749' style='display:none'>LAMPADA</span>
<span id='database_1750' style='display:none'>Potsdam Commentary Corpus 2.0</span>
<span id='database_1751' style='display:none'>EPO corpus</span>
<span id='database_1752' style='display:none'>TinySVM</span>
<span id='database_1753' style='display:none'>Domain Adaptive Relation Extraction (DARE)</span>
<span id='database_1754' style='display:none'>ComplexChineseQAtestdata</span>
<span id='database_1755' style='display:none'>Dependency Shift Reduce parser (DeSR)</span>
<span id='database_1756' style='display:none'>DSim</span>
<span id='database_1757' style='display:none'>Tork Bootstrap Word Sense Inventory</span>
<span id='database_1758' style='display:none'>HunPos</span>
<span id='database_1759' style='display:none'>A Praat script for extacting pitch targets from vocal signals (PENTAtrainer)</span>
<span id='database_1760' style='display:none'>TALC-sef</span>
<span id='database_1761' style='display:none'>Automatic annotated training data for temporal slot filling</span>
<span id='database_1762' style='display:none'>Dihana</span>
<span id='database_1763' style='display:none'>The Indiana Cooperative Remote Search Task (CReST) Corpus</span>
<span id='database_1764' style='display:none'>NTCIR CLQA Chinese Questions</span>
<span id='database_1765' style='display:none'>LDC Chinese-English parallel corpus</span>
<span id='database_1766' style='display:none'>FTW multi-speaker synchronous acoustic and 3D facial marker data in Austrian German</span>
<span id='database_1767' style='display:none'>RoGER</span>
<span id='database_1768' style='display:none'>Corpus Quaero</span>
<span id='database_1769' style='display:none'>Discosuite</span>
<span id='database_1770' style='display:none'>HNZ segmented and POS tagged corpus</span>
<span id='database_1771' style='display:none'>CorefGraph</span>
<span id='database_1772' style='display:none'>Corpus of the focus of negation in Japanese</span>
<span id='database_1773' style='display:none'>Switchboard</span>
<span id='database_1774' style='display:none'>CLiPS Stylometry Investigation (CSI) corpus</span>
<span id='database_1775' style='display:none'>Unified Eventity Representation (UER)</span>
<span id='database_1776' style='display:none'>Tweet Ranking System</span>
<span id='database_1777' style='display:none'>Customized Europarl Corpus for Translation Studies</span>
<span id='database_1778' style='display:none'>TimeEL</span>
<span id='database_1779' style='display:none'>AIRA</span>
<span id='database_1780' style='display:none'>UIUC Learning Baed Java (LBJ) Named Entity Tagger</span>
<span id='database_1781' style='display:none'>Deceptive Opinion Spam Corpus v1</span>
<span id='database_1782' style='display:none'>German Web Corpus (DeWaC)</span>
<span id='database_1783' style='display:none'>Database of Lexical Simplification Errors</span>
<span id='database_1784' style='display:none'>Predicate Implication Signatures Lexicon</span>
<span id='database_1785' style='display:none'>SWiiT</span>
<span id='database_1786' style='display:none'>Japanese National Pension Law Corpus</span>
<span id='database_1787' style='display:none'>LT corpus</span>
<span id='database_1788' style='display:none'>Legal Corpus</span>
<span id='database_1789' style='display:none'>2009's Text summarization corpus for the credibility of information from the WEB</span>
<span id='database_1790' style='display:none'>Connexor Machinese Syntax</span>
<span id='database_1791' style='display:none'>dialogue speech act corpus</span>
<span id='database_1792' style='display:none'>MPQA Dataset</span>
<span id='database_1793' style='display:none'>CogFLUX</span>
<span id='database_1794' style='display:none'>CiNii articles</span>
<span id='database_1795' style='display:none'>SPEECON</span>
<span id='database_1796' style='display:none'>Mdbg Chinese-English dictionary</span>
<span id='database_1797' style='display:none'>openEAR</span>
<span id='database_1798' style='display:none'>OntoGen</span>
<span id='database_1799' style='display:none'>Hunglish</span>
<span id='database_1800' style='display:none'>Mac-morpho with EAGLES-like tags</span>
<span id='database_1801' style='display:none'>Bulgarian Sense Annotated Corpus</span>
<span id='database_1802' style='display:none'>sMail Speech Act Mining Rules</span>
<span id='database_1803' style='display:none'>AffectiveTask SemEval2007 Subsets with Figurative Language Annotations</span>
<span id='database_1804' style='display:none'>Master Metaphor List</span>
<span id='database_1805' style='display:none'>AUTONOMATA Spoken Name Corpus (ASNC)</span>
<span id='database_1806' style='display:none'>Koeling et al. (2005) corpus</span>
<span id='database_1807' style='display:none'>GMA (Geometric Mapping and Alignment)</span>
<span id='database_1808' style='display:none'>freWN3.0</span>
<span id='database_1809' style='display:none'>Verb Thesaurus</span>
<span id='database_1810' style='display:none'>uimaSolrCAS</span>
<span id='database_1811' style='display:none'>World Wide Arabic corpus</span>
<span id='database_1812' style='display:none'>Chinese Treebank 5.1</span>
<span id='database_1813' style='display:none'>Morphological Analyzer for Automatic Analysis of Ancient and Modern Persian</span>
<span id='database_1814' style='display:none'>Annotated RTE-5 Search Data Set</span>
<span id='database_1815' style='display:none'>WikiWarsDE</span>
<span id='database_1816' style='display:none'>Chinese Proposition Bank 1.0</span>
<span id='database_1817' style='display:none'>Emilya: Emotional body expression in daily actions database</span>
<span id='database_1818' style='display:none'>ISO 639-3</span>
<span id='database_1819' style='display:none'>MaJo</span>
<span id='database_1820' style='display:none'>TRIOS-TimeBank corpus</span>
<span id='database_1821' style='display:none'>GLÀFF</span>
<span id='database_1822' style='display:none'>Arabic Wordlist for Spellchecking</span>
<span id='database_1823' style='display:none'>Mo Piu data base</span>
<span id='database_1824' style='display:none'>Multimodal corpus in multi-party conversations</span>
<span id='database_1825' style='display:none'>UKWAC</span>
<span id='database_1826' style='display:none'>ISST-SST</span>
<span id='database_1827' style='display:none'>Suffix Tree Language Model</span>
<span id='database_1828' style='display:none'>PAN Plagiarism Corpus</span>
<span id='database_1829' style='display:none'>MorphOz</span>
<span id='database_1830' style='display:none'>Verb Ocean</span>
<span id='database_1831' style='display:none'>CPA-It Italian Pattern Dictionary</span>
<span id='database_1832' style='display:none'>Statistical Engish-Myanmar Machine Translation System</span>
<span id='database_1833' style='display:none'>IceNLP</span>
<span id='database_1834' style='display:none'>English Entity Detection and Tracking corpus for 2004 ACE project</span>
<span id='database_1835' style='display:none'>DUC</span>
<span id='database_1836' style='display:none'>Saturnalia corpus</span>
<span id='database_1837' style='display:none'>Apopsis</span>
<span id='database_1838' style='display:none'>CARDS-FLY</span>
<span id='database_1839' style='display:none'>PES</span>
<span id='database_1840' style='display:none'>Bulgarian National Corpus</span>
<span id='database_1841' style='display:none'>Freeling</span>
<span id='database_1842' style='display:none'>TP Perception Tests Tool</span>
<span id='database_1843' style='display:none'>CIAIR in-car speech corpus</span>
<span id='database_1844' style='display:none'>Text_Classification_Reuters_Corpus</span>
<span id='database_1845' style='display:none'>The Penn Chinese Treebank 6.0</span>
<span id='database_1846' style='display:none'>English-Russian Wiki-dictionary</span>
<span id='database_1847' style='display:none'>UofT Blog Corpus</span>
<span id='database_1848' style='display:none'>Reuters RCV1/RCV2 Multilingual Corpus</span>
<span id='database_1849' style='display:none'>ACE-2 Version 1.0</span>
<span id='database_1850' style='display:none'>Bitext's DataLexica</span>
<span id='database_1851' style='display:none'>OpenWordnet-PT</span>
<span id='database_1852' style='display:none'>The DGT Multilingual Translation Memory of the Acquis Communautaire: DGT-TM</span>
<span id='database_1853' style='display:none'>Brills Tagger</span>
<span id='database_1854' style='display:none'>Reuters-21578</span>
<span id='database_1855' style='display:none'>Icelandic Frequency Dictionary</span>
<span id='database_1856' style='display:none'>Humour in Religious Discourse</span>
<span id='database_1857' style='display:none'>CIAIR Back-Channel Utterance Corpus</span>
<span id='database_1858' style='display:none'>Named Entity Annotated Data</span>
<span id='database_1859' style='display:none'>Dialectal Arabic Data From Social Media</span>
<span id='database_1860' style='display:none'>Smart Sensor Integration</span>
<span id='database_1861' style='display:none'>Mitchell and Lapata 2008 Dataset</span>
<span id='database_1862' style='display:none'>Language Function Analysis Corpus (LFA-11)</span>
<span id='database_1863' style='display:none'>mate-tools</span>
<span id='database_1864' style='display:none'>TüBa-D/Z Release 7</span>
<span id='database_1865' style='display:none'>Vystadial 2013 – scripts</span>
<span id='database_1866' style='display:none'>FBIS and MTC data sets</span>
<span id='database_1867' style='display:none'>Corpus of Computer-mediated Communication in Hindi</span>
<span id='database_1868' style='display:none'>LT4eL terminological lexicons in IT domain</span>
<span id='database_1869' style='display:none'>James Pustejovsky</span>
<span id='database_1870' style='display:none'>National corpus of spoken Slovenian</span>
<span id='database_1871' style='display:none'>an online Chinese-English biomedical dictionary</span>
<span id='database_1872' style='display:none'>MAchine Learning for LanguagE Toolkit (MALLET)</span>
<span id='database_1873' style='display:none'>A Corpus of Spontaneous Multi-party Conversation in Bosnian Serbo-Croatian and British English</span>
<span id='database_1874' style='display:none'>Quaero Broadcast News Named Entity Corpus</span>
<span id='database_1875' style='display:none'>EMM NewsExplorer</span>
<span id='database_1876' style='display:none'>SPIDIX</span>
<span id='database_1877' style='display:none'>HIFI-AV</span>
<span id='database_1878' style='display:none'>Prague Dependency Treebank 2.5 (PDT 2.5)</span>
<span id='database_1879' style='display:none'>Test data</span>
<span id='database_1880' style='display:none'>ClueWeb09-JA</span>
<span id='database_1881' style='display:none'>Chinse Verb Error Evaluation Corpus</span>
<span id='database_1882' style='display:none'>UMLS</span>
<span id='database_1883' style='display:none'>LUNA corpus of conversational speech in Italian</span>
<span id='database_1884' style='display:none'>OpenNLP POS FraMed Model</span>
<span id='database_1885' style='display:none'>EventMapping.rdf</span>
<span id='database_1886' style='display:none'>German lexical substitution dataset</span>
<span id='database_1887' style='display:none'>Question Analyzer for Romanian</span>
<span id='database_1888' style='display:none'>SCI-FRAN-EURADIC Dictionnaire bilingue franÃ§ais-anglais</span>
<span id='database_1889' style='display:none'>mwttoolkit</span>
<span id='database_1890' style='display:none'>FunGramKB Suite</span>
<span id='database_1891' style='display:none'>The New York Times Annotated Corpus</span>
<span id='database_1892' style='display:none'>Burst-Annotated Co-Occurrence Network for the Arab Spring Domain</span>
<span id='database_1893' style='display:none'>Weka</span>
<span id='database_1894' style='display:none'>Editorials and opinionated texts</span>
<span id='database_1895' style='display:none'>PRONTO Firefighter Corpus</span>
<span id='database_1896' style='display:none'>newstest2010</span>
<span id='database_1897' style='display:none'>Message Understanding Conference (MUC) 4 Terrorism Corpus</span>
<span id='database_1898' style='display:none'>XStandoff 2.1</span>
<span id='database_1899' style='display:none'>JeuxDeMots French lexical network</span>
<span id='database_1900' style='display:none'>LocalNER</span>
<span id='database_1901' style='display:none'>uima-common</span>
<span id='database_1902' style='display:none'>Illinois Named Entity Tagger</span>
<span id='database_1903' style='display:none'>Sympalog SymRec</span>
<span id='database_1904' style='display:none'>CoreSC Annotation Guidelines</span>
<span id='database_1905' style='display:none'>ArguMeet</span>
<span id='database_1906' style='display:none'>Annotation Tool for Concepts and Relations (Recon) ???</span>
<span id='database_1907' style='display:none'>English Child Language Data Exchange System (CHILDES) Verb Construction Database</span>
<span id='database_1908' style='display:none'>Corpus LE-PAROLE</span>
<span id='database_1909' style='display:none'>The Wiki Machine</span>
<span id='database_1910' style='display:none'>Turin University Treebank</span>
<span id='database_1911' style='display:none'>Sanchay</span>
<span id='database_1912' style='display:none'>CROMER</span>
<span id='database_1913' style='display:none'>SciTex Corpus</span>
<span id='database_1914' style='display:none'>Subjectivity Lexicon for Dutch Adjectives</span>
<span id='database_1915' style='display:none'>FastKwic</span>
<span id='database_1916' style='display:none'>Corpus of Bilingual Emphasized Speech</span>
<span id='database_1917' style='display:none'>LIMA</span>
<span id='database_1918' style='display:none'>OAK system</span>
<span id='database_1919' style='display:none'>TripAdvisor Data Set</span>
<span id='database_1920' style='display:none'>RACAI web service</span>
<span id='database_1921' style='display:none'>NIST</span>
<span id='database_1922' style='display:none'>AIMed</span>
<span id='database_1923' style='display:none'>Zaya</span>
<span id='database_1924' style='display:none'>Tiger Treebank</span>
<span id='database_1925' style='display:none'>Wikipedia dump and scripts</span>
<span id='database_1926' style='display:none'>Persian TimeBank (PerTimeBank)</span>
<span id='database_1927' style='display:none'>Personalitwit3</span>
<span id='database_1928' style='display:none'>GRASS</span>
<span id='database_1929' style='display:none'>Jasnopis</span>
<span id='database_1930' style='display:none'>English Web Treebank</span>
<span id='database_1931' style='display:none'>CLC FCE Dataset</span>
<span id='database_1932' style='display:none'>Arabic Propbank</span>
<span id='database_1933' style='display:none'>Verbaction</span>
<span id='database_1934' style='display:none'>Italian BART</span>
<span id='database_1935' style='display:none'>Test Bench for transliteration of Indian language to English</span>
<span id='database_1936' style='display:none'>A Library for Large Linear Classification (LIBLINEAR)</span>
<span id='database_1937' style='display:none'>Estonian Multiparty dialogues</span>
<span id='database_1938' style='display:none'>Swahili-English parallel corpus</span>
<span id='database_1939' style='display:none'>Environmental -Legal Corpus</span>
<span id='database_1940' style='display:none'>Digital Review Data Set</span>
<span id='database_1941' style='display:none'>Portuguese Twitter corpus</span>
<span id='database_1942' style='display:none'>Semeval 2010 Japanese Lexical Sample corpus</span>
<span id='database_1943' style='display:none'>Al Akhawayn Instructor texts</span>
<span id='database_1944' style='display:none'>Pattern Dictionary of English Verbs (PDEV)</span>
<span id='database_1945' style='display:none'>UPC_ESMA</span>
<span id='database_1946' style='display:none'>Collection of Croatian Financial Texts</span>
<span id='database_1947' style='display:none'>MSTParser for Slovene</span>
<span id='database_1948' style='display:none'>Web 1t 5-gram corpus (1.1)</span>
<span id='database_1949' style='display:none'>Latvian Frame-semantic parser</span>
<span id='database_1950' style='display:none'>PDT-VALLEX 2.0</span>
<span id='database_1951' style='display:none'>Tiger</span>
<span id='database_1952' style='display:none'>MADA</span>
<span id='database_1953' style='display:none'>MAGEAD: A Morphological Analyzer and Generator for Arabic and its Dialects</span>
<span id='database_1954' style='display:none'>UIMA</span>
<span id='database_1955' style='display:none'>Lucene index of the English OpenDirectory</span>
<span id='database_1956' style='display:none'>Hebrew CHILDES corpus</span>
<span id='database_1957' style='display:none'>AnnoSem</span>
<span id='database_1958' style='display:none'>SemCor</span>
<span id='database_1959' style='display:none'>answer selection dataset</span>
<span id='database_1960' style='display:none'>IWSLT 09</span>
<span id='database_1961' style='display:none'>Leeds Web Genre Corpus</span>
<span id='database_1962' style='display:none'>SUPPLE</span>
<span id='database_1963' style='display:none'>SpeechDat, Callfriends, Broadcast news</span>
<span id='database_1964' style='display:none'>Free dictionary download</span>
<span id='database_1965' style='display:none'>Croatian Sentiment Lexicon</span>
<span id='database_1966' style='display:none'>GAWEX</span>
<span id='database_1967' style='display:none'>Croatian collocations gold set</span>
<span id='database_1968' style='display:none'>Persian TimeML (PerTimeML)</span>
<span id='database_1969' style='display:none'>Witchcraft</span>
<span id='database_1970' style='display:none'>ILSP FBT Tagger</span>
<span id='database_1971' style='display:none'>CGN</span>
<span id='database_1972' style='display:none'>BABEL Hungarian Speech Databases</span>
<span id='database_1973' style='display:none'>Erlangen Valency Pattern Bank</span>
<span id='database_1974' style='display:none'>ProductSentiRus</span>
<span id='database_1975' style='display:none'>AOL 2006 Query Log</span>
<span id='database_1976' style='display:none'>GF Resource Grammar Library</span>
<span id='database_1977' style='display:none'>ACE 2003</span>
<span id='database_1978' style='display:none'>Chinese law articles</span>
<span id='database_1979' style='display:none'>Sign Language Logical Model Creator</span>
<span id='database_1980' style='display:none'>Modality Lexicon</span>
<span id='database_1981' style='display:none'>multi-score summarizer</span>
<span id='database_1982' style='display:none'>AMI Corpus</span>
<span id='database_1983' style='display:none'>MUC6 Annotations</span>
<span id='database_1984' style='display:none'>Associative Concept Dictionary (ACD) for Verbs</span>
<span id='database_1985' style='display:none'>Swedish Framenet (SweFN)</span>
<span id='database_1986' style='display:none'>Bulgarian X-Language Parallel Corpus</span>
<span id='database_1987' style='display:none'>SAL Database</span>
<span id='database_1988' style='display:none'>TANL (Text Analytics for Natural Language)</span>
<span id='database_1989' style='display:none'>Foreign Language Examination Corpus of the University of Warsaw (FLEC UW)</span>
<span id='database_1990' style='display:none'>IWSLT 2012</span>
<span id='database_1991' style='display:none'>The Stanford Parser</span>
<span id='database_1992' style='display:none'>Coreference Chain Genre dependent identification module (CoRefGen)</span>
<span id='database_1993' style='display:none'>Tsinghua Chinese Treebank (TCT)</span>
<span id='database_1994' style='display:none'>GerNED</span>
<span id='database_1995' style='display:none'>Propbank-Br</span>
<span id='database_1996' style='display:none'>LX-Service</span>
<span id='database_1997' style='display:none'>Joshua</span>
<span id='database_1998' style='display:none'>ukWaC corpus</span>
<span id='database_1999' style='display:none'>NLTK for Python</span>
<span id='database_2000' style='display:none'>SRI Language Modeling Toolkit (SRILM)</span>
<span id='database_2001' style='display:none'>Dicionario General de la Llengua Española</span>
<span id='database_2002' style='display:none'>Czech WordNet 1.9 PDT</span>
<span id='database_2003' style='display:none'>Egyptian Arabic Treebank</span>
<span id='database_2004' style='display:none'>Guidelines for the Syntactic Annotation of Latin Treebanks</span>
<span id='database_2005' style='display:none'>Topic Detection and Tracking (TDT Phase 3)</span>
<span id='database_2006' style='display:none'>Gold Standard for Sentence Clustering</span>
<span id='database_2007' style='display:none'>SSP-Net Vocalisation Corpus</span>
<span id='database_2008' style='display:none'>GH-MAP</span>
<span id='database_2009' style='display:none'>context sensitive variant dictionary</span>
<span id='database_2010' style='display:none'>Spoken Corpus of Standard European Portuguese</span>
<span id='database_2011' style='display:none'>EAC Translation Memory</span>
<span id='database_2012' style='display:none'>DIPS</span>
<span id='database_2013' style='display:none'>CALL-SLT English child learner corpus</span>
<span id='database_2014' style='display:none'>Araucaria</span>
<span id='database_2015' style='display:none'>The WaveSurfer Automatic Speech Recognition Plugin</span>
<span id='database_2016' style='display:none'>Java Syntaxico-semantic French Analyser (J-Safran)</span>
<span id='database_2017' style='display:none'>Czech Bushbank Blog</span>
<span id='database_2018' style='display:none'>French preprocessing OpenNLP Models</span>
<span id='database_2019' style='display:none'>A Cross-Lingual Dictionary for English Wikipedia Concepts</span>
<span id='database_2020' style='display:none'>Italian Wikipedia</span>
<span id='database_2021' style='display:none'>Swedish Scientific Medical Corpus</span>
<span id='database_2022' style='display:none'>DIINAR.1</span>
<span id='database_2023' style='display:none'>Dicta-Sign : API for plugins</span>
<span id='database_2024' style='display:none'>KRYS-I</span>
<span id='database_2025' style='display:none'>Radziszewski Acedanski Tagging Evaluation Method</span>
<span id='database_2026' style='display:none'>Linguistica</span>
<span id='database_2027' style='display:none'>REBECA</span>
<span id='database_2028' style='display:none'>Porter stemmer</span>
<span id='database_2029' style='display:none'>Common Crawl Language Model</span>
<span id='database_2030' style='display:none'>ERDO</span>
<span id='database_2031' style='display:none'>Finite State Tokenizer</span>
<span id='database_2032' style='display:none'>Microsoft Research Paraphrase Corpus (MSRP)</span>
<span id='database_2033' style='display:none'>Catalan Webcorpus</span>
<span id='database_2034' style='display:none'>Languages in Danger Website</span>
<span id='database_2035' style='display:none'>Lakota Finite State Morphological Analyzer</span>
<span id='database_2036' style='display:none'>TREAT</span>
<span id='database_2037' style='display:none'>CoNLL-X and CoNLL 2007 datasets</span>
<span id='database_2038' style='display:none'>PLLM</span>
<span id='database_2039' style='display:none'>Stuttgart MORPhology (SMOR)</span>
<span id='database_2040' style='display:none'>PennTree Bank</span>
<span id='database_2041' style='display:none'>SYNERGY</span>
<span id='database_2042' style='display:none'>Suicide risk annotation guidelines</span>
<span id='database_2043' style='display:none'>Sock-puppet Dataset</span>
<span id='database_2044' style='display:none'>LT4eL English Learning Objects</span>
<span id='database_2045' style='display:none'>an XML Based System For Corpora Development (CLaRK)</span>
<span id='database_2046' style='display:none'>The Essex Arabic Summaries Corpus (EASC)</span>
<span id='database_2047' style='display:none'>MuLeXFoR</span>
<span id='database_2048' style='display:none'>PAROLE Lexicon</span>
<span id='database_2049' style='display:none'>FAUST</span>
<span id='database_2050' style='display:none'>spoken language samples for the South African official languages</span>
<span id='database_2051' style='display:none'>WSMT corpus annotated for sentiment</span>
<span id='database_2052' style='display:none'>Corpus of negociation between users and virtual characters</span>
<span id='database_2053' style='display:none'>Carnegie Mellon University (CMU) Sphinx</span>
<span id='database_2054' style='display:none'>Dish Names in Chinese Language Blog Reviews</span>
<span id='database_2055' style='display:none'>CLEF-IP 2011 Corpus - Title part</span>
<span id='database_2056' style='display:none'>Sequence-based aligner</span>
<span id='database_2057' style='display:none'>JAPIO patent abstracts</span>
<span id='database_2058' style='display:none'>Wordsmith tools</span>
<span id='database_2059' style='display:none'>TrendMiner_lex</span>
<span id='database_2060' style='display:none'>Information Science Institute Elicited Imitation Corpus</span>
<span id='database_2061' style='display:none'>Japanese-Czech Dictionary</span>
<span id='database_2062' style='display:none'>Maupassant: segmented and tagged text into types by XML tags</span>
<span id='database_2063' style='display:none'>Comparisons in Product Reviews</span>
<span id='database_2064' style='display:none'>Semantic Case Frames of English</span>
<span id='database_2065' style='display:none'>FrAG (French Annotation Grammar)</span>
<span id='database_2066' style='display:none'>Dictionaries of Swedish names and common words</span>
<span id='database_2067' style='display:none'>Chinese-English Translation Lexicon Version 3.0</span>
<span id='database_2068' style='display:none'>English-German word alignment gold standard</span>
<span id='database_2069' style='display:none'>Kulkarni Name Corpus</span>
<span id='database_2070' style='display:none'>Format for Linguistic Annotation (FoLia)</span>
<span id='database_2071' style='display:none'>Romnaian syllables data base</span>
<span id='database_2072' style='display:none'>OntoSearcher</span>
<span id='database_2073' style='display:none'>Short Movie Reviews for Opinion Mining and Authorship Attribution Experiments</span>
<span id='database_2074' style='display:none'>An averaged perceptron-based morphological disambiguator for Turkish text</span>
<span id='database_2075' style='display:none'>Department of Education Text Books</span>
<span id='database_2076' style='display:none'>Agriculture domain parallel corpus</span>
<span id='database_2077' style='display:none'>German LFG grammar</span>
<span id='database_2078' style='display:none'>Global Automotive Industry Corpus</span>
<span id='database_2079' style='display:none'>elexiko</span>
<span id='database_2080' style='display:none'>Mozilla localization es-eu parallel corpus</span>
<span id='database_2081' style='display:none'>open thesaurus</span>
<span id='database_2082' style='display:none'>Babel</span>
<span id='database_2083' style='display:none'>FTA Labeled ACL Anthology Abstracts</span>
<span id='database_2084' style='display:none'>Valency Lexicon of Czech Verbs (VALLEX 2.6)</span>
<span id='database_2085' style='display:none'>Named Entity Recognizer for Slovene</span>
<span id='database_2086' style='display:none'>YAMCHA</span>
<span id='database_2087' style='display:none'>EDICT</span>
<span id='database_2088' style='display:none'>MST Parser</span>
<span id='database_2089' style='display:none'>MINELex</span>
<span id='database_2090' style='display:none'>Head Pose and Eye Gaze dataset HPEG</span>
<span id='database_2091' style='display:none'>WTMF-PK</span>
<span id='database_2092' style='display:none'>Bio Corpus</span>
<span id='database_2093' style='display:none'>Reference Engine Development and Evaluation Environment</span>
<span id='database_2094' style='display:none'>Dekang Lin's Similarity Thesauruses</span>
<span id='database_2095' style='display:none'>A manual for definiteness annotations</span>
<span id='database_2096' style='display:none'>Aakalak: Multiword Expressions Extractor</span>
<span id='database_2097' style='display:none'>breakSent-multi-lf.pl</span>
<span id='database_2098' style='display:none'>GALE Phase 5 Chinese Parallel Word Alignment and Tagging Part 1</span>
<span id='database_2099' style='display:none'>Email corpus annotated with social power relations</span>
<span id='database_2100' style='display:none'>FarsNet</span>
<span id='database_2101' style='display:none'>LGLex 3.3</span>
<span id='database_2102' style='display:none'>WordNet::Similarity</span>
<span id='database_2103' style='display:none'>SentiLex-PT_02</span>
<span id='database_2104' style='display:none'>SPSS</span>
<span id='database_2105' style='display:none'>Minho Quotation Bank</span>
<span id='database_2106' style='display:none'>Arbil</span>
<span id='database_2107' style='display:none'>Reference Corpus for the Processing of Basque (EPEC)</span>
<span id='database_2108' style='display:none'>WSJ Penn Treebank</span>
<span id='database_2109' style='display:none'>TWSI 2.0: Turk Bootstrap Word Sense Inventory</span>
<span id='database_2110' style='display:none'>English Resource Grammar in ALE</span>
<span id='database_2111' style='display:none'>Serbian Wordnet (SrpWN)</span>
<span id='database_2112' style='display:none'>Question Pairs from WikiAnswers</span>
<span id='database_2113' style='display:none'>MINGLE-3</span>
<span id='database_2114' style='display:none'>Zmorge</span>
<span id='database_2115' style='display:none'>IllinoisCloudNLP</span>
<span id='database_2116' style='display:none'>CLC FCE dataset</span>
<span id='database_2117' style='display:none'>Hindi Treebank</span>
<span id='database_2118' style='display:none'>Simplified Corpus Semantically Annotated with Wh-Question Labels</span>
<span id='database_2119' style='display:none'>Ontology of Italian Linguistics</span>
<span id='database_2120' style='display:none'>Customer review data set</span>
<span id='database_2121' style='display:none'>A supersense taxonomy for adjectives</span>
<span id='database_2122' style='display:none'>SIGHAN</span>
<span id='database_2123' style='display:none'>ALICO Active Listening Corpus</span>
<span id='database_2124' style='display:none'>NYU IE Relations for Technical Documents</span>
<span id='database_2125' style='display:none'>PsyCoL Maltese Lexical Corpus (PMLC)</span>
<span id='database_2126' style='display:none'>tweet-norm_es</span>
<span id='database_2127' style='display:none'>Malaga</span>
<span id='database_2128' style='display:none'>Open American National Corpus (OANC)</span>
<span id='database_2129' style='display:none'>Annotated Corpus of Difficult-Antecedent Referring Expressions (DAREs)</span>
<span id='database_2130' style='display:none'>Disjoint Events</span>
<span id='database_2131' style='display:none'>NgramQuery language</span>
<span id='database_2132' style='display:none'>Utsunomiya University Spoken Dialogue Database for Paralinguistic Information Studies</span>
<span id='database_2133' style='display:none'>Open Directory Project Full Corpus</span>
<span id='database_2134' style='display:none'>GTM</span>
<span id='database_2135' style='display:none'>ToTaLe</span>
<span id='database_2136' style='display:none'>Beygingarlýsing íslensks nútímamáls (BÍN)</span>
<span id='database_2137' style='display:none'>English_VPN</span>
<span id='database_2138' style='display:none'>Bijankhan Corpus</span>
<span id='database_2139' style='display:none'>Blizzard Challenge 2010</span>
<span id='database_2140' style='display:none'>Meter</span>
<span id='database_2141' style='display:none'>UGC tokenizer</span>
<span id='database_2142' style='display:none'>Business News Story Corpus</span>
<span id='database_2143' style='display:none'>Arabic Tweets NER test set</span>
<span id='database_2144' style='display:none'>LetsMT!</span>
<span id='database_2145' style='display:none'>OntoWordNet</span>
<span id='database_2146' style='display:none'>PROIEL</span>
<span id='database_2147' style='display:none'>IM-NS-NNS</span>
<span id='database_2148' style='display:none'>SentiProductLexicon</span>
<span id='database_2149' style='display:none'>BohnetParser</span>
<span id='database_2150' style='display:none'>Moderated multiparty dialogues</span>
<span id='database_2151' style='display:none'>Japanese Web Corpus</span>
<span id='database_2152' style='display:none'>stand-off annotation proposed by ISO committee on Language Resources Management</span>
<span id='database_2153' style='display:none'>English Gigaword Second Edition</span>
<span id='database_2154' style='display:none'>Second International Chinese Word Segmentation Bakeoff Data</span>
<span id='database_2155' style='display:none'>SAWDUST</span>
<span id='database_2156' style='display:none'>French EMA dataset LORIA Nov 2008</span>
<span id='database_2157' style='display:none'>Aymara LFG Grammar</span>
<span id='database_2158' style='display:none'>QA corpus for answer justification</span>
<span id='database_2159' style='display:none'>MEDLINE database</span>
<span id='database_2160' style='display:none'>Automatic Annotator software</span>
<span id='database_2161' style='display:none'>A Repository of State of the Art and Competitive Baseline Summaries for DUC 2004</span>
<span id='database_2162' style='display:none'>person name ontology</span>
<span id='database_2163' style='display:none'>Emospeech MPCorpus</span>
<span id='database_2164' style='display:none'>Lymba's abbreviation dictionary</span>
<span id='database_2165' style='display:none'>wikipedia</span>
<span id='database_2166' style='display:none'>WSJ0-WSJ1</span>
<span id='database_2167' style='display:none'>Freebase Semantic Parsing Dataset</span>
<span id='database_2168' style='display:none'>POS tagged Data set</span>
<span id='database_2169' style='display:none'>Langid.py</span>
<span id='database_2170' style='display:none'>TREC-8 collection</span>
<span id='database_2171' style='display:none'>NIST MT 03-05 training and test corpora</span>
<span id='database_2172' style='display:none'>German Reference Corpus DeReKo</span>
<span id='database_2173' style='display:none'>B3DB</span>
<span id='database_2174' style='display:none'>LDC wordlist</span>
<span id='database_2175' style='display:none'>WordsmithTools</span>
<span id='database_2176' style='display:none'>BioMorphoAnalyzer</span>
<span id='database_2177' style='display:none'>drhuman</span>
<span id='database_2178' style='display:none'>U-Compare</span>
<span id='database_2179' style='display:none'>EEP search interface evaluation</span>
<span id='database_2180' style='display:none'>Dutch de/het noun classification script for TiMBL/Frog output</span>
<span id='database_2181' style='display:none'>VerbeNet</span>
<span id='database_2182' style='display:none'>Text+Berg Corpus</span>
<span id='database_2183' style='display:none'>OntoPopulate</span>
<span id='database_2184' style='display:none'>Subtitles Dataset Ground Truth</span>
<span id='database_2185' style='display:none'>UniDic-2.1.0</span>
<span id='database_2186' style='display:none'>Institute of Computing Technology Chinese Lexical Analysis System (ICTCLAS)</span>
<span id='database_2187' style='display:none'>TAC 2011 KBP Cross-lingual Training Entity Linking V1.1</span>
<span id='database_2188' style='display:none'>Semantic Types</span>
<span id='database_2189' style='display:none'>Chinese Spell Checking Dataset</span>
<span id='database_2190' style='display:none'>BAF corpus</span>
<span id='database_2191' style='display:none'>GyaanNidhi</span>
<span id='database_2192' style='display:none'>Rakuten review data</span>
<span id='database_2193' style='display:none'>SignWriting improved fast transcriber (SWift)</span>
<span id='database_2194' style='display:none'>WordNet 2.1</span>
<span id='database_2195' style='display:none'>Paraphrase Corpus</span>
<span id='database_2196' style='display:none'>Szeged Dependency Treebank</span>
<span id='database_2197' style='display:none'>Russian automotive corpus</span>
<span id='database_2198' style='display:none'>Dictionary of Iraqi Arabic (Arabic-English)</span>
<span id='database_2199' style='display:none'>Emotion Annotated data for Urdu</span>
<span id='database_2200' style='display:none'>TermExtractor</span>
<span id='database_2201' style='display:none'>IMAGACT Annotation Infrastructure</span>
<span id='database_2202' style='display:none'>GALE Y1 Q2 Release - LDC/FBIS/NVTC Parallel Text V2.0</span>
<span id='database_2203' style='display:none'>Translation errors</span>
<span id='database_2204' style='display:none'>MeSH (Medical Subject Heading)</span>
<span id='database_2205' style='display:none'>Europarl v.2</span>
<span id='database_2206' style='display:none'>Romanized Text Language Identifier</span>
<span id='database_2207' style='display:none'>IMS Open Corpus Workbench</span>
<span id='database_2208' style='display:none'>ItWac</span>
<span id='database_2209' style='display:none'>Poliqarp</span>
<span id='database_2210' style='display:none'>Spanish Webcorpus</span>
<span id='database_2211' style='display:none'>HOESI</span>
<span id='database_2212' style='display:none'>CINEMO</span>
<span id='database_2213' style='display:none'>Recipe Flow Graph Corpus</span>
<span id='database_2214' style='display:none'>Genia corpus annotation for BioNLP/NLPBA 2004</span>
<span id='database_2215' style='display:none'>NAIST Text Corpus</span>
<span id='database_2216' style='display:none'>GPU Based Phrase Extraction Tool</span>
<span id='database_2217' style='display:none'>Enquête Socio-Linguistique à Orléans (ESLO)</span>
<span id='database_2218' style='display:none'>Bidirectional tagger</span>
<span id='database_2219' style='display:none'>Cross Language Evaluation Forum</span>
<span id='database_2220' style='display:none'>DB-CoM</span>
<span id='database_2221' style='display:none'>Clear Toolkit (ClearTK)</span>
<span id='database_2222' style='display:none'>English Lexical Substitution Dataset</span>
<span id='database_2223' style='display:none'>named entity taggers</span>
<span id='database_2224' style='display:none'>Lexkit</span>
<span id='database_2225' style='display:none'>International Corpus of English</span>
<span id='database_2226' style='display:none'>Aleda</span>
<span id='database_2227' style='display:none'>20 newsgroups</span>
<span id='database_2228' style='display:none'>ECI Multilingual Text Corpus</span>
<span id='database_2229' style='display:none'>Lucene</span>
<span id='database_2230' style='display:none'>Granular Time Ontology for Temporal Underspecification</span>
<span id='database_2231' style='display:none'>Anotador de Relações Semânticas - ARS</span>
<span id='database_2232' style='display:none'>Stanford POS tagger</span>
<span id='database_2233' style='display:none'>Dan Bikel Multilingual Statistical Parsing Engine</span>
<span id='database_2234' style='display:none'>Machine Reading P1 NFL Scoring Training Data (LDC2009E112)</span>
<span id='database_2235' style='display:none'>language generation evaluation toolkit (lg-eval)</span>
<span id='database_2236' style='display:none'>Corpus de Referencia del Español Actual (CREA)</span>
<span id='database_2237' style='display:none'>The EASR Corpus of French Elderly Speech</span>
<span id='database_2238' style='display:none'>Semantic Map @ CEA LIST</span>
<span id='database_2239' style='display:none'>MaltParsr</span>
<span id='database_2240' style='display:none'>AOL query log</span>
<span id='database_2241' style='display:none'>test</span>
<span id='database_2242' style='display:none'>Signing footage recorded from TV with simultaneously broadcasted subtitles</span>
<span id='database_2243' style='display:none'>IT-PANACEA SCF test suite</span>
<span id='database_2244' style='display:none'>TRmorph</span>
<span id='database_2245' style='display:none'>Chinese Collocation Dictionary of Content Words</span>
<span id='database_2246' style='display:none'>Hausa Internet Corpus</span>
<span id='database_2247' style='display:none'>French and English  Contexonym Databases</span>
<span id='database_2248' style='display:none'>Syntax-oriented corpus of Portuguese Dialects - CORDIAL-SIN</span>
<span id='database_2249' style='display:none'>Arabic Wikipedia</span>
<span id='database_2250' style='display:none'>Yahoo! Answers QA Pairs under Healthcare Domain</span>
<span id='database_2251' style='display:none'>Punjabi News Articles on sports domain</span>
<span id='database_2252' style='display:none'>Antconc</span>
<span id='database_2253' style='display:none'>Brown Coherence Toolkit</span>
<span id='database_2254' style='display:none'>morph</span>
<span id='database_2255' style='display:none'>JEMO</span>
<span id='database_2256' style='display:none'>Dysarthric Speech Database</span>
<span id='database_2257' style='display:none'>VariKN Language Modeling toolkit</span>
<span id='database_2258' style='display:none'>Verb Lexicon and Event Durations</span>
<span id='database_2259' style='display:none'>TermoStat Web 3.0</span>
<span id='database_2260' style='display:none'>Corpas na Gaeilge 1882-1926</span>
<span id='database_2261' style='display:none'>Terminesp LD</span>
<span id='database_2262' style='display:none'>Hebrew and Arabic Morphologically Segmented</span>
<span id='database_2263' style='display:none'>SemEval 2007 Lexical Substitution Task Dataset</span>
<span id='database_2264' style='display:none'>Noor Book Corpus</span>
<span id='database_2265' style='display:none'>i2b2 2010 corpus</span>
<span id='database_2266' style='display:none'>Helsinki First Encounters Corpus</span>
<span id='database_2267' style='display:none'>GUM-Space: Evaluation Data and Annotation Instructions</span>
<span id='database_2268' style='display:none'>iula2standoff</span>
<span id='database_2269' style='display:none'>Sentiment Emotion Blog Corpus</span>
<span id='database_2270' style='display:none'>CAVaT - Corpus Analysis and Validation for TimeML</span>
<span id='database_2271' style='display:none'>Hindi Dependency Treebank</span>
<span id='database_2272' style='display:none'>Edit Plus</span>
<span id='database_2273' style='display:none'>Qatari Arabic Corpus</span>
<span id='database_2274' style='display:none'>Dutch Speech-to-text Transcriber</span>
<span id='database_2275' style='display:none'>Wikilinks</span>
<span id='database_2276' style='display:none'>MW_Extractor</span>
<span id='database_2277' style='display:none'>SERA</span>
<span id='database_2278' style='display:none'>SEMILAR</span>
<span id='database_2279' style='display:none'>IMAIL-SSIA-2009</span>
<span id='database_2280' style='display:none'>CLARIN Component Metadata</span>
<span id='database_2281' style='display:none'>Corpus of Arabic Functional Morphology</span>
<span id='database_2282' style='display:none'>Punjabi Resource Grammar</span>
<span id='database_2283' style='display:none'>Aurora Project Database - Revised Aurora Noisy TI digits database - (Version 2.0)</span>
<span id='database_2284' style='display:none'>NTCIR's Japanese patent document corpus</span>
<span id='database_2285' style='display:none'>DECA Species Corpus</span>
<span id='database_2286' style='display:none'>Markus Dickinson</span>
<span id='database_2287' style='display:none'>EDR Electronic Dictionary</span>
<span id='database_2288' style='display:none'>Cambridge Cookie-theft Corpus</span>
<span id='database_2289' style='display:none'>TAC 2011 KBP Cross-lingual Evaluation Entity Linking Annotation V1.1</span>
<span id='database_2290' style='display:none'>Kyoto University Case Frames</span>
<span id='database_2291' style='display:none'>LEMLAT</span>
<span id='database_2292' style='display:none'>Speech In Minimal Invasive Surgery (SIMIS)</span>
<span id='database_2293' style='display:none'>EmotiBlog corpus</span>
<span id='database_2294' style='display:none'>Nouveau Corpus d'Amsterdam</span>
<span id='database_2295' style='display:none'>Document Understanding Conference (DUC) 2005 Dataset</span>
<span id='database_2296' style='display:none'>Prague Czech English Dependency Treebank</span>
<span id='database_2297' style='display:none'>Multi-media Multi-lingual concept, relation and event annotated corpus</span>
<span id='database_2298' style='display:none'>NERD-ML</span>
<span id='database_2299' style='display:none'>TaggerTools</span>
<span id='database_2300' style='display:none'>Giellatekno parser</span>
<span id='database_2301' style='display:none'>Component MetaData Infrastructure (CMDI) Information Page</span>
<span id='database_2302' style='display:none'>extension of Pang and Lee: polarity 2.0</span>
<span id='database_2303' style='display:none'>SemEval 2010 Task 5</span>
<span id='database_2304' style='display:none'>Yet Another Term Extractor (YATE)</span>
<span id='database_2305' style='display:none'>Fresa</span>
<span id='database_2306' style='display:none'>LG</span>
<span id='database_2307' style='display:none'>CompanyM</span>
<span id='database_2308' style='display:none'>Yamabuki</span>
<span id='database_2309' style='display:none'>TREC QA questions</span>
<span id='database_2310' style='display:none'>Microblog/Twitter Summarization Data Set</span>
<span id='database_2311' style='display:none'>alpino</span>
<span id='database_2312' style='display:none'>Classification of News Articles on Contentious Issues</span>
<span id='database_2313' style='display:none'>NEOROM</span>
<span id='database_2314' style='display:none'>TICCL</span>
<span id='database_2315' style='display:none'>ir package</span>
<span id='database_2316' style='display:none'>Peykareh</span>
<span id='database_2317' style='display:none'>English Chinese Discussion Forum Translation Treebank</span>
<span id='database_2318' style='display:none'>DutchParl</span>
<span id='database_2319' style='display:none'>WOLF+WoNeF</span>
<span id='database_2320' style='display:none'>ACL Anthology Corpus</span>
<span id='database_2321' style='display:none'>Romanian and English word form lexicons</span>
<span id='database_2322' style='display:none'>Amazon Reviews</span>
<span id='database_2323' style='display:none'>Slovenian BNSI Broadcast News</span>
<span id='database_2324' style='display:none'>Bijankhan</span>
<span id='database_2325' style='display:none'>SANA</span>
<span id='database_2326' style='display:none'>Clause Boundary Annotation Program</span>
<span id='database_2327' style='display:none'>MAE Annotation tool</span>
<span id='database_2328' style='display:none'>Trained Phrase based SMT models</span>
<span id='database_2329' style='display:none'>NICT_JC_SP</span>
<span id='database_2330' style='display:none'>English–Croatian tourism parallel corpus</span>
<span id='database_2331' style='display:none'>JRC Quotes Collection for Sentiment Analysis</span>
<span id='database_2332' style='display:none'>TSUBAKI</span>
<span id='database_2333' style='display:none'>SILADON_CZ</span>
<span id='database_2334' style='display:none'>American Local News Corpus</span>
<span id='database_2335' style='display:none'>Szeged Corpus 2.5</span>
<span id='database_2336' style='display:none'>LDC2009E73 Standard Arabic Morphological Analyzer (SAMA) Version 3.1</span>
<span id='database_2337' style='display:none'>The OpenDirectory</span>
<span id='database_2338' style='display:none'>Yahoo! web searcher</span>
<span id='database_2339' style='display:none'>Priberam Compressive Summarization Corpus</span>
<span id='database_2340' style='display:none'>You tube</span>
<span id='database_2341' style='display:none'>Text-2-Knowledge (T2K)</span>
<span id='database_2342' style='display:none'>Text Encoding Iinitiative (TEI)</span>
<span id='database_2343' style='display:none'>Verb similarity evaluation datasets</span>
<span id='database_2344' style='display:none'>Open source morphology for Finnish (omorfi)</span>
<span id='database_2345' style='display:none'>Neue Zürcher Zeitung</span>
<span id='database_2346' style='display:none'>Contextes</span>
<span id='database_2347' style='display:none'>Very Large Pronunciation Vocabulary for Russian</span>
<span id='database_2348' style='display:none'>Estonian Emotional Speech Corpus</span>
<span id='database_2349' style='display:none'>Indian Language Part-of-Speech Tagset: Hindi</span>
<span id='database_2350' style='display:none'>Giza++ Tool</span>
<span id='database_2351' style='display:none'>Basic Travel Expression Corpus (BTEC)</span>
<span id='database_2352' style='display:none'>GernEdiT</span>
<span id='database_2353' style='display:none'>MedT_NER</span>
<span id='database_2354' style='display:none'>SLAM</span>
<span id='database_2355' style='display:none'>JST scientific paper abstracts</span>
<span id='database_2356' style='display:none'>The REX corpora</span>
<span id='database_2357' style='display:none'>WebLicht</span>
<span id='database_2358' style='display:none'>JWNLSimple</span>
<span id='database_2359' style='display:none'>SFU Review corpus</span>
<span id='database_2360' style='display:none'>30 noun pairs from Rubenstein and Goodenough, and by replacing them with their definitions from the Collins Cobuild dict</span>
<span id='database_2361' style='display:none'>PAN</span>
<span id='database_2362' style='display:none'>Chinese acedemic papers</span>
<span id='database_2363' style='display:none'>Symantec Translation Memory</span>
<span id='database_2364' style='display:none'>Polish Clara corpus</span>
<span id='database_2365' style='display:none'>WordNet mapping to Kyoto Ontology</span>
<span id='database_2366' style='display:none'>Hyponymy extraction tool</span>
<span id='database_2367' style='display:none'>NOUN COMPOUNDS IN CZECH, ENGLISH AND ZULU</span>
<span id='database_2368' style='display:none'>Microsoft Researc Lab India's Hindi-ENglish Transliterated Song Lyric Data</span>
<span id='database_2369' style='display:none'>Tunisian lexicon for deverbal nouns</span>
<span id='database_2370' style='display:none'>Lexeed</span>
<span id='database_2371' style='display:none'>EDR bilingual dictionary</span>
<span id='database_2372' style='display:none'>Gold Standard for English human nouns</span>
<span id='database_2373' style='display:none'>ELRA -M0033</span>
<span id='database_2374' style='display:none'>Basic dictionary of FinSL example text corpus (Suvi)</span>
<span id='database_2375' style='display:none'>Austrian Academy Corpus</span>
<span id='database_2376' style='display:none'>RULE (Russian Polarity Lexicon)</span>
<span id='database_2377' style='display:none'>am_tools</span>
<span id='database_2378' style='display:none'>2011 ACL WMT</span>
<span id='database_2379' style='display:none'>Cross-Language Entity Linking Test Collection</span>
<span id='database_2380' style='display:none'>FATE corpus</span>
<span id='database_2381' style='display:none'>Chinese Semantic Dictionary</span>
<span id='database_2382' style='display:none'>BioASQ Tool Suite</span>
<span id='database_2383' style='display:none'>NLTK</span>
<span id='database_2384' style='display:none'>Java Wikipedia Library (JWPL)</span>
<span id='database_2385' style='display:none'>CORIS/CODIS</span>
<span id='database_2386' style='display:none'>Biomedical Risk Factors Corpus</span>
<span id='database_2387' style='display:none'>Kemo/Demo</span>
<span id='database_2388' style='display:none'>Multi-Domain Sentiment Dataset 2.0</span>
<span id='database_2389' style='display:none'>CID: Other-Repetitions Annotation</span>
<span id='database_2390' style='display:none'>Katakana-English Scientific Terms Lexicon</span>
<span id='database_2391' style='display:none'>Document Understanding Conference Past Data for Text Summarization</span>
<span id='database_2392' style='display:none'>Brandeis Annotation Tool</span>
<span id='database_2393' style='display:none'>MASS - Modal Annotation in Spontaneous Speech</span>
<span id='database_2394' style='display:none'>DEGELS1</span>
<span id='database_2395' style='display:none'>Learning Based Java</span>
<span id='database_2396' style='display:none'>Tagged Quranic Corpora</span>
<span id='database_2397' style='display:none'>Sentiment Annotated Sentences</span>
<span id='database_2398' style='display:none'>Corpus de Investigación en Español de México del Posgrado de Ingeniería Eléctrica y Servicio Social (CIEMPIESS)</span>
<span id='database_2399' style='display:none'>HunOr</span>
<span id='database_2400' style='display:none'>Concraft</span>
<span id='database_2401' style='display:none'>Unsupervised, Language Independent Sentence Aligner</span>
<span id='database_2402' style='display:none'>SentiStrength</span>
<span id='database_2403' style='display:none'>Chinese to Uyghur Bilingual Dictionary</span>
<span id='database_2404' style='display:none'>LDOS-PerAff-1</span>
<span id='database_2405' style='display:none'>CMC ICD coding corpus</span>
<span id='database_2406' style='display:none'>Penn Parsed Corpus of Modern British English</span>
<span id='database_2407' style='display:none'>ACE data 2007</span>
<span id='database_2408' style='display:none'>Moby</span>
<span id='database_2409' style='display:none'>English-Spanish Large Statistical Dictionary of Inflectional Forms</span>
<span id='database_2410' style='display:none'>CzEng</span>
<span id='database_2411' style='display:none'>Bonsai Dependency Parser v3.2</span>
<span id='database_2412' style='display:none'>Daniel Pipes</span>
<span id='database_2413' style='display:none'>QuÃ¦ro QA corpus</span>
<span id='database_2414' style='display:none'>Metadata editor</span>
<span id='database_2415' style='display:none'>CoCo</span>
<span id='database_2416' style='display:none'>BitPar</span>
<span id='database_2417' style='display:none'>Swedish fuzzy wordnet</span>
<span id='database_2418' style='display:none'>SSPNet Mobile Corpus</span>
<span id='database_2419' style='display:none'>JOS</span>
<span id='database_2420' style='display:none'>AbarHitz</span>
<span id='database_2421' style='display:none'>Beginning and end time-stamps for all CV and VC stimuli from the LDC-2005S22 corpus.</span>
<span id='database_2422' style='display:none'>MIT Address Corpus</span>
<span id='database_2423' style='display:none'>French Treebank</span>
<span id='database_2424' style='display:none'>Ellogon language engineering platform</span>
<span id='database_2425' style='display:none'>HiEve</span>
<span id='database_2426' style='display:none'>natural language toolkit</span>
<span id='database_2427' style='display:none'>SloWNet</span>
<span id='database_2428' style='display:none'>PolArt</span>
<span id='database_2429' style='display:none'>TREC eval</span>
<span id='database_2430' style='display:none'>TREC Tweets2011 Corpus</span>
<span id='database_2431' style='display:none'>MultiLingWebCorpus</span>
<span id='database_2432' style='display:none'>Fred</span>
<span id='database_2433' style='display:none'>TIGER Corpus 1.0</span>
<span id='database_2434' style='display:none'>Stockholm MULtilingual TReebank (SMULTRON)</span>
<span id='database_2435' style='display:none'>Verb Pattern Sample, 30 English verbs (VPS-30-En)</span>
<span id='database_2436' style='display:none'>Khoja Stemmer</span>
<span id='database_2437' style='display:none'>P</span>
<span id='database_2438' style='display:none'>Markov thebeast</span>
<span id='database_2439' style='display:none'>Java library for detecting Multi-Word Expressions (jMWE)</span>
<span id='database_2440' style='display:none'>DicoLSF</span>
<span id='database_2441' style='display:none'>SMR-Cmp</span>
<span id='database_2442' style='display:none'>WordNet-Affect</span>
<span id='database_2443' style='display:none'>Multilingual Medical Text and Speech Corpus</span>
<span id='database_2444' style='display:none'>Al-Kitaab textbook texts</span>
<span id='database_2445' style='display:none'>ConceptNet</span>
<span id='database_2446' style='display:none'>C-PhonoGenre</span>
<span id='database_2447' style='display:none'>Collective Action Framing Corpus</span>
<span id='database_2448' style='display:none'>Resource for Evaluation of Cross-lingual Semantic Annotation (RECSA)</span>
<span id='database_2449' style='display:none'>WES base</span>
<span id='database_2450' style='display:none'>IDIAP corpus of political debates</span>
<span id='database_2451' style='display:none'>PlayMancer Database</span>
<span id='database_2452' style='display:none'>Bulgarian Dictionary</span>
<span id='database_2453' style='display:none'>GeoQueries250</span>
<span id='database_2454' style='display:none'>TDT corpus</span>
<span id='database_2455' style='display:none'>SAMPLE</span>
<span id='database_2456' style='display:none'>WordNet Libre du Français (WOLF)</span>
<span id='database_2457' style='display:none'>VICLO</span>
<span id='database_2458' style='display:none'>SICK</span>
<span id='database_2459' style='display:none'>CoRoLa</span>
<span id='database_2460' style='display:none'>SVMlight</span>
<span id='database_2461' style='display:none'>Khawas V3.0</span>
<span id='database_2462' style='display:none'>TTS AD-CAT</span>
<span id='database_2463' style='display:none'>Morfeusz SGJP</span>
<span id='database_2464' style='display:none'>Wiki-Biographic-Corpus</span>
<span id='database_2465' style='display:none'>Morfeusz</span>
<span id='database_2466' style='display:none'>Restaurant-Review-Snyder and Barzilay (2007)</span>
<span id='database_2467' style='display:none'>Bilingwis</span>
<span id='database_2468' style='display:none'>Dadegan Tools</span>
<span id='database_2469' style='display:none'>PPI-DDI Silverstandard Corpus</span>
<span id='database_2470' style='display:none'>PAPEL</span>
<span id='database_2471' style='display:none'>MPQA subjectivity lexicon</span>
<span id='database_2472' style='display:none'>Portuguese Wikipedia Tagger</span>
<span id='database_2473' style='display:none'>YAGO</span>
<span id='database_2474' style='display:none'>RevLem</span>
<span id='database_2475' style='display:none'>LEX corpus</span>
<span id='database_2476' style='display:none'>TaLAPi</span>
<span id='database_2477' style='display:none'>Addicter</span>
<span id='database_2478' style='display:none'>Bi-sentences,lexicon LDC2005T34,Name Entity LDC2005T34</span>
<span id='database_2479' style='display:none'>Connexor Machinese Syntax parser</span>
<span id='database_2480' style='display:none'>Named Entity Evolution Dataset</span>
<span id='database_2481' style='display:none'>PiTagger</span>
<span id='database_2482' style='display:none'>Maeda's articulatory synthesizer</span>
<span id='database_2483' style='display:none'>Dicionário Aberto</span>
<span id='database_2484' style='display:none'>Japanese to English MT rules for Multiword Functional Expressions</span>
<span id='database_2485' style='display:none'>YaMTG</span>
<span id='database_2486' style='display:none'>Software for Clustering High-Dimensional Datasets (CLUTO)</span>
<span id='database_2487' style='display:none'>Arabic Treebank (ATB)</span>
<span id='database_2488' style='display:none'>MARF and its Applications</span>
<span id='database_2489' style='display:none'>PhysicalFeatures</span>
<span id='database_2490' style='display:none'>English Incremental Right-Corner Grammar for HHMM</span>
<span id='database_2491' style='display:none'>LinkÃ¶ping English-Swedish Parallel Treebank (LinES)</span>
<span id='database_2492' style='display:none'>Nuance ASR/NLU Grammar</span>
<span id='database_2493' style='display:none'>French corpus of event nominals</span>
<span id='database_2494' style='display:none'>Evalita2011 FA corpus</span>
<span id='database_2495' style='display:none'>ArabOrth lexicon</span>
<span id='database_2496' style='display:none'>Augmented Reality Authoring Tool with Online Information Integration Platform</span>
<span id='database_2497' style='display:none'>TIGR Evaluation Methodolgy / Guide</span>
<span id='database_2498' style='display:none'>TDT5 Brown Word Clusters</span>
<span id='database_2499' style='display:none'>YUWEI Corpus</span>
<span id='database_2500' style='display:none'>the Callhome Mandarin Chinese Corpus</span>
<span id='database_2501' style='display:none'>SoNaR</span>
<span id='database_2502' style='display:none'>Base de Français Médiéval</span>
<span id='database_2503' style='display:none'>target dataset</span>
<span id='database_2504' style='display:none'>FR-TimeBank</span>
<span id='database_2505' style='display:none'>ScopeEval</span>
<span id='database_2506' style='display:none'>Evaluation Data for Hyponymy Relation Mining</span>
<span id='database_2507' style='display:none'>OpenThesaurus</span>
<span id='database_2508' style='display:none'>Domain-Specific Gold Standard Translation Set</span>
<span id='database_2509' style='display:none'>Polarity Reversing Construction Lexicon</span>
<span id='database_2510' style='display:none'>Spanish SpeeCon database</span>
<span id='database_2511' style='display:none'>LEXUS</span>
<span id='database_2512' style='display:none'>DUC2002</span>
<span id='database_2513' style='display:none'>Light Sliding-Window PoS tagger</span>
<span id='database_2514' style='display:none'>XLIFF</span>
<span id='database_2515' style='display:none'>German (P)LTAG lexicon</span>
<span id='database_2516' style='display:none'>Czech Webcorpus</span>
<span id='database_2517' style='display:none'>LT4el ontology on computing</span>
<span id='database_2518' style='display:none'>Guidelines for word alignment evaluation and manual alignment</span>
<span id='database_2519' style='display:none'>T2T3 TIMEX3 corpora</span>
<span id='database_2520' style='display:none'>SegEval</span>
<span id='database_2521' style='display:none'>Urdu Transliteration Tools</span>
<span id='database_2522' style='display:none'>Humor</span>
<span id='database_2523' style='display:none'>German Healthiness Food Corpus</span>
<span id='database_2524' style='display:none'>VerbaLex</span>
<span id='database_2525' style='display:none'>Paradigm based Oriya Morphological Analyzer</span>
<span id='database_2526' style='display:none'>Aalto WebCon FI</span>
<span id='database_2527' style='display:none'>PAN Plagiarism Corpus PAN-PC-09</span>
<span id='database_2528' style='display:none'>Name Matching Evaluation Framework</span>
<span id='database_2529' style='display:none'>MLT-prealigner</span>
<span id='database_2530' style='display:none'>Gister</span>
<span id='database_2531' style='display:none'>El-WOZ</span>
<span id='database_2532' style='display:none'>PORT-MEDIA Domain</span>
<span id='database_2533' style='display:none'>Corpus NGT</span>
<span id='database_2534' style='display:none'>Diabase</span>
<span id='database_2535' style='display:none'>ADN-Classifier</span>
<span id='database_2536' style='display:none'>Stuttgart Finite State Transducer Tools</span>
<span id='database_2537' style='display:none'>TREC09_Chat</span>
<span id='database_2538' style='display:none'>Machinese Syntax</span>
<span id='database_2539' style='display:none'>HESITA</span>
<span id='database_2540' style='display:none'>Konan-JIEM Learner Corpus</span>
<span id='database_2541' style='display:none'>Tiger treebank</span>
<span id='database_2542' style='display:none'>RWTH-BOSTON-104</span>
<span id='database_2543' style='display:none'>Discourse ontology</span>
<span id='database_2544' style='display:none'>GENIA Event</span>
<span id='database_2545' style='display:none'>Text-To-Speech Synthesis System for Punjabi Language</span>
<span id='database_2546' style='display:none'>Paragraph alignment list (LINA-PAL-1.0)</span>
<span id='database_2547' style='display:none'>AhoTransf</span>
<span id='database_2548' style='display:none'>CoNLL 2008 Shared Task Data</span>
<span id='database_2549' style='display:none'>Communicator</span>
<span id='database_2550' style='display:none'>LS-COLIN</span>
<span id='database_2551' style='display:none'>MaxParser</span>
<span id='database_2552' style='display:none'>Gold Standard for Evaluating Arabic Morphological Analyzers</span>
<span id='database_2553' style='display:none'>Statistical Parser for Latvian (MaltParser module)</span>
<span id='database_2554' style='display:none'>BinQE</span>
<span id='database_2555' style='display:none'>Berkeley parser</span>
<span id='database_2556' style='display:none'>myres</span>
<span id='database_2557' style='display:none'>Wanfang Data Chinese-English Science and Technology Bilin-gual Dictionary</span>
<span id='database_2558' style='display:none'>Internal structure of Chinese synthetic words</span>
<span id='database_2559' style='display:none'>NIST Open Machine Translation (OpenMT) Evaluation</span>
<span id='database_2560' style='display:none'>LFG Grammar of Polish</span>
<span id='database_2561' style='display:none'>JStylo-Anonymouth</span>
<span id='database_2562' style='display:none'>NEU-Restaurant-Review</span>
<span id='database_2563' style='display:none'>bible</span>
<span id='database_2564' style='display:none'>Nenek</span>
<span id='database_2565' style='display:none'>Corpus of evolution of designation of events in French</span>
<span id='database_2566' style='display:none'>Portuguese Web Corpus</span>
<span id='database_2567' style='display:none'>Squoia Spellcheck</span>
<span id='database_2568' style='display:none'>UMLF</span>
<span id='database_2569' style='display:none'>Frankfurter Allgemeine Zeitung, FAZ</span>
<span id='database_2570' style='display:none'>Hindi Treebank (HTB) released as a part of the Coling Shared Task 2012</span>
<span id='database_2571' style='display:none'>Switchboard corpus annotated with dialogue acts</span>
<span id='database_2572' style='display:none'>SelectPOS</span>
<span id='database_2573' style='display:none'>ZT Corpusa</span>
<span id='database_2574' style='display:none'>Ontoverbetymos</span>
<span id='database_2575' style='display:none'>Danish Dependency Treebank</span>
<span id='database_2576' style='display:none'>Basque Postedition corpus</span>
<span id='database_2577' style='display:none'>Italian MWE database</span>
<span id='database_2578' style='display:none'>Kyoto Termdatabase</span>
<span id='database_2579' style='display:none'>QuÃ¦ro named entity corpora</span>
<span id='database_2580' style='display:none'>CommentExtract 1.0</span>
<span id='database_2581' style='display:none'>EULIA: Tool for Morphological Annotation</span>
<span id='database_2582' style='display:none'>Multilingual Question-Answer Pair Corpus</span>
<span id='database_2583' style='display:none'>Subtitles Dataset</span>
<span id='database_2584' style='display:none'>Hitchcock</span>
<span id='database_2585' style='display:none'>Uyghur parsing corpus</span>
<span id='database_2586' style='display:none'>ESTER 2005 database development set</span>
<span id='database_2587' style='display:none'>Romanized Moroccan Arabic</span>
<span id='database_2588' style='display:none'>Elezkari</span>
<span id='database_2589' style='display:none'>GENIA Corpus</span>
<span id='database_2590' style='display:none'>Interactive Communication Situations (ISU) of the Multimodal Communication Research Group (MUSU)</span>
<span id='database_2591' style='display:none'>Opinion Annotation Tool (OAT)</span>
<span id='database_2592' style='display:none'>RiTa.WordNet a WordNet library for Java/Processing</span>
<span id='database_2593' style='display:none'>Japanese-Chinese Parallel Sentence Pairs extracted from Patent Families</span>
<span id='database_2594' style='display:none'>DCTFinder</span>
<span id='database_2595' style='display:none'>Corpus brut amazighe</span>
<span id='database_2596' style='display:none'>SzegedParallelFX</span>
<span id='database_2597' style='display:none'>Basque-Chinese comparable corpora</span>
<span id='database_2598' style='display:none'>Approaches to Automatic Quality Estimation of Manual Translations in Crowdsourcing Parallel Corpora Development</span>
<span id='database_2599' style='display:none'>Non-projective dependency parsing using spanning tree algorithms</span>
<span id='database_2600' style='display:none'>POS tagger models for EU languages</span>
<span id='database_2601' style='display:none'>C-ORAL-BRASIL I</span>
<span id='database_2602' style='display:none'>Kyoto Text Analysis Toolkit (KyTea)</span>
<span id='database_2603' style='display:none'>Predicate Matrix</span>
<span id='database_2604' style='display:none'>Eurogene Multilingual Genetic Ontology</span>
<span id='database_2605' style='display:none'>ANALEC</span>
<span id='database_2606' style='display:none'>CMU ARCTIC database</span>
<span id='database_2607' style='display:none'>MIM-GOLD</span>
<span id='database_2608' style='display:none'>Courses on the writing of safe and safely translatable alert messages and protocols</span>
<span id='database_2609' style='display:none'>Epinions Annotated Reviews Dataset</span>
<span id='database_2610' style='display:none'>SRL annotated data for Urdu</span>
<span id='database_2611' style='display:none'>EmotiWord</span>
<span id='database_2612' style='display:none'>Chinese Proposition Bank</span>
<span id='database_2613' style='display:none'>NTU Chinese PText</span>
<span id='database_2614' style='display:none'>BoB dialogue logs</span>
<span id='database_2615' style='display:none'>The Prague Dependency Treebank 2.0</span>
<span id='database_2616' style='display:none'>The Nijmegen Corpus of Casual Czech</span>
<span id='database_2617' style='display:none'>Ancora-3LB-POS</span>
<span id='database_2618' style='display:none'>AVE evaluation collections</span>
<span id='database_2619' style='display:none'>a Library for Support Vector Machines (LIBSVM)</span>
<span id='database_2620' style='display:none'>Art History Corpus</span>
<span id='database_2621' style='display:none'>Stanford Dependency Parser</span>
<span id='database_2622' style='display:none'>RWTH-Fingerspelling</span>
<span id='database_2623' style='display:none'>Number Sense Disambiguation annotations of the Enron Corpus</span>
<span id='database_2624' style='display:none'>ChineseBlogsWithTags</span>
<span id='database_2625' style='display:none'>Hungarian-Slovenian parallel corpus</span>
<span id='database_2626' style='display:none'>ModeLex Corpus</span>
<span id='database_2627' style='display:none'>Hansard, HLT evaluation data</span>
<span id='database_2628' style='display:none'>Text Encoding Initiative (TEI)</span>
<span id='database_2629' style='display:none'>Bohnet Dependency Parser</span>
<span id='database_2630' style='display:none'>Parallel corpora</span>
<span id='database_2631' style='display:none'>Mannheim Corpus of Historical Newspapers and Magazines</span>
<span id='database_2632' style='display:none'>Arabic Nonnative Speaker Pronunciation Error Model</span>
<span id='database_2633' style='display:none'>Speech Recordings for Unit Selection Corpus</span>
<span id='database_2634' style='display:none'>Argo (A Web-based Text Mining Workbench)</span>
<span id='database_2635' style='display:none'>Julius</span>
<span id='database_2636' style='display:none'>Multi-Domain Sentiment Dataset (version 2.0)</span>
<span id='database_2637' style='display:none'>Kazakh Text Corpus</span>
<span id='database_2638' style='display:none'>Il Narratore audiolibri</span>
<span id='database_2639' style='display:none'>Apertium Bilingual Dictionary from Spanish-Catalan language pair</span>
<span id='database_2640' style='display:none'>Prague Czech English Dependency Treebank 2.0</span>
<span id='database_2641' style='display:none'>JWPL-Java Wikipedia Library</span>
<span id='database_2642' style='display:none'>Morphosyntactically annotated Greek corpus</span>
<span id='database_2643' style='display:none'>Portal Língua Portuguesa</span>
<span id='database_2644' style='display:none'>Tweets Dataset For Sentiment Analysis</span>
<span id='database_2645' style='display:none'>Julie Hochgesang</span>
<span id='database_2646' style='display:none'>Prague Discourse Treebank 1.0</span>
<span id='database_2647' style='display:none'>Euskalterm</span>
<span id='database_2648' style='display:none'>NUS SMS Corpus</span>
<span id='database_2649' style='display:none'>Vera-am-Mittag</span>
<span id='database_2650' style='display:none'>Stanford Named Entity Recognizer (NER)</span>
<span id='database_2651' style='display:none'>WorNet 3.0</span>
<span id='database_2652' style='display:none'>LS-Colin</span>
<span id='database_2653' style='display:none'>CLTC corpus</span>
<span id='database_2654' style='display:none'>GBDLex</span>
<span id='database_2655' style='display:none'>FZ NER Tool</span>
<span id='database_2656' style='display:none'>Slovene Term Extractor</span>
<span id='database_2657' style='display:none'>SpeedRead Pipeline</span>
<span id='database_2658' style='display:none'>Unitex-PB</span>
<span id='database_2659' style='display:none'>CopyCat Corpus</span>
<span id='database_2660' style='display:none'>eXtensible MetaGrammar</span>
<span id='database_2661' style='display:none'>Heloise</span>
<span id='database_2662' style='display:none'>CLIMB</span>
<span id='database_2663' style='display:none'>System for integration Corpus Management, Processing and Analysis</span>
<span id='database_2664' style='display:none'>WebAnnotator</span>
<span id='database_2665' style='display:none'>Recognising Textual Entailment (RTE) 2 Test Set</span>
<span id='database_2666' style='display:none'>Phase One data release for Blizzard 2012</span>
<span id='database_2667' style='display:none'>Hunmorph</span>
<span id='database_2668' style='display:none'>Gold Standard corpus for Dutch-English word alignment</span>
<span id='database_2669' style='display:none'>ACE 2007</span>
<span id='database_2670' style='display:none'>NTU Multilingual Corpus</span>
<span id='database_2671' style='display:none'>ARPToolkit</span>
<span id='database_2672' style='display:none'>Dundee Corpus</span>
<span id='database_2673' style='display:none'>20newsgroup</span>
<span id='database_2674' style='display:none'>EmotiBlog</span>
<span id='database_2675' style='display:none'>North American News Text Corpus</span>
<span id='database_2676' style='display:none'>Olympus NLU Evaluation Framework</span>
<span id='database_2677' style='display:none'>Italian Valence Lexicon</span>
<span id='database_2678' style='display:none'>English - Oromo Parallel Corpus</span>
<span id='database_2679' style='display:none'>Basque Speecon-like database</span>
<span id='database_2680' style='display:none'>Ontology for Equipping Upper and Domain Ontologies With Time</span>
<span id='database_2681' style='display:none'>Chinese Medical Subject Headings</span>
<span id='database_2682' style='display:none'>XFST Murrinh-Patha morphological analyzer</span>
<span id='database_2683' style='display:none'>Cost-Conscious Annotation Supervised by Humans (CCASH)</span>
<span id='database_2684' style='display:none'>MPRO</span>
<span id='database_2685' style='display:none'>AnCora</span>
<span id='database_2686' style='display:none'>TED-LIUM</span>
<span id='database_2687' style='display:none'>WashU-UCLA Corpus of Subglottal Acoustics</span>
<span id='database_2688' style='display:none'>R52</span>
<span id='database_2689' style='display:none'>Multi-API</span>
<span id='database_2690' style='display:none'>CLEF 2009 test collections</span>
<span id='database_2691' style='display:none'>German Food Health Lexicon</span>
<span id='database_2692' style='display:none'>Danish GATE</span>
<span id='database_2693' style='display:none'>Multilingual, Sense-disambiguated Wiktionary</span>
<span id='database_2694' style='display:none'>WikiWars</span>
<span id='database_2695' style='display:none'>Generalised Patterns for a Basque MT system</span>
<span id='database_2696' style='display:none'>IceParser</span>
<span id='database_2697' style='display:none'>upparse</span>
<span id='database_2698' style='display:none'>Latvian Treebank</span>
<span id='database_2699' style='display:none'>BulTreeBank</span>
<span id='database_2700' style='display:none'>Gann - Graphical Annotation Tool</span>
<span id='database_2701' style='display:none'>SentiSense</span>
<span id='database_2702' style='display:none'>Webcorp</span>
<span id='database_2703' style='display:none'>Multimodal Russian Corpus (MURCO)</span>
<span id='database_2704' style='display:none'>Old Hungarian Corpus</span>
<span id='database_2705' style='display:none'>Acquis , UNs , Meedan , LDC2004T17</span>
<span id='database_2706' style='display:none'>SrpRec</span>
<span id='database_2707' style='display:none'>Engvallex</span>
<span id='database_2708' style='display:none'>English-Korean Parallel Corpus</span>
<span id='database_2709' style='display:none'>EAGLES Recommendations for the morphosyntactic and the syntactic annotation of corpora</span>
<span id='database_2710' style='display:none'>Annotation Manual for Evaluation of Agent Dialogue</span>
<span id='database_2711' style='display:none'>Lowlands Twitter data</span>
<span id='database_2712' style='display:none'>Large Scale Syntactic Annotation of written Dutch (LASSY)</span>
<span id='database_2713' style='display:none'>TWSI lexical substitution dataset</span>
<span id='database_2714' style='display:none'>Multilingual Wikipedia Training Datasets</span>
<span id='database_2715' style='display:none'>ANERcorp + Our Own Corpus</span>
<span id='database_2716' style='display:none'>Bilingual Dictionaries</span>
<span id='database_2717' style='display:none'>Mainichi Newspaper Article</span>
<span id='database_2718' style='display:none'>ILC - NLP Statistical Tools</span>
<span id='database_2719' style='display:none'>Constrative Lexical Evaluation of Machine Translation</span>
<span id='database_2720' style='display:none'>Contact Center Data</span>
<span id='database_2721' style='display:none'>CLEF corpus</span>
<span id='database_2722' style='display:none'>English Gigaword Corpus</span>
<span id='database_2723' style='display:none'>CORILGA</span>
<span id='database_2724' style='display:none'>PhraseSyn - 54</span>
<span id='database_2725' style='display:none'>Extensible Markup Language for Discourse Annotation (EXMARaLDA) Partitur-Editor</span>
<span id='database_2726' style='display:none'>Alignment of WordNet, Wikipedia, and Wiktionary</span>
<span id='database_2727' style='display:none'>HowNet</span>
<span id='database_2728' style='display:none'>Controlled Legal German (CLG)</span>
<span id='database_2729' style='display:none'>WebMAUS</span>
<span id='database_2730' style='display:none'>Sign Language Gibberish Generator</span>
<span id='database_2731' style='display:none'>Speech data corpus for verbal intelligence estimation</span>
<span id='database_2732' style='display:none'>NUS SMS corpus</span>
<span id='database_2733' style='display:none'>Gold Standard for Spanish human nouns</span>
<span id='database_2734' style='display:none'>Morče</span>
<span id='database_2735' style='display:none'>SCReAM: Stockholm Corpus of Respiratory Activity in Multiparty Conversation</span>
<span id='database_2736' style='display:none'>Concordance Line Generator</span>
<span id='database_2737' style='display:none'>CorpusTagger</span>
<span id='database_2738' style='display:none'>BLEU/NIST</span>
<span id='database_2739' style='display:none'>IT-TempeEval-2 Data Set</span>
<span id='database_2740' style='display:none'>MultiUN</span>
<span id='database_2741' style='display:none'>Biomedical Gene Mention Linking Corpus</span>
<span id='database_2742' style='display:none'>WebKB</span>
<span id='database_2743' style='display:none'>TextCoop-Dislog</span>
<span id='database_2744' style='display:none'>Narrative Chains</span>
<span id='database_2745' style='display:none'>English-Croatian Parallel Corpus (EngCro)</span>
<span id='database_2746' style='display:none'>The Nijmegen Corpus of Casual French - NCCFr</span>
<span id='database_2747' style='display:none'>ACE 2004 training data</span>
<span id='database_2748' style='display:none'>Enriched GENIA Event Corpus</span>
<span id='database_2749' style='display:none'>Felicittà</span>
<span id='database_2750' style='display:none'>NTCIR-7 Patent Mining data</span>
<span id='database_2751' style='display:none'>Listening-oriented Dialogues</span>
<span id='database_2752' style='display:none'>JULIE Lab’s UIMA Collection Reader for WIKIPEDIA</span>
<span id='database_2753' style='display:none'>Japanese Spontaneous Spoken Queries</span>
<span id='database_2754' style='display:none'>Quaero Named Entities evaluation tool</span>
<span id='database_2755' style='display:none'>Tagged Icelandic Corpus (MIM)</span>
<span id='database_2756' style='display:none'>Annotated Corpora for South African languages</span>
<span id='database_2757' style='display:none'>Jurisdic</span>
<span id='database_2758' style='display:none'>TSR Corpus</span>
<span id='database_2759' style='display:none'>Princeton Wordnet</span>
<span id='database_2760' style='display:none'>IPOSTagger</span>
<span id='database_2761' style='display:none'>The NewSoMe Set of Corpora</span>
<span id='database_2762' style='display:none'>AMRs in Multiple Languages</span>
<span id='database_2763' style='display:none'>EMU-webApp</span>
<span id='database_2764' style='display:none'>Electropalatographic corpus for Standard Chinese</span>
<span id='database_2765' style='display:none'>Sina Weibo DataSet</span>
<span id='database_2766' style='display:none'>English Chinese Translation Treebank 1.0</span>
<span id='database_2767' style='display:none'>Szeged LVC Corpus</span>
<span id='database_2768' style='display:none'>Extended REX-J Corpus</span>
<span id='database_2769' style='display:none'>Bliwikimodels</span>
<span id='database_2770' style='display:none'>Wikipedia dataset</span>
<span id='database_2771' style='display:none'>Rapidminer 4.6</span>
<span id='database_2772' style='display:none'>AhoTTS</span>
<span id='database_2773' style='display:none'>Enron questins and answers</span>
<span id='database_2774' style='display:none'>Annotation of Syntactic Categories in Chinese Word Structures</span>
<span id='database_2775' style='display:none'>Turkish Treebank as a Morphological Disambiguation resource</span>
<span id='database_2776' style='display:none'>Linguistic Linked Open Data cloud</span>
<span id='database_2777' style='display:none'>CSJ SDR Test collection</span>
<span id='database_2778' style='display:none'>Hashtags identifying motivational  language</span>
<span id='database_2779' style='display:none'>SuperMatrix</span>
<span id='database_2780' style='display:none'>Italian Legal FrameNet</span>
<span id='database_2781' style='display:none'>A Large English-Chinese Parallel Corpus</span>
<span id='database_2782' style='display:none'>CORPS - a CORpus of tagged Political Speeches</span>
<span id='database_2783' style='display:none'>Sentences from Experience Project</span>
<span id='database_2784' style='display:none'>Blei's latent Dirichlet allocation and correlated topic model implmentation</span>
<span id='database_2785' style='display:none'>Indiana Cooperative Remote Search Task (CReST) Corpus</span>
<span id='database_2786' style='display:none'>differential semantics synset annotation</span>
<span id='database_2787' style='display:none'>YADAC</span>
<span id='database_2788' style='display:none'>DBOX dialogue corpus</span>
<span id='database_2789' style='display:none'>PMC Open Access Subset</span>
<span id='database_2790' style='display:none'>NN13205 document collection</span>
<span id='database_2791' style='display:none'>Patent Evaluation Set</span>
<span id='database_2792' style='display:none'>Kitten</span>
<span id='database_2793' style='display:none'>Ihardetsi</span>
<span id='database_2794' style='display:none'>Index Thomisticus Valency Lexicon</span>
<span id='database_2795' style='display:none'>Translation memories of Oﬁs ar Brezhoneg</span>
<span id='database_2796' style='display:none'>Japanese Speaking Style Parallel Database for Expressive Speech Synthesis</span>
<span id='database_2797' style='display:none'>Helsinki Finite-State Technology (HFST) tools</span>
<span id='database_2798' style='display:none'>TAC 2009 Document Sets</span>
<span id='database_2799' style='display:none'>SINDACDB</span>
<span id='database_2800' style='display:none'>OpenMWE</span>
<span id='database_2801' style='display:none'>TDT3, TDT4, TDT5</span>
<span id='database_2802' style='display:none'>Morfologik</span>
<span id='database_2803' style='display:none'>PSI-Toolkit</span>
<span id='database_2804' style='display:none'>CMU Pronunciation Dictionary</span>
<span id='database_2805' style='display:none'>Reuters Corpus annotated with NP coreference</span>
<span id='database_2806' style='display:none'>LACELL's POS tagger</span>
<span id='database_2807' style='display:none'>AkaneRE</span>
<span id='database_2808' style='display:none'>Simultaneous Interpretation Database</span>
<span id='database_2809' style='display:none'>Penn Arabic Treebank</span>
<span id='database_2810' style='display:none'>Non-Native Corpus of English (NOCE)</span>
<span id='database_2811' style='display:none'>I3Media multilingual emotional speech corpus</span>
<span id='database_2812' style='display:none'>Baltic Language Named Entity Recognition (NER) corpus</span>
<span id='database_2813' style='display:none'>Corpus IDV</span>
<span id='database_2814' style='display:none'>Tutorbot Corpus</span>
<span id='database_2815' style='display:none'>TDT4</span>
<span id='database_2816' style='display:none'>SentiWordNet (Bengali)</span>
<span id='database_2817' style='display:none'>test collections</span>
<span id='database_2818' style='display:none'>NoSta-D: German NER Dataset Train/Dev</span>
<span id='database_2819' style='display:none'>Frequency Dictionary of Verb Phrase Constructions</span>
<span id='database_2820' style='display:none'>BDNyme</span>
<span id='database_2821' style='display:none'>Others</span>
<span id='database_2822' style='display:none'>TRIPS Ontology</span>
<span id='database_2823' style='display:none'>Italian TimeBank</span>
<span id='database_2824' style='display:none'>Youtube Video Metadata and Comments</span>
<span id='database_2825' style='display:none'>OpenSubtitles</span>
<span id='database_2826' style='display:none'>OPENCV Processing and Java Library</span>
<span id='database_2827' style='display:none'>NE lists</span>
<span id='database_2828' style='display:none'>Sanity Check Tookit</span>
<span id='database_2829' style='display:none'>Double-Edged WordNet (DEWN)</span>
<span id='database_2830' style='display:none'>Brown corpus</span>
<span id='database_2831' style='display:none'>Aalto DSP2013</span>
<span id='database_2832' style='display:none'>Multimodal Deception Detection</span>
<span id='database_2833' style='display:none'>The PIT Corpus of German Multi-Party Dialogues</span>
<span id='database_2834' style='display:none'>AVATecH database</span>
<span id='database_2835' style='display:none'>SIGHAN Bakeoff 2006 Chinese Word Segmentation Data</span>
<span id='database_2836' style='display:none'>Phonological Feature Specifications for Computing Phoneme Similarity</span>
<span id='database_2837' style='display:none'>CRITT Translation Process Research (TPR) Database</span>
<span id='database_2838' style='display:none'>Opinosis Summarization Demo Software</span>
<span id='database_2839' style='display:none'>DeLex</span>
<span id='database_2840' style='display:none'>Mila morphologial analyzer</span>
<span id='database_2841' style='display:none'>C-ORAL-ROM - Integrated Reference Corpora for Spoken Romance Languages</span>
<span id='database_2842' style='display:none'>TRIPOD Corpus of Annotated Image Captions</span>
<span id='database_2843' style='display:none'>English-Latvian localization TM</span>
<span id='database_2844' style='display:none'>20 Newsgroups</span>
<span id='database_2845' style='display:none'>Russian WordNet, Russian WordNet Grid</span>
<span id='database_2846' style='display:none'>Gold Standard POS Tagged Corpus</span>
<span id='database_2847' style='display:none'>LT4eL multilingual corpora in IT domain</span>
<span id='database_2848' style='display:none'>Stanford Classifier</span>
<span id='database_2849' style='display:none'>CoNLL Shared Task 2009 Corpus</span>
<span id='database_2850' style='display:none'>Simple English Wiktionary</span>
<span id='database_2851' style='display:none'>BC3 Conversational Corpus</span>
<span id='database_2852' style='display:none'>LGTagger</span>
<span id='database_2853' style='display:none'>Solr</span>
<span id='database_2854' style='display:none'>Improved Automatically Trainable Recognizer of Speech (iATROS)</span>
<span id='database_2855' style='display:none'>Turkish Paraphrase Corpus</span>
<span id='database_2856' style='display:none'>FragmentSeeker</span>
<span id='database_2857' style='display:none'>Diachronic Ontologies from People's Daily</span>
<span id='database_2858' style='display:none'>Pilot Arabic CCGbank</span>
<span id='database_2859' style='display:none'>Minima</span>
<span id='database_2860' style='display:none'>Absolute Point of View</span>
<span id='database_2861' style='display:none'>SemSim</span>
<span id='database_2862' style='display:none'>Igbo Corpus</span>
<span id='database_2863' style='display:none'>Google AJAX Language API</span>
<span id='database_2864' style='display:none'>Minna  no Hon'yaku (MNH, Translation for ALL)</span>
<span id='database_2865' style='display:none'>RECC-Rovereto Emotion and Cooperation Corpus</span>
<span id='database_2866' style='display:none'>Arabic new words</span>
<span id='database_2867' style='display:none'>FIRE-2010 ad-hoc English and Bengali collections</span>
<span id='database_2868' style='display:none'>The ADHD and SLI corpus UvA database</span>
<span id='database_2869' style='display:none'>MPC: A Multi-Party Chat Corpus for Modeling Social Phenomena in Discourse</span>
<span id='database_2870' style='display:none'>Kiel Corpus of Spontaneous Speech</span>
<span id='database_2871' style='display:none'>Crosslinguistic Corpus of Hesitation Phenomena</span>
<span id='database_2872' style='display:none'>Lwazi ASR corpora (vSep2009</span>
<span id='database_2873' style='display:none'>Swiss-German SpeechDat(II)</span>
<span id='database_2874' style='display:none'>Cnet product reviews</span>
<span id='database_2875' style='display:none'>Arabic Treebank Part 2 v 3.0</span>
<span id='database_2876' style='display:none'>Mogura</span>
<span id='database_2877' style='display:none'>Korean TV drama scripts</span>
<span id='database_2878' style='display:none'>Bacteria/Biotope corpus for BioNLP 2013</span>
<span id='database_2879' style='display:none'>Proprietory Interactive Voice Response Corpus</span>
<span id='database_2880' style='display:none'>Topic-Images data set</span>
<span id='database_2881' style='display:none'>UWN</span>
<span id='database_2882' style='display:none'>Italian CCG Treebank (CCG-TUT)</span>
<span id='database_2883' style='display:none'>Trésor de la Langue Française informatisé (TLFi)</span>
<span id='database_2884' style='display:none'>woof</span>
<span id='database_2885' style='display:none'>FreeDict</span>
<span id='database_2886' style='display:none'>Shalmaneser</span>
<span id='database_2887' style='display:none'>Moodle</span>
<span id='database_2888' style='display:none'>Crowdsourced Sense Annotations and Guidelines</span>
<span id='database_2889' style='display:none'>CALLAS gesture expressivity corpus</span>
<span id='database_2890' style='display:none'>HFST-SweNER</span>
<span id='database_2891' style='display:none'>IRAPT</span>
<span id='database_2892' style='display:none'>RELISH LMF</span>
<span id='database_2893' style='display:none'>ALIZE</span>
<span id='database_2894' style='display:none'>Urdu Verbs Lexicocn Builder</span>
<span id='database_2895' style='display:none'>TaaS terminology services</span>
<span id='database_2896' style='display:none'>Revenue Corpus</span>
<span id='database_2897' style='display:none'>SRI language model toolkit</span>
<span id='database_2898' style='display:none'>Feauture Vector Set and Tree Forest (SVM-LIGHT-TK 1.2)</span>
<span id='database_2899' style='display:none'>LDC2005T20</span>
<span id='database_2900' style='display:none'>Emotion Topic annotated blog</span>
<span id='database_2901' style='display:none'>The ESP_C Corpus</span>
<span id='database_2902' style='display:none'>OpenWordNet.PT</span>
<span id='database_2903' style='display:none'>STeP-1-Tokenizer</span>
<span id='database_2904' style='display:none'>Species2000</span>
<span id='database_2905' style='display:none'>A world-wide survey of affix borrowing</span>
<span id='database_2906' style='display:none'>ISOCat</span>
<span id='database_2907' style='display:none'>A Sentence Database for Chinese Reference Grammar</span>
<span id='database_2908' style='display:none'>Database of Narrative Schemas</span>
<span id='database_2909' style='display:none'>Standalone Urdu Tagger</span>
<span id='database_2910' style='display:none'>Chinese-English sentence level aligned bilingual corpus</span>
<span id='database_2911' style='display:none'>POLEX</span>
<span id='database_2912' style='display:none'>Similarity Metric Library (SimMetrics)</span>
<span id='database_2913' style='display:none'>IULA Spanish-English Technical Corpus</span>
<span id='database_2914' style='display:none'>CINTIL Logical Form Bank</span>
<span id='database_2915' style='display:none'>JASigning</span>
<span id='database_2916' style='display:none'>Apertium Spanish monolingual dictionary from the Spanish-Catalan language pair</span>
<span id='database_2917' style='display:none'>DK-CLARIN LSP corpus</span>
<span id='database_2918' style='display:none'>reference corpus</span>
<span id='database_2919' style='display:none'>Ariadne</span>
<span id='database_2920' style='display:none'>Kaldi speech recognition engine</span>
<span id='database_2921' style='display:none'>FBIS Chinese-English Corpus</span>
<span id='database_2922' style='display:none'>Swiss statutes and regulations (semantically analyzed)</span>
<span id='database_2923' style='display:none'>InterCorp - a multilingual parallel corpus</span>
<span id='database_2924' style='display:none'>Robocup sportscasting corpus</span>
<span id='database_2925' style='display:none'>WMT data</span>
<span id='database_2926' style='display:none'>Chinese product reviews</span>
<span id='database_2927' style='display:none'>GENIA Meta-Knowledge (GENIA-MK)</span>
<span id='database_2928' style='display:none'>RestQueries250</span>
<span id='database_2929' style='display:none'>TYPO (misspelling) CORPUS</span>
<span id='database_2930' style='display:none'>NTCIR-8 Mainichi Shinbun 2005</span>
<span id='database_2931' style='display:none'>movie review data</span>
<span id='database_2932' style='display:none'>The Online Linguistic Database (version 0.2)</span>
<span id='database_2933' style='display:none'>SentiFul</span>
<span id='database_2934' style='display:none'>JRC Acquis</span>
<span id='database_2935' style='display:none'>TMC</span>
<span id='database_2936' style='display:none'>JMdict</span>
<span id='database_2937' style='display:none'>Russian Dependency Syntax Multi-Treebank</span>
<span id='database_2938' style='display:none'>D-ANS</span>
<span id='database_2939' style='display:none'>Mandarin Chinese Grammar</span>
<span id='database_2940' style='display:none'>Illinois Named Entity tagger</span>
<span id='database_2941' style='display:none'>NEMLAR Broadcast News Speech Corpus</span>
<span id='database_2942' style='display:none'>NLTK Sentence FraMed Model</span>
<span id='database_2943' style='display:none'>Leuven Arabic Database</span>
<span id='database_2944' style='display:none'>IR Multilingual Resources at UniNE</span>
<span id='database_2945' style='display:none'>PTB NP Bracketing Data 1.0</span>
<span id='database_2946' style='display:none'>XML model of Wikipedia</span>
<span id='database_2947' style='display:none'>German compound splitter</span>
<span id='database_2948' style='display:none'>Brill's Tagger</span>
<span id='database_2949' style='display:none'>Tigrinya Language Word List,Stop Words and Affix List</span>
<span id='database_2950' style='display:none'>TLFi</span>
<span id='database_2951' style='display:none'>texrex web corpus tools</span>
<span id='database_2952' style='display:none'>UM-Corpus</span>
<span id='database_2953' style='display:none'>annie-alpha</span>
<span id='database_2954' style='display:none'>IARPA Babel Language Pack Tagalog (training set)</span>
<span id='database_2955' style='display:none'>Inputlog</span>
<span id='database_2956' style='display:none'>Behemoth</span>
<span id='database_2957' style='display:none'>OR2012 twitter corpus</span>
<span id='database_2958' style='display:none'>The Hindi PropBank</span>
<span id='database_2959' style='display:none'>Near-Identity Relations for Coreference (NIDENT)</span>
<span id='database_2960' style='display:none'>SVM-lighT-TK 1.2</span>
<span id='database_2961' style='display:none'>ShARe/CLEF eHealth 2013 corpus</span>
<span id='database_2962' style='display:none'>Tübingen Treebank of Written German (TüBa-D/Z)</span>
<span id='database_2963' style='display:none'>Reuter-21578</span>
<span id='database_2964' style='display:none'>Stanford NER</span>
<span id='database_2965' style='display:none'>WebSourd</span>
<span id='database_2966' style='display:none'>CUNY ASL Motion-Capture Corpus</span>
<span id='database_2967' style='display:none'>The Minho News Resource for Financial Text Mining</span>
<span id='database_2968' style='display:none'>Wikipedia Infobox Extracts</span>
<span id='database_2969' style='display:none'>Lemuoklis</span>
<span id='database_2970' style='display:none'>GeoQueries250ext</span>
<span id='database_2971' style='display:none'>Darpa TIDES Surprise Language Dataset</span>
<span id='database_2972' style='display:none'>MC_v2_ENV_IT</span>
<span id='database_2973' style='display:none'>KTH eXtract Corpus (kthxc)</span>
<span id='database_2974' style='display:none'>EASYLEX</span>
<span id='database_2975' style='display:none'>Sloleks</span>
<span id='database_2976' style='display:none'>Italian SpeechDat(II)</span>
<span id='database_2977' style='display:none'>ICT corpus for speech recognition evaluation</span>
<span id='database_2978' style='display:none'>LocalMaxs</span>
<span id='database_2979' style='display:none'>Women’s Studies Encyclopedia</span>
<span id='database_2980' style='display:none'>Mutation Impact Tagger</span>
<span id='database_2981' style='display:none'>WQuery</span>
<span id='database_2982' style='display:none'>Dialectal Arabic</span>
<span id='database_2983' style='display:none'>KAIST silver standard corpus</span>
<span id='database_2984' style='display:none'>Lwazi TTS corpus</span>
<span id='database_2985' style='display:none'>Luo Part-of-Speech Tagger</span>
<span id='database_2986' style='display:none'>UNL Ontology</span>
<span id='database_2987' style='display:none'>doxa-jv-corpus</span>
<span id='database_2988' style='display:none'>early Government Records</span>
<span id='database_2989' style='display:none'>CallSurf ManTransTopics-140</span>
<span id='database_2990' style='display:none'>Trilingual Parallel (Arabic-Spanish-English)Corpus-Sample</span>
<span id='database_2991' style='display:none'>KIT Lecture Corpus for Speech Translation</span>
<span id='database_2992' style='display:none'>Brandeis Annotation Tool (BAT)</span>
<span id='database_2993' style='display:none'>EnClueWeb</span>
<span id='database_2994' style='display:none'>The Slovak Categorized News Corpus</span>
<span id='database_2995' style='display:none'>VerbNet.Br</span>
<span id='database_2996' style='display:none'>Language Similarity Table</span>
<span id='database_2997' style='display:none'>Abstracts of the 39 Journals</span>
<span id='database_2998' style='display:none'>Intentions Corpus</span>
<span id='database_2999' style='display:none'>IRIS Handwriting Recognition Corpus and Evaluation</span>
<span id='database_3000' style='display:none'>Multi-language word alignments annotation guidelines</span>
<span id='database_3001' style='display:none'>Example Database of Japanese Multiword Functional Expressions</span>
<span id='database_3002' style='display:none'>Parameterized & Annotated CMU Let's Go Database (LEGO)</span>
<span id='database_3003' style='display:none'>Crowdsourced Icelandic</span>
<span id='database_3004' style='display:none'>RTGen</span>
<span id='database_3005' style='display:none'>IRNA  newspaper text corpus</span>
<span id='database_3006' style='display:none'>Yahoo! Local categories</span>
<span id='database_3007' style='display:none'>Ren_CECps 1.0</span>
<span id='database_3008' style='display:none'>Japanese Extended Named Entity Corpus</span>
<span id='database_3009' style='display:none'>NTCIR-3 WEB (Web Retrieval Test Collection)</span>
<span id='database_3010' style='display:none'>Question Ranking</span>
<span id='database_3011' style='display:none'>Multiword Expressions Toolkit (mwetoolkit)</span>
<span id='database_3012' style='display:none'>Ritel-nca</span>
<span id='database_3013' style='display:none'>Korean RDC corpus</span>
<span id='database_3014' style='display:none'>Blast</span>
<span id='database_3015' style='display:none'>Yahoo!-TREC Question Corpus</span>
<span id='database_3016' style='display:none'>Extended Named Entity Dictionary</span>
<span id='database_3017' style='display:none'>NOMAD Argument Corpus</span>
<span id='database_3018' style='display:none'>Hungarian Gigaword Corpus</span>
<span id='database_3019' style='display:none'>DUC 2002</span>
<span id='database_3020' style='display:none'>ImagAct</span>
<span id='database_3021' style='display:none'>Corpus of Indefinite Uses</span>
<span id='database_3022' style='display:none'>Documents, Taxonomies and Ratings</span>
<span id='database_3023' style='display:none'>fnTBL</span>
<span id='database_3024' style='display:none'>Penn Treebank 3.0</span>
<span id='database_3025' style='display:none'>KYOTO-architecture / Knowledge Yielding Ontologies for Transition-based Organizationâ€™ - architecture</span>
<span id='database_3026' style='display:none'>SoNar IPR Acquisition Manual</span>
<span id='database_3027' style='display:none'>Ontology on Narural Sciences and Technology</span>
<span id='database_3028' style='display:none'>Coreference Named Entity (CoRefEN)</span>
<span id='database_3029' style='display:none'>Deep Sequoia</span>
<span id='database_3030' style='display:none'>Language Product Evaluation Tool</span>
<span id='database_3031' style='display:none'>Simplext</span>
<span id='database_3032' style='display:none'>Arabic Gigaword</span>
<span id='database_3033' style='display:none'>SupeSense Tagger</span>
<span id='database_3034' style='display:none'>TREC Entity 2010</span>
<span id='database_3035' style='display:none'>KACSTACT and Ghwwas</span>
<span id='database_3036' style='display:none'>Jibiki</span>
<span id='database_3037' style='display:none'>WiPLEr</span>
<span id='database_3038' style='display:none'>British National Corpus (BNC), spoken</span>
<span id='database_3039' style='display:none'>Clarin Web Services at Wroclaw University of Technology</span>
<span id='database_3040' style='display:none'>SinoCoreferencer</span>
<span id='database_3041' style='display:none'>Valency lexicon of English (EngValLex)</span>
<span id='database_3042' style='display:none'>Netgraph</span>
<span id='database_3043' style='display:none'>Köbler etymological dictionaries, RDF edition</span>
<span id='database_3044' style='display:none'>The Bikel Statistical Parsing Engine</span>
<span id='database_3045' style='display:none'>Arabic Web Crawl</span>
<span id='database_3046' style='display:none'>DepPattern</span>
<span id='database_3047' style='display:none'>ACLP American English Messaging Lexicon</span>
<span id='database_3048' style='display:none'>GECCo</span>
<span id='database_3049' style='display:none'>The Leeds Arabic Discourse Treebank (LADTB)</span>
<span id='database_3050' style='display:none'>Chinese Penn Treebank</span>
<span id='database_3051' style='display:none'>RWTH-PHOENIX-Weather Corpus</span>
<span id='database_3052' style='display:none'>Korean FrameNet Lexical Units</span>
<span id='database_3053' style='display:none'>ScienQuest</span>
<span id='database_3054' style='display:none'>Distress Analysis Interview Corpus</span>
<span id='database_3055' style='display:none'>The CMU pronouncing dictionary</span>
<span id='database_3056' style='display:none'>GTAA</span>
<span id='database_3057' style='display:none'>Świgra</span>
<span id='database_3058' style='display:none'>Charniak-Johnson reranking parser</span>
<span id='database_3059' style='display:none'>SPeech Phonetization Alignment and Syllabification (SPPAS)</span>
<span id='database_3060' style='display:none'>Microsoft Research Question Answering Corpus</span>
<span id='database_3061' style='display:none'>Semantic Role Tagger</span>
<span id='database_3062' style='display:none'>TGA: Time Group Analyser</span>
<span id='database_3063' style='display:none'>WMT12</span>
<span id='database_3064' style='display:none'>idiomatic sentences test dataset</span>
<span id='database_3065' style='display:none'>TAC 2009 KBP Evaluation Entity Linking List</span>
<span id='database_3066' style='display:none'>German dependency treebank with new automatic features</span>
<span id='database_3067' style='display:none'>Encyclo</span>
<span id='database_3068' style='display:none'>Multilingual voice creation toolkit</span>
<span id='database_3069' style='display:none'>GDep</span>
<span id='database_3070' style='display:none'>Tree-to-tree alignment tool Lingua-Align</span>
<span id='database_3071' style='display:none'>Tamil Dependency Treebank (TamilTB)</span>
<span id='database_3072' style='display:none'>libiXML</span>
<span id='database_3073' style='display:none'>Microsoft Web N-gram Services</span>
<span id='database_3074' style='display:none'>ALLEGRA</span>
<span id='database_3075' style='display:none'>A simple C++ library for maximum entropy classification</span>
<span id='database_3076' style='display:none'>Hunpos</span>
<span id='database_3077' style='display:none'>Probabilistic Word Classes with LDA</span>
<span id='database_3078' style='display:none'>Deceptive Opinion Spam Corpus</span>
<span id='database_3079' style='display:none'>NIST MT 03-06 training and test corpora</span>
<span id='database_3080' style='display:none'>label-lex-sw</span>
<span id='database_3081' style='display:none'>FRROAUF</span>
<span id='database_3082' style='display:none'>CorpusShufflingTool</span>
<span id='database_3083' style='display:none'>PELCRA Search Engine for the National Corpus of Polish</span>
<span id='database_3084' style='display:none'>BTagger</span>
<span id='database_3085' style='display:none'>SETIMES</span>
<span id='database_3086' style='display:none'>TGX</span>
<span id='database_3087' style='display:none'>EuroTermBank</span>
<span id='database_3088' style='display:none'>Recon Annotation Tool</span>
<span id='database_3089' style='display:none'>RecAlign</span>
<span id='database_3090' style='display:none'>The D-TUNA Corpus</span>
<span id='database_3091' style='display:none'>BioCreative 2 Gene Mention Recognition Corpus</span>
<span id='database_3092' style='display:none'>Perspicuous and Adjustable Links Annotator (PALinkA)</span>
<span id='database_3093' style='display:none'>Juman</span>
<span id='database_3094' style='display:none'>MUC7T</span>
<span id='database_3095' style='display:none'>To be announced</span>
<span id='database_3096' style='display:none'>MuNPEx</span>
<span id='database_3097' style='display:none'>Polish Word Sketches</span>
<span id='database_3098' style='display:none'>Spoken Dialogue Ontology</span>
<span id='database_3099' style='display:none'>An Open Source Persian Computational Grammar</span>
<span id='database_3100' style='display:none'>Polish Multimodal Corpus</span>
<span id='database_3101' style='display:none'>Tagged and Cleaned Wikipedia</span>
<span id='database_3102' style='display:none'>German Voice Services Agender DB</span>
<span id='database_3103' style='display:none'>TiGer</span>
<span id='database_3104' style='display:none'>Geppetto</span>
<span id='database_3105' style='display:none'>Birkbeck spelling error corpus</span>
<span id='database_3106' style='display:none'>MorphTagger</span>
<span id='database_3107' style='display:none'>Appraisal lexicon</span>
<span id='database_3108' style='display:none'>Base Concepts</span>
<span id='database_3109' style='display:none'>JAKOB-Lexikon</span>
<span id='database_3110' style='display:none'>OmegaWiki</span>
<span id='database_3111' style='display:none'>Wortschatz Universität Leipzig - Corpora and Language Statistics</span>
<span id='database_3112' style='display:none'>Bleu</span>
<span id='database_3113' style='display:none'>Hindi Wordnet</span>
<span id='database_3114' style='display:none'>DMoZ corpus</span>
<span id='database_3115' style='display:none'>Spamassassin</span>
<span id='database_3116' style='display:none'>Ethics and Big Data Charter</span>
<span id='database_3117' style='display:none'>Corpws Cysill ar-lein</span>
<span id='database_3118' style='display:none'>Orthographic Agreement's Knowledge Base</span>
<span id='database_3119' style='display:none'>Spatial Annotation Scheme</span>
<span id='database_3120' style='display:none'>SciTex</span>
<span id='database_3121' style='display:none'>Everyday telephone conversations of older adults</span>
<span id='database_3122' style='display:none'>MMAX2</span>
<span id='database_3123' style='display:none'>Bank of Russian Constructions and Valencies</span>
<span id='database_3124' style='display:none'>Automatically Aligned Conversational Arabic Speech Corpus</span>
<span id='database_3125' style='display:none'>Stanford POS Tagger 1.6</span>
<span id='database_3126' style='display:none'>Greek-Chinese Interlinear of the New Testament</span>
<span id='database_3127' style='display:none'>Japanese Lexicon Acquirer</span>
<span id='database_3128' style='display:none'>XTrans</span>
<span id='database_3129' style='display:none'>Corpus of Cantonese Verbal Comments</span>
<span id='database_3130' style='display:none'>Leffe</span>
<span id='database_3131' style='display:none'>MSNBC News test set</span>
<span id='database_3132' style='display:none'>PubMed Central</span>
<span id='database_3133' style='display:none'>SemLink</span>
<span id='database_3134' style='display:none'>Large Vocabulary Thai Continuous Speech Broadcast News corpus (LOTUS-BN)</span>
<span id='database_3135' style='display:none'>Croatian Dependency Treebank</span>
<span id='database_3136' style='display:none'>EUROSENTIMENT domain-specific sentiment lexicons</span>
<span id='database_3137' style='display:none'>BabelNet 2.0 as Linked Data</span>
<span id='database_3138' style='display:none'>BioC-PMC</span>
<span id='database_3139' style='display:none'>movie review data subjectivity datasets</span>
<span id='database_3140' style='display:none'>MINT.tools</span>
<span id='database_3141' style='display:none'>Ellogon</span>
<span id='database_3142' style='display:none'>CRF Chunker</span>
<span id='database_3143' style='display:none'>KORAIS speech database</span>
<span id='database_3144' style='display:none'>Regression-Forest Tagger</span>
<span id='database_3145' style='display:none'>XWN2.0-1.1</span>
<span id='database_3146' style='display:none'>ALGerian Arabic Speech Database (ALGASD corpus)</span>
<span id='database_3147' style='display:none'>Nao-Children</span>
<span id='database_3148' style='display:none'>Domain-specific Document Collection</span>
<span id='database_3149' style='display:none'>DIALEKT</span>
<span id='database_3150' style='display:none'>Lang-8 Learner Corpus</span>
<span id='database_3151' style='display:none'>Illinois Named Entity Recognizer</span>
<span id='database_3152' style='display:none'>SynonymsDB</span>
<span id='database_3153' style='display:none'>IDV</span>
<span id='database_3154' style='display:none'>bilingual corpus</span>
<span id='database_3155' style='display:none'>Libsvm</span>
<span id='database_3156' style='display:none'>Authorship Attribution Corpus</span>
<span id='database_3157' style='display:none'>Verb Lexicon for Second Language Learners</span>
<span id='database_3158' style='display:none'>MLIF/MultiLingual Information Framework</span>
<span id='database_3159' style='display:none'>Movie review data set</span>
<span id='database_3160' style='display:none'>Time4SCI</span>
<span id='database_3161' style='display:none'>Poio API</span>
<span id='database_3162' style='display:none'>SzegedParalellFX</span>
<span id='database_3163' style='display:none'>Chiba three-party conversation corpus</span>
<span id='database_3164' style='display:none'>Tools for web-scale N-grams</span>
<span id='database_3165' style='display:none'>ZAPIweb</span>
<span id='database_3166' style='display:none'>Leipzig Corpora Collection</span>
<span id='database_3167' style='display:none'>GUM-3-Space</span>
<span id='database_3168' style='display:none'>The Database of Catalan Adjectives</span>
<span id='database_3169' style='display:none'>Hindi-English Code-Switch Corpus</span>
<span id='database_3170' style='display:none'>Parallel Bible Corpus</span>
<span id='database_3171' style='display:none'>Cross-lingual event predicate clusters</span>
<span id='database_3172' style='display:none'>ICWSM 2009 Spinn3r Blog Dataset</span>
<span id='database_3173' style='display:none'>Romanian Russian WordNet-Affect    RoRuWNA</span>
<span id='database_3174' style='display:none'>OntoTag's abstract architecture</span>
<span id='database_3175' style='display:none'>Quaero ASR corpora</span>
<span id='database_3176' style='display:none'>Mind Reading</span>
<span id='database_3177' style='display:none'>Metaphor Corpus</span>
<span id='database_3178' style='display:none'>Word Clipping Test Set</span>
<span id='database_3179' style='display:none'>Text summarization corpus for the credibility of information on the Web</span>
<span id='database_3180' style='display:none'>BoB (Bozen-Bolzano Library Bot) user dialogues</span>
<span id='database_3181' style='display:none'>GMTK-DBN-transliteration-model-scripts</span>
<span id='database_3182' style='display:none'>Croatia Weekly 100 kw Corpus (CW100)</span>
<span id='database_3183' style='display:none'>WordNet Affect</span>
<span id='database_3184' style='display:none'>Aryanpour  Persian to English dictionary</span>
<span id='database_3185' style='display:none'>German Wikipedia articles</span>
<span id='database_3186' style='display:none'>Dense Temporal Relation Annotator</span>
<span id='database_3187' style='display:none'>Large Dataset of French-English SMT Output Corrections</span>
<span id='database_3188' style='display:none'>Intention Dataset</span>
<span id='database_3189' style='display:none'>CLANN</span>
<span id='database_3190' style='display:none'>MotaMot French-Khmer Pivot Database</span>
<span id='database_3191' style='display:none'>PAN'10 plagiarism detection corpus</span>
<span id='database_3192' style='display:none'>ILSP Dependency Parser</span>
<span id='database_3193' style='display:none'>Nifty-Serve Data</span>
<span id='database_3194' style='display:none'>CMU Let's Go Data</span>
<span id='database_3195' style='display:none'>Choix de Textes de Français Parlé</span>
<span id='database_3196' style='display:none'>A Modern Standard Arabic Closed-Class Word List</span>
<span id='database_3197' style='display:none'>Casual English Generation Phoneme Database</span>
<span id='database_3198' style='display:none'>UW Bio-NLP X-ray Event Corpus</span>
<span id='database_3199' style='display:none'>EIpGiFCiS</span>
<span id='database_3200' style='display:none'>Mainichi Newspaper Database</span>
<span id='database_3201' style='display:none'>ILP-based abductive reasoner</span>
<span id='database_3202' style='display:none'>NomLex-BR</span>
<span id='database_3203' style='display:none'>Vystadial 2013 – Czech data</span>
<span id='database_3204' style='display:none'>NetDC Arabic Broadcast News Speech Corpus</span>
<span id='database_3205' style='display:none'>SenTube</span>
<span id='database_3206' style='display:none'>CText Tagset for Afrikaans</span>
<span id='database_3207' style='display:none'>Target-dependent Twitter Sentiment Classification Annotation</span>
<span id='database_3208' style='display:none'>Internet text</span>
<span id='database_3209' style='display:none'>The KIT Lecture Corpus for Spoken Language Processing and Translation</span>
<span id='database_3210' style='display:none'>Pre-processed test corpus of Russian</span>
<span id='database_3211' style='display:none'>RPAH-ICU</span>
<span id='database_3212' style='display:none'>The OGI Multi-language Telephone Speech Corpus</span>
<span id='database_3213' style='display:none'>ANC Manually Annotated Sub-corpus (MASC)</span>
<span id='database_3214' style='display:none'>DARE</span>
<span id='database_3215' style='display:none'>Morris hiztegia</span>
<span id='database_3216' style='display:none'>Gene renaming corpus</span>
<span id='database_3217' style='display:none'>Ukwabelana corpus</span>
<span id='database_3218' style='display:none'>Spatial Containment Relations Between Events</span>
<span id='database_3219' style='display:none'>Plane Crash Dataset</span>
<span id='database_3220' style='display:none'>Onno Crasborn</span>
<span id='database_3221' style='display:none'>Celex EPW (English pronunciations)</span>
<span id='database_3222' style='display:none'>Modular Architecture for Research on speech sYnthesis Text-to-Speech System (MARY TTS)</span>
<span id='database_3223' style='display:none'>Cyclone</span>
<span id='database_3224' style='display:none'>Grefenstette and Sadrzadeh 2011 Dataset</span>
<span id='database_3225' style='display:none'>Ancient Greek Dependency Treebank</span>
<span id='database_3226' style='display:none'>Manipuri POS tagger</span>
<span id='database_3227' style='display:none'>Corpus of Arabic Speech</span>
<span id='database_3228' style='display:none'>FOLK</span>
<span id='database_3229' style='display:none'>I*Link</span>
<span id='database_3230' style='display:none'>Hyderabad Dependency Treebank</span>
<span id='database_3231' style='display:none'>TermWise</span>
<span id='database_3232' style='display:none'>Kashmiri Part of Speech Tagger</span>
<span id='database_3233' style='display:none'>Iban corpus</span>
<span id='database_3234' style='display:none'>Polish Coreference Corpus</span>
<span id='database_3235' style='display:none'>Tagalog Wiktionary</span>
<span id='database_3236' style='display:none'>Argo, a Web-based Text Mining Workbench</span>
<span id='database_3237' style='display:none'>Project Gutenberg</span>
<span id='database_3238' style='display:none'>WMT 2012 Training data</span>
<span id='database_3239' style='display:none'>No name</span>
<span id='database_3240' style='display:none'>Phonetisaurus</span>
<span id='database_3241' style='display:none'>Hindi-Punjabi parallel corpus</span>
<span id='database_3242' style='display:none'>Czech-English Parallel Corpus (CzEng)</span>
<span id='database_3243' style='display:none'>Ontologies of Linguistic Annotation (OLiA ontologies), discourse extensions</span>
<span id='database_3244' style='display:none'>FipsRomanian</span>
<span id='database_3245' style='display:none'>Clean English ACE 2005 Event Trigger Corpus</span>
<span id='database_3246' style='display:none'>Croatian Webcorpus</span>
<span id='database_3247' style='display:none'>Vergina speech database</span>
<span id='database_3248' style='display:none'>Yahoo!Answers</span>
<span id='database_3249' style='display:none'>Topic-specific sentiments in financial texts</span>
<span id='database_3250' style='display:none'>SDEWAC</span>
<span id='database_3251' style='display:none'>Connexor's FDG Parser</span>
<span id='database_3252' style='display:none'>BKB (BKB-nytfootball-v0.7.5)</span>
<span id='database_3253' style='display:none'>Wikipedia (the Spanish version)</span>
<span id='database_3254' style='display:none'>Bengali NEWS Editorial Opinion Corpus</span>
<span id='database_3255' style='display:none'>LT4eL ontology in IT domain</span>
<span id='database_3256' style='display:none'>Corpus of Sentences for User Interaction in Pronunciation Learning Systems</span>
<span id='database_3257' style='display:none'>WikiNet</span>
<span id='database_3258' style='display:none'>UIUC Question classification Dataset</span>
<span id='database_3259' style='display:none'>20 Newsgroups Data Set</span>
<span id='database_3260' style='display:none'>Spanish Multi-task dialog corpus</span>
<span id='database_3261' style='display:none'>Word Clustering Evaluation Data</span>
<span id='database_3262' style='display:none'>Elhuyar English-Basque Parallel Corpus</span>
<span id='database_3263' style='display:none'>Evaluation des Systèmes de Transcription enrichie d'Émissions Radiophoniques - ESTER</span>
<span id='database_3264' style='display:none'>HornMorpho</span>
<span id='database_3265' style='display:none'>English GigaWord</span>
<span id='database_3266' style='display:none'>TalkBank</span>
<span id='database_3267' style='display:none'>Spanish Learner Language Oral Corpora (SPLLOC)</span>
<span id='database_3268' style='display:none'>LEX monolingual corpus</span>
<span id='database_3269' style='display:none'>A Reference Dependency Bank for Analyzing Complex Predicates</span>
<span id='database_3270' style='display:none'>Modern Arabic Representative Corpus 2000 (MARC-2000)</span>
<span id='database_3271' style='display:none'>cTAKES</span>
<span id='database_3272' style='display:none'>Giellatekno-Divvun Infrastructure</span>
<span id='database_3273' style='display:none'>Rubenstein and Goodenough</span>
<span id='database_3274' style='display:none'>The World Loanword Database</span>
<span id='database_3275' style='display:none'>ILTIMEX2012 Corpus</span>
<span id='database_3276' style='display:none'>German (P)LTAG treebank</span>
<span id='database_3277' style='display:none'>Translated Wikipedia Infoboxes</span>
<span id='database_3278' style='display:none'>Morphological tagger of the Hungarian National Corpus</span>
<span id='database_3279' style='display:none'>SexIt</span>
<span id='database_3280' style='display:none'>Polish websites corpus</span>
<span id='database_3281' style='display:none'>CLEF Data Collections: LA Times 94, Glasgow Herald 95, topics and human relevance judgements</span>
<span id='database_3282' style='display:none'>Wiktionary relation disambiguation datasets</span>
<span id='database_3283' style='display:none'>Syntactic Reference Corpus of Medieval French (SRCMF)</span>
<span id='database_3284' style='display:none'>A Multiparty Multi-Lingual Chat Corpus for Modeling Social Phenomena in Language (MMPC)</span>
<span id='database_3285' style='display:none'>Minho Quotation Resource</span>
<span id='database_3286' style='display:none'>Modal sense corpus</span>
<span id='database_3287' style='display:none'>Refractive Source Code</span>
<span id='database_3288' style='display:none'>Elhuyar dictionaries</span>
<span id='database_3289' style='display:none'>voice clone toolkit</span>
<span id='database_3290' style='display:none'>Chinese Bi-Character Words' Morphological Types Corpus</span>
<span id='database_3291' style='display:none'>EPEC (Reference Corpus for the Processing of Basque).</span>
<span id='database_3292' style='display:none'>ACE 2004 Multilingual Training Corpus</span>
<span id='database_3293' style='display:none'>Infomap NLP Software</span>
<span id='database_3294' style='display:none'>Bayon</span>
<span id='database_3295' style='display:none'>Kanji Tester response logs</span>
<span id='database_3296' style='display:none'>Colorado Richly Annotated Full Text</span>
<span id='database_3297' style='display:none'>TLAXCALA corpus</span>
<span id='database_3298' style='display:none'>Persian Treebank (PerTreeBank)</span>
<span id='database_3299' style='display:none'>ChineseBookDescriptionWithTags</span>
<span id='database_3300' style='display:none'>PubMed Stopword List</span>
<span id='database_3301' style='display:none'>CTL</span>
<span id='database_3302' style='display:none'>RelEx</span>
<span id='database_3303' style='display:none'>SpeechDat(II) EN</span>
<span id='database_3304' style='display:none'>WikiXMLDumpTextExtractor</span>
<span id='database_3305' style='display:none'>Words</span>
<span id='database_3306' style='display:none'>Corpus of Editorials from Newspapers published in Nepal and worldwide and annotation of arguments and opinions</span>
<span id='database_3307' style='display:none'>ANVIL tool</span>
<span id='database_3308' style='display:none'>Munich BioVoice Corpus</span>
<span id='database_3309' style='display:none'>MultiTree</span>
<span id='database_3310' style='display:none'>morphisto</span>
<span id='database_3311' style='display:none'>Language Identification for eleven South African Languages</span>
<span id='database_3312' style='display:none'>DeepBankPT</span>
<span id='database_3313' style='display:none'>MPPDB</span>
<span id='database_3314' style='display:none'>Magyar Értelemező Kéziszótár / Hungarian Explanatory Dictionary</span>
<span id='database_3315' style='display:none'>Gĩkũyũ Corpus</span>
<span id='database_3316' style='display:none'>Additional Review Datasets (9 products)</span>
<span id='database_3317' style='display:none'>Xinhua of Gigaword</span>
<span id='database_3318' style='display:none'>Old-Czech Text Bank</span>
<span id='database_3319' style='display:none'>NorKompLeks</span>
<span id='database_3320' style='display:none'>Russian corpus</span>
<span id='database_3321' style='display:none'>SmartSUMO</span>
<span id='database_3322' style='display:none'>Compositionality gradings for physics terminology</span>
<span id='database_3323' style='display:none'>AnCora-Verb-Es</span>
<span id='database_3324' style='display:none'>Encarta treasures</span>
<span id='database_3325' style='display:none'>POM</span>
<span id='database_3326' style='display:none'>NewsReader Data set</span>
<span id='database_3327' style='display:none'>Natural language API retrieval javadoc collection</span>
<span id='database_3328' style='display:none'>Human-annotated event coreference corpus in the Intelligence Community domain</span>
<span id='database_3329' style='display:none'>Xinhua Chinese</span>
<span id='database_3330' style='display:none'>Polish wordnet, plWordNet (Słowosieć)</span>
<span id='database_3331' style='display:none'>AutoTagTCG</span>
<span id='database_3332' style='display:none'>Arabic Gigaword Fourth Edition</span>
<span id='database_3333' style='display:none'>TüNDRA</span>
<span id='database_3334' style='display:none'>MNH SDF corpus</span>
<span id='database_3335' style='display:none'>MAPLE</span>
<span id='database_3336' style='display:none'>JAPE Editor</span>
<span id='database_3337' style='display:none'>LIBLINEAR</span>
<span id='database_3338' style='display:none'>ICLLT LOD Model</span>
<span id='database_3339' style='display:none'>The SLI RU-Kentalis data base</span>
<span id='database_3340' style='display:none'>The AQUAINT Corpus of English News Text</span>
<span id='database_3341' style='display:none'>Groningen Meaning Bank</span>
<span id='database_3342' style='display:none'>EN1M</span>
<span id='database_3343' style='display:none'>Estonian resource grammar for the Grammatical Framework (GF) Resource Grammar Library</span>
<span id='database_3344' style='display:none'>Simultaneous Interpretation Database (SIDB)</span>
<span id='database_3345' style='display:none'>Perugia Corpus (PEC)</span>
<span id='database_3346' style='display:none'>Scorer</span>
<span id='database_3347' style='display:none'>Corpus for L2 pronunciation errors</span>
<span id='database_3348' style='display:none'>AVATecH wrappers and utility recognizers</span>
<span id='database_3349' style='display:none'>Fairy tale corpus</span>
<span id='database_3350' style='display:none'>CINTIL Corpus</span>
<span id='database_3351' style='display:none'>Croatian Memories</span>
<span id='database_3352' style='display:none'>Czech Web Corpus</span>
<span id='database_3353' style='display:none'>Japanese FrameNet</span>
<span id='database_3354' style='display:none'>Tycho Brahe parsed corpus</span>
<span id='database_3355' style='display:none'>XLE English ParGram grammar + AKR</span>
<span id='database_3356' style='display:none'>GATE Access and Interpretation of SentiWordNet</span>
<span id='database_3357' style='display:none'>The Lemur Toolkit</span>
<span id='database_3358' style='display:none'>WEB-QA and TREC-QA</span>
<span id='database_3359' style='display:none'>Est Républican Parsed Corpora</span>
<span id='database_3360' style='display:none'>Link Grammar</span>
<span id='database_3361' style='display:none'>Wikipedia article names</span>
<span id='database_3362' style='display:none'>Hong Kong University of Science and Technology English Examination Corpus (HKUST)</span>
<span id='database_3363' style='display:none'>Finnish Language Text Collection</span>
<span id='database_3364' style='display:none'>An n-gram analysis extension for ELAN</span>
<span id='database_3365' style='display:none'>ZT Corpusa - Zientzia eta Teknologia Corpusa</span>
<span id='database_3366' style='display:none'>Semantic spaces</span>
<span id='database_3367' style='display:none'>CRF Suite</span>
<span id='database_3368' style='display:none'>Gold Standard for Chemistry-Disease Relations in Patent Texts</span>
<span id='database_3369' style='display:none'>MindNet</span>
<span id='database_3370' style='display:none'>Javabased MaxEnt package</span>
<span id='database_3371' style='display:none'>Diachronic German Corpus TüBa-D/DC</span>
<span id='database_3372' style='display:none'>AQUAINT-2</span>
<span id='database_3373' style='display:none'>A Large-Scale Unified Lexical-Semantic Resource (UBY)</span>
<span id='database_3374' style='display:none'>Ontology-based Semantic Annotation X Corpus</span>
<span id='database_3375' style='display:none'>Transducersaurus</span>
<span id='database_3376' style='display:none'>TIGER Treebank (release August 2007)</span>
<span id='database_3377' style='display:none'>KyTea - the Kyoto Text Analysis Toolkit</span>
<span id='database_3378' style='display:none'>Chinese Penn Treebank 6.0</span>
<span id='database_3379' style='display:none'>Gold Standard for Dutch sentiment bearing adjectives</span>
<span id='database_3380' style='display:none'>Review references</span>
<span id='database_3381' style='display:none'>ukWaC + itWaC</span>
<span id='database_3382' style='display:none'>BRIGHAM YOUNG UNIVERSITY British National Corpus (BYU-BNC)</span>
<span id='database_3383' style='display:none'>MaltOptimizer</span>
<span id='database_3384' style='display:none'>Annotated Corpora (AnCora)</span>
<span id='database_3385' style='display:none'>Adaptation of the David Chiang's (2000) STIG parser (eg. Hybrid)</span>
<span id='database_3386' style='display:none'>NIMITEK Corpus</span>
<span id='database_3387' style='display:none'>Bulgarian FrameNet</span>
<span id='database_3388' style='display:none'>A Corpus of Plagiarised Short Answers</span>
<span id='database_3389' style='display:none'>ASL Motion Capture Corpus: Facial Expression Stimuli</span>
<span id='database_3390' style='display:none'>PanLex</span>
<span id='database_3391' style='display:none'>LLabSwahiliFST</span>
<span id='database_3392' style='display:none'>HealthSurveillanceFramework</span>
<span id='database_3393' style='display:none'>Microsoft Research Video Description Corpus</span>
<span id='database_3394' style='display:none'>TIMEN</span>
<span id='database_3395' style='display:none'>Europarl (Manually annotated)</span>
<span id='database_3396' style='display:none'>Delicious</span>
<span id='database_3397' style='display:none'>Multi-Annotated Corpus of Answers to Questions, MACAQ</span>
<span id='database_3398' style='display:none'>SogouT Corpus</span>
<span id='database_3399' style='display:none'>List of questions and their answers</span>
<span id='database_3400' style='display:none'>Tohoku RTE</span>
<span id='database_3401' style='display:none'>IWSLT 2011 Arabic-English data</span>
<span id='database_3402' style='display:none'>Data sets of parsed texts</span>
<span id='database_3403' style='display:none'>Annotated Corpus of Automotive Engineering</span>
<span id='database_3404' style='display:none'>Shared Swedish/English Regulus grammar</span>
<span id='database_3405' style='display:none'>The EMIME Mandarin/English Bilingual Database</span>
<span id='database_3406' style='display:none'>IWSLT 2013 Training data</span>
<span id='database_3407' style='display:none'>Broadcast audio</span>
<span id='database_3408' style='display:none'>BLOGS06</span>
<span id='database_3409' style='display:none'>LINA-PAL 1.0</span>
<span id='database_3410' style='display:none'>Multilingual glossary of technical and popular medical terms</span>
<span id='database_3411' style='display:none'>Eijiro, Third Edition</span>
<span id='database_3412' style='display:none'>Zemberek spell checker word list</span>
<span id='database_3413' style='display:none'>AUTOTERM</span>
<span id='database_3414' style='display:none'>AMI</span>
<span id='database_3415' style='display:none'>SALDO</span>
<span id='database_3416' style='display:none'>PET</span>
<span id='database_3417' style='display:none'>TLexs: Thai lexeme analyser</span>
<span id='database_3418' style='display:none'>ODIN database</span>
<span id='database_3419' style='display:none'>A Multi-layered/Multi-representational Treebank for Hindi</span>
<span id='database_3420' style='display:none'>Bilingual corpus on patent domain</span>
<span id='database_3421' style='display:none'>FrameNet to WordNet mapping</span>
<span id='database_3422' style='display:none'>Quranic Arabic Corpus</span>
<span id='database_3423' style='display:none'>FAU IISAH</span>
<span id='database_3424' style='display:none'>Dutch Parallel Corpus (DPC)</span>
<span id='database_3425' style='display:none'>OpenCV</span>
<span id='database_3426' style='display:none'>Stockholm EPR  Corpus / Speculative clinical text</span>
<span id='database_3427' style='display:none'>Yahoo! News Resolution Set</span>
<span id='database_3428' style='display:none'>Eindhoven Corpus</span>
<span id='database_3429' style='display:none'>oracle database qa forum</span>
<span id='database_3430' style='display:none'>TIGER Corpus</span>
<span id='database_3431' style='display:none'>An annotation scheme of modality in a broad sense</span>
<span id='database_3432' style='display:none'>WordNet 2.0</span>
<span id='database_3433' style='display:none'>VERT</span>
<span id='database_3434' style='display:none'>QurSim</span>
<span id='database_3435' style='display:none'>Morphological tagger of the Corpus of the Contemporary Lithuanian Language</span>
<span id='database_3436' style='display:none'>Natural Language Programming Corpus</span>
<span id='database_3437' style='display:none'>UCU Accent Project</span>
<span id='database_3438' style='display:none'>TAC 2009 Knowledge Base Population Track: corpora, knowledge base, guidelines, queries, and assessments</span>
<span id='database_3439' style='display:none'>Document Understanding Conference</span>
<span id='database_3440' style='display:none'>Corpus Analysis and Validation for TimeML (CAVaT)</span>
<span id='database_3441' style='display:none'>The DARE Corpus: A Resource for Anaphora Resolution in Dialogue Based Intelligent Tutoring Systems</span>
<span id='database_3442' style='display:none'>MADA (Morphological Analysis and Disambiguation for Arabic) tool kit.</span>
<span id='database_3443' style='display:none'>Formality Word Lists</span>
<span id='database_3444' style='display:none'>Stuttgart-Tübingen Tagset of German</span>
<span id='database_3445' style='display:none'>GRID</span>
<span id='database_3446' style='display:none'>ACTIV-ES 0.1</span>
<span id='database_3447' style='display:none'>PukWaC</span>
<span id='database_3448' style='display:none'>EU bookshop</span>
<span id='database_3449' style='display:none'>Dictionnaire fondamental de l'informatique et de l'Internet (DiCoInfo)</span>
<span id='database_3450' style='display:none'>sMail</span>
<span id='database_3451' style='display:none'>Wami Toolkit</span>
<span id='database_3452' style='display:none'>D64 Multimodal Conversational Corpus</span>
<span id='database_3453' style='display:none'>GeneReg</span>
<span id='database_3454' style='display:none'>Euronews</span>
<span id='database_3455' style='display:none'>TRECVID MED</span>
<span id='database_3456' style='display:none'>Vocal Tract Animation Model</span>
<span id='database_3457' style='display:none'>CMU SLM toolkit</span>
<span id='database_3458' style='display:none'>Charlatan Synthetic Dialog Corpus</span>
<span id='database_3459' style='display:none'>Citation annotation in patents</span>
<span id='database_3460' style='display:none'>è°·æ­Œé‡‘å±±è¯éœ¸2.0 (Google and Kingsoft Dictionary 2.0)</span>
<span id='database_3461' style='display:none'>Chinese Web 5-gram</span>
<span id='database_3462' style='display:none'>HunLearner</span>
<span id='database_3463' style='display:none'>ANERcorp</span>
<span id='database_3464' style='display:none'>Proposition database</span>
<span id='database_3465' style='display:none'>Guidelines for Caption Annotation:  Notes for annotators on how to identify and ground toponym  expressions in captions.</span>
<span id='database_3466' style='display:none'>Afazio TestSuite</span>
<span id='database_3467' style='display:none'>CAT</span>
<span id='database_3468' style='display:none'>Tamil Speech Data</span>
<span id='database_3469' style='display:none'>Latvian news corpus</span>
<span id='database_3470' style='display:none'>Word clAss taGGER (WAGGER)</span>
<span id='database_3471' style='display:none'>Keio Emotional Speech Database (Keio-ESD)</span>
<span id='database_3472' style='display:none'>Semantic markup in Russian movie sites</span>
<span id='database_3473' style='display:none'>TUKE-BNews-SK: A Slovak Broadcast News Corpus</span>
<span id='database_3474' style='display:none'>TAC KBP Annotation and Assessment Guidelines</span>
<span id='database_3475' style='display:none'>Casa de la Lhéngua</span>
<span id='database_3476' style='display:none'>Princeton English WordNet</span>
<span id='database_3477' style='display:none'>Quaero Football Corpus</span>
<span id='database_3478' style='display:none'>TMEKO, Tutoring Methodology for the Enrichment of the Kyoto Ontology</span>
<span id='database_3479' style='display:none'>Xinhua News Corpus</span>
<span id='database_3480' style='display:none'>De Mauro Paravia Dictionary of the Italian Language</span>
<span id='database_3481' style='display:none'>VOAR</span>
<span id='database_3482' style='display:none'>Arabic Penn Treebank</span>
<span id='database_3483' style='display:none'>Japanese phonetically-balanced word speech database</span>
<span id='database_3484' style='display:none'>Chungdahm English Learner Corpus</span>
<span id='database_3485' style='display:none'>FunGramKB Ontology</span>
<span id='database_3486' style='display:none'>TermFactory</span>
<span id='database_3487' style='display:none'>CQP</span>
<span id='database_3488' style='display:none'>Dutch Parallel Corpus (DPC) (subpart)</span>
<span id='database_3489' style='display:none'>Basque SpeechDat MDB-600</span>
<span id='database_3490' style='display:none'>Sextant</span>
<span id='database_3491' style='display:none'>New York Times Annotated corpus</span>
<span id='database_3492' style='display:none'>MMASCS</span>
<span id='database_3493' style='display:none'>RTE data set</span>
<span id='database_3494' style='display:none'>NTCIR Opinion Corpus</span>
<span id='database_3495' style='display:none'>PRObability-based PROlog-implemented Parser for RObust Grammatical Relation Extraction System (Pro3Gres)</span>
<span id='database_3496' style='display:none'>Maltparser</span>
<span id='database_3497' style='display:none'>Catalan Sign Language Corpus on the weather report domain</span>
<span id='database_3498' style='display:none'>Turkish treebank-to-disambiguator format conversion scripts</span>
<span id='database_3499' style='display:none'>Lexicon</span>
<span id='database_3500' style='display:none'>Semeval 2010 word sense induction and disambiguation dataset</span>
<span id='database_3501' style='display:none'>DSL Corpus Colleciton</span>
<span id='database_3502' style='display:none'>ILSP/ELEFTHEROTYPIA MODERN GREEK CORPUS</span>
<span id='database_3503' style='display:none'>MiniPar</span>
<span id='database_3504' style='display:none'>Aranea family of web corpora</span>
<span id='database_3505' style='display:none'>MB-Tagger</span>
<span id='database_3506' style='display:none'>MIRE based shallow parser</span>
<span id='database_3507' style='display:none'>KALAKA 2</span>
<span id='database_3508' style='display:none'>GreekSemSimEval</span>
<span id='database_3509' style='display:none'>Wordsmith Tools</span>
<span id='database_3510' style='display:none'>Acl Anthology Network (AAN)</span>
<span id='database_3511' style='display:none'>AnCora-Es</span>
<span id='database_3512' style='display:none'>IRASubcat</span>
<span id='database_3513' style='display:none'>TAC 2011 Summarization Track Data</span>
<span id='database_3514' style='display:none'>Wikipedia Category Hierarchy</span>
<span id='database_3515' style='display:none'>2013 Arabic Newspapers Corpus</span>
<span id='database_3516' style='display:none'>FixISS database</span>
<span id='database_3517' style='display:none'>eg-GRIDS+</span>
<span id='database_3518' style='display:none'>Corpus of Computer-mediated Communication in Hindi (CO3H)</span>
<span id='database_3519' style='display:none'>Japanese hierarchical Ontology for Multiword Functional Expressions Tsutsuji</span>
<span id='database_3520' style='display:none'>hunspell</span>
<span id='database_3521' style='display:none'>CG treebank</span>
<span id='database_3522' style='display:none'>Bilingual dictionary</span>
<span id='database_3523' style='display:none'>Ritual Descriptions Corpus</span>
<span id='database_3524' style='display:none'>Todai Robot Project</span>
<span id='database_3525' style='display:none'>Audio BNC</span>
<span id='database_3526' style='display:none'>Babel Cantonese</span>
<span id='database_3527' style='display:none'>DegExt</span>
<span id='database_3528' style='display:none'>Fisher</span>
<span id='database_3529' style='display:none'>Quaero Terminology Extraction Evaluation Patents Corpus</span>
<span id='database_3530' style='display:none'>Croatian CG Morphological Disambiguation Rules</span>
<span id='database_3531' style='display:none'>Annotation guidelines for CoNLL</span>
<span id='database_3532' style='display:none'>Catvar</span>
<span id='database_3533' style='display:none'>Specialized Datasets for Textual Entailment</span>
<span id='database_3534' style='display:none'>Language Technology Resource Center</span>
<span id='database_3535' style='display:none'>Tajik-Farsi Persian Transliteration System</span>
<span id='database_3536' style='display:none'>AV-LASYN Database</span>
<span id='database_3537' style='display:none'>Dutch Webcorpus</span>
<span id='database_3538' style='display:none'>MILA Hebrew Corpus</span>
<span id='database_3539' style='display:none'>PANACEA_MWE_IT</span>
<span id='database_3540' style='display:none'>Hansard Corpus, Public Release of Haitian Creole Language Data by Carnegie Mellon, FBIS</span>
<span id='database_3541' style='display:none'>I-EN-SAMPLE</span>
<span id='database_3542' style='display:none'>corpusEditor</span>
<span id='database_3543' style='display:none'>Kiel Corpus</span>
<span id='database_3544' style='display:none'>Lexicon of negation cues</span>
<span id='database_3545' style='display:none'>Granska</span>
<span id='database_3546' style='display:none'>20 Newsgroups Document Categorization dataset</span>
<span id='database_3547' style='display:none'>Ngram Statistics Package (NSP)</span>
<span id='database_3548' style='display:none'>CorTrad</span>
<span id='database_3549' style='display:none'>Austrian Phonetic Database (ADABA)</span>
<span id='database_3550' style='display:none'>Qurany</span>
<span id='database_3551' style='display:none'>EmotiBlog annotation model</span>
<span id='database_3552' style='display:none'>Simple Parser for Hindi</span>
<span id='database_3553' style='display:none'>MaltEval</span>
<span id='database_3554' style='display:none'>Wikicorpus</span>
<span id='database_3555' style='display:none'>Companion WoZ corpus</span>
<span id='database_3556' style='display:none'>Chinese menu corpus</span>
<span id='database_3557' style='display:none'>IULA Text Handler</span>
<span id='database_3558' style='display:none'>annie-rdf</span>
<span id='database_3559' style='display:none'>CoNLL 2003 Shared Task Named Entity data</span>
<span id='database_3560' style='display:none'>Lectra</span>
<span id='database_3561' style='display:none'>NeoTag</span>
<span id='database_3562' style='display:none'>CorefPro</span>
<span id='database_3563' style='display:none'>CoreSC/ART corpus</span>
<span id='database_3564' style='display:none'>Golden Collection of Parallel Multi-language Word Alignment</span>
<span id='database_3565' style='display:none'>AVATecH Application</span>
<span id='database_3566' style='display:none'>Technical-Lay paraphrase lexicon</span>
<span id='database_3567' style='display:none'>Semantic categories in Classical Chinese</span>
<span id='database_3568' style='display:none'>Quaero named entity corpora</span>
<span id='database_3569' style='display:none'>MoCap Toolbox</span>
<span id='database_3570' style='display:none'>YouTube Comedy Slam Preference Data Data Set</span>
<span id='database_3571' style='display:none'>Buckwalter lexicon</span>
<span id='database_3572' style='display:none'>SignWiki</span>
<span id='database_3573' style='display:none'>The Concisus Corpus of Event Summaries</span>
<span id='database_3574' style='display:none'>LBJ Coreferene Package</span>
<span id='database_3575' style='display:none'>First Certificate in English (FCE) exams of Cambridge Learner Corpus (CLC)</span>
<span id='database_3576' style='display:none'>HuLaPos</span>
<span id='database_3577' style='display:none'>SiBol/Port corpus</span>
<span id='database_3578' style='display:none'>Corpus of Meaning-Text Structures (CoMTeS)</span>
<span id='database_3579' style='display:none'>GSEDC (Gold Standard for Event Detection in Croatian)</span>
<span id='database_3580' style='display:none'>English-Gujarati Dictionary</span>
<span id='database_3581' style='display:none'>2007 Computational Medicine Center Challenge Corpus</span>
<span id='database_3582' style='display:none'>Corpora of Corpus Factory</span>
<span id='database_3583' style='display:none'>Afrikaans Beeld Corpus</span>
<span id='database_3584' style='display:none'>Merlin</span>
<span id='database_3585' style='display:none'>WebCrawler</span>
<span id='database_3586' style='display:none'>PKUtreebank</span>
<span id='database_3587' style='display:none'>Chinese Open Wordnet (COW)</span>
<span id='database_3588' style='display:none'>Bosque 8.0</span>
<span id='database_3589' style='display:none'>LGLex</span>
<span id='database_3590' style='display:none'>C-Comparator v0.23</span>
<span id='database_3591' style='display:none'>ASL VLT</span>
<span id='database_3592' style='display:none'>The BANNER NER system</span>
<span id='database_3593' style='display:none'>Robertino-game</span>
<span id='database_3594' style='display:none'>KTHNC - KTH News Corpus</span>
<span id='database_3595' style='display:none'>TagParser</span>
<span id='database_3596' style='display:none'>Nordic Dialect Corpus</span>
<span id='database_3597' style='display:none'>TREC Genomics data from 2006 and 2007</span>
<span id='database_3598' style='display:none'>Resource grammar in GF</span>
<span id='database_3599' style='display:none'>EnLex</span>
<span id='database_3600' style='display:none'>BioNLP'09 Shared Task on Event Extraction</span>
<span id='database_3601' style='display:none'>VarClass</span>
<span id='database_3602' style='display:none'>Projection-based Polish Dependency Treebank</span>
<span id='database_3603' style='display:none'>RSS-ToBI</span>
<span id='database_3604' style='display:none'>Modeling Textual Organization (MTO) Corpus</span>
<span id='database_3605' style='display:none'>Directional corpora in Europarl</span>
<span id='database_3606' style='display:none'>DBpedia multilingual chapters inconsistencies (pilot annotation)</span>
<span id='database_3607' style='display:none'>FAU Aibo</span>
<span id='database_3608' style='display:none'>Sentence-level aligned corpora</span>
<span id='database_3609' style='display:none'>Arabic Treebank (ATB) Part 3 v 3.2</span>
<span id='database_3610' style='display:none'>Ceramica</span>
<span id='database_3611' style='display:none'>Creagest</span>
<span id='database_3612' style='display:none'>Hurricane Irene 2011 Dataset</span>
<span id='database_3613' style='display:none'>MaltConverter</span>
<span id='database_3614' style='display:none'>event annotated ontonotes</span>
<span id='database_3615' style='display:none'>Behavior Language Corpus Database</span>
<span id='database_3616' style='display:none'>Arabic Subcategorization Frames in the Arabic Treebank</span>
<span id='database_3617' style='display:none'>Cross Language Entity Linking in 21 Languages (XLEL-21)</span>
<span id='database_3618' style='display:none'>PASSAGE cpcv3</span>
<span id='database_3619' style='display:none'>CDS dataset</span>
<span id='database_3620' style='display:none'>TSUBAKI document collection</span>
<span id='database_3621' style='display:none'>TIGERexp</span>
<span id='database_3622' style='display:none'>GENIA Ontology</span>
<span id='database_3623' style='display:none'>Concept mapping table between video and text</span>
<span id='database_3624' style='display:none'>Estonian reference Corpus</span>
<span id='database_3625' style='display:none'>NAIST Japanese Dictionary</span>
<span id='database_3626' style='display:none'>English Wiktionary</span>
<span id='database_3627' style='display:none'>LDC2007T23 GALE Phase 1 Chinese Broadcast News Parallel Text</span>
<span id='database_3628' style='display:none'>Newstrain-08</span>
<span id='database_3629' style='display:none'>The Switchboard-1 Telephone Speech Corpus</span>
<span id='database_3630' style='display:none'>DINASTI</span>
<span id='database_3631' style='display:none'>sentence tagger</span>
<span id='database_3632' style='display:none'>Infomat</span>
<span id='database_3633' style='display:none'>EMU Speech Database System</span>
<span id='database_3634' style='display:none'>DEFT Textual Entailment Corpus</span>
<span id='database_3635' style='display:none'>TREC Genomics 2006 and 2007</span>
<span id='database_3636' style='display:none'>VOCE data collection platform</span>
<span id='database_3637' style='display:none'>BAMDES</span>
<span id='database_3638' style='display:none'>SAMA</span>
<span id='database_3639' style='display:none'>ABBYY FineReader 10</span>
<span id='database_3640' style='display:none'>RelProp Adjective Corpus</span>
<span id='database_3641' style='display:none'>Yago</span>
<span id='database_3642' style='display:none'>Czech-English Parallel Corpus (CzEng) 1.0</span>
<span id='database_3643' style='display:none'>TAHA</span>
<span id='database_3644' style='display:none'>Elhuyar Basque-English dictionary</span>
<span id='database_3645' style='display:none'>Konkani Verb Paradigm Repository</span>
<span id='database_3646' style='display:none'>CorpusSearch2</span>
<span id='database_3647' style='display:none'>Irony detection</span>
<span id='database_3648' style='display:none'>Proprietory Interactive Voice Response Data</span>
<span id='database_3649' style='display:none'>MPC -- multii-party chat corpus</span>
<span id='database_3650' style='display:none'>SwitchBoard Corpus</span>
<span id='database_3651' style='display:none'>Runeberg project</span>
<span id='database_3652' style='display:none'>apertium-es-an</span>
<span id='database_3653' style='display:none'>Just.Ask</span>
<span id='database_3654' style='display:none'>TypeCraft</span>
<span id='database_3655' style='display:none'>Temporal Entailment Rules for RDFS and OWL-Horst dialect</span>
<span id='database_3656' style='display:none'>Language Editing Dataset of Academic Texts</span>
<span id='database_3657' style='display:none'>Similarity-based Pseudowords</span>
<span id='database_3658' style='display:none'>Corpus Chaines de Reference (CoChainRef)</span>
<span id='database_3659' style='display:none'>Percy Online Experiments</span>
<span id='database_3660' style='display:none'>RankLib</span>
<span id='database_3661' style='display:none'>WordNet Atlas</span>
<span id='database_3662' style='display:none'>Dutch Grapheme-to-phoneme Converter</span>
<span id='database_3663' style='display:none'>Dagbani Corpus</span>
<span id='database_3664' style='display:none'>Malay Emotional Speech Database (MESD)</span>
<span id='database_3665' style='display:none'>Enron Corpus</span>
<span id='database_3666' style='display:none'>Corpus of Czech sentences with manually annotated clause boundaries</span>
<span id='database_3667' style='display:none'>Textual Emigration Analysis (TEA)</span>
<span id='database_3668' style='display:none'>Khresmoi Query Translation Test Data for the Medical Domain version 1.0</span>
<span id='database_3669' style='display:none'>Annotation style guide for the Blinker Project</span>
<span id='database_3670' style='display:none'>Generator of Referring Expressions via Refinements</span>
<span id='database_3671' style='display:none'>Software</span>
<span id='database_3672' style='display:none'>FAUSS</span>
<span id='database_3673' style='display:none'>Conventionalized Metaphors</span>
<span id='database_3674' style='display:none'>TED data</span>
<span id='database_3675' style='display:none'>TEI (Text Encoding Initiative)</span>
<span id='database_3676' style='display:none'>Kyoto Ontology</span>
<span id='database_3677' style='display:none'>International Workshop on Spoken Language Translation</span>
<span id='database_3678' style='display:none'>German political news corpus</span>
<span id='database_3679' style='display:none'>French Social Media Bank</span>
<span id='database_3680' style='display:none'>Kaldi</span>
<span id='database_3681' style='display:none'>Spanish-English Europarl</span>
<span id='database_3682' style='display:none'>PANACEA Environment English monolingual corpus</span>
<span id='database_3683' style='display:none'>CRAFT Corpus</span>
<span id='database_3684' style='display:none'>Spoken Opinions</span>
<span id='database_3685' style='display:none'>MultiTwit</span>
<span id='database_3686' style='display:none'>Prague Labeller</span>
<span id='database_3687' style='display:none'>Potential opinion bearing unigrams from editorial corpus</span>
<span id='database_3688' style='display:none'>Coreference resolution data in opinion mining domain</span>
<span id='database_3689' style='display:none'>SALSA Corpus</span>
<span id='database_3690' style='display:none'>Casual English Conversion System Database</span>
<span id='database_3691' style='display:none'>Finnish national foreign language certificate (FSD) corpus</span>
<span id='database_3692' style='display:none'>ESD: ERG Semantic Documentation</span>
<span id='database_3693' style='display:none'>Quaero NE patent corpus</span>
<span id='database_3694' style='display:none'>AffectiveTask SemEval2007</span>
<span id='database_3695' style='display:none'>The Multimillion Q&A Pair Collection</span>
<span id='database_3696' style='display:none'>FBIS Chinese English parallel corpus</span>
<span id='database_3697' style='display:none'>LTP</span>
<span id='database_3698' style='display:none'>Crowd-Key-phrases</span>
<span id='database_3699' style='display:none'>Coreference Annotation of ACL Anthology papers</span>
<span id='database_3700' style='display:none'>Veteran Tapes</span>
<span id='database_3701' style='display:none'>ANCOR_Centre</span>
<span id='database_3702' style='display:none'>Chinese PennTreebank</span>
<span id='database_3703' style='display:none'>SeCo-600</span>
<span id='database_3704' style='display:none'>KOSAC: Korean Sentiment Analysis Corpus</span>
<span id='database_3705' style='display:none'>LinES guidelines</span>
<span id='database_3706' style='display:none'>Customer Product Reviews Corpus (CPRC)</span>
<span id='database_3707' style='display:none'>Scheherazade</span>
<span id='database_3708' style='display:none'>EDBL : Lexical database for Basque</span>
<span id='database_3709' style='display:none'>IPI PAN Corpus of Polish (manually disambiguated part)</span>
<span id='database_3710' style='display:none'>TAC 2008 Update Summarization</span>
<span id='database_3711' style='display:none'>Syntactic lexicon of Arabic verbs</span>
<span id='database_3712' style='display:none'>EUSEMCOR</span>
<span id='database_3713' style='display:none'>RST Discourse Treebank</span>
<span id='database_3714' style='display:none'>EventSpotter</span>
<span id='database_3715' style='display:none'>XuxenB</span>
<span id='database_3716' style='display:none'>Framenet</span>
<span id='database_3717' style='display:none'>DiLAF African languages-French dictionaries</span>
<span id='database_3718' style='display:none'>Factuality and Opinions</span>
<span id='database_3719' style='display:none'>GNU Linear Programming Kit (GLPK)</span>
<span id='database_3720' style='display:none'>Ent2Wiki</span>
<span id='database_3721' style='display:none'>Twinity</span>
<span id='database_3722' style='display:none'>A Corpus of Opinion Attributes in Polish</span>
<span id='database_3723' style='display:none'>Scripts for cleaning bilingual dictionaries</span>
<span id='database_3724' style='display:none'>RECI</span>
<span id='database_3725' style='display:none'>Stripey Zebra</span>
<span id='database_3726' style='display:none'>Stockholm EPR PHI Corpus</span>
<span id='database_3727' style='display:none'>Multi-perspective question answering corpus (MPQA)</span>
<span id='database_3728' style='display:none'>QuestionBank</span>
<span id='database_3729' style='display:none'>IMF_news</span>
<span id='database_3730' style='display:none'>Kamusi Global Online Living Dictionary</span>
<span id='database_3731' style='display:none'>Etymological Wordnet</span>
<span id='database_3732' style='display:none'>Lexicon totius latinitatis</span>
<span id='database_3733' style='display:none'>Multilingual corpus of 1M Wikipedia documents</span>
<span id='database_3734' style='display:none'>LDC MADCAT Management Web App</span>
<span id='database_3735' style='display:none'>BLIPP 1987 & 1988 corpus</span>
<span id='database_3736' style='display:none'>Jubilee</span>
<span id='database_3737' style='display:none'>Curran and Clark POS Tagger</span>
<span id='database_3738' style='display:none'>Roget's Thesaurus</span>
<span id='database_3739' style='display:none'>Helsinki First Encounters Data</span>
<span id='database_3740' style='display:none'>pattern.it</span>
<span id='database_3741' style='display:none'>Nanyang  Technological  University  Multilingual  Corpus  (NTU-MC)</span>
<span id='database_3742' style='display:none'>Amarakosha</span>
<span id='database_3743' style='display:none'>Empoli e dintorni</span>
<span id='database_3744' style='display:none'>Flexible Error Annotation Tool (feat)</span>
<span id='database_3745' style='display:none'>Obeliks tagger</span>
<span id='database_3746' style='display:none'>Ester 2 Named Entity Corpus</span>
<span id='database_3747' style='display:none'>LiveMemories Corpus for Italian</span>
<span id='database_3748' style='display:none'>MOAT</span>
<span id='database_3749' style='display:none'>Medical Subject Headings (MeSH)</span>
<span id='database_3750' style='display:none'>Alpino Treebank</span>
<span id='database_3751' style='display:none'>Nordic multimodal Corpus (NOMCO)</span>
<span id='database_3752' style='display:none'>GATE Morphological Analyser  (part of the GATE system)</span>
<span id='database_3753' style='display:none'>ConceptNet 5</span>
<span id='database_3754' style='display:none'>Sentiment-annotated set of quotations</span>
<span id='database_3755' style='display:none'>Croatian CG Morphological Tagger</span>
<span id='database_3756' style='display:none'>LAT Bridge</span>
<span id='database_3757' style='display:none'>Language Tag</span>
<span id='database_3758' style='display:none'>American Sign Language Lexicon Video Dataset</span>
<span id='database_3759' style='display:none'>DOME</span>
<span id='database_3760' style='display:none'>Review corpus annotated for speculation and negation</span>
<span id='database_3761' style='display:none'>ROMBAC</span>
<span id='database_3762' style='display:none'>Reuters Corpus, Volume 2 (RCV2) Multilingual</span>
<span id='database_3763' style='display:none'>CLP2010 Testing Dataset of the Chinese Word Sense Induction Task</span>
<span id='database_3764' style='display:none'>METU Turkish Corpus</span>
<span id='database_3765' style='display:none'>Indonesian Subjectivity (Sentiment) Lexicon</span>
<span id='database_3766' style='display:none'>Standoff Multi-Microphone Speech Corpus,</span>
<span id='database_3767' style='display:none'>TempEval-2</span>
<span id='database_3768' style='display:none'>LIME (Linguistic Metadata Vocabulary)</span>
<span id='database_3769' style='display:none'>Multiple-Chinese Translation Part 4 (MTC4) dataset</span>
<span id='database_3770' style='display:none'>JRC Acquis Latvian-English parallel corpus</span>
<span id='database_3771' style='display:none'>Joint Research Centre (JRC) Eurovoc Indexer JEX</span>
<span id='database_3772' style='display:none'>Basic Travel Expression Corpus</span>
<span id='database_3773' style='display:none'>veneto-english parallel corpus</span>
<span id='database_3774' style='display:none'>CLE Urdu Digest Corpus</span>
<span id='database_3775' style='display:none'>MEDLINE/PUBMED</span>
<span id='database_3776' style='display:none'>Romanian TimeBank corpus</span>
<span id='database_3777' style='display:none'>WMT12 Data</span>
<span id='database_3778' style='display:none'>AMICA Medical Dialogue Corpus</span>
<span id='database_3779' style='display:none'>Onto.PT</span>
<span id='database_3780' style='display:none'>questioncorpus.pt</span>
<span id='database_3781' style='display:none'>Persian Dependenc Treebank</span>
<span id='database_3782' style='display:none'>WMT 2009 dataset</span>
<span id='database_3783' style='display:none'>REMBRANDT</span>
<span id='database_3784' style='display:none'>Columbia Arabic Treebank Converter</span>
<span id='database_3785' style='display:none'>The GENETAG corpus</span>
<span id='database_3786' style='display:none'>Wikipedia Vandalism Corpus WEBIS-VC07-11</span>
<span id='database_3787' style='display:none'>An Annotated, Multilingual Parallel Corpus for Hybrid Machine Translation</span>
<span id='database_3788' style='display:none'>Java WordNet::Similarity (beta)</span>
<span id='database_3789' style='display:none'>Koshik Source Code</span>
<span id='database_3790' style='display:none'>French Tree Bank</span>
<span id='database_3791' style='display:none'>The Balanced Corpus of Contemporary Written Japanese</span>
<span id='database_3792' style='display:none'>A text corpus annotated with usage information</span>
<span id='database_3793' style='display:none'>foma</span>
<span id='database_3794' style='display:none'>OntoLing's ontologies</span>
<span id='database_3795' style='display:none'>Evaluation Benchmark for Bilingual Lexicon Extraction</span>
<span id='database_3796' style='display:none'>PAROLE-SIMPLE-CLIPS PISA</span>
<span id='database_3797' style='display:none'>TnT</span>
<span id='database_3798' style='display:none'>GALE_Chinese_WA_tagging_guidelines</span>
<span id='database_3799' style='display:none'>Gyaan Nidhi</span>
<span id='database_3800' style='display:none'>AnCoraPipe</span>
<span id='database_3801' style='display:none'>MIR-1K</span>
<span id='database_3802' style='display:none'>mstparser</span>
<span id='database_3803' style='display:none'>Aozora Bunko</span>
<span id='database_3804' style='display:none'>PAC (Predicate Argument Clustering)</span>
<span id='database_3805' style='display:none'>Service Quality Evaluation Data Set</span>
<span id='database_3806' style='display:none'>WordNet Spectrums and Sensory Words</span>
<span id='database_3807' style='display:none'>HCRC Map Task Corpus</span>
<span id='database_3808' style='display:none'>Enron Emails</span>
<span id='database_3809' style='display:none'>Robertino-game corpus</span>
<span id='database_3810' style='display:none'>Lyrics&Notes</span>
<span id='database_3811' style='display:none'>HCS vLab</span>
<span id='database_3812' style='display:none'>SEMAINE corpus headnods and shakes</span>
<span id='database_3813' style='display:none'>Persian Syntactic Dependency Treebank</span>
<span id='database_3814' style='display:none'>CLE Urdu POS Tagset</span>
<span id='database_3815' style='display:none'>RODRIGO</span>
<span id='database_3816' style='display:none'>Named Entity Recognition Corpus from the Fourth International SIGHAN Bakeoff Data Sets</span>
<span id='database_3817' style='display:none'>Multimodal Filtering tool</span>
<span id='database_3818' style='display:none'>OSS Online Communication Messages</span>
<span id='database_3819' style='display:none'>Romanian Speech Synthesis corpus</span>
<span id='database_3820' style='display:none'>CoNLL 2003 NER shared task corpus</span>
<span id='database_3821' style='display:none'>TEOK</span>
<span id='database_3822' style='display:none'>CombiTagger</span>
<span id='database_3823' style='display:none'>EMM NewsBrief</span>
<span id='database_3824' style='display:none'>ANNIE</span>
<span id='database_3825' style='display:none'>A Toolkit for Efficient Learning of Lexical Units for Speech Recognition</span>
<span id='database_3826' style='display:none'>WSJ acoustic and language models</span>
<span id='database_3827' style='display:none'>GraPAT (Graph-based Potsdam Annotation Tool)</span>
<span id='database_3828' style='display:none'>opennlp</span>
<span id='database_3829' style='display:none'>C-ORAL-CHINA</span>
<span id='database_3830' style='display:none'>Grammar error ratio</span>
<span id='database_3831' style='display:none'>Multilingual summary evaluation data</span>
<span id='database_3832' style='display:none'>goo300k corpus of historical Slovene</span>
<span id='database_3833' style='display:none'>Annotated Intellectual Property Claims in Complaint Documents</span>
<span id='database_3834' style='display:none'>WSD-IXA</span>
<span id='database_3835' style='display:none'>Tigrinya Word lexicon</span>
<span id='database_3836' style='display:none'>MPAligner</span>
<span id='database_3837' style='display:none'>Japanese Wikipedia</span>
<span id='database_3838' style='display:none'>Japanese Particle Corpus</span>
<span id='database_3839' style='display:none'>Task-10 test and training data Semeval 2010</span>
<span id='database_3840' style='display:none'>Multimodal corpus of cognitively disabled in HRI with NAO</span>
<span id='database_3841' style='display:none'>Image description Corpus</span>
<span id='database_3842' style='display:none'>Yahoo Chiebukuro Corpus Humor Index</span>
<span id='database_3843' style='display:none'>pantera-tagger</span>
<span id='database_3844' style='display:none'>Universal Word - Hindi Dictionary</span>
<span id='database_3845' style='display:none'>FISCALDB</span>
<span id='database_3846' style='display:none'>Telltale trips</span>
<span id='database_3847' style='display:none'>Yet Another Multipurpose CHunk Annotator (YamCha)</span>
<span id='database_3848' style='display:none'>Base de datos sintácticos</span>
<span id='database_3849' style='display:none'>NICT Kyoto tour guide dialogue corpus</span>
<span id='database_3850' style='display:none'>Edit Distance Textual Entailment Suite (EDITS)</span>
<span id='database_3851' style='display:none'>Audio-Visual Corpus (3D Faces and Speech)</span>
<span id='database_3852' style='display:none'>OpinionFinder Subjecitivity Lexicon</span>
<span id='database_3853' style='display:none'>Term-minator</span>
<span id='database_3854' style='display:none'>The Accents of the British Isles (ABI-1) Speech Corpus</span>
<span id='database_3855' style='display:none'>German Food Relation Database</span>
<span id='database_3856' style='display:none'>chunking-synaf-en</span>
<span id='database_3857' style='display:none'>Japanese Dependency Corpus</span>
<span id='database_3858' style='display:none'>Italian Webcorpus</span>
<span id='database_3859' style='display:none'>LESLLA</span>
<span id='database_3860' style='display:none'>IEEE Corpus</span>
<span id='database_3861' style='display:none'>Narrativity-Annotation</span>
<span id='database_3862' style='display:none'>WebCAGe</span>
<span id='database_3863' style='display:none'>MEAD Summarization Tool</span>
<span id='database_3864' style='display:none'>"Yahoo! Chiebukuro" data</span>
<span id='database_3865' style='display:none'>AVALON</span>
<span id='database_3866' style='display:none'>Has not been named yet</span>
<span id='database_3867' style='display:none'>Lazy Hypergraph Search</span>
<span id='database_3868' style='display:none'>Error-Annotated German Learner Corpus (EAGLE)</span>
<span id='database_3869' style='display:none'>JuriDico</span>
<span id='database_3870' style='display:none'>DYNEMO: A corpus of dynamic and spontaneous emotional facial expressions</span>
<span id='database_3871' style='display:none'>Speedi-db</span>
<span id='database_3872' style='display:none'>WinPitch</span>
<span id='database_3873' style='display:none'>Japanese word user ratio</span>
<span id='database_3874' style='display:none'>Galician lexicon</span>
<span id='database_3875' style='display:none'>A Gesture Analysis and Modeling Tool for ANVIL (GAnTool)</span>
<span id='database_3876' style='display:none'>TPE</span>
<span id='database_3877' style='display:none'>MIM</span>
<span id='database_3878' style='display:none'>Phonologie du Français contemporain</span>
<span id='database_3879' style='display:none'>Fisher English Training Speech</span>
<span id='database_3880' style='display:none'>AraNLP</span>
<span id='database_3881' style='display:none'>The Global Voices Report Linking Corpus</span>
<span id='database_3882' style='display:none'>Web-based Bengali news corpus</span>
<span id='database_3883' style='display:none'>Humor Morphological Analyzer</span>
<span id='database_3884' style='display:none'>MicroKnowing: Microconceptual-Knowledge Spreading</span>
<span id='database_3885' style='display:none'>NBA video pages collection</span>
<span id='database_3886' style='display:none'>Plagiarism Detection Evaluation</span>
<span id='database_3887' style='display:none'>BEST</span>
<span id='database_3888' style='display:none'>TreeLex</span>
<span id='database_3889' style='display:none'>Body part ontology</span>
<span id='database_3890' style='display:none'>HandAlign</span>
<span id='database_3891' style='display:none'>Ren-CECps 1.0</span>
<span id='database_3892' style='display:none'>SoNaR Named Entities Annotatierichtlijnen</span>
<span id='database_3893' style='display:none'>Lin-EBMT^REC+</span>
<span id='database_3894' style='display:none'>KALAKA</span>
<span id='database_3895' style='display:none'>asa-sentiment-analysis</span>
<span id='database_3896' style='display:none'>Xelda</span>
<span id='database_3897' style='display:none'>Wapiti</span>
<span id='database_3898' style='display:none'>MCPG</span>
<span id='database_3899' style='display:none'>Terminological Resource for Crisis Management</span>
<span id='database_3900' style='display:none'>Entity Similarity Evaluation Data</span>
<span id='database_3901' style='display:none'>UTEP-ICT Cross-Cultural Multiparty Multimodal Dialog Corpus</span>
<span id='database_3902' style='display:none'>Annotated corpus of German political news</span>
<span id='database_3903' style='display:none'>Automatic Statistical SEmantic Role Tagger (ASSERT)</span>
<span id='database_3904' style='display:none'>Lemmald</span>
<span id='database_3905' style='display:none'>International Corpus of Portuguese (CINTIL)</span>
<span id='database_3906' style='display:none'>mogura HPSG parser</span>
<span id='database_3907' style='display:none'>Enabling Minority Language Engineering (EMILLE)</span>
<span id='database_3908' style='display:none'>FrameNet and WordNet</span>
<span id='database_3909' style='display:none'>BEEP Lexicon</span>
<span id='database_3910' style='display:none'>Sign verification system</span>
<span id='database_3911' style='display:none'>A free/open-source marker-driven example-based machine translation system (OpenMaTrEx)</span>
<span id='database_3912' style='display:none'>Nordic Syntactic Judgments Database</span>
<span id='database_3913' style='display:none'>The Ellogon language engineering platform</span>
<span id='database_3914' style='display:none'>Normalizer</span>
<span id='database_3915' style='display:none'>Penn TreeBank</span>
<span id='database_3916' style='display:none'>the Penn Arabic Treebank (Part 1 v. 2.0)</span>
<span id='database_3917' style='display:none'>Institute for Signal Processing (ISIP) environmental noise signals</span>
<span id='database_3918' style='display:none'>Cross-lingual WSD Benchmark Data Set</span>
<span id='database_3919' style='display:none'>A taxonomy of discourse (coherence) relations</span>
<span id='database_3920' style='display:none'>CELEX</span>
<span id='database_3921' style='display:none'>FAUST quality assessments</span>
<span id='database_3922' style='display:none'>Sign Language Pose Estimation 3D Parse Trees</span>
<span id='database_3923' style='display:none'>General Ontology for Linguistic Description (GOLD)</span>
<span id='database_3924' style='display:none'>BioGRID</span>
<span id='database_3925' style='display:none'>Chinese Learner English Corpus</span>
<span id='database_3926' style='display:none'>Turkish NER Gazetteers</span>
<span id='database_3927' style='display:none'>GermanSentimentData</span>
<span id='database_3928' style='display:none'>Wiktionary lexical network</span>
<span id='database_3929' style='display:none'>iPeen Corpus</span>
<span id='database_3930' style='display:none'>LoonyBin</span>
<span id='database_3931' style='display:none'>Evaluation Annotated/Unannotated data and evaluation code for NP coordination disambiguation</span>
<span id='database_3932' style='display:none'>The Nordic Dialect Corpus</span>
<span id='database_3933' style='display:none'>MULTIPHONIA</span>
<span id='database_3934' style='display:none'>Retokenized Europarl</span>
<span id='database_3935' style='display:none'>Corpus Annotator (CorA)</span>
<span id='database_3936' style='display:none'>Inspec Database Keyword Extraction Data Set</span>
<span id='database_3937' style='display:none'>NLTK package</span>
<span id='database_3938' style='display:none'>RPM2 Summarization and Sentence Compression Corpora</span>
<span id='database_3939' style='display:none'>Web 2.0 Treebank</span>
<span id='database_3940' style='display:none'>Charniak and Charniak Johnson Parser</span>
<span id='database_3941' style='display:none'>PiNER</span>
<span id='database_3942' style='display:none'>CALL-SLT</span>
<span id='database_3943' style='display:none'>AMARA Corpus</span>
<span id='database_3944' style='display:none'>Iban-Malay Lexicon</span>
<span id='database_3945' style='display:none'>WMBT</span>
<span id='database_3946' style='display:none'>Freepal</span>
<span id='database_3947' style='display:none'>tSearch</span>
<span id='database_3948' style='display:none'>Postech Learner Corpus (POLC)</span>
<span id='database_3949' style='display:none'>Joint Research Centre (JRC) Names</span>
<span id='database_3950' style='display:none'>The Eurogene ontology of human genetics</span>
<span id='database_3951' style='display:none'>Qatar Arabic Language Bank (QALB)</span>
<span id='database_3952' style='display:none'>EUR-LEX translation memory</span>
<span id='database_3953' style='display:none'>Indic Language Transliteration Data</span>
<span id='database_3954' style='display:none'>Lancaster-Oslo/Bergen (LOB) Corpus</span>
<span id='database_3955' style='display:none'>People's Daily Corpus</span>
<span id='database_3956' style='display:none'>JWPL/JWKTL</span>
<span id='database_3957' style='display:none'>Japanese Corpus of Diverse Document Leads with Anaphoric Annotation</span>
<span id='database_3958' style='display:none'>The Arabic drug corpus</span>
<span id='database_3959' style='display:none'>SemEval-2007 Task 17</span>
<span id='database_3960' style='display:none'>Paraphrase Fragment Corpus</span>
<span id='database_3961' style='display:none'>Fpgrowth Algorithm Implementation</span>
<span id='database_3962' style='display:none'>Anaphora resolution data for Bangla</span>
<span id='database_3963' style='display:none'>Japanese Word Dependency Corpus</span>
<span id='database_3964' style='display:none'>Parallel Text Corpora for 3 SA language pairs</span>
<span id='database_3965' style='display:none'>Greek POS Tagger</span>
<span id='database_3966' style='display:none'>Bulgarian Morphological Dictionary</span>
<span id='database_3967' style='display:none'>Sherlock</span>
<span id='database_3968' style='display:none'>Easy-first coreference resolver</span>
<span id='database_3969' style='display:none'>Geography Dictionaries</span>
<span id='database_3970' style='display:none'>The Revised Chinese Dictionary</span>
<span id='database_3971' style='display:none'>SAPIENT:Semantic Annotation of Papers Interface and Enrichment Tool</span>
<span id='database_3972' style='display:none'>Walenty</span>
<span id='database_3973' style='display:none'>Powerset Gold-Parsed Wikipedia Corpus</span>
<span id='database_3974' style='display:none'>cSR</span>
<span id='database_3975' style='display:none'>Icelandic Parsed Historical Corpus (IcePaHC)</span>
<span id='database_3976' style='display:none'>VoLIP</span>
<span id='database_3977' style='display:none'>MetricsMATR08 development data</span>
<span id='database_3978' style='display:none'>Spontal</span>
<span id='database_3979' style='display:none'>Concord-ED</span>
<span id='database_3980' style='display:none'>corpus_shell</span>
<span id='database_3981' style='display:none'>BananaSplit</span>
<span id='database_3982' style='display:none'>Amazon Mechanical Turk</span>
<span id='database_3983' style='display:none'>Polish Shadow Library Metadata</span>
<span id='database_3984' style='display:none'>Bilingual dictionaries</span>
<span id='database_3985' style='display:none'>ACL Anthology Network Corpus</span>
<span id='database_3986' style='display:none'>USAGE Corpus</span>
<span id='database_3987' style='display:none'>Uighur to Chinese Dictionary Database</span>
<span id='database_3988' style='display:none'>Arabic Treebank Part 5 V1.0</span>
<span id='database_3989' style='display:none'>Wikinews Multidocument Summarization Data Collection Tool</span>
<span id='database_3990' style='display:none'>Eventiveness Relative Weight Lexicon</span>
<span id='database_3991' style='display:none'>OSCAR</span>
<span id='database_3992' style='display:none'>HTML and PDF text and metadata extractors</span>
<span id='database_3993' style='display:none'>Hindi Discourse Relation Bank</span>
<span id='database_3994' style='display:none'>English Penn Treebank, Chinese Penn Treebank</span>
<span id='database_3995' style='display:none'>pronunciation error annotation tool</span>
<span id='database_3996' style='display:none'>CST Corpus of Synsicate-labour terminology</span>
<span id='database_3997' style='display:none'>COAE2008-task3</span>
<span id='database_3998' style='display:none'>ACE 2005 Arabic</span>
<span id='database_3999' style='display:none'>ISO 24617-2 Semantic annotation framework, Part 2: Dialogue acts</span>
<span id='database_4000' style='display:none'>RWSCor</span>
<span id='database_4001' style='display:none'>The German-Russian Parallel Corpus of Sigmund Freudâ€™s â€žThe Interpretation of Dreamsâ€œ</span>
<span id='database_4002' style='display:none'>Pronunciation Errors from Learners of English Coprus and Annotation (PELECAN)</span>
<span id='database_4003' style='display:none'>LDC Data Exploration Toolkit</span>
<span id='database_4004' style='display:none'>HTMLAsText v1.11</span>
<span id='database_4005' style='display:none'>DPC: Dutch Parallel Corpus</span>
<span id='database_4006' style='display:none'>Lexicon-Grammar tables</span>
<span id='database_4007' style='display:none'>AnIta</span>
<span id='database_4008' style='display:none'>Parallel sentences for error detection and correction from WIkipedia 2012 and 2013</span>
<span id='database_4009' style='display:none'>MARIE</span>
<span id='database_4010' style='display:none'>Cross Language Translation and Retrieval</span>
<span id='database_4011' style='display:none'>Corpus for Cross-Sentence Relation Extraction</span>
<span id='database_4012' style='display:none'>West African Language Archive (WALA)</span>
<span id='database_4013' style='display:none'>Penn Chinese Treebank 5</span>
<span id='database_4014' style='display:none'>The TDU table talk corpus 2007</span>
<span id='database_4015' style='display:none'>Translation Annotator</span>
<span id='database_4016' style='display:none'>Accentological corpus of Russian</span>
<span id='database_4017' style='display:none'>mteval scoring script</span>
<span id='database_4018' style='display:none'>SenSem</span>
<span id='database_4019' style='display:none'>Estonian lexicon in Grammatical Framework</span>
<span id='database_4020' style='display:none'>ADL NLP Analytic Tool</span>
<span id='database_4021' style='display:none'>Connexor machines syntax</span>
<span id='database_4022' style='display:none'>Bengali NEWS Editorial Opinion</span>
<span id='database_4023' style='display:none'>CPROD1.1</span>
<span id='database_4024' style='display:none'>Name and Location Clusters</span>
<span id='database_4025' style='display:none'>Text Analysis Conference</span>
<span id='database_4026' style='display:none'>JavaRAP</span>
<span id='database_4027' style='display:none'>Uyghur-Kazahk One-to-One Mapping Bilingual Dictionary</span>
<span id='database_4028' style='display:none'>multidisciplinary medical meetings</span>
<span id='database_4029' style='display:none'>NameDat</span>
<span id='database_4030' style='display:none'>Diccionario Clave de Uso Del Español Actual</span>
<span id='database_4031' style='display:none'>TAC 2011 AESOP Task</span>
<span id='database_4032' style='display:none'>Sandhi Parallel Corpus</span>
<span id='database_4033' style='display:none'>Penn Chinese Treebank 5.1</span>
<span id='database_4034' style='display:none'>SummTerm</span>
<span id='database_4035' style='display:none'>BrandPitt</span>
<span id='database_4036' style='display:none'>FFV Spectrum Computation in Snack Sound Toolkit</span>
<span id='database_4037' style='display:none'>Czech-Russian Valency Lexicon</span>
<span id='database_4038' style='display:none'>Javadoc doclet corpus generatio tool</span>
<span id='database_4039' style='display:none'>A Multi-layered Reference Corpus for German Sentiment Analysis (MLSA)</span>
<span id='database_4040' style='display:none'>European name models</span>
<span id='database_4041' style='display:none'>MPQA</span>
<span id='database_4042' style='display:none'>Amazon.com Review Rating Prediction Dataset</span>
<span id='database_4043' style='display:none'>French FrameNet built with Bilingual Dictionaries : FR.FrameNet.BiDic</span>
<span id='database_4044' style='display:none'>Next Generation Localisation Process Map</span>
<span id='database_4045' style='display:none'>A fragment of Northern Sotho grammar: The verb of Northern Sotho</span>
<span id='database_4046' style='display:none'>SemInVeSt (Semantically Interpreted Verb-centred Structures)</span>
<span id='database_4047' style='display:none'>Emotion holder Annotator</span>
<span id='database_4048' style='display:none'>Data categories for communicative functions</span>
<span id='database_4049' style='display:none'>Medisys</span>
<span id='database_4050' style='display:none'>CoreLex</span>
<span id='database_4051' style='display:none'>Open Mind Indoor Common Sense (OMICS)</span>
<span id='database_4052' style='display:none'>Relative Entropy Pruner</span>
<span id='database_4053' style='display:none'>Topic Coherence Dataset-300</span>
<span id='database_4054' style='display:none'>FN transformer</span>
<span id='database_4055' style='display:none'>LEXCONN</span>
<span id='database_4056' style='display:none'>Annodis corpus</span>
<span id='database_4057' style='display:none'>Boundary-Annotated Qur'an</span>
<span id='database_4058' style='display:none'>RST Spanish Treebank</span>
<span id='database_4059' style='display:none'>Chinese to Kazahk Bilingual Dictionary</span>
<span id='database_4060' style='display:none'>Alignment of hashtags between English and French tweets</span>
<span id='database_4061' style='display:none'>NIST 2009 MT Evaluation Set</span>
<span id='database_4062' style='display:none'>Kyoto University's Case Frame Data 1.0</span>
<span id='database_4063' style='display:none'>ADAM</span>
<span id='database_4064' style='display:none'>Turin University Treebank (TUT)</span>
<span id='database_4065' style='display:none'>UK PubMed Central (UKPMC)</span>
<span id='database_4066' style='display:none'>Mörkuð íslensk málheild (MÍM)</span>
<span id='database_4067' style='display:none'>DBpedia Wiktionary</span>
<span id='database_4068' style='display:none'>HamleDT 2.0</span>
<span id='database_4069' style='display:none'>Konkani verb paradigm repository</span>
<span id='database_4070' style='display:none'>A Japanese corpus annotated with labels in a scheme of extended modality</span>
<span id='database_4071' style='display:none'>PWKP</span>
<span id='database_4072' style='display:none'>ydta-yanswers-manner-questions-v1.0</span>
<span id='database_4073' style='display:none'>Stanford Chinese Segmenter</span>
<span id='database_4074' style='display:none'>LX-Parser Webservice</span>
<span id='database_4075' style='display:none'>Wall Street Journal (WSJ)</span>
<span id='database_4076' style='display:none'>LDC Multiple English translation pairs</span>
<span id='database_4077' style='display:none'>FSR - Feature Structure Representation (ISO 24610-1)</span>
<span id='database_4078' style='display:none'>Test Audio Data for Repeatition Detection</span>
<span id='database_4079' style='display:none'>MRI-TIMIT Database</span>
<span id='database_4080' style='display:none'>A system for automatically identifying changes in the semantic orientation of words</span>
<span id='database_4081' style='display:none'>TAC KBP 2009 Data</span>
<span id='database_4082' style='display:none'>Australian National Corpus</span>
<span id='database_4083' style='display:none'>SubTle</span>
<span id='database_4084' style='display:none'>Vystadial 2013 – English data</span>
<span id='database_4085' style='display:none'>Lwazi primary pronunciation dictionaries v1.0</span>
<span id='database_4086' style='display:none'>Synmttk</span>
<span id='database_4087' style='display:none'>Chintang audiovisual corpus</span>
<span id='database_4088' style='display:none'>TAC 2009 KBP Gold Standard Entity Linking Entity Type list</span>
<span id='database_4089' style='display:none'>HIWIRE (Human Input that Works In Real Environments) database</span>
<span id='database_4090' style='display:none'>Time4SMS</span>
<span id='database_4091' style='display:none'>NIKKEI_BP</span>
<span id='database_4092' style='display:none'>Semistructured Reviews</span>
<span id='database_4093' style='display:none'>Graph Based Word Sense Disambiguation and Similarity (UKB)</span>
<span id='database_4094' style='display:none'>German Voice Services Agender Database</span>
<span id='database_4095' style='display:none'>Sandhi Rules</span>
<span id='database_4096' style='display:none'>AZ-II annotation guidelines</span>
<span id='database_4097' style='display:none'>A Parallel Corpus of Monologues and Expository Dialogues</span>
<span id='database_4098' style='display:none'>Penn Arabic Treebank 2</span>
<span id='database_4099' style='display:none'>Nordisk Språkteknologi (NST) corpus</span>
<span id='database_4100' style='display:none'>French treebank converted into dependencies</span>
<span id='database_4101' style='display:none'>Basque Web Corpus</span>
<span id='database_4102' style='display:none'>GATE resources for sarcasm detection</span>
<span id='database_4103' style='display:none'>Event Extractor</span>
<span id='database_4104' style='display:none'>POETICON CORPUS</span>
<span id='database_4105' style='display:none'>Squoia Treebank</span>
<span id='database_4106' style='display:none'>corte-e-costura</span>
<span id='database_4107' style='display:none'>Arabic-English Parallel Aligned Treebanks</span>
<span id='database_4108' style='display:none'>Famous French speakers datasets</span>
<span id='database_4109' style='display:none'>BerkeleyParser</span>
<span id='database_4110' style='display:none'>Europarl v.6</span>
<span id='database_4111' style='display:none'>DGT Translation Memory</span>
<span id='database_4112' style='display:none'>MMSEG</span>
<span id='database_4113' style='display:none'>Online Transcription Tool (OTTO)</span>
<span id='database_4114' style='display:none'>A part-of-speech tagger for English</span>
<span id='database_4115' style='display:none'>GrAF</span>
<span id='database_4116' style='display:none'>SUMMA</span>
<span id='database_4117' style='display:none'>DeriNet</span>
<span id='database_4118' style='display:none'>Mongolian speech corpus for speech synthesis (NUM, NITP, NECTEC)</span>
<span id='database_4119' style='display:none'>LexIt</span>
<span id='database_4120' style='display:none'>an online Chinese MeSH</span>
<span id='database_4121' style='display:none'>SentiWordNet 1.0.1</span>
<span id='database_4122' style='display:none'>dbpedia</span>
<span id='database_4123' style='display:none'>nltk</span>
<span id='database_4124' style='display:none'>SO-CAL</span>
<span id='database_4125' style='display:none'>French dysarthric corpus</span>
<span id='database_4126' style='display:none'>JeuxDeMots lexical network</span>
<span id='database_4127' style='display:none'>XTRANS</span>
<span id='database_4128' style='display:none'>Review data set</span>
<span id='database_4129' style='display:none'>LLL</span>
<span id='database_4130' style='display:none'>Simulated Contact Center Dialogues</span>
<span id='database_4131' style='display:none'>MWE Childes corpus</span>
<span id='database_4132' style='display:none'>NTCIR CIRB040</span>
<span id='database_4133' style='display:none'>maltparser</span>
<span id='database_4134' style='display:none'>The USC CreativeIT Database</span>
<span id='database_4135' style='display:none'>Lttoolbox</span>
<span id='database_4136' style='display:none'>Corpora for Named Entity Recognition of Chemical Compounds (IUPAC Corpus)</span>
<span id='database_4137' style='display:none'>The Edwardians: family life and work before 1918</span>
<span id='database_4138' style='display:none'>Official Documents of the Congress of Deputies in XML format</span>
<span id='database_4139' style='display:none'>ANNEX</span>
<span id='database_4140' style='display:none'>BBC News forums data set</span>
<span id='database_4141' style='display:none'>SrpFSD</span>
<span id='database_4142' style='display:none'>The LTH Constituent-to-Dependency Conversion Tool for Penn-style Treebanks</span>
<span id='database_4143' style='display:none'>MWE 2008 data sets</span>
<span id='database_4144' style='display:none'>ESTER</span>
<span id='database_4145' style='display:none'>AMI meeting corpus</span>
<span id='database_4146' style='display:none'>N3-Collection</span>
<span id='database_4147' style='display:none'>Machine translation error identification</span>
<span id='database_4148' style='display:none'>Corpora from the web (COW)</span>
<span id='database_4149' style='display:none'>Yahoo!'s local listings in Chicago</span>
<span id='database_4150' style='display:none'>uima-connectors</span>
<span id='database_4151' style='display:none'>KLAIR Toolkit</span>
<span id='database_4152' style='display:none'>Morphological Paradigms</span>
<span id='database_4153' style='display:none'>Latvian Speech Recognition Corpus</span>
<span id='database_4154' style='display:none'>German Idiomatic PNV-Triples in Context (GIPIC)</span>
<span id='database_4155' style='display:none'>French Framenet</span>
<span id='database_4156' style='display:none'>Chinese Treebank</span>
<span id='database_4157' style='display:none'>Perseus Latin Dependency Treebank</span>
<span id='database_4158' style='display:none'>WordNet 1.5 and 3.0</span>
<span id='database_4159' style='display:none'>Annotation Pro</span>
<span id='database_4160' style='display:none'>Italian FrameNet</span>
<span id='database_4161' style='display:none'>Choose the Right Word Formality Pairs</span>
<span id='database_4162' style='display:none'>New annotations for IAC</span>
<span id='database_4163' style='display:none'>Illinois Wikifier</span>
<span id='database_4164' style='display:none'>Syllabeur-v2.1.jar</span>
<span id='database_4165' style='display:none'>Speech Feature Tool available at Centre for Speech Technology University of Edinburgh</span>
<span id='database_4166' style='display:none'>SMALLWorlds</span>
<span id='database_4167' style='display:none'>UKB: Graph Based Word Sense Disambiguation and Similarity</span>
<span id='database_4168' style='display:none'>European Parliament Proceedings Parallel Corpus 1996-2006</span>
<span id='database_4169' style='display:none'>Czech Digital Mathematics Library, DML-CZ</span>
<span id='database_4170' style='display:none'>Author Gender Analysis of Text</span>
<span id='database_4171' style='display:none'>Annotated diagnostic narratives 1</span>
<span id='database_4172' style='display:none'>eXtended WordNet Domains</span>
<span id='database_4173' style='display:none'>The PDTB XML converter</span>
<span id='database_4174' style='display:none'>ILCI Annotation Tool</span>
<span id='database_4175' style='display:none'>Stanford Lexicalized Parser</span>
<span id='database_4176' style='display:none'>Paraphrase Tables</span>
<span id='database_4177' style='display:none'>Converter Freeling 2 DESR</span>
<span id='database_4178' style='display:none'>Stanford Part-Of-Speech Tagger</span>
<span id='database_4179' style='display:none'>Kres</span>
<span id='database_4180' style='display:none'>The Atlas of Pidgin and Creole Language Structures Online</span>
<span id='database_4181' style='display:none'>Babouk</span>
<span id='database_4182' style='display:none'>LT World Ontology</span>
<span id='database_4183' style='display:none'>English-Swedish word alignment gold standard</span>
<span id='database_4184' style='display:none'>WPSenseReference</span>
<span id='database_4185' style='display:none'>TIPSem</span>
<span id='database_4186' style='display:none'>BRAT</span>
<span id='database_4187' style='display:none'>SrpWN</span>
<span id='database_4188' style='display:none'>The Stanford Parser: A statistical parser</span>
<span id='database_4189' style='display:none'>GermanSentiStrengthLexicon</span>
<span id='database_4190' style='display:none'>CREAM TBX</span>
<span id='database_4191' style='display:none'>Gigaword</span>
<span id='database_4192' style='display:none'>list-question-answering pargraphs</span>
<span id='database_4193' style='display:none'>Web 1T 5-gram Version 1</span>
<span id='database_4194' style='display:none'>COSMAT</span>
<span id='database_4195' style='display:none'>Identity Matching Adjudication Collector+: IMAC+</span>
<span id='database_4196' style='display:none'>Annotation of instructional texts</span>
<span id='database_4197' style='display:none'>Text Simplification Data Set</span>
<span id='database_4198' style='display:none'>CELCT Corpus</span>
<span id='database_4199' style='display:none'>The DGT Multilingual Translation Memory of the Acquis Communautaire (DGT-TM)</span>
<span id='database_4200' style='display:none'>DeCour</span>
<span id='database_4201' style='display:none'>ISST-TANL Dependency Annotated Corpus</span>
<span id='database_4202' style='display:none'>SCOLA broadcast news</span>
<span id='database_4203' style='display:none'>QAST 2007-2009 evaluation package</span>
<span id='database_4204' style='display:none'>Treebank-3</span>
<span id='database_4205' style='display:none'>Signs of Ireland Corpus</span>
<span id='database_4206' style='display:none'>Lefff 3.0</span>
<span id='database_4207' style='display:none'>MODAR 2.0</span>
<span id='database_4208' style='display:none'>Domain Adaptation Corpus</span>
<span id='database_4209' style='display:none'>Polish Sejm Corpus</span>
<span id='database_4210' style='display:none'>euromobil 2</span>
<span id='database_4211' style='display:none'>Pimorfo</span>
<span id='database_4212' style='display:none'>Metaphors for Economic Inequality in English, Farsi, Spanish, and Russian</span>
<span id='database_4213' style='display:none'>Spanish FreeLing Dependency Grammar (EsTxala)</span>
<span id='database_4214' style='display:none'>Annotation Discursive (ANNODIS) Corpus</span>
<span id='database_4215' style='display:none'>Morfette</span>
<span id='database_4216' style='display:none'>Georgian-Russian-Ukrainian-German Parallel Treebank</span>
<span id='database_4217' style='display:none'>DiCoEnviro. Dictionnaire fondamental de l'environnement</span>
<span id='database_4218' style='display:none'>Modelblocks</span>
<span id='database_4219' style='display:none'>A Treebank for Finnish (FinnTreeBank)</span>
<span id='database_4220' style='display:none'>GIVE-2 Corpus</span>
<span id='database_4221' style='display:none'>Two-level utterance-unit annotation scheme</span>
<span id='database_4222' style='display:none'>Bengali Poem Corpus</span>
<span id='database_4223' style='display:none'>Dialectal Arabic Resources</span>
<span id='database_4224' style='display:none'>B3-tool</span>
<span id='database_4225' style='display:none'>Unified Medical Language System (UMLS) Metathesaurus</span>
<span id='database_4226' style='display:none'>IDENTIC Corpus</span>
<span id='database_4227' style='display:none'>English-Galician Europarl parallel corpus</span>
<span id='database_4228' style='display:none'>Type Vector Similarity Tool</span>
<span id='database_4229' style='display:none'>Test set: TREC 10 questions</span>
<span id='database_4230' style='display:none'>Czech Speecon database</span>
<span id='database_4231' style='display:none'>TVD</span>
<span id='database_4232' style='display:none'>LSA models for Wikipedia and TASA</span>
<span id='database_4233' style='display:none'>互联网词库(Internet lexicon SogouW)</span>
<span id='database_4234' style='display:none'>Chinese Emotion Corpus</span>
<span id='database_4235' style='display:none'>Link Parser</span>
<span id='database_4236' style='display:none'>xpressive Speech Labeling Tool Incorporating the Temporal Characteristics of Emotion</span>
<span id='database_4237' style='display:none'>Matlab SOM-Toolbox</span>
<span id='database_4238' style='display:none'>SLMmotion</span>
<span id='database_4239' style='display:none'>CINTIL Treebank</span>
<span id='database_4240' style='display:none'>Trinity College Dublin Speaker Ageing (TCDSA) Database</span>
<span id='database_4241' style='display:none'>Speech database for unit selection synthesis of Viennese varieties</span>
<span id='database_4242' style='display:none'>Dicionario del Real Academia Española</span>
<span id='database_4243' style='display:none'>Apertium infrastructure</span>
<span id='database_4244' style='display:none'>Reusable Resources for the Romanian Language (RRRL)</span>
<span id='database_4245' style='display:none'>Gold Standard for Spanish semiotic nouns</span>
<span id='database_4246' style='display:none'>London Riots corpus</span>
<span id='database_4247' style='display:none'>English Web Reviews Multiword Expressions Corpus</span>
<span id='database_4248' style='display:none'>Swedish Kelly list</span>
<span id='database_4249' style='display:none'>GERTWOL/GERGEN</span>
<span id='database_4250' style='display:none'>The ABLE biodiversity corpus</span>
<span id='database_4251' style='display:none'>Russian-English works of V. Nabokov</span>
<span id='database_4252' style='display:none'>Yahoo! Answers Comprehensive Questions and Answers corpus [version 1.0]</span>
<span id='database_4253' style='display:none'>Spoken Turkish Corpus</span>
<span id='database_4254' style='display:none'>American Sign Language Synthesizer</span>
<span id='database_4255' style='display:none'>VietnameseNER</span>
<span id='database_4256' style='display:none'>Romanized Arabic Corpus</span>
<span id='database_4257' style='display:none'>DOLCE Foundational ontology</span>
<span id='database_4258' style='display:none'>Blademistress corpus</span>
<span id='database_4259' style='display:none'>CIPS-eval data</span>
<span id='database_4260' style='display:none'>EPAC corpus</span>
<span id='database_4261' style='display:none'>CALIMA</span>
<span id='database_4262' style='display:none'>Question Answer Sentence Pair</span>
<span id='database_4263' style='display:none'>SmartKom</span>
<span id='database_4264' style='display:none'>Finnish WordNet (FinnWordNet)</span>
<span id='database_4265' style='display:none'>JWPL TimeMachine</span>
<span id='database_4266' style='display:none'>MAtrixware REsearch Collection (MAREC)</span>
<span id='database_4267' style='display:none'>S-pot</span>
<span id='database_4268' style='display:none'>MULTEXT-East Version 4</span>
<span id='database_4269' style='display:none'>morpha</span>
<span id='database_4270' style='display:none'>OpenNLP Tokenizer FraMed Model</span>
<span id='database_4271' style='display:none'>The N2 Corpus</span>
<span id='database_4272' style='display:none'>GRISP (General Research Insight in Scientific and technical Publications)</span>
<span id='database_4273' style='display:none'>Multimodal Task-Based Communication</span>
<span id='database_4274' style='display:none'>Health Information Readability Corpus</span>
<span id='database_4275' style='display:none'>ACE 2005 Handwritten Arabic</span>
<span id='database_4276' style='display:none'>Interrogatio Anselmi</span>
<span id='database_4277' style='display:none'>Pledari Grond</span>
<span id='database_4278' style='display:none'>German Logical Metonymy Database</span>
<span id='database_4279' style='display:none'>Conference on Computational Natural Language Learning (CoNLL) 2003 Shared Task Named Entity data</span>
<span id='database_4280' style='display:none'>Manipuri-English Parallel Corpus</span>
<span id='database_4281' style='display:none'>A common benchmark for text wikification tools</span>
<span id='database_4282' style='display:none'>ILSP Lemmatiser</span>
<span id='database_4283' style='display:none'>Verb Noun List</span>
<span id='database_4284' style='display:none'>FrameNet 1.5</span>
<span id='database_4285' style='display:none'>NLPipe</span>
<span id='database_4286' style='display:none'>MGIZA++</span>
<span id='database_4287' style='display:none'>Frequency Word Lists</span>
<span id='database_4288' style='display:none'>QualGestDB</span>
<span id='database_4289' style='display:none'>Duden 11 / Redewendungen: Wörterbuch der deutschen Idiomatik</span>
<span id='database_4290' style='display:none'>EvalGrammar</span>
<span id='database_4291' style='display:none'>The AIMed corpus</span>
<span id='database_4292' style='display:none'>Java Statistical Classes</span>
<span id='database_4293' style='display:none'>STeP1</span>
<span id='database_4294' style='display:none'>Genia</span>
<span id='database_4295' style='display:none'>HTTrack Website Copier</span>
<span id='database_4296' style='display:none'>Myanmar Name Entity Recogniser</span>
<span id='database_4297' style='display:none'>NICT JEL corpus</span>
<span id='database_4298' style='display:none'>Arabic-English parallel corpus of sentences with relative clauses, English Glosses for Arabic</span>
<span id='database_4299' style='display:none'>MultiLing dataset</span>
<span id='database_4300' style='display:none'>Polish Webcorpus</span>
<span id='database_4301' style='display:none'>Divergence Measure Tool (DMT)</span>
<span id='database_4302' style='display:none'>NICT Kyoto tour dialogue corpus</span>
<span id='database_4303' style='display:none'>EmoVox</span>
<span id='database_4304' style='display:none'>Hotko</span>
<span id='database_4305' style='display:none'>International Workshop on Spoken Language Translation (IWSLT) 2011 parallel TED Corpus</span>
<span id='database_4306' style='display:none'>Microsoft Video Description Corpus</span>
<span id='database_4307' style='display:none'>GIDOC prototype</span>
<span id='database_4308' style='display:none'>LingSync</span>
<span id='database_4309' style='display:none'>the FreeTalk Conversation Corpus</span>
<span id='database_4310' style='display:none'>Mooney dataset: Geoquery data</span>
<span id='database_4311' style='display:none'>English/Hindi and English Arabic Gold Standard for Transliteration</span>
<span id='database_4312' style='display:none'>KPWr (part annotated syntactically)</span>
<span id='database_4313' style='display:none'>CODA-Vis</span>
<span id='database_4314' style='display:none'>Hespress texts</span>
<span id='database_4315' style='display:none'>Mainichi News Paper</span>
<span id='database_4316' style='display:none'>ALeSKo: Annotiertes LernerSprachenKorpus (‘annotated learner language corpus’)</span>
<span id='database_4317' style='display:none'>PerLex</span>
<span id='database_4318' style='display:none'>Metadata as LOD</span>
<span id='database_4319' style='display:none'>WikiNER  (semantically annotated corpus for Catalan)</span>
<span id='database_4320' style='display:none'>CatchWord Speech Synthesiser</span>
<span id='database_4321' style='display:none'>Dutch sentiment lexicon</span>
<span id='database_4322' style='display:none'>Chinese emotion lexicons(five emotions)</span>
<span id='database_4323' style='display:none'>DisMo</span>
<span id='database_4324' style='display:none'>UPC Smart Room Impulse Response Database</span>
<span id='database_4325' style='display:none'>Ossetic National Corpus</span>
<span id='database_4326' style='display:none'>French-Romanian parallel corpus</span>
<span id='database_4327' style='display:none'>FilMED - Filipino Multimodal Emotion Database</span>
<span id='database_4328' style='display:none'>W2C - Web To Corpus</span>
<span id='database_4329' style='display:none'>Providence Corpus for studying Child Directed Speech</span>
<span id='database_4330' style='display:none'>ReEscreve</span>
<span id='database_4331' style='display:none'>Annotations for progressive aspect sentences in the spoken section of the BNC</span>
<span id='database_4332' style='display:none'>Treebank-2</span>
<span id='database_4333' style='display:none'>GLISSANDO</span>
<span id='database_4334' style='display:none'>Kazakh to Chinese Dictionary Database</span>
<span id='database_4335' style='display:none'>SemEval2010-task 10</span>
<span id='database_4336' style='display:none'>Multi-Lingual Image captions</span>
<span id='database_4337' style='display:none'>Lemur</span>
<span id='database_4338' style='display:none'>TUNA-Lex corpus</span>
<span id='database_4339' style='display:none'>Event and Sentiment Segmentation Gold Standard</span>
<span id='database_4340' style='display:none'>Polish Parallel Corpora</span>
<span id='database_4341' style='display:none'>UnixMan Corpus</span>
<span id='database_4342' style='display:none'>Verbnet like classification of French verbs</span>
<span id='database_4343' style='display:none'>GSplit 3</span>
<span id='database_4344' style='display:none'>AWATIF</span>
<span id='database_4345' style='display:none'>LIR/Lexical Information Repository</span>
<span id='database_4346' style='display:none'>LM/PRU</span>
<span id='database_4347' style='display:none'>Event and non-event nouns test-set</span>
<span id='database_4348' style='display:none'>Europarl v3</span>
<span id='database_4349' style='display:none'>Estonian resource grammar for Grammatical Framework</span>
<span id='database_4350' style='display:none'>Instructional Manuals</span>
<span id='database_4351' style='display:none'>a Collection of Translation Error-Annotated Corpora (Terra)</span>
<span id='database_4352' style='display:none'>Illinois Semantic Role Labeler</span>
<span id='database_4353' style='display:none'>an online Chinese-English dictionary</span>
<span id='database_4354' style='display:none'>English Treebank</span>
<span id='database_4355' style='display:none'>PAN Plagiarism Corpus PAN-PC-10</span>
<span id='database_4356' style='display:none'>Public Health Opinion Corpus (PHOC)</span>
<span id='database_4357' style='display:none'>A Chinese-English Code-Switching Speech Database (CECOS)</span>
<span id='database_4358' style='display:none'>Guampa Collaborative Translation Toolkit</span>
<span id='database_4359' style='display:none'>Składnica semantyczna</span>
<span id='database_4360' style='display:none'>Irish Dependency Treebank</span>
<span id='database_4361' style='display:none'>LIPS 2008 AV Corpus</span>
<span id='database_4362' style='display:none'>TimenEval</span>
<span id='database_4363' style='display:none'>Pharmacology Patents corpus for Quaero</span>
<span id='database_4364' style='display:none'>eXtended WordNet Knowledge Base</span>
<span id='database_4365' style='display:none'>Autonomata TOO native infrequent and multilingual speech corpus</span>
<span id='database_4366' style='display:none'>DiLAF dictionaries</span>
<span id='database_4367' style='display:none'>Datenbank für Gesprochenes Deutsch</span>
<span id='database_4368' style='display:none'>Government Records</span>
<span id='database_4369' style='display:none'>Authorship Paraphrase Corpus</span>
<span id='database_4370' style='display:none'>Trading Consequences Commodity Initial Lexicon</span>
<span id='database_4371' style='display:none'>DORISpointingApril10</span>
<span id='database_4372' style='display:none'>poldata_RatingExtractor</span>
<span id='database_4373' style='display:none'>OwlExporter</span>
<span id='database_4374' style='display:none'>Wikipedia (English)</span>
<span id='database_4375' style='display:none'>VIT</span>
<span id='database_4376' style='display:none'>DEXONLINE</span>
<span id='database_4377' style='display:none'>Centre for Spoken Language Understanding (CSLU) Names v1.3</span>
<span id='database_4378' style='display:none'>Penn Discourse Treebank Parser</span>
<span id='database_4379' style='display:none'>Math</span>
<span id='database_4380' style='display:none'>Twitter dictionary</span>
<span id='database_4381' style='display:none'>Latvian taggers - POS/Morphology and NER</span>
<span id='database_4382' style='display:none'>Hidden Markov Model Toolkit</span>
<span id='database_4383' style='display:none'>J Kalita</span>
<span id='database_4384' style='display:none'>KIT lecture data</span>
<span id='database_4385' style='display:none'>Alignment of Parallel Texts from Cyrillic to Latin</span>
<span id='database_4386' style='display:none'>SProUT</span>
<span id='database_4387' style='display:none'>ASIt - (Atlante Sintattico d'Italia, Syntactic Atlas of Italy)</span>
<span id='database_4388' style='display:none'>Albany Metaphor Repository</span>
<span id='database_4389' style='display:none'>SinoBerryPicker</span>
<span id='database_4390' style='display:none'>MPQA subjectivity clues</span>
<span id='database_4391' style='display:none'>FAUST Feedback Annotation</span>
<span id='database_4392' style='display:none'>Chinese Temporal Annotation Data Set</span>
<span id='database_4393' style='display:none'>Google Books Ngram dataset (english)</span>
<span id='database_4394' style='display:none'>GEMS</span>
<span id='database_4395' style='display:none'>Boundary Annotated Qur'an Corpus</span>
<span id='database_4396' style='display:none'>QA4MRE datasets</span>
<span id='database_4397' style='display:none'>GSL Classifier Corpus</span>
<span id='database_4398' style='display:none'>Urdu Resource Grammar</span>
<span id='database_4399' style='display:none'>Free Text File Merging Tool (TXTcollector)</span>
<span id='database_4400' style='display:none'>Corpus Internacional do Português (CINTIL) Propbank</span>
<span id='database_4401' style='display:none'>Graded Compositionality Scores for Compound Nouns</span>
<span id='database_4402' style='display:none'>RDG-Game Corpus</span>
<span id='database_4403' style='display:none'>MITRE Dialogue Kit (MIDIKI)</span>
<span id='database_4404' style='display:none'>Picture Books Ontology</span>
<span id='database_4405' style='display:none'>Black Bean Chinese Word Segmentation System</span>
<span id='database_4406' style='display:none'>Mainichi Newspaper</span>
<span id='database_4407' style='display:none'>Expansion of the MRC Psycholinguistic Database Imageability Ratings</span>
<span id='database_4408' style='display:none'>Rhetorical Structure Theory Tool (RSTTool)</span>
<span id='database_4409' style='display:none'>WT10g</span>
<span id='database_4410' style='display:none'>NTCIR-9 SpokenDoc test collection</span>
<span id='database_4411' style='display:none'>Comparative Toxicogenomics Database</span>
<span id='database_4412' style='display:none'>BART Anaphora Resolution Toolkit</span>
<span id='database_4413' style='display:none'>SANTOS</span>
<span id='database_4414' style='display:none'>Taalportaal</span>
<span id='database_4415' style='display:none'>CLEF - QAST 2007-2009 Evaluation Package</span>
<span id='database_4416' style='display:none'>LitRec</span>
<span id='database_4417' style='display:none'>Hebrew annotated documents</span>
<span id='database_4418' style='display:none'>Sogou User Input Record</span>
<span id='database_4419' style='display:none'>ConceptMapper</span>
<span id='database_4420' style='display:none'>Regulus</span>
<span id='database_4421' style='display:none'>IIIT-Tides</span>
<span id='database_4422' style='display:none'>Joshua - open source hierarchical phrase based system</span>
<span id='database_4423' style='display:none'>Korpusik US II PWr (Small Corpus for WSD)</span>
<span id='database_4424' style='display:none'>Unsupervised Dependency Parser for English v 1.0</span>
<span id='database_4425' style='display:none'>longest_ner</span>
<span id='database_4426' style='display:none'>Slovenian Lombard Speech Database</span>
<span id='database_4427' style='display:none'>General Enquirer</span>
<span id='database_4428' style='display:none'>Sense Folder Corpus</span>
<span id='database_4429' style='display:none'>SORA corpus</span>
<span id='database_4430' style='display:none'>Phoneme Similarity Matrices</span>
<span id='database_4431' style='display:none'>AQUIANT</span>
<span id='database_4432' style='display:none'>Arabic Verbnet</span>
<span id='database_4433' style='display:none'>VenPro</span>
<span id='database_4434' style='display:none'>SUCRE</span>
<span id='database_4435' style='display:none'>Miller and Charles</span>
<span id='database_4436' style='display:none'>Cross-Corpus Model</span>
<span id='database_4437' style='display:none'>Conference on Computational Natural Language Learning (CoNLL) 2006 Shared Task Dependency Parsing</span>
<span id='database_4438' style='display:none'>BREF</span>
<span id='database_4439' style='display:none'>FreeLing Standardised Web Service (FreeLing SWS)</span>
<span id='database_4440' style='display:none'>UKB</span>
<span id='database_4441' style='display:none'>Ajka</span>
<span id='database_4442' style='display:none'>Aggregated texts and their semantic representations</span>
<span id='database_4443' style='display:none'>Gensim</span>
<span id='database_4444' style='display:none'>Daemonette</span>
<span id='database_4445' style='display:none'>Chinese Wordnet</span>
<span id='database_4446' style='display:none'>Oral History Annotation Tool</span>
<span id='database_4447' style='display:none'>Consolidated Multilingual Terminology Acquisition Workflows</span>
<span id='database_4448' style='display:none'>Stanford MaxEnt Classifier</span>
<span id='database_4449' style='display:none'>J-Server</span>
<span id='database_4450' style='display:none'>LASSY</span>
<span id='database_4451' style='display:none'>VOCE Corpus</span>
<span id='database_4452' style='display:none'>YamCha: Yet Another Multipurpose CHunk Annotator</span>
<span id='database_4453' style='display:none'>Internazionale</span>
<span id='database_4454' style='display:none'>A Dynamic HPSG Treebank of the Wall Street Journal sections of the Penn Treebank</span>
<span id='database_4455' style='display:none'>AdaptiveCoref</span>
<span id='database_4456' style='display:none'>Real-word error corpus</span>
<span id='database_4457' style='display:none'>BS Computer Science Corpus</span>
<span id='database_4458' style='display:none'>wordnets in various languages</span>
<span id='database_4459' style='display:none'>GALE_Chinese_alignment_guidelines</span>
<span id='database_4460' style='display:none'>WMT Human rankings of MT Output</span>
<span id='database_4461' style='display:none'>Moses Phrase-based Statistical Machine Translation system</span>
<span id='database_4462' style='display:none'>DysLæs</span>
<span id='database_4463' style='display:none'>LEXUS, Toolbox</span>
<span id='database_4464' style='display:none'>Apertium</span>
<span id='database_4465' style='display:none'>Basque WordNet</span>
<span id='database_4466' style='display:none'>LDC HUB4</span>
<span id='database_4467' style='display:none'>Multi-Domain Sentiment Dataset v2.0</span>
<span id='database_4468' style='display:none'>CALBC Corpora</span>
<span id='database_4469' style='display:none'>TAC 2010 Guided Summarization</span>
<span id='database_4470' style='display:none'>A corpus of manually revised essays</span>
<span id='database_4471' style='display:none'>RACAI TTS Speech Segmentation</span>
<span id='database_4472' style='display:none'>Generative Semantic Parsing Model using Hybrid Tree Framework</span>
<span id='database_4473' style='display:none'>(no-name)</span>
<span id='database_4474' style='display:none'>IMS-corpus collection</span>
<span id='database_4475' style='display:none'>Gigafida</span>
<span id='database_4476' style='display:none'>nerc-fr</span>
<span id='database_4477' style='display:none'>ParTUT</span>
<span id='database_4478' style='display:none'>RASC</span>
<span id='database_4479' style='display:none'>German Parliament Sessions</span>
<span id='database_4480' style='display:none'>Ontology created from Wikipedia Animal articles</span>
<span id='database_4481' style='display:none'>handAlignedCorporaJPEN</span>
<span id='database_4482' style='display:none'>Emille</span>
<span id='database_4483' style='display:none'>Annotation scheme or semantic tagset,</span>
<span id='database_4484' style='display:none'>Spatial patterns dataset</span>
<span id='database_4485' style='display:none'>CARTÃO</span>
<span id='database_4486' style='display:none'>BlogBuster</span>
<span id='database_4487' style='display:none'>English (P)LTAG lexicon</span>
<span id='database_4488' style='display:none'>Google Search API</span>
<span id='database_4489' style='display:none'>CatCG</span>
<span id='database_4490' style='display:none'>Multilingual dictionary</span>
<span id='database_4491' style='display:none'>HPSG-WSJ</span>
<span id='database_4492' style='display:none'>LDC2008T19</span>
<span id='database_4493' style='display:none'>ICL-Searcher</span>
<span id='database_4494' style='display:none'>Language Collage</span>
<span id='database_4495' style='display:none'>McRae's semantic feature generation norms</span>
<span id='database_4496' style='display:none'>The Accents of the British Isles (ABI-1)</span>
<span id='database_4497' style='display:none'>NEGRA: treebank of German</span>
<span id='database_4498' style='display:none'>NIST SRE Evaluations</span>
<span id='database_4499' style='display:none'>NIST SRE evaluations</span>
<span id='database_4500' style='display:none'>Parallel opinion corpus</span>
<span id='database_4501' style='display:none'>parallel opinion corpus</span>
<span id='database_4502' style='display:none'>Opinion Corpus for Arabic (OCA)</span>
<span id='database_4503' style='display:none'>OCA</span>
<span id='database_4504' style='display:none'>Annotated Diagnostic Narratives 2</span>
<span id='database_4505' style='display:none'>Annotated diagnostic narratives 2</span>
<span id='database_4506' style='display:none'>Statistical Machine Translation System (Moses)</span>
<span id='database_4507' style='display:none'>Fast_align</span>
<span id='database_4508' style='display:none'>fast_align</span>
<span id='database_4509' style='display:none'>TIDigits</span>
<span id='database_4510' style='display:none'>TIDIGITS</span>
<span id='database_4511' style='display:none'>Software Asset Reuse Framework (SARF)</span>
<span id='database_4512' style='display:none'>Sarf</span>
<span id='database_4513' style='display:none'>CSJ SDR Test Collection</span>
<span id='database_4514' style='display:none'>Icltt-dictionaries</span>
<span id='database_4515' style='display:none'>icltt-dictionaries</span>
<span id='database_4516' style='display:none'>Tech Coreference Dictionary</span>
<span id='database_4517' style='display:none'>Tech coreference dictionary</span>
<span id='database_4518' style='display:none'>English Version of OCA (EVOCA)</span>
<span id='database_4519' style='display:none'>EVOCA</span>
<span id='database_4520' style='display:none'>A Database of Japanese Adjective Examples</span>
<span id='database_4521' style='display:none'>a Database of Japanese Adjective Examples</span>
<span id='database_4522' style='display:none'>Multi-Annotator Competence Estimation (MACE)</span>
<span id='database_4523' style='display:none'>MACE (Multi-Annotator Competence Estimation)</span>
<span id='database_4524' style='display:none'>Edinburgh Speech Production Facility (ESPF)</span>
<span id='database_4525' style='display:none'>Edinburgh Speech Production Facility: ESPF corpus</span>
<span id='database_4526' style='display:none'>Hunspell</span>
<span id='database_4527' style='display:none'>CSLM Toolkit</span>
<span id='database_4528' style='display:none'>CSLM toolkit</span>
<span id='database_4529' style='display:none'>Nettalk15k</span>
<span id='database_4530' style='display:none'>nettalk15k</span>
<span id='database_4531' style='display:none'>BLOGS06 : a TREC test collection, created and distributed by the University of Glasgow</span>
<span id='database_4532' style='display:none'>Minimal Pair ABX Tasks for the Evaluation of Speech Features</span>
<span id='database_4533' style='display:none'>Minimal pair ABX tasks for the evaluation of speech features</span>
<span id='database_4534' style='display:none'>Ssj500k</span>
<span id='database_4535' style='display:none'>ssj500k</span>
<span id='database_4536' style='display:none'>Question Answering Corpus for Answer Extraction</span>
<span id='database_4537' style='display:none'>Question Answering corpus for answer extraction</span>
<span id='database_4538' style='display:none'>Ncode</span>
<span id='database_4539' style='display:none'>ncode</span>
<span id='database_4540' style='display:none'>Company_name Translation Memory</span>
<span id='database_4541' style='display:none'>company_name Translation Memory</span>
<span id='database_4542' style='display:none'>Switchboard Transcription Project</span>
<span id='database_4543' style='display:none'>Switchboard transcription project</span>
<span id='database_4544' style='display:none'>SloWCrowd</span>
<span id='database_4545' style='display:none'>sloWCrowd</span>
<span id='database_4546' style='display:none'>Serbian web corpus (srWaC)</span>
<span id='database_4547' style='display:none'>srWaC</span>
<span id='database_4548' style='display:none'>GIZA++: Training of statistical translation models</span>
<span id='database_4549' style='display:none'>N-grams data from Corpus of Contemporary American English  (COCA)</span>
<span id='database_4550' style='display:none'>N-grams data from the corpus of contemporary american english (coca)</span>
<span id='database_4551' style='display:none'>German Reference Corpus (DeReKo)</span>
<span id='database_4552' style='display:none'>WordNet - Princeton University</span>
<span id='database_4553' style='display:none'>Princeton WordNet</span>
<span id='database_4554' style='display:none'>English Gigaword - Linguistic Data Consortium - LDC Catalog</span>
<span id='database_4555' style='display:none'>English Gigaword, LDC2003T05</span>
<span id='database_4556' style='display:none'>tl_dv2_ladl : a Subcategorisation Lexicon for French Verbs</span>
<span id='database_4557' style='display:none'>tl_dv2_ladl</span>
<span id='database_4558' style='display:none'>Relation Extraction Corpus</span>
<span id='database_4559' style='display:none'>Relation extraction corpus</span>
<span id='database_4560' style='display:none'>Slovene Web Corpus (slWaC)</span>
<span id='database_4561' style='display:none'>slWaC</span>
<span id='database_4562' style='display:none'>Question-answer Pairs by Role Play</span>
<span id='database_4563' style='display:none'>Question-answer pairs by role play</span>
<span id='database_4564' style='display:none'>LNE Evaluation Tools</span>
<span id='database_4565' style='display:none'>LNE evaluation tools</span>
<span id='database_4566' style='display:none'>Chinese Evaluation Information Corpus</span>
<span id='database_4567' style='display:none'>Chinese evaluation information corpus</span>
<span id='database_4568' style='display:none'>Computer Science Terms</span>
<span id='database_4569' style='display:none'>computer science terms</span>
<span id='database_4570' style='display:none'>Turkish-English Gold Morpheme Alignments</span>
<span id='database_4571' style='display:none'>Turkish-English gold morpheme alignments</span>
<span id='database_4572' style='display:none'>Multimodal Corpus of Emotional Expression During Spontaneous Japanese Dialog (OEIP)</span>
<span id='database_4573' style='display:none'>OEIP Multimodal corpus of emotional expression during spontaneous Japanese dialog</span>
<span id='database_4574' style='display:none'>Multilingual probabilistic morphological databasa</span>
<span id='database_4575' style='display:none'>multilingual probabilistic morphological databasa</span>
<span id='database_4576' style='display:none'>The Munich Versatile and Fast Open-Source Audio Feature Extractor (openSMILE)</span>
<span id='database_4577' style='display:none'>openSMILE</span>
<span id='database_4578' style='display:none'>Qa-mert.py</span>
<span id='database_4579' style='display:none'>qa-mert.py</span>
<span id='database_4580' style='display:none'>Aligned Parallel Sentences</span>
<span id='database_4581' style='display:none'>aligned parallel sentences</span>
<span id='database_4582' style='display:none'>Princeton Annotated Gloss Corpus</span>
<span id='database_4583' style='display:none'>Takahe</span>
<span id='database_4584' style='display:none'>takahe</span>
<span id='database_4585' style='display:none'>Human Phenotype Ontology (HPO)</span>
<span id='database_4586' style='display:none'>HPO</span>
<span id='database_4587' style='display:none'>Bosnian web corpus (bsWaC)</span>
<span id='database_4588' style='display:none'>bsWaC</span>
<span id='database_4589' style='display:none'>Multi-domain sentiment datase</span>
<span id='database_4590' style='display:none'>multi-domain sentiment dataset</span>
<span id='database_4591' style='display:none'>Par-lvf</span>
<span id='database_4592' style='display:none'>par-lvf</span>
<span id='database_4593' style='display:none'>Two-level morphological analyzer (Oflazer, 1994)</span>
<span id='database_4594' style='display:none'>two-level morphological analyzer (Oflazer, 1994)</span>
<span id='database_4595' style='display:none'>Dialogue Data</span>
<span id='database_4596' style='display:none'>dialogue data</span>
<span id='database_4597' style='display:none'>T2K²: a System for Automatically Extracting and Organizing Knowledge from Texts</span>
<span id='database_4598' style='display:none'>T2K^2</span>
<span id='database_4599' style='display:none'>libLBFGS: a library of Limited-memory Broyden-Fletcher-Goldfarb-Shanno (L-BFGS)</span>
<span id='database_4600' style='display:none'>libLBFGS</span>
<span id='database_4601' style='display:none'>The Federated Ontology of the TrendMiner Project (TMO)</span>
<span id='database_4602' style='display:none'>TMO</span>
<span id='database_4603' style='display:none'>Exacc-wiki-boosted-train-dev-test-data.tar.gz</span>
<span id='database_4604' style='display:none'>exacc-wiki-boosted-train-dev-test-data.tar.gz</span>
<span id='database_4605' style='display:none'>EventCorefBank</span>
<span id='database_4606' style='display:none'>ECB+</span>
<span id='database_4607' style='display:none'>Augmented Multi-party Interaction (AMI) Meeting Corpus</span>
<span id='database_4608' style='display:none'>Large Vocabulary Thai Continuous Speech Broadcast News Corpus (LOTUS-BN)</span>
<span id='database_4609' style='display:none'>UKWaC: Corpora of English</span>
<span id='database_4610' style='display:none'>Svplund</span>
<span id='database_4611' style='display:none'>svplund</span>
<span id='database_4612' style='display:none'>Hand Aligned Data</span>
<span id='database_4613' style='display:none'>Hand aligned data</span>
<span id='database_4614' style='display:none'>MaltParser: a system for data-driven dependency parsing</span>
<span id='database_4615' style='display:none'>MALT Parser</span>
<span id='database_4616' style='display:none'>En_w_fr_articles_subset_Wikipedia</span>
<span id='database_4617' style='display:none'>en_w_fr_articles_subset_Wikipedia</span>
<span id='database_4618' style='display:none'>Speech Reconstruction</span>
<span id='database_4619' style='display:none'>Speech reconstruction</span>
<span id='database_4620' style='display:none'>Historical Books Corpus of Computer Research Center of Islamic Sciences (NOOR co. Qom, Iran)</span>
<span id='database_4621' style='display:none'>historical books corpus of Computer Research Center of Islamic Sciences, NOOR co. Qom, Iran</span>
<span id='database_4622' style='display:none'>Conditional Random Fields (CRF++)</span>
<span id='database_4623' style='display:none'>BLOGS08: a TREC test collection, created and distributed by the University of Glasgow</span>
<span id='database_4624' style='display:none'>BLOGS08</span>
<span id='database_4625' style='display:none'>Advertising Datasets</span>
<span id='database_4626' style='display:none'>advertising datasets</span>
<span id='database_4627' style='display:none'>Finnish Imitation Corpus</span>
<span id='database_4628' style='display:none'>Finnish imitation corpus</span>
<span id='database_4629' style='display:none'>OpenFst</span>
<span id='database_4630' style='display:none'>Openfst</span>
<span id='database_4631' style='display:none'>Light Verb Constructions in a Multilingual Parallel Corpus ( 4FX)</span>
<span id='database_4632' style='display:none'>4FX</span>
<span id='database_4633' style='display:none'>CMU Kids</span>
<span id='database_4634' style='display:none'>CMUKIDS</span>
<span id='database_4635' style='display:none'>FilesRo.zip</span>
<span id='database_4636' style='display:none'>filesRo.zip</span>
<span id='database_4637' style='display:none'>Compound meta-data German Europarl</span>
<span id='database_4638' style='display:none'>compound meta-data German Europarl</span>
<span id='database_4639' style='display:none'>HUnCertainty</span>
<span id='database_4640' style='display:none'>hUnCertainty</span>
<span id='database_4641' style='display:none'>The Balanced Corpus of Contemporary Written Japanese (BCCWJ)</span>
<span id='database_4642' style='display:none'>obi2/B9</span>
<span id='database_4643' style='display:none'>Sanskrit Library Syntax Corpus</span>
<span id='database_4644' style='display:none'>Sanskrit Library syntax corpus</span>
<span id='database_4645' style='display:none'>MKCLS</span>
<span id='database_4646' style='display:none'>mkcls</span>
<span id='database_4647' style='display:none'>Mate-tools</span>
<span id='database_4648' style='display:none'>GENIA Corpus: Primary Collection of Biomedical Literature</span>
<span id='database_4649' style='display:none'>Webann</span>
<span id='database_4650' style='display:none'>webann</span>
<span id='database_4651' style='display:none'>Electronic Dictionary Research (EDR)</span>
<span id='database_4652' style='display:none'>Brat</span>
<span id='database_4653' style='display:none'>brat</span>
<span id='database_4654' style='display:none'>Quero Hungarian</span>
<span id='database_4655' style='display:none'>A Parallel Corpus for Statistical Machine Translation (Europarl)</span>
<span id='database_4656' style='display:none'>Center for Computational Language and EducAtion Research (CLEAR) Parser</span>
<span id='database_4657' style='display:none'>Clear Parser</span>
<span id='database_4658' style='display:none'>Lemon translation module</span>
<span id='database_4659' style='display:none'>lemon translation module</span>
<span id='database_4660' style='display:none'>LDC Chinese-English Parallel Corpus</span>
<span id='database_4661' style='display:none'>Sonic</span>
<span id='database_4662' style='display:none'>sonic</span>
<span id='database_4663' style='display:none'>ITAAL Speech Corpus</span>
<span id='database_4664' style='display:none'>ITAAL speech corpus</span>
<span id='database_4665' style='display:none'>Stylistic Seed Terms</span>
<span id='database_4666' style='display:none'>Stylistic Seed terms</span>
<span id='database_4667' style='display:none'>Text Simplification Corpus</span>
<span id='database_4668' style='display:none'>Text simplification corpus</span>
<span id='database_4669' style='display:none'>Negra</span>
<span id='database_4670' style='display:none'>Hanyu Shuiping Kaoshi (HSK) Corpus</span>
<span id='database_4671' style='display:none'>HSK corpus</span>
<span id='database_4672' style='display:none'>Croatian Web Corpus (hrWac)</span>
<span id='database_4673' style='display:none'>hrWac</span>
<span id='database_4674' style='display:none'>Disordered Voice Synthesizer</span>
<span id='database_4675' style='display:none'>Disordered voice synthesizer</span>
<span id='database_4676' style='display:none'>Wikigaiak4koa.pl</span>
<span id='database_4677' style='display:none'>wikigaiak4koa.pl</span>
<span id='database_4678' style='display:none'>Twitter Corpus</span>
<span id='database_4679' style='display:none'>he Open American National Corpus (OANC)</span>
<span id='database_4680' style='display:none'>Open American National Corpus</span>
<span id='database_4681' style='display:none'>Resources.xml</span>
<span id='database_4682' style='display:none'>resources.xml</span>
<span id='database_4683' style='display:none'>Electronic Program Guide (EPG)</span>
<span id='database_4684' style='display:none'>EPG</span>
<span id='database_4685' style='display:none'>Quaero</span>
<span id='database_4686' style='display:none'>Novel “Quo Vadis”</span>
<span id='database_4687' style='display:none'>novel “Quo Vadis”</span>
<span id='database_4688' style='display:none'>Codeine</span>
<span id='database_4689' style='display:none'>codeine</span>
<span id='database_4690' style='display:none'>Web tool for Quran Chronology</span>
<span id='database_4691' style='display:none'>web tool for Quran Chronology</span>
<span id='database_4692' style='display:none'>Catalan Web Corpus (caWaC)</span>
<span id='database_4693' style='display:none'>caWaC</span>
<span id='database_4694' style='display:none'>Pennconvertor</span>
<span id='database_4695' style='display:none'>pennconvertor</span>
<span id='database_4696' style='display:none'>Croatian web corpus (hrWaC)</span>
<span id='database_4697' style='display:none'>hrWaC</span>
<span id='database_4698' style='display:none'>Web-derived Pronunciations</span>
<span id='database_4699' style='display:none'>Web-derived pronunciations</span>
<span id='database_4700' style='display:none'>English and Arabic WordNet</span>
<span id='database_4701' style='display:none'>english and Arabic WordNet</span>
<span id='database_4702' style='display:none'>Sanskrit Library Morphological Tagging Interface</span>
<span id='database_4703' style='display:none'>Sanskrit Library morphological tagging interface</span>
<span id='database_4704' style='display:none'>Corpus pre-processing script</span>
<span id='database_4705' style='display:none'>corpus pre-processing script</span>
<span id='database_4706' style='display:none'>Maui-indexer Keyword Extraction Datasets</span>
<span id='database_4707' style='display:none'>maui-indexer keyword extraction datasets</span>
<span id='database_4708' style='display:none'>Implementation of the Minimal Pair ABX Tasks for the Evaluation of Speech Features</span>
<span id='database_4709' style='display:none'>Implementation of the minimal pair ABX tasks for the evaluation of speech features</span>
<span id='database_4710' style='display:none'>Wiki_zh_ja_corpus</span>
<span id='database_4711' style='display:none'>wiki_zh_ja_corpus</span>
<span id='database_4712' style='display:none'>GlobalPhone Corpus</span>
<span id='database_4713' style='display:none'>GlobalPhone corpus</span>
<span id='database_4714' style='display:none'>NE Signatures</span>
<span id='database_4715' style='display:none'>NE signatures</span>
<span id='database_4716' style='display:none'>Database</span>
<span id='database_4717' style='display:none'>Morphological Analyzer</span>
<span id='database_4718' style='display:none'>Search Engine</span>
<span id='database_4719' style='display:none'>Coreference Resolution</span>
<span id='database_4720' style='display:none'>Typological Database</span>
<span id='database_4721' style='display:none'>Stemmer</span>
<span id='database_4722' style='display:none'>Classifier</span>
<span id='database_4723' style='display:none'>Morphological Analyzer and Generator</span>
<span id='database_4724' style='display:none'>Lexicon Tool</span>
<span id='database_4725' style='display:none'>Compound Splitter</span>
<span id='database_4726' style='display:none'>Knowledge Processing Tool</span>
<span id='database_4727' style='display:none'>Terminological Database</span>
<span id='database_4728' style='display:none'>Corpus Search Tool</span>
<span id='database_4729' style='display:none'>Word Alignment</span>
<span id='database_4730' style='display:none'>Linguistic Research</span>
<span id='database_4731' style='display:none'>Extracting semantic relations</span>
<span id='database_4732' style='display:none'>NLP Tool provided as service</span>
<span id='database_4733' style='display:none'>Natural Language Understanding</span>
<span id='database_4734' style='display:none'>Terminology Extraction</span>
<span id='database_4735' style='display:none'>multiple uses</span>
<span id='database_4736' style='display:none'>Tokenization</span>
<span id='database_4737' style='display:none'>Temporal Reasoning</span>
<span id='database_4738' style='display:none'>Word Segmentation</span>
<span id='database_4739' style='display:none'>Extraction of Multiword Expressions</span>


<?php 
include("./lremapso_files/languages.js");
?>

<script type="text/javascript">


// these are the names of all the resources
var database_ResourceName = [
0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,
100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,
200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,
300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377,378,379,380,381,382,383,384,385,386,387,388,389,390,391,392,393,394,395,396,397,398,399,
400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,418,419,420,421,422,423,424,425,426,427,428,429,430,431,432,433,434,435,436,437,438,439,440,441,442,443,444,445,446,447,448,449,450,451,452,453,454,455,456,457,458,459,460,461,462,463,464,465,466,467,468,469,470,471,472,473,474,475,476,477,478,479,480,481,482,483,484,485,486,487,488,489,490,491,492,493,494,495,496,497,498,499,
500,501,502,503,504,505,506,507,508,509,510,511,512,513,514,515,516,517,518,519,520,521,522,523,524,525,526,527,528,529,530,531,532,533,534,535,536,537,538,539,540,541,542,543,544,545,546,547,548,549,550,551,552,553,554,555,556,557,558,559,560,561,562,563,564,565,566,567,568,569,570,571,572,573,574,575,576,577,578,579,580,581,582,583,584,585,586,587,588,589,590,591,592,593,594,595,596,597,598,599,
600,601,602,603,604,605,606,607,608,609,610,611,612,613,614,615,616,617,618,619,620,621,622,623,624,625,626,627,628,629,630,631,632,633,634,635,636,637,638,639,640,641,642,643,644,645,646,647,648,649,650,651,652,653,654,655,656,657,658,659,660,661,662,663,664,665,666,667,668,669,670,671,672,673,674,675,676,677,678,679,680,681,682,683,684,685,686,687,688,689,690,691,692,693,694,695,696,697,698,699,
700,701,702,703,704,705,706,707,708,709,710,711,712,713,714,715,716,717,718,719,720,721,722,723,724,725,726,727,728,729,730,731,732,733,734,735,736,737,738,739,740,741,742,743,744,745,746,747,748,749,750,751,752,753,754,755,756,757,758,759,760,761,762,763,764,765,766,767,768,769,770,771,772,773,774,775,776,777,778,779,780,781,782,783,784,785,786,787,788,789,790,791,792,793,794,795,796,797,798,799,
800,801,802,803,804,805,806,807,808,809,810,811,812,813,814,815,816,817,818,819,820,821,822,823,824,825,826,827,828,829,830,831,832,833,834,835,836,837,838,839,840,841,842,843,844,845,846,847,848,849,850,851,852,853,854,855,856,857,858,859,860,861,862,863,864,865,866,867,868,869,870,871,872,873,874,875,876,877,878,879,880,881,882,883,884,885,886,887,888,889,890,891,892,893,894,895,896,897,898,899,
900,901,902,903,904,905,906,907,908,909,910,911,912,913,914,915,916,917,918,919,920,921,922,923,924,925,926,927,928,929,930,931,932,933,934,935,936,937,938,939,940,941,942,943,944,945,946,947,948,949,950,951,952,953,954,955,956,957,958,959,960,961,962,963,964,965,966,967,968,969,970,971,972,973,974,975,976,977,978,979,980,981,982,983,984,985,986,987,988,989,990,991,992,993,994,995,996,997,998,999,
1000,1001,1002,1003,1004,1005,1006,1007,1008,1009,1010,1011,1012,1013,1014,1015,1016,1017,1018,1019,1020,1021,1022,1023,1024,1025,1026,1027,1028,1029,1030,1031,1032,1033,1034,1035,1036,1037,1038,1039,1040,1041,1042,1043,1044,1045,1046,1047,1048,1049,1050,1051,1052,1053,1054,1055,1056,1057,1058,1059,1060,1061,1062,1063,1064,1065,1066,1067,1068,1069,1070,1071,1072,1073,1074,1075,1076,1077,1078,1079,1080,1081,1082,1083,1084,1085,1086,1087,1088,1089,1090,1091,1092,1093,1094,1095,1096,1097,1098,1099,
1100,1101,1102,1103,1104,1105,1106,1107,1108,1109,1110,1111,1112,1113,1114,1115,1116,1117,1118,1119,1120,1121,1122,1123,1124,1125,1126,1127,1128,1129,1130,1131,1132,1133,1134,1135,1136,1137,1138,1139,1140,1141,1142,1143,1144,1145,1146,1147,1148,1149,1150,1151,1152,1153,1154,1155,1156,1157,1158,1159,1160,1161,1162,1163,1164,1165,1166,1167,1168,1169,1170,1171,1172,1173,1174,1175,1176,1177,1178,1179,1180,1181,1182,1183,1184,1185,1186,1187,1188,1189,1190,1191,1192,1193,1194,1195,1196,1197,1198,1199,
1200,1201,1202,1203,1204,1205,1206,1207,1208,1209,1210,1211,1212,1213,1214,1215,1216,1217,1218,1219,1220,1221,1222,1223,1224,1225,1226,1227,1228,1229,1230,1231,1232,1233,1234,1235,1236,1237,1238,1239,1240,1241,1242,1243,1244,1245,1246,1247,1248,1249,1250,1251,1252,1253,1254,1255,1256,1257,1258,1259,1260,1261,1262,1263,1264,1265,1266,1267,1268,1269,1270,1271,1272,1273,1274,1275,1276,1277,1278,1279,1280,1281,1282,1283,1284,1285,1286,1287,1288,1289,1290,1291,1292,1293,1294,1295,1296,1297,1298,1299,
1300,1301,1302,1303,1304,1305,1306,1307,1308,1309,1310,1311,1312,1313,1314,1315,1316,1317,1318,1319,1320,1321,1322,1323,1324,1325,1326,1327,1328,1329,1330,1331,1332,1333,1334,1335,1336,1337,1338,1339,1340,1341,1342,1343,1344,1345,1346,1347,1348,1349,1350,1351,1352,1353,1354,1355,1356,1357,1358,1359,1360,1361,1362,1363,1364,1365,1366,1367,1368,1369,1370,1371,1372,1373,1374,1375,1376,1377,1378,1379,1380,1381,1382,1383,1384,1385,1386,1387,1388,1389,1390,1391,1392,1393,1394,1395,1396,1397,1398,1399,
1400,1401,1402,1403,1404,1405,1406,1407,1408,1409,1410,1411,1412,1413,1414,1415,1416,1417,1418,1419,1420,1421,1422,1423,1424,1425,1426,1427,1428,1429,1430,1431,1432,1433,1434,1435,1436,1437,1438,1439,1440,1441,1442,1443,1444,1445,1446,1447,1448,1449,1450,1451,1452,1453,1454,1455,1456,1457,1458,1459,1460,1461,1462,1463,1464,1465,1466,1467,1468,1469,1470,1471,1472,1473,1474,1475,1476,1477,1478,1479,1480,1481,1482,1483,1484,1485,1486,1487,1488,1489,1490,1491,1492,1493,1494,1495,1496,1497,1498,1499,
1500,1501,1502,1503,1504,1505,1506,1507,1508,1509,1510,1511,1512,1513,1514,1515,1516,1517,1518,1519,1520,1521,1522,1523,1524,1525,1526,1527,1528,1529,1530,1531,1532,1533,1534,1535,1536,1537,1538,1539,1540,1541,1542,1543,1544,1545,1546,1547,1548,1549,1550,1551,1552,1553,1554,1555,1556,1557,1558,1559,1560,1561,1562,1563,1564,1565,1566,1567,1568,1569,1570,1571,1572,1573,1574,1575,1576,1577,1578,1579,1580,1581,1582,1583,1584,1585,1586,1587,1588,1589,1590,1591,1592,1593,1594,1595,1596,1597,1598,1599,
1600,1601,1602,1603,1604,1605,1606,1607,1608,1609,1610,1611,1612,1613,1614,1615,1616,1617,1618,1619,1620,1621,1622,1623,1624,1625,1626,1627,1628,1629,1630,1631,1632,1633,1634,1635,1636,1637,1638,1639,1640,1641,1642,1643,1644,1645,1646,1647,1648,1649,1650,1651,1652,1653,1654,1655,1656,1657,1658,1659,1660,1661,1662,1663,1664,1665,1666,1667,1668,1669,1670,1671,1672,1673,1674,1675,1676,1677,1678,1679,1680,1681,1682,1683,1684,1685,1686,1687,1688,1689,1690,1691,1692,1693,1694,1695,1696,1697,1698,1699,
1700,1701,1702,1703,1704,1705,1706,1707,1708,1709,1710,1711,1712,1713,1714,1715,1716,1717,1718,1719,1720,1721,1722,1723,1724,1725,1726,1727,1728,1729,1730,1731,1732,1733,1734,1735,1736,1737,1738,1739,1740,1741,1742,1743,1744,1745,1746,1747,1748,1749,1750,1751,1752,1753,1754,1755,1756,1757,1758,1759,1760,1761,1762,1763,1764,1765,1766,1767,1768,1769,1770,1771,1772,1773,1774,1775,1776,1777,1778,1779,1780,1781,1782,1783,1784,1785,1786,1787,1788,1789,1790,1791,1792,1793,1794,1795,1796,1797,1798,1799,
1800,1801,1802,1803,1804,1805,1806,1807,1808,1809,1810,1811,1812,1813,1814,1815,1816,1817,1818,1819,1820,1821,1822,1823,1824,1825,1826,1827,1828,1829,1830,1831,1832,1833,1834,1835,1836,1837,1838,1839,1840,1841,1842,1843,1844,1845,1846,1847,1848,1849,1850,1851,1852,1853,1854,1855,1856,1857,1858,1859,1860,1861,1862,1863,1864,1865,1866,1867,1868,1869,1870,1871,1872,1873,1874,1875,1876,1877,1878,1879,1880,1881,1882,1883,1884,1885,1886,1887,1888,1889,1890,1891,1892,1893,1894,1895,1896,1897,1898,1899,
1900,1901,1902,1903,1904,1905,1906,1907,1908,1909,1910,1911,1912,1913,1914,1915,1916,1917,1918,1919,1920,1921,1922,1923,1924,1925,1926,1927,1928,1929,1930,1931,1932,1933,1934,1935,1936,1937,1938,1939,1940,1941,1942,1943,1944,1945,1946,1947,1948,1949,1950,1951,1952,1953,1954,1955,1956,1957,1958,1959,1960,1961,1962,1963,1964,1965,1966,1967,1968,1969,1970,1971,1972,1973,1974,1975,1976,1977,1978,1979,1980,1981,1982,1983,1984,1985,1986,1987,1988,1989,1990,1991,1992,1993,1994,1995,1996,1997,1998,1999,
2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018,2019,2020,2021,2022,2023,2024,2025,2026,2027,2028,2029,2030,2031,2032,2033,2034,2035,2036,2037,2038,2039,2040,2041,2042,2043,2044,2045,2046,2047,2048,2049,2050,2051,2052,2053,2054,2055,2056,2057,2058,2059,2060,2061,2062,2063,2064,2065,2066,2067,2068,2069,2070,2071,2072,2073,2074,2075,2076,2077,2078,2079,2080,2081,2082,2083,2084,2085,2086,2087,2088,2089,2090,2091,2092,2093,2094,2095,2096,2097,2098,2099,
2100,2101,2102,2103,2104,2105,2106,2107,2108,2109,2110,2111,2112,2113,2114,2115,2116,2117,2118,2119,2120,2121,2122,2123,2124,2125,2126,2127,2128,2129,2130,2131,2132,2133,2134,2135,2136,2137,2138,2139,2140,2141,2142,2143,2144,2145,2146,2147,2148,2149,2150,2151,2152,2153,2154,2155,2156,2157,2158,2159,2160,2161,2162,2163,2164,2165,2166,2167,2168,2169,2170,2171,2172,2173,2174,2175,2176,2177,2178,2179,2180,2181,2182,2183,2184,2185,2186,2187,2188,2189,2190,2191,2192,2193,2194,2195,2196,2197,2198,2199,
2200,2201,2202,2203,2204,2205,2206,2207,2208,2209,2210,2211,2212,2213,2214,2215,2216,2217,2218,2219,2220,2221,2222,2223,2224,2225,2226,2227,2228,2229,2230,2231,2232,2233,2234,2235,2236,2237,2238,2239,2240,2241,2242,2243,2244,2245,2246,2247,2248,2249,2250,2251,2252,2253,2254,2255,2256,2257,2258,2259,2260,2261,2262,2263,2264,2265,2266,2267,2268,2269,2270,2271,2272,2273,2274,2275,2276,2277,2278,2279,2280,2281,2282,2283,2284,2285,2286,2287,2288,2289,2290,2291,2292,2293,2294,2295,2296,2297,2298,2299,
2300,2301,2302,2303,2304,2305,2306,2307,2308,2309,2310,2311,2312,2313,2314,2315,2316,2317,2318,2319,2320,2321,2322,2323,2324,2325,2326,2327,2328,2329,2330,2331,2332,2333,2334,2335,2336,2337,2338,2339,2340,2341,2342,2343,2344,2345,2346,2347,2348,2349,2350,2351,2352,2353,2354,2355,2356,2357,2358,2359,2360,2361,2362,2363,2364,2365,2366,2367,2368,2369,2370,2371,2372,2373,2374,2375,2376,2377,2378,2379,2380,2381,2382,2383,2384,2385,2386,2387,2388,2389,2390,2391,2392,2393,2394,2395,2396,2397,2398,2399,
2400,2401,2402,2403,2404,2405,2406,2407,2408,2409,2410,2411,2412,2413,2414,2415,2416,2417,2418,2419,2420,2421,2422,2423,2424,2425,2426,2427,2428,2429,2430,2431,2432,2433,2434,2435,2436,2437,2438,2439,2440,2441,2442,2443,2444,2445,2446,2447,2448,2449,2450,2451,2452,2453,2454,2455,2456,2457,2458,2459,2460,2461,2462,2463,2464,2465,2466,2467,2468,2469,2470,2471,2472,2473,2474,2475,2476,2477,2478,2479,2480,2481,2482,2483,2484,2485,2486,2487,2488,2489,2490,2491,2492,2493,2494,2495,2496,2497,2498,2499,
2500,2501,2502,2503,2504,2505,2506,2507,2508,2509,2510,2511,2512,2513,2514,2515,2516,2517,2518,2519,2520,2521,2522,2523,2524,2525,2526,2527,2528,2529,2530,2531,2532,2533,2534,2535,2536,2537,2538,2539,2540,2541,2542,2543,2544,2545,2546,2547,2548,2549,2550,2551,2552,2553,2554,2555,2556,2557,2558,2559,2560,2561,2562,2563,2564,2565,2566,2567,2568,2569,2570,2571,2572,2573,2574,2575,2576,2577,2578,2579,2580,2581,2582,2583,2584,2585,2586,2587,2588,2589,2590,2591,2592,2593,2594,2595,2596,2597,2598,2599,
2600,2601,2602,2603,2604,2605,2606,2607,2608,2609,2610,2611,2612,2613,2614,2615,2616,2617,2618,2619,2620,2621,2622,2623,2624,2625,2626,2627,2628,2629,2630,2631,2632,2633,2634,2635,2636,2637,2638,2639,2640,2641,2642,2643,2644,2645,2646,2647,2648,2649,2650,2651,2652,2653,2654,2655,2656,2657,2658,2659,2660,2661,2662,2663,2664,2665,2666,2667,2668,2669,2670,2671,2672,2673,2674,2675,2676,2677,2678,2679,2680,2681,2682,2683,2684,2685,2686,2687,2688,2689,2690,2691,2692,2693,2694,2695,2696,2697,2698,2699,
2700,2701,2702,2703,2704,2705,2706,2707,2708,2709,2710,2711,2712,2713,2714,2715,2716,2717,2718,2719,2720,2721,2722,2723,2724,2725,2726,2727,2728,2729,2730,2731,2732,2733,2734,2735,2736,2737,2738,2739,2740,2741,2742,2743,2744,2745,2746,2747,2748,2749,2750,2751,2752,2753,2754,2755,2756,2757,2758,2759,2760,2761,2762,2763,2764,2765,2766,2767,2768,2769,2770,2771,2772,2773,2774,2775,2776,2777,2778,2779,2780,2781,2782,2783,2784,2785,2786,2787,2788,2789,2790,2791,2792,2793,2794,2795,2796,2797,2798,2799,
2800,2801,2802,2803,2804,2805,2806,2807,2808,2809,2810,2811,2812,2813,2814,2815,2816,2817,2818,2819,2820,2821,2822,2823,2824,2825,2826,2827,2828,2829,2830,2831,2832,2833,2834,2835,2836,2837,2838,2839,2840,2841,2842,2843,2844,2845,2846,2847,2848,2849,2850,2851,2852,2853,2854,2855,2856,2857,2858,2859,2860,2861,2862,2863,2864,2865,2866,2867,2868,2869,2870,2871,2872,2873,2874,2875,2876,2877,2878,2879,2880,2881,2882,2883,2884,2885,2886,2887,2888,2889,2890,2891,2892,2893,2894,2895,2896,2897,2898,2899,
2900,2901,2902,2903,2904,2905,2906,2907,2908,2909,2910,2911,2912,2913,2914,2915,2916,2917,2918,2919,2920,2921,2922,2923,2924,2925,2926,2927,2928,2929,2930,2931,2932,2933,2934,2935,2936,2937,2938,2939,2940,2941,2942,2943,2944,2945,2946,2947,2948,2949,2950,2951,2952,2953,2954,2955,2956,2957,2958,2959,2960,2961,2962,2963,2964,2965,2966,2967,2968,2969,2970,2971,2972,2973,2974,2975,2976,2977,2978,2979,2980,2981,2982,2983,2984,2985,2986,2987,2988,2989,2990,2991,2992,2993,2994,2995,2996,2997,2998,2999,
3000,3001,3002,3003,3004,3005,3006,3007,3008,3009,3010,3011,3012,3013,3014,3015,3016,3017,3018,3019,3020,3021,3022,3023,3024,3025,3026,3027,3028,3029,3030,3031,3032,3033,3034,3035,3036,3037,3038,3039,3040,3041,3042,3043,3044,3045,3046,3047,3048,3049,3050,3051,3052,3053,3054,3055,3056,3057,3058,3059,3060,3061,3062,3063,3064,3065,3066,3067,3068,3069,3070,3071,3072,3073,3074,3075,3076,3077,3078,3079,3080,3081,3082,3083,3084,3085,3086,3087,3088,3089,3090,3091,3092,3093,3094,3095,3096,3097,3098,3099,
3100,3101,3102,3103,3104,3105,3106,3107,3108,3109,3110,3111,3112,3113,3114,3115,3116,3117,3118,3119,3120,3121,3122,3123,3124,3125,3126,3127,3128,3129,3130,3131,3132,3133,3134,3135,3136,3137,3138,3139,3140,3141,3142,3143,3144,3145,3146,3147,3148,3149,3150,3151,3152,3153,3154,3155,3156,3157,3158,3159,3160,3161,3162,3163,3164,3165,3166,3167,3168,3169,3170,3171,3172,3173,3174,3175,3176,3177,3178,3179,3180,3181,3182,3183,3184,3185,3186,3187,3188,3189,3190,3191,3192,3193,3194,3195,3196,3197,3198,3199,
3200,3201,3202,3203,3204,3205,3206,3207,3208,3209,3210,3211,3212,3213,3214,3215,3216,3217,3218,3219,3220,3221,3222,3223,3224,3225,3226,3227,3228,3229,3230,3231,3232,3233,3234,3235,3236,3237,3238,3239,3240,3241,3242,3243,3244,3245,3246,3247,3248,3249,3250,3251,3252,3253,3254,3255,3256,3257,3258,3259,3260,3261,3262,3263,3264,3265,3266,3267,3268,3269,3270,3271,3272,3273,3274,3275,3276,3277,3278,3279,3280,3281,3282,3283,3284,3285,3286,3287,3288,3289,3290,3291,3292,3293,3294,3295,3296,3297,3298,3299,
3300,3301,3302,3303,3304,3305,3306,3307,3308,3309,3310,3311,3312,3313,3314,3315,3316,3317,3318,3319,3320,3321,3322,3323,3324,3325,3326,3327,3328,3329,3330,3331,3332,3333,3334,3335,3336,3337,3338,3339,3340,3341,3342,3343,3344,3345,3346,3347,3348,3349,3350,3351,3352,3353,3354,3355,3356,3357,3358,3359,3360,3361,3362,3363,3364,3365,3366,3367,3368,3369,3370,3371,3372,3373,3374,3375,3376,3377,3378,3379,3380,3381,3382,3383,3384,3385,3386,3387,3388,3389,3390,3391,3392,3393,3394,3395,3396,3397,3398,3399,
3400,3401,3402,3403,3404,3405,3406,3407,3408,3409,3410,3411,3412,3413,3414,3415,3416,3417,3418,3419,3420,3421,3422,3423,3424,3425,3426,3427,3428,3429,3430,3431,3432,3433,3434,3435,3436,3437,3438,3439,3440,3441,3442,3443,3444,3445,3446,3447,3448,3449,3450,3451,3452,3453,3454,3455,3456,3457,3458,3459,3460,3461,3462,3463,3464,3465,3466,3467,3468,3469,3470,3471,3472,3473,3474,3475,3476,3477,3478,3479,3480,3481,3482,3483,3484,3485,3486,3487,3488,3489,3490,3491,3492,3493,3494,3495,3496,3497,3498,3499,
3500,3501,3502,3503,3504,3505,3506,3507,3508,3509,3510,3511,3512,3513,3514,3515,3516,3517,3518,3519,3520,3521,3522,3523,3524,3525,3526,3527,3528,3529,3530,3531,3532,3533,3534,3535,3536,3537,3538,3539,3540,3541,3542,3543,3544,3545,3546,3547,3548,3549,3550,3551,3552,3553,3554,3555,3556,3557,3558,3559,3560,3561,3562,3563,3564,3565,3566,3567,3568,3569,3570,3571,3572,3573,3574,3575,3576,3577,3578,3579,3580,3581,3582,3583,3584,3585,3586,3587,3588,3589,3590,3591,3592,3593,3594,3595,3596,3597,3598,3599,
3600,3601,3602,3603,3604,3605,3606,3607,3608,3609,3610,3611,3612,3613,3614,3615,3616,3617,3618,3619,3620,3621,3622,3623,3624,3625,3626,3627,3628,3629,3630,3631,3632,3633,3634,3635,3636,3637,3638,3639,3640,3641,3642,3643,3644,3645,3646,3647,3648,3649,3650,3651,3652,3653,3654,3655,3656,3657,3658,3659,3660,3661,3662,3663,3664,3665,3666,3667,3668,3669,3670,3671,3672,3673,3674,3675,3676,3677,3678,3679,3680,3681,3682,3683,3684,3685,3686,3687,3688,3689,3690,3691,3692,3693,3694,3695,3696,3697,3698,3699,
3700,3701,3702,3703,3704,3705,3706,3707,3708,3709,3710,3711,3712,3713,3714,3715,3716,3717,3718,3719,3720,3721,3722,3723,3724,3725,3726,3727,3728,3729,3730,3731,3732,3733,3734,3735,3736,3737,3738,3739,3740,3741,3742,3743,3744,3745,3746,3747,3748,3749,3750,3751,3752,3753,3754,3755,3756,3757,3758,3759,3760,3761,3762,3763,3764,3765,3766,3767,3768,3769,3770,3771,3772,3773,3774,3775,3776,3777,3778,3779,3780,3781,3782,3783,3784,3785,3786,3787,3788,3789,3790,3791,3792,3793,3794,3795,3796,3797,3798,3799,
3800,3801,3802,3803,3804,3805,3806,3807,3808,3809,3810,3811,3812,3813,3814,3815,3816,3817,3818,3819,3820,3821,3822,3823,3824,3825,3826,3827,3828,3829,3830,3831,3832,3833,3834,3835,3836,3837,3838,3839,3840,3841,3842,3843,3844,3845,3846,3847,3848,3849,3850,3851,3852,3853,3854,3855,3856,3857,3858,3859,3860,3861,3862,3863,3864,3865,3866,3867,3868,3869,3870,3871,3872,3873,3874,3875,3876,3877,3878,3879,3880,3881,3882,3883,3884,3885,3886,3887,3888,3889,3890,3891,3892,3893,3894,3895,3896,3897,3898,3899,
3900,3901,3902,3903,3904,3905,3906,3907,3908,3909,3910,3911,3912,3913,3914,3915,3916,3917,3918,3919,3920,3921,3922,3923,3924,3925,3926,3927,3928,3929,3930,3931,3932,3933,3934,3935,3936,3937,3938,3939,3940,3941,3942,3943,3944,3945,3946,3947,3948,3949,3950,3951,3952,3953,3954,3955,3956,3957,3958,3959,3960,3961,3962,3963,3964,3965,3966,3967,3968,3969,3970,3971,3972,3973,3974,3975,3976,3977,3978,3979,3980,3981,3982,3983,3984,3985,3986,3987,3988,3989,3990,3991,3992,3993,3994,3995,3996,3997,3998,3999,
4000,4001,4002,4003,4004,4005,4006,4007,4008,4009,4010,4011,4012,4013,4014,4015,4016,4017,4018,4019,4020,4021,4022,4023,4024,4025,4026,4027,4028,4029,4030,4031,4032,4033,4034,4035,4036,4037,4038,4039,4040,4041,4042,4043,4044,4045,4046,4047,4048,4049,4050,4051,4052,4053,4054,4055,4056,4057,4058,4059,4060,4061,4062,4063,4064,4065,4066,4067,4068,4069,4070,4071,4072,4073,4074,4075,4076,4077,4078,4079,4080,4081,4082,4083,4084,4085,4086,4087,4088,4089,4090,4091,4092,4093,4094,4095,4096,4097,4098,4099,
4100,4101,4102,4103,4104,4105,4106,4107,4108,4109,4110,4111,4112,4113,4114,4115,4116,4117,4118,4119,4120,4121,4122,4123,4124,4125,4126,4127,4128,4129,4130,4131,4132,4133,4134,4135,4136,4137,4138,4139,4140,4141,4142,4143,4144,4145,4146,4147,4148,4149,4150,4151,4152,4153,4154,4155,4156,4157,4158,4159,4160,4161,4162,4163,4164,4165,4166,4167,4168,4169,4170,4171,4172,4173,4174,4175,4176,4177,4178,4179,4180,4181,4182,4183,4184,4185,4186,4187,4188,4189,4190,4191,4192,4193,4194,4195,4196,4197,4198,4199,
4200,4201,4202,4203,4204,4205,4206,4207,4208,4209,4210,4211,4212,4213,4214,4215,4216,4217,4218,4219,4220,4221,4222,4223,4224,4225,4226,4227,4228,4229,4230,4231,4232,4233,4234,4235,4236,4237,4238,4239,4240,4241,4242,4243,4244,4245,4246,4247,4248,4249,4250,4251,4252,4253,4254,4255,4256,4257,4258,4259,4260,4261,4262,4263,4264,4265,4266,4267,4268,4269,4270,4271,4272,4273,4274,4275,4276,4277,4278,4279,4280,4281,4282,4283,4284,4285,4286,4287,4288,4289,4290,4291,4292,4293,4294,4295,4296,4297,4298,4299,
4300,4301,4302,4303,4304,4305,4306,4307,4308,4309,4310,4311,4312,4313,4314,4315,4316,4317,4318,4319,4320,4321,4322,4323,4324,4325,4326,4327,4328,4329,4330,4331,4332,4333,4334,4335,4336,4337,4338,4339,4340,4341,4342,4343,4344,4345,4346,4347,4348,4349,4350,4351,4352,4353,4354,4355,4356,4357,4358,4359,4360,4361,4362,4363,4364,4365,4366,4367,4368,4369,4370,4371,4372,4373,4374,4375,4376,4377,4378,4379,4380,4381,4382,4383,4384,4385,4386,4387,4388,4389,4390,4391,4392,4393,4394,4395,4396,4397,4398,4399,
4400,4401,4402,4403,4404,4405,4406,4407,4408,4409,4410,4411,4412,4413,4414,4415,4416,4417,4418,4419,4420,4421,4422,4423,4424,4425,4426,4427,4428,4429,4430,4431,4432,4433,4434,4435,4436,4437,4438,4439,4440,4441,4442,4443,4444,4445,4446,4447,4448,4449,4450,4451,4452,4453,4454,4455,4456,4457,4458,4459,4460,4461,4462,4463,4464,4465,4466,4467,4468,4469,4470,4471,4472,4473,4474,4475,4476,4477,4478,4479,4480,4481,4482,4483,4484,4485,4486,4487,4488,4489,4490,4491,4492,4493,4494,4495,4496,4497,4498,4499,
4500,4501,4502,4503,4504,4505,4506,4507,4508,4509,4510,4511,4512,4513,4514,4515,4516,4517,4518,4519,4520,4521,4522,4523,4524,4525,4526,4527,4528,4529,4530,4531,4532,4533,4534,4535,4536,4537,4538,4539,4540,4541,4542,4543,4544,4545,4546,4547,4548,4549,4550,4551,4552,4553,4554,4555,4556,4557,4558,4559,4560,4561,4562,4563,4564,4565,4566,4567,4568,4569,4570,4571,4572,4573,4574,4575,4576,4577,4578,4579,4580,4581,4582,4583,4584,4585,4586,4587,4588,4589,4590,4591,4592,4593,4594,4595,4596,4597,4598,4599,
4600,4601,4602,4603,4604,4605,4606,4607,4608,4609,4610,4611,4612,4613,4614,4615,4616,4617,4618,4619,4620,4621,4622,4623,4624,4625,4626,4627,4628,4629,4630,4631,4632,4633,4634,4635,4636,4637,4638,4639,4640,4641,4642,4643,4644,4645,4646,4647,4648,4649,4650,4651,4652,4653,4654,4655,4656,4657,4658,4659,4660,4661,4662,4663,4664,4665,4666,4667,4668,4669,4670,4671,4672,4673,4674,4675,4676,4677,4678,4679,4680,4681,4682,4683,4684,4685,4686,4687,4688,4689,4690,4691,4692,4693,4694,4695,4696,4697,4698,4699,
4700,4701,4702,4703,4704,4705,4706,4707,4708,4709,4710,4711,4712,4713,4714,4715,
];

// these are the synonims
var database_ResourceName_synonim = [
0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,
100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,
200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,
300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377,378,379,380,381,382,383,384,385,386,387,388,389,390,391,392,393,394,395,396,397,398,399,
400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,418,419,420,421,422,423,424,425,426,427,428,429,430,431,432,433,434,435,436,437,438,439,440,441,442,443,444,445,446,447,448,449,450,451,452,453,454,455,456,457,458,459,460,461,462,463,464,465,466,467,468,469,470,471,472,473,474,475,476,477,478,479,480,481,482,483,484,485,486,487,488,489,490,491,492,493,494,495,496,497,498,499,
500,501,502,503,504,505,506,507,508,509,510,511,512,513,514,515,516,517,518,519,520,521,522,523,524,525,526,527,528,529,530,531,532,533,534,535,536,537,538,539,540,541,542,543,544,545,546,547,548,549,550,551,552,553,554,555,556,557,558,559,560,561,562,563,564,565,566,567,568,569,570,571,572,573,574,575,576,577,578,579,580,581,582,583,584,585,586,587,588,589,590,591,592,593,594,595,596,597,598,599,
600,601,602,603,604,605,606,607,608,609,610,611,612,613,614,615,616,617,618,619,620,621,622,623,624,625,626,627,628,629,630,631,632,633,634,635,636,637,638,639,640,641,642,643,644,645,646,647,648,649,650,651,652,653,654,655,656,657,658,659,660,661,662,663,664,665,666,667,668,669,670,671,672,673,674,675,676,677,678,679,680,681,682,683,684,685,686,687,688,689,690,691,692,693,694,695,696,697,698,699,
700,701,702,703,704,705,706,707,708,709,710,711,712,713,714,715,716,717,718,719,720,721,722,723,724,725,726,727,728,729,730,731,732,733,734,735,736,737,738,739,740,741,742,743,744,745,746,747,748,749,750,751,752,753,754,755,756,757,758,759,760,761,762,763,764,765,766,767,768,769,770,771,772,773,774,775,776,777,778,779,780,781,782,783,784,785,786,787,788,789,790,791,792,793,794,795,796,797,798,799,
800,801,802,803,804,805,806,807,808,809,810,811,812,813,814,815,816,817,818,819,820,821,822,823,824,825,826,827,828,829,830,831,832,833,834,835,836,837,838,839,840,841,842,843,844,845,846,847,848,849,850,851,852,853,854,855,856,857,858,859,860,861,862,863,864,865,866,867,868,869,870,871,872,873,874,875,876,877,878,879,880,881,882,883,884,885,886,887,888,889,890,891,892,893,894,895,896,897,898,899,
900,901,902,903,904,905,906,907,908,909,910,911,912,913,914,915,916,917,918,919,920,921,922,923,924,925,926,927,928,929,930,931,932,933,934,935,936,937,938,939,940,941,942,943,944,945,946,947,948,949,950,951,952,953,954,955,956,957,958,959,960,961,962,963,964,965,966,967,968,969,970,971,972,973,974,975,976,977,978,979,980,981,982,983,984,985,986,987,988,989,990,991,992,993,994,995,996,997,998,999,
1000,1001,1002,1003,1004,1005,1006,1007,1008,1009,1010,1011,1012,1013,1014,1015,1016,1017,1018,1019,1020,1021,1022,1023,1024,1025,1026,1027,1028,1029,1030,1031,1032,1033,1034,1035,1036,1037,1038,1039,1040,1041,1042,1043,1044,1045,1046,1047,1048,1049,1050,1051,1052,1053,1054,1055,1056,1057,1058,1059,1060,1061,1062,1063,1064,1065,1066,1067,1068,1069,1070,1071,1072,1073,1074,1075,1076,1077,1078,1079,1080,1081,1082,1083,1084,1085,1086,1087,1088,1089,1090,1091,1092,1093,1094,1095,1096,1097,1098,1099,
1100,1101,1102,1103,1104,1105,1106,1107,1108,1109,1110,1111,1112,1113,1114,1115,1116,1117,1118,1119,1120,1121,1122,1123,1124,1125,1126,1127,1128,1129,1130,1131,1132,1133,1134,1135,1136,1137,1138,1139,1140,1141,1142,1143,1144,1145,1146,1147,1148,1149,1150,1151,1152,1153,1154,1155,1156,1157,1158,1159,1160,1161,1162,1163,1164,1165,1166,1167,1168,1169,1170,1171,1172,1173,1174,1175,1176,1177,1178,1179,1180,1181,1182,1183,1184,1185,1186,1187,1188,1189,1190,1191,1192,1193,1194,1195,1196,1197,1198,1199,
1200,1201,1202,1203,1204,1205,1206,1207,1208,1209,1210,1211,1212,1213,1214,1215,1216,1217,1218,1219,1220,1221,1222,1223,1224,1225,1226,1227,1228,1229,1230,1231,1232,1233,1234,1235,1236,1237,1238,1239,1240,1241,1242,1243,1244,1245,1246,1247,1248,1249,1250,1251,1252,1253,1254,1255,1256,1257,1258,1259,1260,1261,1262,1263,1264,1265,1266,1267,1268,1269,1270,1271,1272,1273,1274,1275,1276,1277,1278,1279,1280,1281,1282,1283,1284,1285,1286,1287,1288,1289,1290,1291,1292,1293,1294,1295,1296,1297,1298,1299,
1300,1301,1302,1303,1304,1305,1306,1307,1308,1309,1310,1311,1312,1313,1314,1315,1316,1317,1318,1319,1320,1321,1322,1323,1324,1325,1326,1327,1328,1329,1330,1331,1332,1333,1334,1335,1336,1337,1338,1339,1340,1341,1342,1343,1344,1345,1346,1347,1348,1349,1350,1351,1352,1353,1354,1355,1356,1357,1358,1359,1360,1361,1362,1363,1364,1365,1366,1367,1368,1369,1370,1371,1372,1373,1374,1375,1376,1377,1378,1379,1380,1381,1382,1383,1384,1385,1386,1387,1388,1389,1390,1391,1392,1393,1394,1395,1396,1397,1398,1399,
1400,1401,1402,1403,1404,1405,1406,1407,1408,1409,1410,1411,1412,1413,1414,1415,1416,1417,1418,1419,1420,1421,1422,1423,1424,1425,1426,1427,1428,1429,1430,1431,1432,1433,1434,1435,1436,1437,1438,1439,1440,1441,1442,1443,1444,1445,1446,1447,1448,1449,1450,1451,1452,1453,1454,1455,1456,1457,1458,1459,1460,1461,1462,1463,1464,1465,1466,1467,1468,1469,1470,1471,1472,1473,1474,1475,1476,1477,1478,1479,1480,1481,1482,1483,1484,1485,1486,1487,1488,1489,1490,1491,1492,1493,1494,1495,1496,1497,1498,1499,
1500,1501,1502,1503,1504,1505,1506,1507,1508,1509,1510,1511,1512,1513,1514,1515,1516,1517,1518,1519,1520,1521,1522,1523,1524,1525,1526,1527,1528,1529,1530,1531,1532,1533,1534,1535,1536,1537,1538,1539,1540,1541,1542,1543,1544,1545,1546,1547,1548,1549,1550,1551,1552,1553,1554,1555,1556,1557,1558,1559,1560,1561,1562,1563,1564,1565,1566,1567,1568,1569,1570,1571,1572,1573,1574,1575,1576,1577,1578,1579,1580,1581,1582,1583,1584,1585,1586,1587,1588,1589,1590,1591,1592,1593,1594,1595,1596,1597,1598,1599,
1600,1601,1602,1603,1604,1605,1606,1607,1608,1609,1610,1611,1612,1613,1614,1615,1616,1617,1618,1619,1620,1621,1622,1623,1624,1625,1626,1627,1628,1629,1630,1631,1632,1633,1634,1635,1636,1637,1638,1639,1640,1641,1642,1643,1644,1645,1646,1647,1648,1649,1650,1651,1652,1653,1654,1655,1656,1657,1658,1659,1660,1661,1662,1663,1664,1665,1666,1667,1668,1669,1670,1671,1672,1673,1674,1675,1676,1677,1678,1679,1680,1681,1682,1683,1684,1685,1686,1687,1688,1689,1690,1691,1692,1693,1694,1695,1696,1697,1698,1699,
1700,1701,1702,1703,1704,1705,1706,1707,1708,1709,1710,1711,1712,1713,1714,1715,1716,1717,1718,1719,1720,1721,1722,1723,1724,1725,1726,1727,1728,1729,1730,1731,1732,1733,1734,1735,1736,1737,1738,1739,1740,1741,1742,1743,1744,1745,1746,1747,1748,1749,1750,1751,1752,1753,1754,1755,1756,1757,1758,1759,1760,1761,1762,1763,1764,1765,1766,1767,1768,1769,1770,1771,1772,1773,1774,1775,1776,1777,1778,1779,1780,1781,1782,1783,1784,1785,1786,1787,1788,1789,1790,1791,1792,1793,1794,1795,1796,1797,1798,1799,
1800,1801,1802,1803,1804,1805,1806,1807,1808,1809,1810,1811,1812,1813,1814,1815,1816,1817,1818,1819,1820,1821,1822,1823,1824,1825,1826,1827,1828,1829,1830,1831,1832,1833,1834,1835,1836,1837,1838,1839,1840,1841,1842,1843,1844,1845,1846,1847,1848,1849,1850,1851,1852,1853,1854,1855,1856,1857,1858,1859,1860,1861,1862,1863,1864,1865,1866,1867,1868,1869,1870,1871,1872,1873,1874,1875,1876,1877,1878,1879,1880,1881,1882,1883,1884,1885,1886,1887,1888,1889,1890,1891,1892,1893,1894,1895,1896,1897,1898,1899,
1900,1901,1902,1903,1904,1905,1906,1907,1908,1909,1910,1911,1912,1913,1914,1915,1916,1917,1918,1919,1920,1921,1922,1923,1924,1925,1926,1927,1928,1929,1930,1931,1932,1933,1934,1935,1936,1937,1938,1939,1940,1941,1942,1943,1944,1945,1946,1947,1948,1949,1950,1951,1952,1953,1954,1955,1956,1957,1958,1959,1960,1961,1962,1963,1964,1965,1966,1967,1968,1969,1970,1971,1972,1973,1974,1975,1976,1977,1978,1979,1980,1981,1982,1983,1984,1985,1986,1987,1988,1989,1990,1991,1992,1993,1994,1995,1996,1997,1998,1999,
2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018,2019,2020,2021,2022,2023,2024,2025,2026,2027,2028,2029,2030,2031,2032,2033,2034,2035,2036,2037,2038,2039,2040,2041,2042,2043,2044,2045,2046,2047,2048,2049,2050,2051,2052,2053,2054,2055,2056,2057,2058,2059,2060,2061,2062,2063,2064,2065,2066,2067,2068,2069,2070,2071,2072,2073,2074,2075,2076,2077,2078,2079,2080,2081,2082,2083,2084,2085,2086,2087,2088,2089,2090,2091,2092,2093,2094,2095,2096,2097,2098,2099,
2100,2101,2102,2103,2104,2105,2106,2107,2108,2109,2110,2111,2112,2113,2114,2115,2116,2117,2118,2119,2120,2121,2122,2123,2124,2125,2126,2127,2128,2129,2130,2131,2132,2133,2134,2135,2136,2137,2138,2139,2140,2141,2142,2143,2144,2145,2146,2147,2148,2149,2150,2151,2152,2153,2154,2155,2156,2157,2158,2159,2160,2161,2162,2163,2164,2165,2166,2167,2168,2169,2170,2171,2172,2173,2174,2175,2176,2177,2178,2179,2180,2181,2182,2183,2184,2185,2186,2187,2188,2189,2190,2191,2192,2193,2194,2195,2196,2197,2198,2199,
2200,2201,2202,2203,2204,2205,2206,2207,2208,2209,2210,2211,2212,2213,2214,2215,2216,2217,2218,2219,2220,2221,2222,2223,2224,2225,2226,2227,2228,2229,2230,2231,2232,2233,2234,2235,2236,2237,2238,2239,2240,2241,2242,2243,2244,2245,2246,2247,2248,2249,2250,2251,2252,2253,2254,2255,2256,2257,2258,2259,2260,2261,2262,2263,2264,2265,2266,2267,2268,2269,2270,2271,2272,2273,2274,2275,2276,2277,2278,2279,2280,2281,2282,2283,2284,2285,2286,2287,2288,2289,2290,2291,2292,2293,2294,2295,2296,2297,2298,2299,
2300,2301,2302,2303,2304,2305,2306,2307,2308,2309,2310,2311,2312,2313,2314,2315,2316,2317,2318,2319,2320,2321,2322,2323,2324,2325,2326,2327,2328,2329,2330,2331,2332,2333,2334,2335,2336,2337,2338,2339,2340,2341,2342,2343,2344,2345,2346,2347,2348,2349,2350,2351,2352,2353,2354,2355,2356,2357,2358,2359,2360,2361,2362,2363,2364,2365,2366,2367,2368,2369,2370,2371,2372,2373,2374,2375,2376,2377,2378,2379,2380,2381,2382,2383,2384,2385,2386,2387,2388,2389,2390,2391,2392,2393,2394,2395,2396,2397,2398,2399,
2400,2401,2402,2403,2404,2405,2406,2407,2408,2409,2410,2411,2412,2413,2414,2415,2416,2417,2418,2419,2420,2421,2422,2423,2424,2425,2426,2427,2428,2429,2430,2431,2432,2433,2434,2435,2436,2437,2438,2439,2440,2441,2442,2443,2444,2445,2446,2447,2448,2449,2450,2451,2452,2453,2454,2455,2456,2457,2458,2459,2460,2461,2462,2463,2464,2465,2466,2467,2468,2469,2470,2471,2472,2473,2474,2475,2476,2477,2478,2479,2480,2481,2482,2483,2484,2485,2486,2487,2488,2489,2490,2491,2492,2493,2494,2495,2496,2497,2498,2499,
2500,2501,2502,2503,2504,2505,2506,2507,2508,2509,2510,2511,2512,2513,2514,2515,2516,2517,2518,2519,2520,2521,2522,2523,2524,2525,2526,2527,2528,2529,2530,2531,2532,2533,2534,2535,2536,2537,2538,2539,2540,2541,2542,2543,2544,2545,2546,2547,2548,2549,2550,2551,2552,2553,2554,2555,2556,2557,2558,2559,2560,2561,2562,2563,2564,2565,2566,2567,2568,2569,2570,2571,2572,2573,2574,2575,2576,2577,2578,2579,2580,2581,2582,2583,2584,2585,2586,2587,2588,2589,2590,2591,2592,2593,2594,2595,2596,2597,2598,2599,
2600,2601,2602,2603,2604,2605,2606,2607,2608,2609,2610,2611,2612,2613,2614,2615,2616,2617,2618,2619,2620,2621,2622,2623,2624,2625,2626,2627,2628,2629,2630,2631,2632,2633,2634,2635,2636,2637,2638,2639,2640,2641,2642,2643,2644,2645,2646,2647,2648,2649,2650,2651,2652,2653,2654,2655,2656,2657,2658,2659,2660,2661,2662,2663,2664,2665,2666,2667,2668,2669,2670,2671,2672,2673,2674,2675,2676,2677,2678,2679,2680,2681,2682,2683,2684,2685,2686,2687,2688,2689,2690,2691,2692,2693,2694,2695,2696,2697,2698,2699,
2700,2701,2702,2703,2704,2705,2706,2707,2708,2709,2710,2711,2712,2713,2714,2715,2716,2717,2718,2719,2720,2721,2722,2723,2724,2725,2726,2727,2728,2729,2730,2731,2732,2733,2734,2735,2736,2737,2738,2739,2740,2741,2742,2743,2744,2745,2746,2747,2748,2749,2750,2751,2752,2753,2754,2755,2756,2757,2758,2759,2760,2761,2762,2763,2764,2765,2766,2767,2768,2769,2770,2771,2772,2773,2774,2775,2776,2777,2778,2779,2780,2781,2782,2783,2784,2785,2786,2787,2788,2789,2790,2791,2792,2793,2794,2795,2796,2797,2798,2799,
2800,2801,2802,2803,2804,2805,2806,2807,2808,2809,2810,2811,2812,2813,2814,2815,2816,2817,2818,2819,2820,2821,2822,2823,2824,2825,2826,2827,2828,2829,2830,2831,2832,2833,2834,2835,2836,2837,2838,2839,2840,2841,2842,2843,2844,2845,2846,2847,2848,2849,2850,2851,2852,2853,2854,2855,2856,2857,2858,2859,2860,2861,2862,2863,2864,2865,2866,2867,2868,2869,2870,2871,2872,2873,2874,2875,2876,2877,2878,2879,2880,2881,2882,2883,2884,2885,2886,2887,2888,2889,2890,2891,2892,2893,2894,2895,2896,2897,2898,2899,
2900,2901,2902,2903,2904,2905,2906,2907,2908,2909,2910,2911,2912,2913,2914,2915,2916,2917,2918,2919,2920,2921,2922,2923,2924,2925,2926,2927,2928,2929,2930,2931,2932,2933,2934,2935,2936,2937,2938,2939,2940,2941,2942,2943,2944,2945,2946,2947,2948,2949,2950,2951,2952,2953,2954,2955,2956,2957,2958,2959,2960,2961,2962,2963,2964,2965,2966,2967,2968,2969,2970,2971,2972,2973,2974,2975,2976,2977,2978,2979,2980,2981,2982,2983,2984,2985,2986,2987,2988,2989,2990,2991,2992,2993,2994,2995,2996,2997,2998,2999,
3000,3001,3002,3003,3004,3005,3006,3007,3008,3009,3010,3011,3012,3013,3014,3015,3016,3017,3018,3019,3020,3021,3022,3023,3024,3025,3026,3027,3028,3029,3030,3031,3032,3033,3034,3035,3036,3037,3038,3039,3040,3041,3042,3043,3044,3045,3046,3047,3048,3049,3050,3051,3052,3053,3054,3055,3056,3057,3058,3059,3060,3061,3062,3063,3064,3065,3066,3067,3068,3069,3070,3071,3072,3073,3074,3075,3076,3077,3078,3079,3080,3081,3082,3083,3084,3085,3086,3087,3088,3089,3090,3091,3092,3093,3094,3095,3096,3097,3098,3099,
3100,3101,3102,3103,3104,3105,3106,3107,3108,3109,3110,3111,3112,3113,3114,3115,3116,3117,3118,3119,3120,3121,3122,3123,3124,3125,3126,3127,3128,3129,3130,3131,3132,3133,3134,3135,3136,3137,3138,3139,3140,3141,3142,3143,3144,3145,3146,3147,3148,3149,3150,3151,3152,3153,3154,3155,3156,3157,3158,3159,3160,3161,3162,3163,3164,3165,3166,3167,3168,3169,3170,3171,3172,3173,3174,3175,3176,3177,3178,3179,3180,3181,3182,3183,3184,3185,3186,3187,3188,3189,3190,3191,3192,3193,3194,3195,3196,3197,3198,3199,
3200,3201,3202,3203,3204,3205,3206,3207,3208,3209,3210,3211,3212,3213,3214,3215,3216,3217,3218,3219,3220,3221,3222,3223,3224,3225,3226,3227,3228,3229,3230,3231,3232,3233,3234,3235,3236,3237,3238,3239,3240,3241,3242,3243,3244,3245,3246,3247,3248,3249,3250,3251,3252,3253,3254,3255,3256,3257,3258,3259,3260,3261,3262,3263,3264,3265,3266,3267,3268,3269,3270,3271,3272,3273,3274,3275,3276,3277,3278,3279,3280,3281,3282,3283,3284,3285,3286,3287,3288,3289,3290,3291,3292,3293,3294,3295,3296,3297,3298,3299,
3300,3301,3302,3303,3304,3305,3306,3307,3308,3309,3310,3311,3312,3313,3314,3315,3316,3317,3318,3319,3320,3321,3322,3323,3324,3325,3326,3327,3328,3329,3330,3331,3332,3333,3334,3335,3336,3337,3338,3339,3340,3341,3342,3343,3344,3345,3346,3347,3348,3349,3350,3351,3352,3353,3354,3355,3356,3357,3358,3359,3360,3361,3362,3363,3364,3365,3366,3367,3368,3369,3370,3371,3372,3373,3374,3375,3376,3377,3378,3379,3380,3381,3382,3383,3384,3385,3386,3387,3388,3389,3390,3391,3392,3393,3394,3395,3396,3397,3398,3399,
3400,3401,3402,3403,3404,3405,3406,3407,3408,3409,3410,3411,3412,3413,3414,3415,3416,3417,3418,3419,3420,3421,3422,3423,3424,3425,3426,3427,3428,3429,3430,3431,3432,3433,3434,3435,3436,3437,3438,3439,3440,3441,3442,3443,3444,3445,3446,3447,3448,3449,3450,3451,3452,3453,3454,3455,3456,3457,3458,3459,3460,3461,3462,3463,3464,3465,3466,3467,3468,3469,3470,3471,3472,3473,3474,3475,3476,3477,3478,3479,3480,3481,3482,3483,3484,3485,3486,3487,3488,3489,3490,3491,3492,3493,3494,3495,3496,3497,3498,3499,
3500,3501,3502,3503,3504,3505,3506,3507,3508,3509,3510,3511,3512,3513,3514,3515,3516,3517,3518,3519,3520,3521,3522,3523,3524,3525,3526,3527,3528,3529,3530,3531,3532,3533,3534,3535,3536,3537,3538,3539,3540,3541,3542,3543,3544,3545,3546,3547,3548,3549,3550,3551,3552,3553,3554,3555,3556,3557,3558,3559,3560,3561,3562,3563,3564,3565,3566,3567,3568,3569,3570,3571,3572,3573,3574,3575,3576,3577,3578,3579,3580,3581,3582,3583,3584,3585,3586,3587,3588,3589,3590,3591,3592,3593,3594,3595,3596,3597,3598,3599,
3600,3601,3602,3603,3604,3605,3606,3607,3608,3609,3610,3611,3612,3613,3614,3615,3616,3617,3618,3619,3620,3621,3622,3623,3624,3625,3626,3627,3628,3629,3630,3631,3632,3633,3634,3635,3636,3637,3638,3639,3640,3641,3642,3643,3644,3645,3646,3647,3648,3649,3650,3651,3652,3653,3654,3655,3656,3657,3658,3659,3660,3661,3662,3663,3664,3665,3666,3667,3668,3669,3670,3671,3672,3673,3674,3675,3676,3677,3678,3679,3680,3681,3682,3683,3684,3685,3686,3687,3688,3689,3690,3691,3692,3693,3694,3695,3696,3697,3698,3699,
3700,3701,3702,3703,3704,3705,3706,3707,3708,3709,3710,3711,3712,3713,3714,3715,3716,3717,3718,3719,3720,3721,3722,3723,3724,3725,3726,3727,3728,3729,3730,3731,3732,3733,3734,3735,3736,3737,3738,3739,3740,3741,3742,3743,3744,3745,3746,3747,3748,3749,3750,3751,3752,3753,3754,3755,3756,3757,3758,3759,3760,3761,3762,3763,3764,3765,3766,3767,3768,3769,3770,3771,3772,3773,3774,3775,3776,3777,3778,3779,3780,3781,3782,3783,3784,3785,3786,3787,3788,3789,3790,3791,3792,3793,3794,3795,3796,3797,3798,3799,
3800,3801,3802,3803,3804,3805,3806,3807,3808,3809,3810,3811,3812,3813,3814,3815,3816,3817,3818,3819,3820,3821,3822,3823,3824,3825,3826,3827,3828,3829,3830,3831,3832,3833,3834,3835,3836,3837,3838,3839,3840,3841,3842,3843,3844,3845,3846,3847,3848,3849,3850,3851,3852,3853,3854,3855,3856,3857,3858,3859,3860,3861,3862,3863,3864,3865,3866,3867,3868,3869,3870,3871,3872,3873,3874,3875,3876,3877,3878,3879,3880,3881,3882,3883,3884,3885,3886,3887,3888,3889,3890,3891,3892,3893,3894,3895,3896,3897,3898,3899,
3900,3901,3902,3903,3904,3905,3906,3907,3908,3909,3910,3911,3912,3913,3914,3915,3916,3917,3918,3919,3920,3921,3922,3923,3924,3925,3926,3927,3928,3929,3930,3931,3932,3933,3934,3935,3936,3937,3938,3939,3940,3941,3942,3943,3944,3945,3946,3947,3948,3949,3950,3951,3952,3953,3954,3955,3956,3957,3958,3959,3960,3961,3962,3963,3964,3965,3966,3967,3968,3969,3970,3971,3972,3973,3974,3975,3976,3977,3978,3979,3980,3981,3982,3983,3984,3985,3986,3987,3988,3989,3990,3991,3992,3993,3994,3995,3996,3997,3998,3999,
4000,4001,4002,4003,4004,4005,4006,4007,4008,4009,4010,4011,4012,4013,4014,4015,4016,4017,4018,4019,4020,4021,4022,4023,4024,4025,4026,4027,4028,4029,4030,4031,4032,4033,4034,4035,4036,4037,4038,4039,4040,4041,4042,4043,4044,4045,4046,4047,4048,4049,4050,4051,4052,4053,4054,4055,4056,4057,4058,4059,4060,4061,4062,4063,4064,4065,4066,4067,4068,4069,4070,4071,4072,4073,4074,4075,4076,4077,4078,4079,4080,4081,4082,4083,4084,4085,4086,4087,4088,4089,4090,4091,4092,4093,4094,4095,4096,4097,4098,4099,
4100,4101,4102,4103,4104,4105,4106,4107,4108,4109,4110,4111,4112,4113,4114,4115,4116,4117,4118,4119,4120,4121,4122,4123,4124,4125,4126,4127,4128,4129,4130,4131,4132,4133,4134,4135,4136,4137,4138,4139,4140,4141,4142,4143,4144,4145,4146,4147,4148,4149,4150,4151,4152,4153,4154,4155,4156,4157,4158,4159,4160,4161,4162,4163,4164,4165,4166,4167,4168,4169,4170,4171,4172,4173,4174,4175,4176,4177,4178,4179,4180,4181,4182,4183,4184,4185,4186,4187,4188,4189,4190,4191,4192,4193,4194,4195,4196,4197,4198,4199,
4200,4201,4202,4203,4204,4205,4206,4207,4208,4209,4210,4211,4212,4213,4214,4215,4216,4217,4218,4219,4220,4221,4222,4223,4224,4225,4226,4227,4228,4229,4230,4231,4232,4233,4234,4235,4236,4237,4238,4239,4240,4241,4242,4243,4244,4245,4246,4247,4248,4249,4250,4251,4252,4253,4254,4255,4256,4257,4258,4259,4260,4261,4262,4263,4264,4265,4266,4267,4268,4269,4270,4271,4272,4273,4274,4275,4276,4277,4278,4279,4280,4281,4282,4283,4284,4285,4286,4287,4288,4289,4290,4291,4292,4293,4294,4295,4296,4297,4298,4299,
4300,4301,4302,4303,4304,4305,4306,4307,4308,4309,4310,4311,4312,4313,4314,4315,4316,4317,4318,4319,4320,4321,4322,4323,4324,4325,4326,4327,4328,4329,4330,4331,4332,4333,4334,4335,4336,4337,4338,4339,4340,4341,4342,4343,4344,4345,4346,4347,4348,4349,4350,4351,4352,4353,4354,4355,4356,4357,4358,4359,4360,4361,4362,4363,4364,4365,4366,4367,4368,4369,4370,4371,4372,4373,4374,4375,4376,4377,4378,4379,4380,4381,4382,4383,4384,4385,4386,4387,4388,4389,4390,4391,4392,4393,4394,4395,4396,4397,4398,4399,
4400,4401,4402,4403,4404,4405,4406,4407,4408,4409,4410,4411,4412,4413,4414,4415,4416,4417,4418,4419,4420,4421,4422,4423,4424,4425,4426,4427,4428,4429,4430,4431,4432,4433,4434,4435,4436,4437,4438,4439,4440,4441,4442,4443,4444,4445,4446,4447,4448,4449,4450,4451,4452,4453,4454,4455,4456,4457,4458,4459,4460,4461,4462,4463,4464,4465,4466,4467,4468,4469,4470,4471,4472,4473,4474,4475,4476,4477,4478,4479,4480,4481,4482,4483,4484,4485,4486,4487,4488,4489,4490,4491,4492,4493,4494,4495,4496,4497,4498,4498,
4499,4499,4500,4500,4501,4501,4502,4503,4503,4504,4504,4505,4505,4506,4507,4507,4508,4508,4509,4509,4510,4510,4511,4511,4512,4512,4513,4514,4514,4515,4515,4516,4517,4517,4518,4518,4519,4519,4520,4520,4521,4521,4522,4522,4523,4523,4524,4524,4525,4526,4526,4527,4528,4528,4529,4529,4530,4530,4531,4531,4532,4532,4533,4533,4534,4534,4535,4535,4536,4536,4537,4537,4538,4538,4539,4539,4540,4540,4541,4541,4542,4542,4528,4543,4543,4544,4544,4545,4545,4546,4546,4547,4547,4548,4548,4549,4549,4550,4550,4551,
4551,4552,4552,4553,4553,4554,4554,4555,4556,4557,4558,4558,4559,4559,4560,4560,4561,4561,4562,4562,4563,4563,4564,4565,4565,4566,4566,4567,4567,4568,4568,4569,4569,4570,4570,4571,4571,4572,4572,4573,4573,4574,4574,4575,4575,4576,4576,4577,4578,4579,4579,4580,4581,4581,3175,4582,4583,4583,4584,4584,4585,4586,4586,4587,4587,4588,4588,4589,4589,4497,4590,4590,4591,4591,4592,4592,4593,4593,4594,4595,4595,4596,4596,4597,4597,3175,4598,4598,4599,4599,4600,4600,4601,4601,4602,4602,4603,4603,4604,4604,
4605,4605,4606,4606,4607,4607,4608,4608,4609,4609,4610,4610,4611,4611,4612,4612, 
];

// these are the real (unique) names
var database_ResourceName_normalized = [
0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,
100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,
200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,
300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377,378,379,380,381,382,383,384,385,386,387,388,389,390,391,392,393,394,395,396,397,398,399,
400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,418,419,420,421,422,423,424,425,426,427,428,429,430,431,432,433,434,435,436,437,438,439,440,441,442,443,444,445,446,447,448,449,450,451,452,453,454,455,456,457,458,459,460,461,462,463,464,465,466,467,468,469,470,471,472,473,474,475,476,477,478,479,480,481,482,483,484,485,486,487,488,489,490,491,492,493,494,495,496,497,498,499,
500,501,502,503,504,505,506,507,508,509,510,511,512,513,514,515,516,517,518,519,520,521,522,523,524,525,526,527,528,529,530,531,532,533,534,535,536,537,538,539,540,541,542,543,544,545,546,547,548,549,550,551,552,553,554,555,556,557,558,559,560,561,562,563,564,565,566,567,568,569,570,571,572,573,574,575,576,577,578,579,580,581,582,583,584,585,586,587,588,589,590,591,592,593,594,595,596,597,598,599,
600,601,602,603,604,605,606,607,608,609,610,611,612,613,614,615,616,617,618,619,620,621,622,623,624,625,626,627,628,629,630,631,632,633,634,635,636,637,638,639,640,641,642,643,644,645,646,647,648,649,650,651,652,653,654,655,656,657,658,659,660,661,662,663,664,665,666,667,668,669,670,671,672,673,674,675,676,677,678,679,680,681,682,683,684,685,686,687,688,689,690,691,692,693,694,695,696,697,698,699,
700,701,702,703,704,705,706,707,708,709,710,711,712,713,714,715,716,717,718,719,720,721,722,723,724,725,726,727,728,729,730,731,732,733,734,735,736,737,738,739,740,741,742,743,744,745,746,747,748,749,750,751,752,753,754,755,756,757,758,759,760,761,762,763,764,765,766,767,768,769,770,771,772,773,774,775,776,777,778,779,780,781,782,783,784,785,786,787,788,789,790,791,792,793,794,795,796,797,798,799,
800,801,802,803,804,805,806,807,808,809,810,811,812,813,814,815,816,817,818,819,820,821,822,823,824,825,826,827,828,829,830,831,832,833,834,835,836,837,838,839,840,841,842,843,844,845,846,847,848,849,850,851,852,853,854,855,856,857,858,859,860,861,862,863,864,865,866,867,868,869,870,871,872,873,874,875,876,877,878,879,880,881,882,883,884,885,886,887,888,889,890,891,892,893,894,895,896,897,898,899,
900,901,902,903,904,905,906,907,908,909,910,911,912,913,914,915,916,917,918,919,920,921,922,923,924,925,926,927,928,929,930,931,932,933,934,935,936,937,938,939,940,941,942,943,944,945,946,947,948,949,950,951,952,953,954,955,956,957,958,959,960,961,962,963,964,965,966,967,968,969,970,971,972,973,974,975,976,977,978,979,980,981,982,983,984,985,986,987,988,989,990,991,992,993,994,995,996,997,998,999,
1000,1001,1002,1003,1004,1005,1006,1007,1008,1009,1010,1011,1012,1013,1014,1015,1016,1017,1018,1019,1020,1021,1022,1023,1024,1025,1026,1027,1028,1029,1030,1031,1032,1033,1034,1035,1036,1037,1038,1039,1040,1041,1042,1043,1044,1045,1046,1047,1048,1049,1050,1051,1052,1053,1054,1055,1056,1057,1058,1059,1060,1061,1062,1063,1064,1065,1066,1067,1068,1069,1070,1071,1072,1073,1074,1075,1076,1077,1078,1079,1080,1081,1082,1083,1084,1085,1086,1087,1088,1089,1090,1091,1092,1093,1094,1095,1096,1097,1098,1099,
1100,1101,1102,1103,1104,1105,1106,1107,1108,1109,1110,1111,1112,1113,1114,1115,1116,1117,1118,1119,1120,1121,1122,1123,1124,1125,1126,1127,1128,1129,1130,1131,1132,1133,1134,1135,1136,1137,1138,1139,1140,1141,1142,1143,1144,1145,1146,1147,1148,1149,1150,1151,1152,1153,1154,1155,1156,1157,1158,1159,1160,1161,1162,1163,1164,1165,1166,1167,1168,1169,1170,1171,1172,1173,1174,1175,1176,1177,1178,1179,1180,1181,1182,1183,1184,1185,1186,1187,1188,1189,1190,1191,1192,1193,1194,1195,1196,1197,1198,1199,
1200,1201,1202,1203,1204,1205,1206,1207,1208,1209,1210,1211,1212,1213,1214,1215,1216,1217,1218,1219,1220,1221,1222,1223,1224,1225,1226,1227,1228,1229,1230,1231,1232,1233,1234,1235,1236,1237,1238,1239,1240,1241,1242,1243,1244,1245,1246,1247,1248,1249,1250,1251,1252,1253,1254,1255,1256,1257,1258,1259,1260,1261,1262,1263,1264,1265,1266,1267,1268,1269,1270,1271,1272,1273,1274,1275,1276,1277,1278,1279,1280,1281,1282,1283,1284,1285,1286,1287,1288,1289,1290,1291,1292,1293,1294,1295,1296,1297,1298,1299,
1300,1301,1302,1303,1304,1305,1306,1307,1308,1309,1310,1311,1312,1313,1314,1315,1316,1317,1318,1319,1320,1321,1322,1323,1324,1325,1326,1327,1328,1329,1330,1331,1332,1333,1334,1335,1336,1337,1338,1339,1340,1341,1342,1343,1344,1345,1346,1347,1348,1349,1350,1351,1352,1353,1354,1355,1356,1357,1358,1359,1360,1361,1362,1363,1364,1365,1366,1367,1368,1369,1370,1371,1372,1373,1374,1375,1376,1377,1378,1379,1380,1381,1382,1383,1384,1385,1386,1387,1388,1389,1390,1391,1392,1393,1394,1395,1396,1397,1398,1399,
1400,1401,1402,1403,1404,1405,1406,1407,1408,1409,1410,1411,1412,1413,1414,1415,1416,1417,1418,1419,1420,1421,1422,1423,1424,1425,1426,1427,1428,1429,1430,1431,1432,1433,1434,1435,1436,1437,1438,1439,1440,1441,1442,1443,1444,1445,1446,1447,1448,1449,1450,1451,1452,1453,1454,1455,1456,1457,1458,1459,1460,1461,1462,1463,1464,1465,1466,1467,1468,1469,1470,1471,1472,1473,1474,1475,1476,1477,1478,1479,1480,1481,1482,1483,1484,1485,1486,1487,1488,1489,1490,1491,1492,1493,1494,1495,1496,1497,1498,1499,
1500,1501,1502,1503,1504,1505,1506,1507,1508,1509,1510,1511,1512,1513,1514,1515,1516,1517,1518,1519,1520,1521,1522,1523,1524,1525,1526,1527,1528,1529,1530,1531,1532,1533,1534,1535,1536,1537,1538,1539,1540,1541,1542,1543,1544,1545,1546,1547,1548,1549,1550,1551,1552,1553,1554,1555,1556,1557,1558,1559,1560,1561,1562,1563,1564,1565,1566,1567,1568,1569,1570,1571,1572,1573,1574,1575,1576,1577,1578,1579,1580,1581,1582,1583,1584,1585,1586,1587,1588,1589,1590,1591,1592,1593,1594,1595,1596,1597,1598,1599,
1600,1601,1602,1603,1604,1605,1606,1607,1608,1609,1610,1611,1612,1613,1614,1615,1616,1617,1618,1619,1620,1621,1622,1623,1624,1625,1626,1627,1628,1629,1630,1631,1632,1633,1634,1635,1636,1637,1638,1639,1640,1641,1642,1643,1644,1645,1646,1647,1648,1649,1650,1651,1652,1653,1654,1655,1656,1657,1658,1659,1660,1661,1662,1663,1664,1665,1666,1667,1668,1669,1670,1671,1672,1673,1674,1675,1676,1677,1678,1679,1680,1681,1682,1683,1684,1685,1686,1687,1688,1689,1690,1691,1692,1693,1694,1695,1696,1697,1698,1699,
1700,1701,1702,1703,1704,1705,1706,1707,1708,1709,1710,1711,1712,1713,1714,1715,1716,1717,1718,1719,1720,1721,1722,1723,1724,1725,1726,1727,1728,1729,1730,1731,1732,1733,1734,1735,1736,1737,1738,1739,1740,1741,1742,1743,1744,1745,1746,1747,1748,1749,1750,1751,1752,1753,1754,1755,1756,1757,1758,1759,1760,1761,1762,1763,1764,1765,1766,1767,1768,1769,1770,1771,1772,1773,1774,1775,1776,1777,1778,1779,1780,1781,1782,1783,1784,1785,1786,1787,1788,1789,1790,1791,1792,1793,1794,1795,1796,1797,1798,1799,
1800,1801,1802,1803,1804,1805,1806,1807,1808,1809,1810,1811,1812,1813,1814,1815,1816,1817,1818,1819,1820,1821,1822,1823,1824,1825,1826,1827,1828,1829,1830,1831,1832,1833,1834,1835,1836,1837,1838,1839,1840,1841,1842,1843,1844,1845,1846,1847,1848,1849,1850,1851,1852,1853,1854,1855,1856,1857,1858,1859,1860,1861,1862,1863,1864,1865,1866,1867,1868,1869,1870,1871,1872,1873,1874,1875,1876,1877,1878,1879,1880,1881,1882,1883,1884,1885,1886,1887,1888,1889,1890,1891,1892,1893,1894,1895,1896,1897,1898,1899,
1900,1901,1902,1903,1904,1905,1906,1907,1908,1909,1910,1911,1912,1913,1914,1915,1916,1917,1918,1919,1920,1921,1922,1923,1924,1925,1926,1927,1928,1929,1930,1931,1932,1933,1934,1935,1936,1937,1938,1939,1940,1941,1942,1943,1944,1945,1946,1947,1948,1949,1950,1951,1952,1953,1954,1955,1956,1957,1958,1959,1960,1961,1962,1963,1964,1965,1966,1967,1968,1969,1970,1971,1972,1973,1974,1975,1976,1977,1978,1979,1980,1981,1982,1983,1984,1985,1986,1987,1988,1989,1990,1991,1992,1993,1994,1995,1996,1997,1998,1999,
2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018,2019,2020,2021,2022,2023,2024,2025,2026,2027,2028,2029,2030,2031,2032,2033,2034,2035,2036,2037,2038,2039,2040,2041,2042,2043,2044,2045,2046,2047,2048,2049,2050,2051,2052,2053,2054,2055,2056,2057,2058,2059,2060,2061,2062,2063,2064,2065,2066,2067,2068,2069,2070,2071,2072,2073,2074,2075,2076,2077,2078,2079,2080,2081,2082,2083,2084,2085,2086,2087,2088,2089,2090,2091,2092,2093,2094,2095,2096,2097,2098,2099,
2100,2101,2102,2103,2104,2105,2106,2107,2108,2109,2110,2111,2112,2113,2114,2115,2116,2117,2118,2119,2120,2121,2122,2123,2124,2125,2126,2127,2128,2129,2130,2131,2132,2133,2134,2135,2136,2137,2138,2139,2140,2141,2142,2143,2144,2145,2146,2147,2148,2149,2150,2151,2152,2153,2154,2155,2156,2157,2158,2159,2160,2161,2162,2163,2164,2165,2166,2167,2168,2169,2170,2171,2172,2173,2174,2175,2176,2177,2178,2179,2180,2181,2182,2183,2184,2185,2186,2187,2188,2189,2190,2191,2192,2193,2194,2195,2196,2197,2198,2199,
2200,2201,2202,2203,2204,2205,2206,2207,2208,2209,2210,2211,2212,2213,2214,2215,2216,2217,2218,2219,2220,2221,2222,2223,2224,2225,2226,2227,2228,2229,2230,2231,2232,2233,2234,2235,2236,2237,2238,2239,2240,2241,2242,2243,2244,2245,2246,2247,2248,2249,2250,2251,2252,2253,2254,2255,2256,2257,2258,2259,2260,2261,2262,2263,2264,2265,2266,2267,2268,2269,2270,2271,2272,2273,2274,2275,2276,2277,2278,2279,2280,2281,2282,2283,2284,2285,2286,2287,2288,2289,2290,2291,2292,2293,2294,2295,2296,2297,2298,2299,
2300,2301,2302,2303,2304,2305,2306,2307,2308,2309,2310,2311,2312,2313,2314,2315,2316,2317,2318,2319,2320,2321,2322,2323,2324,2325,2326,2327,2328,2329,2330,2331,2332,2333,2334,2335,2336,2337,2338,2339,2340,2341,2342,2343,2344,2345,2346,2347,2348,2349,2350,2351,2352,2353,2354,2355,2356,2357,2358,2359,2360,2361,2362,2363,2364,2365,2366,2367,2368,2369,2370,2371,2372,2373,2374,2375,2376,2377,2378,2379,2380,2381,2382,2383,2384,2385,2386,2387,2388,2389,2390,2391,2392,2393,2394,2395,2396,2397,2398,2399,
2400,2401,2402,2403,2404,2405,2406,2407,2408,2409,2410,2411,2412,2413,2414,2415,2416,2417,2418,2419,2420,2421,2422,2423,2424,2425,2426,2427,2428,2429,2430,2431,2432,2433,2434,2435,2436,2437,2438,2439,2440,2441,2442,2443,2444,2445,2446,2447,2448,2449,2450,2451,2452,2453,2454,2455,2456,2457,2458,2459,2460,2461,2462,2463,2464,2465,2466,2467,2468,2469,2470,2471,2472,2473,2474,2475,2476,2477,2478,2479,2480,2481,2482,2483,2484,2485,2486,2487,2488,2489,2490,2491,2492,2493,2494,2495,2496,2497,2498,2499,
2500,2501,2502,2503,2504,2505,2506,2507,2508,2509,2510,2511,2512,2513,2514,2515,2516,2517,2518,2519,2520,2521,2522,2523,2524,2525,2526,2527,2528,2529,2530,2531,2532,2533,2534,2535,2536,2537,2538,2539,2540,2541,2542,2543,2544,2545,2546,2547,2548,2549,2550,2551,2552,2553,2554,2555,2556,2557,2558,2559,2560,2561,2562,2563,2564,2565,2566,2567,2568,2569,2570,2571,2572,2573,2574,2575,2576,2577,2578,2579,2580,2581,2582,2583,2584,2585,2586,2587,2588,2589,2590,2591,2592,2593,2594,2595,2596,2597,2598,2599,
2600,2601,2602,2603,2604,2605,2606,2607,2608,2609,2610,2611,2612,2613,2614,2615,2616,2617,2618,2619,2620,2621,2622,2623,2624,2625,2626,2627,2628,2629,2630,2631,2632,2633,2634,2635,2636,2637,2638,2639,2640,2641,2642,2643,2644,2645,2646,2647,2648,2649,2650,2651,2652,2653,2654,2655,2656,2657,2658,2659,2660,2661,2662,2663,2664,2665,2666,2667,2668,2669,2670,2671,2672,2673,2674,2675,2676,2677,2678,2679,2680,2681,2682,2683,2684,2685,2686,2687,2688,2689,2690,2691,2692,2693,2694,2695,2696,2697,2698,2699,
2700,2701,2702,2703,2704,2705,2706,2707,2708,2709,2710,2711,2712,2713,2714,2715,2716,2717,2718,2719,2720,2721,2722,2723,2724,2725,2726,2727,2728,2729,2730,2731,2732,2733,2734,2735,2736,2737,2738,2739,2740,2741,2742,2743,2744,2745,2746,2747,2748,2749,2750,2751,2752,2753,2754,2755,2756,2757,2758,2759,2760,2761,2762,2763,2764,2765,2766,2767,2768,2769,2770,2771,2772,2773,2774,2775,2776,2777,2778,2779,2780,2781,2782,2783,2784,2785,2786,2787,2788,2789,2790,2791,2792,2793,2794,2795,2796,2797,2798,2799,
2800,2801,2802,2803,2804,2805,2806,2807,2808,2809,2810,2811,2812,2813,2814,2815,2816,2817,2818,2819,2820,2821,2822,2823,2824,2825,2826,2827,2828,2829,2830,2831,2832,2833,2834,2835,2836,2837,2838,2839,2840,2841,2842,2843,2844,2845,2846,2847,2848,2849,2850,2851,2852,2853,2854,2855,2856,2857,2858,2859,2860,2861,2862,2863,2864,2865,2866,2867,2868,2869,2870,2871,2872,2873,2874,2875,2876,2877,2878,2879,2880,2881,2882,2883,2884,2885,2886,2887,2888,2889,2890,2891,2892,2893,2894,2895,2896,2897,2898,2899,
2900,2901,2902,2903,2904,2905,2906,2907,2908,2909,2910,2911,2912,2913,2914,2915,2916,2917,2918,2919,2920,2921,2922,2923,2924,2925,2926,2927,2928,2929,2930,2931,2932,2933,2934,2935,2936,2937,2938,2939,2940,2941,2942,2943,2944,2945,2946,2947,2948,2949,2950,2951,2952,2953,2954,2955,2956,2957,2958,2959,2960,2961,2962,2963,2964,2965,2966,2967,2968,2969,2970,2971,2972,2973,2974,2975,2976,2977,2978,2979,2980,2981,2982,2983,2984,2985,2986,2987,2988,2989,2990,2991,2992,2993,2994,2995,2996,2997,2998,2999,
3000,3001,3002,3003,3004,3005,3006,3007,3008,3009,3010,3011,3012,3013,3014,3015,3016,3017,3018,3019,3020,3021,3022,3023,3024,3025,3026,3027,3028,3029,3030,3031,3032,3033,3034,3035,3036,3037,3038,3039,3040,3041,3042,3043,3044,3045,3046,3047,3048,3049,3050,3051,3052,3053,3054,3055,3056,3057,3058,3059,3060,3061,3062,3063,3064,3065,3066,3067,3068,3069,3070,3071,3072,3073,3074,3075,3076,3077,3078,3079,3080,3081,3082,3083,3084,3085,3086,3087,3088,3089,3090,3091,3092,3093,3094,3095,3096,3097,3098,3099,
3100,3101,3102,3103,3104,3105,3106,3107,3108,3109,3110,3111,3112,3113,3114,3115,3116,3117,3118,3119,3120,3121,3122,3123,3124,3125,3126,3127,3128,3129,3130,3131,3132,3133,3134,3135,3136,3137,3138,3139,3140,3141,3142,3143,3144,3145,3146,3147,3148,3149,3150,3151,3152,3153,3154,3155,3156,3157,3158,3159,3160,3161,3162,3163,3164,3165,3166,3167,3168,3169,3170,3171,3172,3173,3174,3175,3176,3177,3178,3179,3180,3181,3182,3183,3184,3185,3186,3187,3188,3189,3190,3191,3192,3193,3194,3195,3196,3197,3198,3199,
3200,3201,3202,3203,3204,3205,3206,3207,3208,3209,3210,3211,3212,3213,3214,3215,3216,3217,3218,3219,3220,3221,3222,3223,3224,3225,3226,3227,3228,3229,3230,3231,3232,3233,3234,3235,3236,3237,3238,3239,3240,3241,3242,3243,3244,3245,3246,3247,3248,3249,3250,3251,3252,3253,3254,3255,3256,3257,3258,3259,3260,3261,3262,3263,3264,3265,3266,3267,3268,3269,3270,3271,3272,3273,3274,3275,3276,3277,3278,3279,3280,3281,3282,3283,3284,3285,3286,3287,3288,3289,3290,3291,3292,3293,3294,3295,3296,3297,3298,3299,
3300,3301,3302,3303,3304,3305,3306,3307,3308,3309,3310,3311,3312,3313,3314,3315,3316,3317,3318,3319,3320,3321,3322,3323,3324,3325,3326,3327,3328,3329,3330,3331,3332,3333,3334,3335,3336,3337,3338,3339,3340,3341,3342,3343,3344,3345,3346,3347,3348,3349,3350,3351,3352,3353,3354,3355,3356,3357,3358,3359,3360,3361,3362,3363,3364,3365,3366,3367,3368,3369,3370,3371,3372,3373,3374,3375,3376,3377,3378,3379,3380,3381,3382,3383,3384,3385,3386,3387,3388,3389,3390,3391,3392,3393,3394,3395,3396,3397,3398,3399,
3400,3401,3402,3403,3404,3405,3406,3407,3408,3409,3410,3411,3412,3413,3414,3415,3416,3417,3418,3419,3420,3421,3422,3423,3424,3425,3426,3427,3428,3429,3430,3431,3432,3433,3434,3435,3436,3437,3438,3439,3440,3441,3442,3443,3444,3445,3446,3447,3448,3449,3450,3451,3452,3453,3454,3455,3456,3457,3458,3459,3460,3461,3462,3463,3464,3465,3466,3467,3468,3469,3470,3471,3472,3473,3474,3475,3476,3477,3478,3479,3480,3481,3482,3483,3484,3485,3486,3487,3488,3489,3490,3491,3492,3493,3494,3495,3496,3497,3498,3499,
3500,3501,3502,3503,3504,3505,3506,3507,3508,3509,3510,3511,3512,3513,3514,3515,3516,3517,3518,3519,3520,3521,3522,3523,3524,3525,3526,3527,3528,3529,3530,3531,3532,3533,3534,3535,3536,3537,3538,3539,3540,3541,3542,3543,3544,3545,3546,3547,3548,3549,3550,3551,3552,3553,3554,3555,3556,3557,3558,3559,3560,3561,3562,3563,3564,3565,3566,3567,3568,3569,3570,3571,3572,3573,3574,3575,3576,3577,3578,3579,3580,3581,3582,3583,3584,3585,3586,3587,3588,3589,3590,3591,3592,3593,3594,3595,3596,3597,3598,3599,
3600,3601,3602,3603,3604,3605,3606,3607,3608,3609,3610,3611,3612,3613,3614,3615,3616,3617,3618,3619,3620,3621,3622,3623,3624,3625,3626,3627,3628,3629,3630,3631,3632,3633,3634,3635,3636,3637,3638,3639,3640,3641,3642,3643,3644,3645,3646,3647,3648,3649,3650,3651,3652,3653,3654,3655,3656,3657,3658,3659,3660,3661,3662,3663,3664,3665,3666,3667,3668,3669,3670,3671,3672,3673,3674,3675,3676,3677,3678,3679,3680,3681,3682,3683,3684,3685,3686,3687,3688,3689,3690,3691,3692,3693,3694,3695,3696,3697,3698,3699,
3700,3701,3702,3703,3704,3705,3706,3707,3708,3709,3710,3711,3712,3713,3714,3715,3716,3717,3718,3719,3720,3721,3722,3723,3724,3725,3726,3727,3728,3729,3730,3731,3732,3733,3734,3735,3736,3737,3738,3739,3740,3741,3742,3743,3744,3745,3746,3747,3748,3749,3750,3751,3752,3753,3754,3755,3756,3757,3758,3759,3760,3761,3762,3763,3764,3765,3766,3767,3768,3769,3770,3771,3772,3773,3774,3775,3776,3777,3778,3779,3780,3781,3782,3783,3784,3785,3786,3787,3788,3789,3790,3791,3792,3793,3794,3795,3796,3797,3798,3799,
3800,3801,3802,3803,3804,3805,3806,3807,3808,3809,3810,3811,3812,3813,3814,3815,3816,3817,3818,3819,3820,3821,3822,3823,3824,3825,3826,3827,3828,3829,3830,3831,3832,3833,3834,3835,3836,3837,3838,3839,3840,3841,3842,3843,3844,3845,3846,3847,3848,3849,3850,3851,3852,3853,3854,3855,3856,3857,3858,3859,3860,3861,3862,3863,3864,3865,3866,3867,3868,3869,3870,3871,3872,3873,3874,3875,3876,3877,3878,3879,3880,3881,3882,3883,3884,3885,3886,3887,3888,3889,3890,3891,3892,3893,3894,3895,3896,3897,3898,3899,
3900,3901,3902,3903,3904,3905,3906,3907,3908,3909,3910,3911,3912,3913,3914,3915,3916,3917,3918,3919,3920,3921,3922,3923,3924,3925,3926,3927,3928,3929,3930,3931,3932,3933,3934,3935,3936,3937,3938,3939,3940,3941,3942,3943,3944,3945,3946,3947,3948,3949,3950,3951,3952,3953,3954,3955,3956,3957,3958,3959,3960,3961,3962,3963,3964,3965,3966,3967,3968,3969,3970,3971,3972,3973,3974,3975,3976,3977,3978,3979,3980,3981,3982,3983,3984,3985,3986,3987,3988,3989,3990,3991,3992,3993,3994,3995,3996,3997,3998,3999,
4000,4001,4002,4003,4004,4005,4006,4007,4008,4009,4010,4011,4012,4013,4014,4015,4016,4017,4018,4019,4020,4021,4022,4023,4024,4025,4026,4027,4028,4029,4030,4031,4032,4033,4034,4035,4036,4037,4038,4039,4040,4041,4042,4043,4044,4045,4046,4047,4048,4049,4050,4051,4052,4053,4054,4055,4056,4057,4058,4059,4060,4061,4062,4063,4064,4065,4066,4067,4068,4069,4070,4071,4072,4073,4074,4075,4076,4077,4078,4079,4080,4081,4082,4083,4084,4085,4086,4087,4088,4089,4090,4091,4092,4093,4094,4095,4096,4097,4098,4099,
4100,4101,4102,4103,4104,4105,4106,4107,4108,4109,4110,4111,4112,4113,4114,4115,4116,4117,4118,4119,4120,4121,4122,4123,4124,4125,4126,4127,4128,4129,4130,4131,4132,4133,4134,4135,4136,4137,4138,4139,4140,4141,4142,4143,4144,4145,4146,4147,4148,4149,4150,4151,4152,4153,4154,4155,4156,4157,4158,4159,4160,4161,4162,4163,4164,4165,4166,4167,4168,4169,4170,4171,4172,4173,4174,4175,4176,4177,4178,4179,4180,4181,4182,4183,4184,4185,4186,4187,4188,4189,4190,4191,4192,4193,4194,4195,4196,4197,4198,4199,
4200,4201,4202,4203,4204,4205,4206,4207,4208,4209,4210,4211,4212,4213,4214,4215,4216,4217,4218,4219,4220,4221,4222,4223,4224,4225,4226,4227,4228,4229,4230,4231,4232,4233,4234,4235,4236,4237,4238,4239,4240,4241,4242,4243,4244,4245,4246,4247,4248,4249,4250,4251,4252,4253,4254,4255,4256,4257,4258,4259,4260,4261,4262,4263,4264,4265,4266,4267,4268,4269,4270,4271,4272,4273,4274,4275,4276,4277,4278,4279,4280,4281,4282,4283,4284,4285,4286,4287,4288,4289,4290,4291,4292,4293,4294,4295,4296,4297,4298,4299,
4300,4301,4302,4303,4304,4305,4306,4307,4308,4309,4310,4311,4312,4313,4314,4315,4316,4317,4318,4319,4320,4321,4322,4323,4324,4325,4326,4327,4328,4329,4330,4331,4332,4333,4334,4335,4336,4337,4338,4339,4340,4341,4342,4343,4344,4345,4346,4347,4348,4349,4350,4351,4352,4353,4354,4355,4356,4357,4358,4359,4360,4361,4362,4363,4364,4365,4366,4367,4368,4369,4370,4371,4372,4373,4374,4375,4376,4377,4378,4379,4380,4381,4382,4383,4384,4385,4386,4387,4388,4389,4390,4391,4392,4393,4394,4395,4396,4397,4398,4399,
4400,4401,4402,4403,4404,4405,4406,4407,4408,4409,4410,4411,4412,4413,4414,4415,4416,4417,4418,4419,4420,4421,4422,4423,4424,4425,4426,4427,4428,4429,4430,4431,4432,4433,4434,4435,4436,4437,4438,4439,4440,4441,4442,4443,4444,4445,4446,4447,4448,4449,4450,4451,4452,4453,4454,4455,4456,4457,4458,4459,4460,4461,4462,4463,4464,4465,4466,4467,4468,4469,4470,4471,4472,4473,4474,4475,4476,4477,4478,4479,4480,4481,4482,4483,4484,4485,4486,4487,4488,4489,4490,4491,4492,4493,4494,4495,4496,4497,4498,4500,
4502,4504,4506,4507,4509,4511,4513,4514,4516,4518,4520,4522,4524,4526,4527,4529,4531,4532,4534,4536,4538,4540,4542,4544,4546,4548,4549,4551,4552,4554,4556,4558,4560,4562,4564,4566,4568,4570,4572,4574,4576,4578,4580,4583,4585,4587,4589,4591,4593,4595,4597,4599,4601,4603,4605,4607,4608,4609,4610,4612,4614,4616,4618,4620,4622,4623,4625,4627,4629,4631,4633,4635,4637,4639,4641,4643,4645,4647,4648,4649,4651,4652,4655,4656,4658,4660,4661,4663,4665,4667,4670,4672,4674,4676,4678,4679,4681,4683,4686,4688,
4690,4692,4694,4696,4698,4700,4702,4704,4706,4708,4710,4712,4714,
];

// these are the prefill value for type other
var database_ResourceType_other = [
4716,
4717,
4718,
4719,
4720,
4721,
4722,
4723,
4724,
4725,
4726,
4727,
4728
];

// these are the prefill value for use other
var database_ResourceUse_other = [
4729,
4730,
4731,
4732,
4733,
4734,
4735,
4736,
4737,
4738,
4739
];



// these are the prefill value for the Defaults
var database_ResourceDefaultName = [

];
var database_ResourceDefaultType = [

];
var database_ResourceDefaultSize = [

];
var database_ResourceDefaultUnit = [

];
var database_ResourceDefaultStatus = [

];
var database_ResourceDefaultLanguagedrop = [

];
var database_ResourceDefaultLang = [

];
var database_ResourceDefaultModality = [

];
var database_ResourceDefaultUse = [

];
var database_ResourceDefaultAvailability = [

];
var database_ResourceDefaultLicense = [

];
var database_ResourceDefaultUrl = [

];
var database_ResourceDefaultDocumentation = [

];
var database_ResourceDefaultDescription = [

];

jQuery(function(){

  // replace the database with the data
  for (var i=0; i< database_ResourceName.length; i++) {
     aaa = document.getElementById('database_'+database_ResourceName[i]);
    if (aaa) {
        database_ResourceName[i] = aaa.innerHTML;
    }
  }
  for (var i=0; i< database_ResourceType_other.length; i++) {
    database_ResourceType_other[i] = document.getElementById('database_'+database_ResourceType_other[i]).innerHTML;
  }
  for (var i=0; i< database_ResourceUse_other.length; i++) {
    database_ResourceUse_other[i] = document.getElementById('database_'+database_ResourceUse_other[i]).innerHTML;
  }
});


function selectResourceName(text, li) {
  for (var i=0; i< database_ResourceName.length; i++) {
    if (database_ResourceName[i] == li.textContent) {
      text.value = document.getElementById('database_'+
		   database_ResourceName_normalized[database_ResourceName_synonim[i]]).innerHTML;
    }
  }
}

function disableResourceILSRN(number) {
  if (jQuery("#disableResourceISLRN"+number).is(":checked")) {
    jQuery("#RISLRN"+number).prop('disabled', 'disabled');
    jQuery("#RISLRN"+number).val('');
  } else {
    jQuery("#RISLRN"+number).removeAttr('disabled');
  }
}

function selectResourceNameUI(id, event, ui) {
  for (var i=0; i< database_ResourceName.length; i++) {
    if (database_ResourceName[i] == ui.item.value) {
      jQuery(id).val( document.getElementById('database_'+
		   database_ResourceName_normalized[database_ResourceName_synonim[i]]).innerHTML );
      return false;
    }
  }
  return true;
}
function setDefaultValid(){
    return true;
    }
/*
function setDefaultValid(number) {
  var indexname;
  var indexdefault;
  var found_name;
  var found_default;
  // get the resource name
  var name = document.getElementById('RN'+number).value;
  var b = document.getElementById('setDefaultButton_'+number);

  // search the name among the various strings
  found_name = 0;
  for (var i=0; i< database_ResourceName.length; i++) {
    if (database_ResourceName[i] == name) {
      indexname = i;
      found_name = 1;
      break;
    }
  }

  if (found_name) {
    // the name has been found. now look if a default value exists

    found_nameindex = 0;
    for (var i=0; i< database_ResourceDefaultName.length; i++) {
      if (database_ResourceDefaultName[i] == indexname) {
        indexdefault = i;
        found_default = 1;
        break;
      }
    }
    if (found_default) {
      b.disabled = false;
    } else {
      b.disabled = true;
    }
  } else {
    b.disabled = true;
  }
}
*/

function setDefaultResourceData(number) {
  var indexname;
  var indexdefault;
  var found_name;
  var found_default;
  // get the resource name
  var name = document.getElementById('RN'+number).value;


  // search the name among the various strings
  found_name = 0;
  for (var i=0; i< database_ResourceName.length; i++) {
    if (database_ResourceName[i] == name) {
      indexname = i;
      found_name = 1;
      break;
    }
  }

  if (found_name) {
    // the name has been found. now look if a default value exists

    found_nameindex = 0;
    for (var i=0; i< database_ResourceDefaultName.length; i++) {
      if (database_ResourceDefaultName[i] == indexname) {
        indexdefault = i;
        found_default = 1;
        break;
      }
    }
    if (found_default) {
      // now set the type
      var thetype = document.getElementById('database_'+database_ResourceDefaultType[indexdefault]).innerHTML;
      if (thetype == '-') {
        thetype = '';
        document.getElementById('redtype'+number).style.color = 'red';
      } else {
        document.getElementById('redtype'+number).style.color = 'black';
      }
      var thetypelist = document.getElementById('RT'+number);
      for (myopt = 0; myopt< thetypelist.options.length; myopt++)
      {    
        if (thetypelist.options[myopt].value == thetype)
        {
          thetypelist.options[myopt].selected = true;
          break;
        }
      }

      // now set the size
      var thesize = document.getElementById('database_'+database_ResourceDefaultSize[indexdefault]).innerHTML;
      if (thesize == '-') {
        thesize = '';
        document.getElementById('redsize'+number).style.color = 'red';
      } else {
        document.getElementById('redsize'+number).style.color = 'black';
      }
      document.getElementById('RSIZE'+number).value = thesize;

      // now set the unit
      var theunit = document.getElementById('database_'+database_ResourceDefaultUnit[indexdefault]).innerHTML;
      if (theunit == '-') { theunit = ''; }
      var theunitlist = document.getElementById('RSIZEU'+number);
      for (myopt = 0; myopt< theunitlist.options.length; myopt++)
      {    
        if (theunitlist.options[myopt].value == theunit)
        {
          theunitlist.options[myopt].selected = true;
          break;
        }
      }

      // now set the status
      var thestatus = document.getElementById('database_'+database_ResourceDefaultStatus[indexdefault]).innerHTML;
      if (thestatus == '-') {
        thestatus = '';
        document.getElementById('redstatus'+number).style.color = 'red';
      } else {
        document.getElementById('redstatus'+number).style.color = 'black';
      }
      var thestatuslist = document.getElementById('RS'+number);
      for (myopt = 0; myopt< thestatuslist.options.length; myopt++)
      {    
        if (thestatuslist.options[myopt].value == thestatus)
        {
          thestatuslist.options[myopt].selected = true;
          break;
        }
      }

      // now set the languagedrop
      var thelanguagedrop = document.getElementById('database_'+database_ResourceDefaultLanguagedrop[indexdefault]).innerHTML;
      if (thelanguagedrop == 'P') {
        document.getElementById('redlang'+number).style.color = 'red';
      } else {
        document.getElementById('redlang'+number).style.color = 'black';
      }
      var thelanguagedroplist = document.getElementById('RL'+number);
      for (myopt = 0; myopt< thelanguagedroplist.options.length; myopt++)
      {    
        if (thelanguagedroplist.options[myopt].value == thelanguagedrop)
        {
          thelanguagedroplist.options[myopt].selected = true;
	  handleOtherLanguageField('RL'+number,'lang'+number,'LAO'+number);
          break;
        }
      }

      // now set the lang
      var thelang = document.getElementById('database_'+database_ResourceDefaultLang[indexdefault]).innerHTML;
      var thelangarray = thelang.split(/###/);
      for (i=0; i<6; i++) {
        if (thelangarray[i] == '-') {
          thelangarray[i] = '';
        }
      }
      document.getElementById('LAO'+number).value = thelangarray[0];
      document.getElementById('LAO2'+number).value = thelangarray[1];
      document.getElementById('LAO3'+number).value = thelangarray[2];
      document.getElementById('LAO4'+number).value = thelangarray[3];
      document.getElementById('LAO5'+number).value = thelangarray[4];
      document.getElementById('LAON'+number).value = thelangarray[5];

      // now set the modality
      var themodality = document.getElementById('database_'+database_ResourceDefaultModality[indexdefault]).innerHTML;
      if (themodality == '-') {
        themodality = '';
        document.getElementById('redmodality'+number).style.color = 'red';
      } else {
        document.getElementById('redmodality'+number).style.color = 'black';
      }
      var themodalitylist = document.getElementById('RM'+number);
      for (myopt = 0; myopt< themodalitylist.options.length; myopt++)
      {    
        if (themodalitylist.options[myopt].value == themodality)
        {
          themodalitylist.options[myopt].selected = true;
          break;
        }
      }

      // now set the use
      var theuse = document.getElementById('database_'+database_ResourceDefaultUse[indexdefault]).innerHTML;
      if (theuse == '-') {
        theuse = '';
        document.getElementById('reduse'+number).style.color = 'red';
      } else {
        document.getElementById('reduse'+number).style.color = 'black';
      }
      var theuselist = document.getElementById('RU'+number);
      for (myopt = 0; myopt< theuselist.options.length; myopt++)
      {    
        if (theuselist.options[myopt].value == theuse)
        {
          theuselist.options[myopt].selected = true;
          break;
        }
      }

      // now set the availability
      var theavailability = document.getElementById('database_'+database_ResourceDefaultAvailability[indexdefault]).innerHTML;
      if (theavailability == '-') {
        theavailability = '';
        document.getElementById('redavailability'+number).style.color = 'red';
      } else {
        document.getElementById('redavailability'+number).style.color = 'black';
      }
      var theavailabilitylist = document.getElementById('RA'+number);
      for (myopt = 0; myopt< theavailabilitylist.options.length; myopt++)
      {    
        if (theavailabilitylist.options[myopt].value == theavailability)
        {
          theavailabilitylist.options[myopt].selected = true;
          break;
        }
      }

      // now set the license
      var thelicense = document.getElementById('database_'+database_ResourceDefaultLicense[indexdefault]).innerHTML;
      if (thelicense == '-') {
	thelicense = '';
        document.getElementById('redlicense'+number).style.color = 'red';
      } else {
        document.getElementById('redlicense'+number).style.color = 'black';
      }
      document.getElementById('RLIC'+number).value = thelicense;

      // now set the url
      var theurl = document.getElementById('database_'+database_ResourceDefaultUrl[indexdefault]).innerHTML;
      if (theurl == '-') {
        theurl = '';
        document.getElementById('redurl'+number).style.color = 'red';
      } else {
        document.getElementById('redurl'+number).style.color = 'black';
      }
      document.getElementById('RURL'+number).value = theurl;

      // now set the documentation
      var thedocumentation = document.getElementById('database_'+database_ResourceDefaultDocumentation[indexdefault]).innerHTML;
      if (thedocumentation == '-') {
        thedocumentation = '';
        document.getElementById('reddocumentation'+number).style.color = 'red';
      } else {
        document.getElementById('reddocumentation'+number).style.color = 'black';
      }
      document.getElementById('RDOC'+number).value = thedocumentation;

      // now set the description
      var thedescription = document.getElementById('database_'+database_ResourceDefaultDescription[indexdefault]).innerHTML;
      if (thedescription == '-') {
        thedescription = '';
        //document.getElementById('reddescription'+number).style.color = 'red';
      } else {
        //document.getElementById('reddescription'+number).style.color = 'black';
      }
      document.getElementById('RD'+number).value = thedescription;


    } else {
      alert("Resource default setting not present for this Resource Name.");
    }
    
  } else {
    alert("Resource name not found. Please check it into the autocompletion options.");
  }
}


function Resource_manage_upload(id) {
 // var chbox;// = document.getElementById(id);
  var filed_uf_id="filed_"+id;    
  var filet_uf_id="filet_"+id;    
  var urld_uf_id="urld_"+id;    
  var urlt_uf_id="urlt_"+id;    
  var tr_uf;
  var url_uf;
    
  filed_uf=document.getElementById(filed_uf_id);
  filet_uf=document.getElementById(filet_uf_id);

  urld_uf=document.getElementById(urld_uf_id);
  urlt_uf=document.getElementById(urlt_uf_id);
  
 /* if (chbox.checked){
    filed_uf.style.display="";
    filet_uf.style.display="";

    urld_uf.style.display="";
    urlt_uf.style.display="";
  } else {
    filed_uf.style.display="none";
    filet_uf.style.display="none";

    urld_uf.style.display="none";
    urlt_uf.style.display="none";
}
*/
  return true;
}

</script>


<p>
If you have more than one resource, please fill in as many forms as the
resources you must describe.
<p>

<div>
<div id=r1>

<table width="95%">
<tr>
<td>#1</td>

<td style="width: 100%">
<table style="width: 100%" border="1">
<tr>
<td style="width: 100%">
<table cellpadding=5 style="width: 100%">

<tr>
<td align="right">Resource Name:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{1}{Name}' 
               type="text"
               value="" 
               size="40" 
               id=RN1> 


	<script type="text/javascript">
	  jQuery(function(){
	    jQuery("#RN1").keyup(function(){setDefaultValid(1)});
            jQuery("#RN1").autocomplete({
              source: database_ResourceName,
              select: function(event, ui) { f = selectResourceNameUI("#RN1", event, ui); setDefaultValid(1); return f;}});
	  });
	</script>
      </td>
      <!--
      <td>
	<input type=button onClick="setDefaultResourceData(1)" value='Load Default Values' id="setDefaultButton_1" disabled>
      </td> -->
    </tr>
  </table>


</td>
<!--
<td>
  <i>If the button "Load default values" is enabled you can prefill some fields with default values available in the LreMap for this resource.</i>
</td>
-->
</tr>

<tr>
<td align="right">ISLRN Field:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{1}{ISLRN}' 
               type="text"
               value="" 
               size="20" 
               id=RISLRN1
	       title="Format: xxx-xxx-xxx-xxx-x"> 
      </td>
      <td>
	&nbsp;&nbsp;&nbsp;
	<input type=checkbox onClick="disableResourceILSRN(1)" value='1'
	       id="disableResourceISLRN1" name='RMap{1}{ISLRNstatus}'
	       > No ISLRN / I don't know
	<script type="text/javascript">
	  jQuery(function(){
	    disableResourceILSRN(1);
	  });
	</script>
      </td>
    </tr>
  </table>
</td>
<td>
  <i>The ISLRN is a unique and universal identification schema for LRs
    which provides LRs with unique identifier using a standardised
    nomenclature.  It also ensures that LRs are correctly identified,
    and consequently, recognised with proper references for their
    usage in applications in R&D projects, products evaluation and
    benchmark as well as in documents and scientific
    papers. Visit <a href="http://www.islrn.org"
    target=_blank>http://www.islrn.org</a>.</i>
</td>
</tr>



<tr>
<td style="width: 20%" align=right><span id="redtype1">Resource Type: </span></td>



<td style="width: 40%" align=left>
<select name='RMap{1}{Type}' 
        size=1 
        id=RT1 
        onchange="handleOtherField('RT1','resOther1','RNO1')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>


<optgroup label="Resource-Dataset">
<option value="Corpus" >Corpus</option>

<option value="Lexicon" >Lexicon</option>

<option value="Grammar/Language Model" >Grammar/Language Model</option>

<option value="Ontology" >Ontology</option>

<option value="Terminology" >Terminology</option>

<option value="Treebank" >Treebank</option>

</optgroup>
<optgroup label="Resource-Tool">
<option value="Corpus Tool" >Corpus Tool</option>

<option value="Aligner" >Aligner</option>

<option value="Annotation Tool" >Annotation Tool</option>

<option value="Language Identifier" >Language Identifier</option>

<option value="Language Modeling Tool" >Language Modeling Tool</option>

<option value="Machine Learning Tool" >Machine Learning Tool</option>

<option value="Machine Translation Tool" >Machine Translation Tool</option>

<option value="Named Entity Recognizer" >Named Entity Recognizer</option>

<option value="Sentiment Analysis Tool" >Sentiment Analysis Tool</option>

<option value="Software Toolkit" >Software Toolkit</option>

<option value="Tagger/Parser" >Tagger/Parser</option>

<option value="Tokenizer" >Tokenizer</option>

<option value="Web Service" >Web Service</option>

<option value="Word Sense Disambiguator" >Word Sense Disambiguator</option>

<option value="Prosodic Analyzer" >Prosodic Analyzer</option>

<option value="Signal Processing/Features Extraction" >Signal Processing/Features Extraction</option>

<option value="Speaker Recognizer" >Speaker Recognizer</option>

<option value="Speech Recognizer/Transcriber" >Speech Recognizer/Transcriber</option>

<option value="Spoken Dialogue Tool" >Spoken Dialogue Tool</option>

<option value="Text-to-Speech Synthesizer" >Text-to-Speech Synthesizer</option>

<option value="Image Analyzer" >Image Analyzer</option>

</optgroup>
<optgroup label="Resource-Guidelines">
<option value="Metadata" >Metadata</option>

<option value="Representation-Annotation Formalism/Guidelines" >Representation-Annotation Formalism/Guidelines</option>

<option value="Representation-Annotation Standards/Best Practices" >Representation-Annotation Standards/Best Practices</option>

<option value="Language Resources/Technologies Infrastructure" >Language Resources/Technologies Infrastructure</option>

</optgroup>
<optgroup label="Evaluation">
<option value="Evaluation Data" >Evaluation Data</option>

<option value="Evaluation Tool" >Evaluation Tool</option>

<option value="Evaluation Package" >Evaluation Package</option>

<option value="Evaluation Methodology/Standards/Guidelines" >Evaluation Methodology/Standards/Guidelines</option>

</optgroup>

<optgroup label="Other">
<option value="Other" >Other - please specify &rarr;</option>

</optgroup>

</select>
</td>


<td style="width: 40%" id=resOther1>
<input type=text
       style="width: 100%;"
       name='RMap{1}{TypeOther}'
       value=""
       size="50"
       id=RNO1>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RNO1").autocomplete({
      source: database_ResourceType_other
    });
  });
</script>

</td>
</tr>




<tr>
<td align="right" valign=center><span id='redsize1'>Size:</span></td>
<td align="left" valign=center>
  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{1}{Size}' value="" size="40" id=RSIZE1 type="text">
      </td>
      <td>
	<select name='RMap{1}{Unit}' 
		size="1" 
		id=RSIZEU1 
		style="width: 100%;">
	  <option value="" selected>-- Please select a value --</option>

	  <option value="KByte  " >KByte  </option>

<option value="MByte  " >MByte  </option>

<option value="GByte  " >GByte  </option>

<option value="sentences  " >sentences  </option>

<option value="words  " >words  </option>

<option value="tokens  " >tokens  </option>

<option value="entries  " >entries  </option>

<option value="lexemes  " >lexemes  </option>

<option value="synsets  " >synsets  </option>

<option value="concepts " >concepts </option>

<option value="rules   " >rules   </option>

<option value="hours  " >hours  </option>

<option value="minutes  " >minutes  </option>

	  <option value="Other" >Other - please specify next to the size value</option>

	</select>
      </td>
    </tr>
  </table>
</td>


<td style="width: 40%" valign=top>
<em>Enter package size - a number and a relevant unit of measurement, e.g., 48Mbyte, 
5500&nbsp;lexemes, 2.1Gbyte.</em>
</td>
</tr>



<tr>

<td align="right"><span id='redstatus1'>Resource Production Status:</span></td>
<td align="left">
<select name='RMap{1}{Status}' 
        size="1" 
        onchange="handleOtherField('RS1','rsOther1','RSO1')"
        id=RS1 
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Newly created-finished" >Newly created-finished</option>

<option value="Newly created-in progress" >Newly created-in progress</option>

<option value="Existing-used" >Existing-used</option>

<option value="Existing-updated" >Existing-updated</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>


<td style="width: 40%" id=rsOther1>
<input type=text
       style="width: 100%;"
       name='RMap{1}{StatusOther}'
       value=""
       size="50"
       id=RSO1>
</td>
</tr>

<tr>
<td align=right><span id='redlang1'>Language(s):</span></td>
<td align="left">
<select name='RMap{1}{Language}' 
        size="1" 
        id=RL1 
        onchange="handleOtherLanguageField('RL1','lang1','LAO1')"
        style="width: 100%;">
<option value="P" >-- Please select a value --</option>
<option value="W" >Enter the language(s) in the box(es) below &darr;</option>
<option value="LI" >Language Independent</option>
<option value="NA" >Not Applicable</option>

</select>
</td>
<td>
</td>
</tr>

<tr>
<td></td>
<td colspan=2>
<span id=lang1>
<table border=0>
<tr>
<td>
<blockquote>
<table border=0>
  <tr>
    <td>
      Language&nbsp;1:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{1}{LanguageOther}'
	     value=""
	     size="50"
	     id=LAO1>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO1").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;2:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{1}{LanguageOther2}'
	     value=""
	     size="50"
	     id=LAO21>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO21").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;3:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{1}{LanguageOther3}'
	     value=""
	     size="50"
	     id=LAO31>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO31").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;4:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{1}{LanguageOther4}'
	     value=""
	     size="50"
	     id=LAO41>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO41").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>


  <tr>
    <td>
      Language&nbsp;5:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{1}{LanguageOther5}'
	     value=""
	     size="50"
	     id=LAO51>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO51").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Other&nbsp;Languages:
    </td>
    <td>
      <textarea style="width: 100%;"
	     name='RMap{1}{LanguageOtherN}'
	     cols="50" rows="3"
	     id=LAON1
		></textarea>
    </td>
  </tr>
</table>
</blockquote>
</td>
<td>
  <i>
    Please enter the languages that apply to your resource.<br>
    Please note that the first 3 boxes have autocompletion.<br>
    If your resource covers more than 3 languages, please use the "Other 
    languages" text area
    <b>and enter the various languages separated by a comma</b>. 
  </i>
</td>
</table>
</span>
</td>
<td width="25%">
</td>
</tr>


<tr>
<td align=right><span id='redmodality1'>Modality:</span></td>
<td align=left>
<select name='RMap{1}{Modality}' 
        size="1" 
        id=RM1 
        onchange="handleOtherField('RM1','rmOther1','RMO1')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Speech" >Speech</option>

<option value="Written" >Written</option>

<option value="Speech/Written" >Speech/Written</option>

<option value="Multimodal/Multimedia" >Multimodal/Multimedia</option>

<option value="Sign Language" >Sign Language</option>

<option value="Modality Independent" >Modality Independent</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>



<td style="width: 40%" id=rmOther1>
<input type=text
       style="width: 100%;"
       name='RMap{1}{ModalityOther}'
       value=""
       size="50"
       id=RMO1>
</td>
</tr>

<tr>
<td align=right><span id='reduse1'>Use of the Resource:</span></td>
<td align=left>
<select name='RMap{1}{Use}' 
        size="1" 
        id=RU1 
        onchange="handleOtherField('RU1','ruOther1','RUO1')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Acquisition" >Acquisition</option>

<option value="Anaphora, Coreference" >Anaphora, Coreference</option>

<option value="Corpus Creation/Annotation" >Corpus Creation/Annotation</option>

<option value="Dialogue" >Dialogue</option>

<option value="Discourse" >Discourse</option>

<option value="Document Classification, Text categorisation" >Document Classification, Text categorisation</option>

<option value="Emotion Recognition/Generation" >Emotion Recognition/Generation</option>

<option value="Evaluation/Validation" >Evaluation/Validation</option>

<option value="Information Extraction, Information Retrieval" >Information Extraction, Information Retrieval</option>

<option value="Knowledge Discovery/Representation" >Knowledge Discovery/Representation</option>

<option value="Language Identification" >Language Identification</option>

<option value="Language Modelling" >Language Modelling</option>

<option value="Lexicon Creation/Annotation" >Lexicon Creation/Annotation</option>

<option value="Machine Learning" >Machine Learning</option>

<option value="Machine Translation, SpeechToSpeech Translation" >Machine Translation, SpeechToSpeech Translation</option>

<option value="Morphological Analysis" >Morphological Analysis</option>

<option value="Multimedia Document Processing" >Multimedia Document Processing</option>

<option value="Named Entity Recognition" >Named Entity Recognition</option>

<option value="Natural Language Generation" >Natural Language Generation</option>

<option value="Opinion Mining/Sentiment Analysis" >Opinion Mining/Sentiment Analysis</option>

<option value="Parsing and Tagging" >Parsing and Tagging</option>

<option value="Person Identification" >Person Identification</option>

<option value="Question Answering" >Question Answering</option>

<option value="Semantic Role Labeling" >Semantic Role Labeling</option>

<option value="Semantic Web" >Semantic Web</option>

<option value="Sign Language Recognition/Generation" >Sign Language Recognition/Generation</option>

<option value="Speech Recognition/Understanding" >Speech Recognition/Understanding</option>

<option value="Speech Synthesis" >Speech Synthesis</option>

<option value="Summarisation" >Summarisation</option>

<option value="Text Mining" >Text Mining</option>

<option value="Textual Entailment and Paraphrasing" >Textual Entailment and Paraphrasing</option>

<option value="Topic Detection and Tracking" >Topic Detection and Tracking</option>

<option value="Voice Control" >Voice Control</option>

<option value="Web Services" >Web Services</option>

<option value="Word Sense Disambiguation" >Word Sense Disambiguation</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=ruOther1>
<input type=text
       style="width: 100%;"
       name='RMap{1}{UseOther}'
       value=""
       size="50"
       id=RUO1>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RUO1").autocomplete({
      source: database_ResourceUse_other
    });
  });
</script>

</td>
</tr>

<tr>
<td align=right><span id='redavailability1'>Resource Availability:</span></td>
<td align=left>
<select name='RMap{1}{Availability}' 
        size="1" 
        id=RA1 
        onchange="handleOtherField('RA1','raOther1','RAO1')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Freely Available" >Freely Available</option>

<option value="From Data Center(s)" >From Data Center(s)</option>

<option value="From Owner" >From Owner</option>

<option value="Not Available" >Not Available</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=raOther1>
<input type=text
       style="width: 100%;"
       name='RMap{1}{AvailabilityOther}'
       value=""
       size="50"
       id=RAO1>
</td>
</tr>



<tr>
<td align="right" valign=center><span id='redlicense1'>License:</span></td>
<td align="left" valign=center>
<input name='RMap{1}{License}' value="" size="40" id=RLIC1 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Licence type, e.g., ELRA, LDC, Gnu, CreativeCommons, OpenSource, etc.</em>
</td>
</tr>




<tr>
<td align=right><span id='redurl1'>Resource URL (if available):</span></td>
<td align=left>
<input name='RMap{1}{URL}' value="" size="40" id=RURL1 style="width: 100%;" type="text">
</td>
</tr>

<tr>
<td align="right" valign=center><span id='reddocumentation1'>Documentation:</span></td>
<td align="left" valign=center>
<input name='RMap{1}{Documentation}' value="" size="40" id=RDOC1 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Is there documentation? In which language? Is the documentation publicly available?</em>
</td>
</tr>




<tr>
<td  align=right valign=top><span id='reddescription1'>Resource Description:</span> </td>
<td colspan="2" align="left">
<textarea name='RMap{1}{Description}' rows="10" cols="70" id=RD1 style="width: 100%;"></textarea>
</td>
</tr>
<!--
<tr>
  <td colspan="3">
The section below applies only if the resource is not available 
at/from established repositories, such as ELRA, LDC, or other data 
centers, as described by the URL entered in the above form. 
ELRA encourages all authors to share the described LRs (data, toosl, services,
etc.), unless already available, to enable their reuse, replicability of experiments,
including evaluation ones, etc.

  </td>
</tr>

<tr>
  <td  align=right valign=top>Do you want to make this LR available through the
<b>"Share Your LRs" initiative?</b></td>
  <td  align="left">
    <input type="checkbox"  id="RLaccepted_1" name="RMap{1}{accepted}" onclick="Resource_manage_upload('RLaccepted_1')" >
  </td>
  <td style="width: 40%" valign=top>
    <em><b>Disclaimer:</b> By clicking the box you agree to share this LR for which rights have been cleared and guarantee that such LR is sharable under the licence you mentioned above.</em>
  </td>
</tr>


<tr>
  <td align=right valign=top><span id="filed_RLaccepted_1">You can upload your LR here if it is smaller than <b>20Mb</b>.</span></td>
  <td align="left">
    <span id="filet_RLaccepted_1">
      <input type="file" name="RMap{1}{FILE}" /> &nbsp;&nbsp;&nbsp;
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>


<tr>
  <td align=right><span id="urld_RLaccepted_1">Otherwise please provide an URL from where the LR can be downloaded:</span></td>
  <td align=left>
    <span id="urlt_RLaccepted_1">
      <input name='RMap{1}{FILEURL}' value="" size="40" id=RURL1 style="width: 100%;" type="text">
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>

-->


</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</div><div id=r2>

<table width="95%">
<tr>
<td>#2</td>

<td style="width: 100%">
<table style="width: 100%" border="1">
<tr>
<td style="width: 100%">
<table cellpadding=5 style="width: 100%">

<tr>
<td align="right">Resource Name:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{2}{Name}' 
               type="text"
               value="" 
               size="40" 
               id=RN2> 


	<script type="text/javascript">
	  jQuery(function(){
	    jQuery("#RN2").keyup(function(){setDefaultValid(2)});
            jQuery("#RN2").autocomplete({
              source: database_ResourceName,
              select: function(event, ui) { f = selectResourceNameUI("#RN2", event, ui); setDefaultValid(2); return f;}});
	  });
	</script>
      </td>
     <!-- <td>
	<input type=button onClick="setDefaultResourceData(2)" value='Load Default Values' id="setDefaultButton_2" disabled>
      </td>
      -->
    </tr>
  </table>


</td>
<!--
<td>
  <i>If the button "Load default values" is enabled you can prefill some fields with default values available in the LreMap for this resource.</i>
</td>
-->
</tr>

<tr>
<td align="right">ISLRN Field:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{2}{ISLRN}' 
               type="text"
               value="" 
               size="20" 
               id=RISLRN2
	       title="Format: xxx-xxx-xxx-xxx-x"> 
      </td>
      <td>
	&nbsp;&nbsp;&nbsp;
	<input type=checkbox onClick="disableResourceILSRN(2)" value='1'
	       id="disableResourceISLRN2" name='RMap{2}{ISLRNstatus}'
	       > No ISLRN / I don't know
	<script type="text/javascript">
	  jQuery(function(){
	    disableResourceILSRN(2);
	  });
	</script>
      </td>
    </tr>
  </table>
</td>
<td>
  <i>The ISLRN is a unique and universal identification schema for LRs
    which provides LRs with unique identifier using a standardised
    nomenclature.  It also ensures that LRs are correctly identified,
    and consequently, recognised with proper references for their
    usage in applications in R&D projects, products evaluation and
    benchmark as well as in documents and scientific
    papers. Visit <a href="http://www.islrn.org"
    target=_blank>http://www.islrn.org</a>.</i>
</td>
</tr>



<tr>
<td style="width: 20%" align=right><span id="redtype2">Resource Type: </span></td>



<td style="width: 40%" align=left>
<select name='RMap{2}{Type}' 
        size=1 
        id=RT2 
        onchange="handleOtherField('RT2','resOther2','RNO2')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>


<optgroup label="Resource-Dataset">
<option value="Corpus" >Corpus</option>

<option value="Lexicon" >Lexicon</option>

<option value="Grammar/Language Model" >Grammar/Language Model</option>

<option value="Ontology" >Ontology</option>

<option value="Terminology" >Terminology</option>

<option value="Treebank" >Treebank</option>

</optgroup>
<optgroup label="Resource-Tool">
<option value="Corpus Tool" >Corpus Tool</option>

<option value="Aligner" >Aligner</option>

<option value="Annotation Tool" >Annotation Tool</option>

<option value="Language Identifier" >Language Identifier</option>

<option value="Language Modeling Tool" >Language Modeling Tool</option>

<option value="Machine Learning Tool" >Machine Learning Tool</option>

<option value="Machine Translation Tool" >Machine Translation Tool</option>

<option value="Named Entity Recognizer" >Named Entity Recognizer</option>

<option value="Sentiment Analysis Tool" >Sentiment Analysis Tool</option>

<option value="Software Toolkit" >Software Toolkit</option>

<option value="Tagger/Parser" >Tagger/Parser</option>

<option value="Tokenizer" >Tokenizer</option>

<option value="Web Service" >Web Service</option>

<option value="Word Sense Disambiguator" >Word Sense Disambiguator</option>

<option value="Prosodic Analyzer" >Prosodic Analyzer</option>

<option value="Signal Processing/Features Extraction" >Signal Processing/Features Extraction</option>

<option value="Speaker Recognizer" >Speaker Recognizer</option>

<option value="Speech Recognizer/Transcriber" >Speech Recognizer/Transcriber</option>

<option value="Spoken Dialogue Tool" >Spoken Dialogue Tool</option>

<option value="Text-to-Speech Synthesizer" >Text-to-Speech Synthesizer</option>

<option value="Image Analyzer" >Image Analyzer</option>

</optgroup>
<optgroup label="Resource-Guidelines">
<option value="Metadata" >Metadata</option>

<option value="Representation-Annotation Formalism/Guidelines" >Representation-Annotation Formalism/Guidelines</option>

<option value="Representation-Annotation Standards/Best Practices" >Representation-Annotation Standards/Best Practices</option>

<option value="Language Resources/Technologies Infrastructure" >Language Resources/Technologies Infrastructure</option>

</optgroup>
<optgroup label="Evaluation">
<option value="Evaluation Data" >Evaluation Data</option>

<option value="Evaluation Tool" >Evaluation Tool</option>

<option value="Evaluation Package" >Evaluation Package</option>

<option value="Evaluation Methodology/Standards/Guidelines" >Evaluation Methodology/Standards/Guidelines</option>

</optgroup>

<optgroup label="Other">
<option value="Other" >Other - please specify &rarr;</option>

</optgroup>

</select>
</td>


<td style="width: 40%" id=resOther2>
<input type=text
       style="width: 100%;"
       name='RMap{2}{TypeOther}'
       value=""
       size="50"
       id=RNO2>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RNO2").autocomplete({
      source: database_ResourceType_other
    });
  });
</script>

</td>
</tr>




<tr>
<td align="right" valign=center><span id='redsize2'>Size:</span></td>
<td align="left" valign=center>
  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{2}{Size}' value="" size="40" id=RSIZE2 type="text">
      </td>
      <td>
	<select name='RMap{2}{Unit}' 
		size="1" 
		id=RSIZEU2 
		style="width: 100%;">
	  <option value="" selected>-- Please select a value --</option>

	  <option value="KByte  " >KByte  </option>

<option value="MByte  " >MByte  </option>

<option value="GByte  " >GByte  </option>

<option value="sentences  " >sentences  </option>

<option value="words  " >words  </option>

<option value="tokens  " >tokens  </option>

<option value="entries  " >entries  </option>

<option value="lexemes  " >lexemes  </option>

<option value="synsets  " >synsets  </option>

<option value="concepts " >concepts </option>

<option value="rules   " >rules   </option>

<option value="hours  " >hours  </option>

<option value="minutes  " >minutes  </option>

	  <option value="Other" >Other - please specify next to the size value</option>

	</select>
      </td>
    </tr>
  </table>
</td>


<td style="width: 40%" valign=top>
<em>Enter package size - a number and a relevant unit of measurement, e.g., 48Mbyte, 
5500&nbsp;lexemes, 2.1Gbyte.</em>
</td>
</tr>



<tr>

<td align="right"><span id='redstatus2'>Resource Production Status:</span></td>
<td align="left">
<select name='RMap{2}{Status}' 
        size="1" 
        onchange="handleOtherField('RS2','rsOther2','RSO2')"
        id=RS2 
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Newly created-finished" >Newly created-finished</option>

<option value="Newly created-in progress" >Newly created-in progress</option>

<option value="Existing-used" >Existing-used</option>

<option value="Existing-updated" >Existing-updated</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>


<td style="width: 40%" id=rsOther2>
<input type=text
       style="width: 100%;"
       name='RMap{2}{StatusOther}'
       value=""
       size="50"
       id=RSO2>
</td>
</tr>

<tr>
<td align=right><span id='redlang2'>Language(s):</span></td>
<td align="left">
<select name='RMap{2}{Language}' 
        size="1" 
        id=RL2 
        onchange="handleOtherLanguageField('RL2','lang2','LAO2')"
        style="width: 100%;">
<option value="P" >-- Please select a value --</option>
<option value="W" >Enter the language(s) in the box(es) below &darr;</option>
<option value="LI" >Language Independent</option>
<option value="NA" >Not Applicable</option>

</select>
</td>
<td>
</td>
</tr>

<tr>
<td></td>
<td colspan=2>
<span id=lang2>
<table border=0>
<tr>
<td>
<blockquote>
<table border=0>
  <tr>
    <td>
      Language&nbsp;1:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{2}{LanguageOther}'
	     value=""
	     size="50"
	     id=LAO2>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO2").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;2:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{2}{LanguageOther2}'
	     value=""
	     size="50"
	     id=LAO22>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO22").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;3:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{2}{LanguageOther3}'
	     value=""
	     size="50"
	     id=LAO32>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO32").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;4:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{2}{LanguageOther4}'
	     value=""
	     size="50"
	     id=LAO42>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO42").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>


  <tr>
    <td>
      Language&nbsp;5:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{2}{LanguageOther5}'
	     value=""
	     size="50"
	     id=LAO52>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO52").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Other&nbsp;Languages:
    </td>
    <td>
      <textarea style="width: 100%;"
	     name='RMap{2}{LanguageOtherN}'
	     cols="50" rows="3"
	     id=LAON2
		></textarea>
    </td>
  </tr>
</table>
</blockquote>
</td>
<td>
  <i>
    Please enter the languages that apply to your resource.<br>
    Please note that the first 3 boxes have autocompletion.<br>
    If your resource covers more than 3 languages, please use the "Other 
    languages" text area
    <b>and enter the various languages separated by a comma</b>. 
  </i>
</td>
</table>
</span>
</td>
<td width="25%">
</td>
</tr>


<tr>
<td align=right><span id='redmodality2'>Modality:</span></td>
<td align=left>
<select name='RMap{2}{Modality}' 
        size="1" 
        id=RM2 
        onchange="handleOtherField('RM2','rmOther2','RMO2')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Speech" >Speech</option>

<option value="Written" >Written</option>

<option value="Speech/Written" >Speech/Written</option>

<option value="Multimodal/Multimedia" >Multimodal/Multimedia</option>

<option value="Sign Language" >Sign Language</option>

<option value="Modality Independent" >Modality Independent</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>



<td style="width: 40%" id=rmOther2>
<input type=text
       style="width: 100%;"
       name='RMap{2}{ModalityOther}'
       value=""
       size="50"
       id=RMO2>
</td>
</tr>

<tr>
<td align=right><span id='reduse2'>Use of the Resource:</span></td>
<td align=left>
<select name='RMap{2}{Use}' 
        size="1" 
        id=RU2 
        onchange="handleOtherField('RU2','ruOther2','RUO2')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Acquisition" >Acquisition</option>

<option value="Anaphora, Coreference" >Anaphora, Coreference</option>

<option value="Corpus Creation/Annotation" >Corpus Creation/Annotation</option>

<option value="Dialogue" >Dialogue</option>

<option value="Discourse" >Discourse</option>

<option value="Document Classification, Text categorisation" >Document Classification, Text categorisation</option>

<option value="Emotion Recognition/Generation" >Emotion Recognition/Generation</option>

<option value="Evaluation/Validation" >Evaluation/Validation</option>

<option value="Information Extraction, Information Retrieval" >Information Extraction, Information Retrieval</option>

<option value="Knowledge Discovery/Representation" >Knowledge Discovery/Representation</option>

<option value="Language Identification" >Language Identification</option>

<option value="Language Modelling" >Language Modelling</option>

<option value="Lexicon Creation/Annotation" >Lexicon Creation/Annotation</option>

<option value="Machine Learning" >Machine Learning</option>

<option value="Machine Translation, SpeechToSpeech Translation" >Machine Translation, SpeechToSpeech Translation</option>

<option value="Morphological Analysis" >Morphological Analysis</option>

<option value="Multimedia Document Processing" >Multimedia Document Processing</option>

<option value="Named Entity Recognition" >Named Entity Recognition</option>

<option value="Natural Language Generation" >Natural Language Generation</option>

<option value="Opinion Mining/Sentiment Analysis" >Opinion Mining/Sentiment Analysis</option>

<option value="Parsing and Tagging" >Parsing and Tagging</option>

<option value="Person Identification" >Person Identification</option>

<option value="Question Answering" >Question Answering</option>

<option value="Semantic Role Labeling" >Semantic Role Labeling</option>

<option value="Semantic Web" >Semantic Web</option>

<option value="Sign Language Recognition/Generation" >Sign Language Recognition/Generation</option>

<option value="Speech Recognition/Understanding" >Speech Recognition/Understanding</option>

<option value="Speech Synthesis" >Speech Synthesis</option>

<option value="Summarisation" >Summarisation</option>

<option value="Text Mining" >Text Mining</option>

<option value="Textual Entailment and Paraphrasing" >Textual Entailment and Paraphrasing</option>

<option value="Topic Detection and Tracking" >Topic Detection and Tracking</option>

<option value="Voice Control" >Voice Control</option>

<option value="Web Services" >Web Services</option>

<option value="Word Sense Disambiguation" >Word Sense Disambiguation</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=ruOther2>
<input type=text
       style="width: 100%;"
       name='RMap{2}{UseOther}'
       value=""
       size="50"
       id=RUO2>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RUO2").autocomplete({
      source: database_ResourceUse_other
    });
  });
</script>

</td>
</tr>

<tr>
<td align=right><span id='redavailability2'>Resource Availability:</span></td>
<td align=left>
<select name='RMap{2}{Availability}' 
        size="1" 
        id=RA2 
        onchange="handleOtherField('RA2','raOther2','RAO2')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Freely Available" >Freely Available</option>

<option value="From Data Center(s)" >From Data Center(s)</option>

<option value="From Owner" >From Owner</option>

<option value="Not Available" >Not Available</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=raOther2>
<input type=text
       style="width: 100%;"
       name='RMap{2}{AvailabilityOther}'
       value=""
       size="50"
       id=RAO2>
</td>
</tr>



<tr>
<td align="right" valign=center><span id='redlicense2'>License:</span></td>
<td align="left" valign=center>
<input name='RMap{2}{License}' value="" size="40" id=RLIC2 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Licence type, e.g., ELRA, LDC, Gnu, CreativeCommons, OpenSource, etc.</em>
</td>
</tr>




<tr>
<td align=right><span id='redurl2'>Resource URL (if available):</span></td>
<td align=left>
<input name='RMap{2}{URL}' value="" size="40" id=RURL2 style="width: 100%;" type="text">
</td>
</tr>

<tr>
<td align="right" valign=center><span id='reddocumentation2'>Documentation:</span></td>
<td align="left" valign=center>
<input name='RMap{2}{Documentation}' value="" size="40" id=RDOC2 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Is there documentation? In which language? Is the documentation publicly available?</em>
</td>
</tr>




<tr>
<td  align=right valign=top><span id='reddescription2'>Resource Description:</span> </td>
<td colspan="2" align="left">
<textarea name='RMap{2}{Description}' rows="10" cols="70" id=RD2 style="width: 100%;"></textarea>
</td>
</tr>
<!--
<tr>
  <td colspan="3">
The section below applies only if the resource is not available 
at/from established repositories, such as ELRA, LDC, or other data 
centers, as described by the URL entered in the above form. 
ELRA encourages all authors to share the described LRs (data, toosl, services,
etc.), unless already available, to enable their reuse, replicability of experiments,
including evaluation ones, etc.

  </td>
</tr>

<tr>
  <td  align=right valign=top>Do you want to make this LR available through the
<b>"Share Your LRs" initiative?</b></td>
  <td  align="left">
    <input type="checkbox"  id="RLaccepted_2" name="RMap{2}{accepted}" onclick="Resource_manage_upload('RLaccepted_2')" >
  </td>
  <td style="width: 40%" valign=top>
    <em><b>Disclaimer:</b> By clicking the box you agree to share this LR for which rights have been cleared and guarantee that such LR is sharable under the licence you mentioned above.</em>
  </td>
</tr>


<tr>
  <td align=right valign=top><span id="filed_RLaccepted_2">You can upload your LR here if it is smaller than <b>20Mb</b>.</span></td>
  <td align="left">
    <span id="filet_RLaccepted_2">
      <input type="file" name="RMap{2}{FILE}" /> &nbsp;&nbsp;&nbsp;
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>


<tr>
  <td align=right><span id="urld_RLaccepted_2">Otherwise please provide an URL from where the LR can be downloaded:</span></td>
  <td align=left>
    <span id="urlt_RLaccepted_2">
      <input name='RMap{2}{FILEURL}' value="" size="40" id=RURL2 style="width: 100%;" type="text">
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>


-->

</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</div><div id=r3>

<table width="95%">
<tr>
<td>#3</td>

<td style="width: 100%">
<table style="width: 100%" border="1">
<tr>
<td style="width: 100%">
<table cellpadding=5 style="width: 100%">

<tr>
<td align="right">Resource Name:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{3}{Name}' 
               type="text"
               value="" 
               size="40" 
               id=RN3> 


	<script type="text/javascript">
	  jQuery(function(){
	    jQuery("#RN3").keyup(function(){setDefaultValid(3)});
            jQuery("#RN3").autocomplete({
              source: database_ResourceName,
              select: function(event, ui) { f = selectResourceNameUI("#RN3", event, ui); setDefaultValid(3); return f;}});
	  });
	</script>
      </td>
      <!-- <td>
	<input type=button onClick="setDefaultResourceData(3)" value='Load Default Values' id="setDefaultButton_3" disabled>
      </td>
      -->
    </tr>
  </table>


</td>
<!--
<td>
  <i>If the button "Load default values" is enabled you can prefill some fields with default values available in the LreMap for this resource.</i>
</td> -->
</tr>

<tr>
<td align="right">ISLRN Field:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{3}{ISLRN}' 
               type="text"
               value="" 
               size="20" 
               id=RISLRN3
	       title="Format: xxx-xxx-xxx-xxx-x"> 
      </td>
      <td>
	&nbsp;&nbsp;&nbsp;
	<input type=checkbox onClick="disableResourceILSRN(3)" value='1'
	       id="disableResourceISLRN3" name='RMap{3}{ISLRNstatus}'
	       > No ISLRN / I don't know
	<script type="text/javascript">
	  jQuery(function(){
	    disableResourceILSRN(3);
	  });
	</script>
      </td>
    </tr>
  </table>
</td>
<td>
  <i>The ISLRN is a unique and universal identification schema for LRs
    which provides LRs with unique identifier using a standardised
    nomenclature.  It also ensures that LRs are correctly identified,
    and consequently, recognised with proper references for their
    usage in applications in R&D projects, products evaluation and
    benchmark as well as in documents and scientific
    papers. Visit <a href="http://www.islrn.org"
    target=_blank>http://www.islrn.org</a>.</i>
</td>
</tr>



<tr>
<td style="width: 20%" align=right><span id="redtype3">Resource Type: </span></td>



<td style="width: 40%" align=left>
<select name='RMap{3}{Type}' 
        size=1 
        id=RT3 
        onchange="handleOtherField('RT3','resOther3','RNO3')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>


<optgroup label="Resource-Dataset">
<option value="Corpus" >Corpus</option>

<option value="Lexicon" >Lexicon</option>

<option value="Grammar/Language Model" >Grammar/Language Model</option>

<option value="Ontology" >Ontology</option>

<option value="Terminology" >Terminology</option>

<option value="Treebank" >Treebank</option>

</optgroup>
<optgroup label="Resource-Tool">
<option value="Corpus Tool" >Corpus Tool</option>

<option value="Aligner" >Aligner</option>

<option value="Annotation Tool" >Annotation Tool</option>

<option value="Language Identifier" >Language Identifier</option>

<option value="Language Modeling Tool" >Language Modeling Tool</option>

<option value="Machine Learning Tool" >Machine Learning Tool</option>

<option value="Machine Translation Tool" >Machine Translation Tool</option>

<option value="Named Entity Recognizer" >Named Entity Recognizer</option>

<option value="Sentiment Analysis Tool" >Sentiment Analysis Tool</option>

<option value="Software Toolkit" >Software Toolkit</option>

<option value="Tagger/Parser" >Tagger/Parser</option>

<option value="Tokenizer" >Tokenizer</option>

<option value="Web Service" >Web Service</option>

<option value="Word Sense Disambiguator" >Word Sense Disambiguator</option>

<option value="Prosodic Analyzer" >Prosodic Analyzer</option>

<option value="Signal Processing/Features Extraction" >Signal Processing/Features Extraction</option>

<option value="Speaker Recognizer" >Speaker Recognizer</option>

<option value="Speech Recognizer/Transcriber" >Speech Recognizer/Transcriber</option>

<option value="Spoken Dialogue Tool" >Spoken Dialogue Tool</option>

<option value="Text-to-Speech Synthesizer" >Text-to-Speech Synthesizer</option>

<option value="Image Analyzer" >Image Analyzer</option>

</optgroup>
<optgroup label="Resource-Guidelines">
<option value="Metadata" >Metadata</option>

<option value="Representation-Annotation Formalism/Guidelines" >Representation-Annotation Formalism/Guidelines</option>

<option value="Representation-Annotation Standards/Best Practices" >Representation-Annotation Standards/Best Practices</option>

<option value="Language Resources/Technologies Infrastructure" >Language Resources/Technologies Infrastructure</option>

</optgroup>
<optgroup label="Evaluation">
<option value="Evaluation Data" >Evaluation Data</option>

<option value="Evaluation Tool" >Evaluation Tool</option>

<option value="Evaluation Package" >Evaluation Package</option>

<option value="Evaluation Methodology/Standards/Guidelines" >Evaluation Methodology/Standards/Guidelines</option>

</optgroup>

<optgroup label="Other">
<option value="Other" >Other - please specify &rarr;</option>

</optgroup>

</select>
</td>


<td style="width: 40%" id=resOther3>
<input type=text
       style="width: 100%;"
       name='RMap{3}{TypeOther}'
       value=""
       size="50"
       id=RNO3>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RNO3").autocomplete({
      source: database_ResourceType_other
    });
  });
</script>

</td>
</tr>




<tr>
<td align="right" valign=center><span id='redsize3'>Size:</span></td>
<td align="left" valign=center>
  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{3}{Size}' value="" size="40" id=RSIZE3 type="text">
      </td>
      <td>
	<select name='RMap{3}{Unit}' 
		size="1" 
		id=RSIZEU3 
		style="width: 100%;">
	  <option value="" selected>-- Please select a value --</option>

	  <option value="KByte  " >KByte  </option>

<option value="MByte  " >MByte  </option>

<option value="GByte  " >GByte  </option>

<option value="sentences  " >sentences  </option>

<option value="words  " >words  </option>

<option value="tokens  " >tokens  </option>

<option value="entries  " >entries  </option>

<option value="lexemes  " >lexemes  </option>

<option value="synsets  " >synsets  </option>

<option value="concepts " >concepts </option>

<option value="rules   " >rules   </option>

<option value="hours  " >hours  </option>

<option value="minutes  " >minutes  </option>

	  <option value="Other" >Other - please specify next to the size value</option>

	</select>
      </td>
    </tr>
  </table>
</td>


<td style="width: 40%" valign=top>
<em>Enter package size - a number and a relevant unit of measurement, e.g., 48Mbyte, 
5500&nbsp;lexemes, 2.1Gbyte.</em>
</td>
</tr>



<tr>

<td align="right"><span id='redstatus3'>Resource Production Status:</span></td>
<td align="left">
<select name='RMap{3}{Status}' 
        size="1" 
        onchange="handleOtherField('RS3','rsOther3','RSO3')"
        id=RS3 
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Newly created-finished" >Newly created-finished</option>

<option value="Newly created-in progress" >Newly created-in progress</option>

<option value="Existing-used" >Existing-used</option>

<option value="Existing-updated" >Existing-updated</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>


<td style="width: 40%" id=rsOther3>
<input type=text
       style="width: 100%;"
       name='RMap{3}{StatusOther}'
       value=""
       size="50"
       id=RSO3>
</td>
</tr>

<tr>
<td align=right><span id='redlang3'>Language(s):</span></td>
<td align="left">
<select name='RMap{3}{Language}' 
        size="1" 
        id=RL3 
        onchange="handleOtherLanguageField('RL3','lang3','LAO3')"
        style="width: 100%;">
<option value="P" >-- Please select a value --</option>
<option value="W" >Enter the language(s) in the box(es) below &darr;</option>
<option value="LI" >Language Independent</option>
<option value="NA" >Not Applicable</option>

</select>
</td>
<td>
</td>
</tr>

<tr>
<td></td>
<td colspan=2>
<span id=lang3>
<table border=0>
<tr>
<td>
<blockquote>
<table border=0>
  <tr>
    <td>
      Language&nbsp;1:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{3}{LanguageOther}'
	     value=""
	     size="50"
	     id=LAO3>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO3").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;2:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{3}{LanguageOther2}'
	     value=""
	     size="50"
	     id=LAO23>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO23").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;3:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{3}{LanguageOther3}'
	     value=""
	     size="50"
	     id=LAO33>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO33").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;4:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{3}{LanguageOther4}'
	     value=""
	     size="50"
	     id=LAO43>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO43").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>


  <tr>
    <td>
      Language&nbsp;5:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{3}{LanguageOther5}'
	     value=""
	     size="50"
	     id=LAO53>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO53").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Other&nbsp;Languages:
    </td>
    <td>
      <textarea style="width: 100%;"
	     name='RMap{3}{LanguageOtherN}'
	     cols="50" rows="3"
	     id=LAON3
		></textarea>
    </td>
  </tr>
</table>
</blockquote>
</td>
<td>
  <i>
    Please enter the languages that apply to your resource.<br>
    Please note that the first 3 boxes have autocompletion.<br>
    If your resource covers more than 3 languages, please use the "Other 
    languages" text area
    <b>and enter the various languages separated by a comma</b>. 
  </i>
</td>
</table>
</span>
</td>
<td width="25%">
</td>
</tr>


<tr>
<td align=right><span id='redmodality3'>Modality:</span></td>
<td align=left>
<select name='RMap{3}{Modality}' 
        size="1" 
        id=RM3 
        onchange="handleOtherField('RM3','rmOther3','RMO3')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Speech" >Speech</option>

<option value="Written" >Written</option>

<option value="Speech/Written" >Speech/Written</option>

<option value="Multimodal/Multimedia" >Multimodal/Multimedia</option>

<option value="Sign Language" >Sign Language</option>

<option value="Modality Independent" >Modality Independent</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>



<td style="width: 40%" id=rmOther3>
<input type=text
       style="width: 100%;"
       name='RMap{3}{ModalityOther}'
       value=""
       size="50"
       id=RMO3>
</td>
</tr>

<tr>
<td align=right><span id='reduse3'>Use of the Resource:</span></td>
<td align=left>
<select name='RMap{3}{Use}' 
        size="1" 
        id=RU3 
        onchange="handleOtherField('RU3','ruOther3','RUO3')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Acquisition" >Acquisition</option>

<option value="Anaphora, Coreference" >Anaphora, Coreference</option>

<option value="Corpus Creation/Annotation" >Corpus Creation/Annotation</option>

<option value="Dialogue" >Dialogue</option>

<option value="Discourse" >Discourse</option>

<option value="Document Classification, Text categorisation" >Document Classification, Text categorisation</option>

<option value="Emotion Recognition/Generation" >Emotion Recognition/Generation</option>

<option value="Evaluation/Validation" >Evaluation/Validation</option>

<option value="Information Extraction, Information Retrieval" >Information Extraction, Information Retrieval</option>

<option value="Knowledge Discovery/Representation" >Knowledge Discovery/Representation</option>

<option value="Language Identification" >Language Identification</option>

<option value="Language Modelling" >Language Modelling</option>

<option value="Lexicon Creation/Annotation" >Lexicon Creation/Annotation</option>

<option value="Machine Learning" >Machine Learning</option>

<option value="Machine Translation, SpeechToSpeech Translation" >Machine Translation, SpeechToSpeech Translation</option>

<option value="Morphological Analysis" >Morphological Analysis</option>

<option value="Multimedia Document Processing" >Multimedia Document Processing</option>

<option value="Named Entity Recognition" >Named Entity Recognition</option>

<option value="Natural Language Generation" >Natural Language Generation</option>

<option value="Opinion Mining/Sentiment Analysis" >Opinion Mining/Sentiment Analysis</option>

<option value="Parsing and Tagging" >Parsing and Tagging</option>

<option value="Person Identification" >Person Identification</option>

<option value="Question Answering" >Question Answering</option>

<option value="Semantic Role Labeling" >Semantic Role Labeling</option>

<option value="Semantic Web" >Semantic Web</option>

<option value="Sign Language Recognition/Generation" >Sign Language Recognition/Generation</option>

<option value="Speech Recognition/Understanding" >Speech Recognition/Understanding</option>

<option value="Speech Synthesis" >Speech Synthesis</option>

<option value="Summarisation" >Summarisation</option>

<option value="Text Mining" >Text Mining</option>

<option value="Textual Entailment and Paraphrasing" >Textual Entailment and Paraphrasing</option>

<option value="Topic Detection and Tracking" >Topic Detection and Tracking</option>

<option value="Voice Control" >Voice Control</option>

<option value="Web Services" >Web Services</option>

<option value="Word Sense Disambiguation" >Word Sense Disambiguation</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=ruOther3>
<input type=text
       style="width: 100%;"
       name='RMap{3}{UseOther}'
       value=""
       size="50"
       id=RUO3>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RUO3").autocomplete({
      source: database_ResourceUse_other
    });
  });
</script>

</td>
</tr>

<tr>
<td align=right><span id='redavailability3'>Resource Availability:</span></td>
<td align=left>
<select name='RMap{3}{Availability}' 
        size="1" 
        id=RA3 
        onchange="handleOtherField('RA3','raOther3','RAO3')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Freely Available" >Freely Available</option>

<option value="From Data Center(s)" >From Data Center(s)</option>

<option value="From Owner" >From Owner</option>

<option value="Not Available" >Not Available</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=raOther3>
<input type=text
       style="width: 100%;"
       name='RMap{3}{AvailabilityOther}'
       value=""
       size="50"
       id=RAO3>
</td>
</tr>



<tr>
<td align="right" valign=center><span id='redlicense3'>License:</span></td>
<td align="left" valign=center>
<input name='RMap{3}{License}' value="" size="40" id=RLIC3 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Licence type, e.g., ELRA, LDC, Gnu, CreativeCommons, OpenSource, etc.</em>
</td>
</tr>




<tr>
<td align=right><span id='redurl3'>Resource URL (if available):</span></td>
<td align=left>
<input name='RMap{3}{URL}' value="" size="40" id=RURL3 style="width: 100%;" type="text">
</td>
</tr>

<tr>
<td align="right" valign=center><span id='reddocumentation3'>Documentation:</span></td>
<td align="left" valign=center>
<input name='RMap{3}{Documentation}' value="" size="40" id=RDOC3 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Is there documentation? In which language? Is the documentation publicly available?</em>
</td>
</tr>




<tr>
<td  align=right valign=top><span id='reddescription3'>Resource Description:</span> </td>
<td colspan="2" align="left">
<textarea name='RMap{3}{Description}' rows="10" cols="70" id=RD3 style="width: 100%;"></textarea>
</td>
</tr>
<!--
<tr>
  <td colspan="3">
The section below applies only if the resource is not available 
at/from established repositories, such as ELRA, LDC, or other data 
centers, as described by the URL entered in the above form. 
ELRA encourages all authors to share the described LRs (data, toosl, services,
etc.), unless already available, to enable their reuse, replicability of experiments,
including evaluation ones, etc.

  </td>
</tr>

<tr>
  <td  align=right valign=top>Do you want to make this LR available through the
<b>"Share Your LRs" initiative?</b></td>
  <td  align="left">
    <input type="checkbox"  id="RLaccepted_3" name="RMap{3}{accepted}" onclick="Resource_manage_upload('RLaccepted_3')" >
  </td>
  <td style="width: 40%" valign=top>
    <em><b>Disclaimer:</b> By clicking the box you agree to share this LR for which rights have been cleared and guarantee that such LR is sharable under the licence you mentioned above.</em>
  </td>
</tr>


<tr>
  <td align=right valign=top><span id="filed_RLaccepted_3">You can upload your LR here if it is smaller than <b>20Mb</b>.</span></td>
  <td align="left">
    <span id="filet_RLaccepted_3">
      <input type="file" name="RMap{3}{FILE}" /> &nbsp;&nbsp;&nbsp;
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>


<tr>
  <td align=right><span id="urld_RLaccepted_3">Otherwise please provide an URL from where the LR can be downloaded:</span></td>
  <td align=left>
    <span id="urlt_RLaccepted_3">
      <input name='RMap{3}{FILEURL}' value="" size="40" id=RURL3 style="width: 100%;" type="text">
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>

-->


</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</div><div id=r4>

<table width="95%">
<tr>
<td>#4</td>

<td style="width: 100%">
<table style="width: 100%" border="1">
<tr>
<td style="width: 100%">
<table cellpadding=5 style="width: 100%">

<tr>
<td align="right">Resource Name:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{4}{Name}' 
               type="text"
               value="" 
               size="40" 
               id=RN4> 


	<script type="text/javascript">
	  jQuery(function(){
	    jQuery("#RN4").keyup(function(){setDefaultValid(4)});
            jQuery("#RN4").autocomplete({
              source: database_ResourceName,
              select: function(event, ui) { f = selectResourceNameUI("#RN4", event, ui); setDefaultValid(4); return f;}});
	  });
	</script>
      </td>
      <!-- <td>
	<input type=button onClick="setDefaultResourceData(4)" value='Load Default Values' id="setDefaultButton_3" disabled>
      </td>
      -->
    </tr>
  </table>


</td>
<!--
<td>
  <i>If the button "Load default values" is enabled you can prefill some fields with default values available in the LreMap for this resource.</i>
</td> -->
</tr>

<tr>
<td align="right">ISLRN Field:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{4}{ISLRN}' 
               type="text"
               value="" 
               size="20" 
               id=RISLRN4
	       title="Format: xxx-xxx-xxx-xxx-x"> 
      </td>
      <td>
	&nbsp;&nbsp;&nbsp;
	<input type=checkbox onClick="disableResourceILSRN(4)" value='1'
	       id="disableResourceISLRN4" name='RMap{4}{ISLRNstatus}'
	       > No ISLRN / I don't know
	<script type="text/javascript">
	  jQuery(function(){
	    disableResourceILSRN(4);
	  });
	</script>
      </td>
    </tr>
  </table>
</td>
<td>
  <i>The ISLRN is a unique and universal identification schema for LRs
    which provides LRs with unique identifier using a standardised
    nomenclature.  It also ensures that LRs are correctly identified,
    and consequently, recognised with proper references for their
    usage in applications in R&D projects, products evaluation and
    benchmark as well as in documents and scientific
    papers. Visit <a href="http://www.islrn.org"
    target=_blank>http://www.islrn.org</a>.</i>
</td>
</tr>



<tr>
<td style="width: 20%" align=right><span id="redtype4">Resource Type: </span></td>



<td style="width: 40%" align=left>
<select name='RMap{4}{Type}' 
        size=1 
        id=RT4 
        onchange="handleOtherField('RT4','resOther4','RNO4')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>


<optgroup label="Resource-Dataset">
<option value="Corpus" >Corpus</option>

<option value="Lexicon" >Lexicon</option>

<option value="Grammar/Language Model" >Grammar/Language Model</option>

<option value="Ontology" >Ontology</option>

<option value="Terminology" >Terminology</option>

<option value="Treebank" >Treebank</option>

</optgroup>
<optgroup label="Resource-Tool">
<option value="Corpus Tool" >Corpus Tool</option>

<option value="Aligner" >Aligner</option>

<option value="Annotation Tool" >Annotation Tool</option>

<option value="Language Identifier" >Language Identifier</option>

<option value="Language Modeling Tool" >Language Modeling Tool</option>

<option value="Machine Learning Tool" >Machine Learning Tool</option>

<option value="Machine Translation Tool" >Machine Translation Tool</option>

<option value="Named Entity Recognizer" >Named Entity Recognizer</option>

<option value="Sentiment Analysis Tool" >Sentiment Analysis Tool</option>

<option value="Software Toolkit" >Software Toolkit</option>

<option value="Tagger/Parser" >Tagger/Parser</option>

<option value="Tokenizer" >Tokenizer</option>

<option value="Web Service" >Web Service</option>

<option value="Word Sense Disambiguator" >Word Sense Disambiguator</option>

<option value="Prosodic Analyzer" >Prosodic Analyzer</option>

<option value="Signal Processing/Features Extraction" >Signal Processing/Features Extraction</option>

<option value="Speaker Recognizer" >Speaker Recognizer</option>

<option value="Speech Recognizer/Transcriber" >Speech Recognizer/Transcriber</option>

<option value="Spoken Dialogue Tool" >Spoken Dialogue Tool</option>

<option value="Text-to-Speech Synthesizer" >Text-to-Speech Synthesizer</option>

<option value="Image Analyzer" >Image Analyzer</option>

</optgroup>
<optgroup label="Resource-Guidelines">
<option value="Metadata" >Metadata</option>

<option value="Representation-Annotation Formalism/Guidelines" >Representation-Annotation Formalism/Guidelines</option>

<option value="Representation-Annotation Standards/Best Practices" >Representation-Annotation Standards/Best Practices</option>

<option value="Language Resources/Technologies Infrastructure" >Language Resources/Technologies Infrastructure</option>

</optgroup>
<optgroup label="Evaluation">
<option value="Evaluation Data" >Evaluation Data</option>

<option value="Evaluation Tool" >Evaluation Tool</option>

<option value="Evaluation Package" >Evaluation Package</option>

<option value="Evaluation Methodology/Standards/Guidelines" >Evaluation Methodology/Standards/Guidelines</option>

</optgroup>

<optgroup label="Other">
<option value="Other" >Other - please specify &rarr;</option>

</optgroup>

</select>
</td>


<td style="width: 40%" id=resOther4>
<input type=text
       style="width: 100%;"
       name='RMap{4}{TypeOther}'
       value=""
       size="50"
       id=RNO4>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RNO4").autocomplete({
      source: database_ResourceType_other
    });
  });
</script>

</td>
</tr>




<tr>
<td align="right" valign=center><span id='redsize4'>Size:</span></td>
<td align="left" valign=center>
  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{4}{Size}' value="" size="40" id=RSIZE4 type="text">
      </td>
      <td>
	<select name='RMap{4}{Unit}' 
		size="1" 
		id=RSIZEU4 
		style="width: 100%;">
	  <option value="" selected>-- Please select a value --</option>

	  <option value="KByte  " >KByte  </option>

<option value="MByte  " >MByte  </option>

<option value="GByte  " >GByte  </option>

<option value="sentences  " >sentences  </option>

<option value="words  " >words  </option>

<option value="tokens  " >tokens  </option>

<option value="entries  " >entries  </option>

<option value="lexemes  " >lexemes  </option>

<option value="synsets  " >synsets  </option>

<option value="concepts " >concepts </option>

<option value="rules   " >rules   </option>

<option value="hours  " >hours  </option>

<option value="minutes  " >minutes  </option>

	  <option value="Other" >Other - please specify next to the size value</option>

	</select>
      </td>
    </tr>
  </table>
</td>


<td style="width: 40%" valign=top>
<em>Enter package size - a number and a relevant unit of measurement, e.g., 48Mbyte, 
5500&nbsp;lexemes, 2.1Gbyte.</em>
</td>
</tr>



<tr>

<td align="right"><span id='redstatus4'>Resource Production Status:</span></td>
<td align="left">
<select name='RMap{4}{Status}' 
        size="1" 
        onchange="handleOtherField('RS4','rsOther4','RSO4')"
        id=RS4 
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Newly created-finished" >Newly created-finished</option>

<option value="Newly created-in progress" >Newly created-in progress</option>

<option value="Existing-used" >Existing-used</option>

<option value="Existing-updated" >Existing-updated</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>


<td style="width: 40%" id=rsOther4>
<input type=text
       style="width: 100%;"
       name='RMap{4}{StatusOther}'
       value=""
       size="50"
       id=RSO4>
</td>
</tr>

<tr>
<td align=right><span id='redlang4'>Language(s):</span></td>
<td align="left">
<select name='RMap{4}{Language}' 
        size="1" 
        id=RL4 
        onchange="handleOtherLanguageField('RL4','lang4','LAO4')"
        style="width: 100%;">
<option value="P" >-- Please select a value --</option>
<option value="W" >Enter the language(s) in the box(es) below &darr;</option>
<option value="LI" >Language Independent</option>
<option value="NA" >Not Applicable</option>

</select>
</td>
<td>
</td>
</tr>

<tr>
<td></td>
<td colspan=2>
<span id=lang4>
<table border=0>
<tr>
<td>
<blockquote>
<table border=0>
  <tr>
    <td>
      Language&nbsp;1:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{4}{LanguageOther}'
	     value=""
	     size="50"
	     id=LAO4>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO4").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;2:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{4}{LanguageOther2}'
	     value=""
	     size="50"
	     id=LAO24>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO24").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;3:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{4}{LanguageOther3}'
	     value=""
	     size="50"
	     id=LAO34>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO34").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;4:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{4}{LanguageOther4}'
	     value=""
	     size="50"
	     id=LAO44>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO44").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>


  <tr>
    <td>
      Language&nbsp;5:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{4}{LanguageOther5}'
	     value=""
	     size="50"
	     id=LAO54>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO54").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Other&nbsp;Languages:
    </td>
    <td>
      <textarea style="width: 100%;"
	     name='RMap{4}{LanguageOtherN}'
	     cols="50" rows="3"
	     id=LAON4
		></textarea>
    </td>
  </tr>
</table>
</blockquote>
</td>
<td>
  <i>
    Please enter the languages that apply to your resource.<br>
    Please note that the first 3 boxes have autocompletion.<br>
    If your resource covers more than 3 languages, please use the "Other 
    languages" text area
    <b>and enter the various languages separated by a comma</b>. 
  </i>
</td>
</table>
</span>
</td>
<td width="25%">
</td>
</tr>


<tr>
<td align=right><span id='redmodality4'>Modality:</span></td>
<td align=left>
<select name='RMap{4}{Modality}' 
        size="1" 
        id=RM4 
        onchange="handleOtherField('RM4','rmOther4','RMO4')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Speech" >Speech</option>

<option value="Written" >Written</option>

<option value="Speech/Written" >Speech/Written</option>

<option value="Multimodal/Multimedia" >Multimodal/Multimedia</option>

<option value="Sign Language" >Sign Language</option>

<option value="Modality Independent" >Modality Independent</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>



<td style="width: 40%" id=rmOther4>
<input type=text
       style="width: 100%;"
       name='RMap{4}{ModalityOther}'
       value=""
       size="50"
       id=RMO4>
</td>
</tr>

<tr>
<td align=right><span id='reduse4'>Use of the Resource:</span></td>
<td align=left>
<select name='RMap{4}{Use}' 
        size="1" 
        id=RU4 
        onchange="handleOtherField('RU4','ruOther4','RUO4')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Acquisition" >Acquisition</option>

<option value="Anaphora, Coreference" >Anaphora, Coreference</option>

<option value="Corpus Creation/Annotation" >Corpus Creation/Annotation</option>

<option value="Dialogue" >Dialogue</option>

<option value="Discourse" >Discourse</option>

<option value="Document Classification, Text categorisation" >Document Classification, Text categorisation</option>

<option value="Emotion Recognition/Generation" >Emotion Recognition/Generation</option>

<option value="Evaluation/Validation" >Evaluation/Validation</option>

<option value="Information Extraction, Information Retrieval" >Information Extraction, Information Retrieval</option>

<option value="Knowledge Discovery/Representation" >Knowledge Discovery/Representation</option>

<option value="Language Identification" >Language Identification</option>

<option value="Language Modelling" >Language Modelling</option>

<option value="Lexicon Creation/Annotation" >Lexicon Creation/Annotation</option>

<option value="Machine Learning" >Machine Learning</option>

<option value="Machine Translation, SpeechToSpeech Translation" >Machine Translation, SpeechToSpeech Translation</option>

<option value="Morphological Analysis" >Morphological Analysis</option>

<option value="Multimedia Document Processing" >Multimedia Document Processing</option>

<option value="Named Entity Recognition" >Named Entity Recognition</option>

<option value="Natural Language Generation" >Natural Language Generation</option>

<option value="Opinion Mining/Sentiment Analysis" >Opinion Mining/Sentiment Analysis</option>

<option value="Parsing and Tagging" >Parsing and Tagging</option>

<option value="Person Identification" >Person Identification</option>

<option value="Question Answering" >Question Answering</option>

<option value="Semantic Role Labeling" >Semantic Role Labeling</option>

<option value="Semantic Web" >Semantic Web</option>

<option value="Sign Language Recognition/Generation" >Sign Language Recognition/Generation</option>

<option value="Speech Recognition/Understanding" >Speech Recognition/Understanding</option>

<option value="Speech Synthesis" >Speech Synthesis</option>

<option value="Summarisation" >Summarisation</option>

<option value="Text Mining" >Text Mining</option>

<option value="Textual Entailment and Paraphrasing" >Textual Entailment and Paraphrasing</option>

<option value="Topic Detection and Tracking" >Topic Detection and Tracking</option>

<option value="Voice Control" >Voice Control</option>

<option value="Web Services" >Web Services</option>

<option value="Word Sense Disambiguation" >Word Sense Disambiguation</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=ruOther4>
<input type=text
       style="width: 100%;"
       name='RMap{4}{UseOther}'
       value=""
       size="50"
       id=RUO4>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RUO4").autocomplete({
      source: database_ResourceUse_other
    });
  });
</script>

</td>
</tr>

<tr>
<td align=right><span id='redavailability4'>Resource Availability:</span></td>
<td align=left>
<select name='RMap{4}{Availability}' 
        size="1" 
        id=RA4 
        onchange="handleOtherField('RA4','raOther4','RAO4')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Freely Available" >Freely Available</option>

<option value="From Data Center(s)" >From Data Center(s)</option>

<option value="From Owner" >From Owner</option>

<option value="Not Available" >Not Available</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=raOther4>
<input type=text
       style="width: 100%;"
       name='RMap{4}{AvailabilityOther}'
       value=""
       size="50"
       id=RAO4>
</td>
</tr>



<tr>
<td align="right" valign=center><span id='redlicense4'>License:</span></td>
<td align="left" valign=center>
<input name='RMap{4}{License}' value="" size="40" id=RLIC4 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Licence type, e.g., ELRA, LDC, Gnu, CreativeCommons, OpenSource, etc.</em>
</td>
</tr>




<tr>
<td align=right><span id='redurl4'>Resource URL (if available):</span></td>
<td align=left>
<input name='RMap{4}{URL}' value="" size="40" id=RURL4 style="width: 100%;" type="text">
</td>
</tr>

<tr>
<td align="right" valign=center><span id='reddocumentation4'>Documentation:</span></td>
<td align="left" valign=center>
<input name='RMap{4}{Documentation}' value="" size="40" id=RDOC4 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Is there documentation? In which language? Is the documentation publicly available?</em>
</td>
</tr>




<tr>
<td  align=right valign=top><span id='reddescription4'>Resource Description:</span> </td>
<td colspan="2" align="left">
<textarea name='RMap{4}{Description}' rows="10" cols="70" id=RD4 style="width: 100%;"></textarea>
</td>
</tr>
<!--
<tr>
  <td colspan="3">
The section below applies only if the resource is not available 
at/from established repositories, such as ELRA, LDC, or other data 
centers, as described by the URL entered in the above form. 
ELRA encourages all authors to share the described LRs (data, toosl, services,
etc.), unless already available, to enable their reuse, replicability of experiments,
including evaluation ones, etc.

  </td>
</tr>

<tr>
  <td  align=right valign=top>Do you want to make this LR available through the
<b>"Share Your LRs" initiative?</b></td>
  <td  align="left">
    <input type="checkbox"  id="RLaccepted_4" name="RMap{4}{accepted}" onclick="Resource_manage_upload('RLaccepted_4')" >
  </td>
  <td style="width: 40%" valign=top>
    <em><b>Disclaimer:</b> By clicking the box you agree to share this LR for which rights have been cleared and guarantee that such LR is sharable under the licence you mentioned above.</em>
  </td>
</tr>


<tr>
  <td align=right valign=top><span id="filed_RLaccepted_4">You can upload your LR here if it is smaller than <b>20Mb</b>.</span></td>
  <td align="left">
    <span id="filet_RLaccepted_4">
      <input type="file" name="RMap{4}{FILE}" /> &nbsp;&nbsp;&nbsp;
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>


<tr>
  <td align=right><span id="urld_RLaccepted_4">Otherwise please provide an URL from where the LR can be downloaded:</span></td>
  <td align=left>
    <span id="urlt_RLaccepted_4">
      <input name='RMap{4}{FILEURL}' value="" size="40" id=RURL4 style="width: 100%;" type="text">
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>

-->


</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</div><div id=r5>

<table width="95%">
<tr>
<td>#5</td>

<td style="width: 100%">
<table style="width: 100%" border="1">
<tr>
<td style="width: 100%">
<table cellpadding=5 style="width: 100%">

<tr>
<td align="right">Resource Name:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{5}{Name}' 
               type="text"
               value="" 
               size="40" 
               id=RN5> 


	<script type="text/javascript">
	  jQuery(function(){
	    jQuery("#RN5").keyup(function(){setDefaultValid(5)});
            jQuery("#RN5").autocomplete({
              source: database_ResourceName,
              select: function(event, ui) { f = selectResourceNameUI("#RN5", event, ui); setDefaultValid(5); return f;}});
	  });
	</script>
      </td>
      <!-- <td>
	<input type=button onClick="setDefaultResourceData(5)" value='Load Default Values' id="setDefaultButton_3" disabled>
      </td>
      -->
    </tr>
  </table>


</td>
<!--
<td>
  <i>If the button "Load default values" is enabled you can prefill some fields with default values available in the LreMap for this resource.</i>
</td> -->
</tr>

<tr>
<td align="right">ISLRN Field:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{5}{ISLRN}' 
               type="text"
               value="" 
               size="20" 
               id=RISLRN5
	       title="Format: xxx-xxx-xxx-xxx-x"> 
      </td>
      <td>
	&nbsp;&nbsp;&nbsp;
	<input type=checkbox onClick="disableResourceILSRN(5)" value='1'
	       id="disableResourceISLRN5" name='RMap{5}{ISLRNstatus}'
	       > No ISLRN / I don't know
	<script type="text/javascript">
	  jQuery(function(){
	    disableResourceILSRN(5);
	  });
	</script>
      </td>
    </tr>
  </table>
</td>
<td>
  <i>The ISLRN is a unique and universal identification schema for LRs
    which provides LRs with unique identifier using a standardised
    nomenclature.  It also ensures that LRs are correctly identified,
    and consequently, recognised with proper references for their
    usage in applications in R&D projects, products evaluation and
    benchmark as well as in documents and scientific
    papers. Visit <a href="http://www.islrn.org"
    target=_blank>http://www.islrn.org</a>.</i>
</td>
</tr>



<tr>
<td style="width: 20%" align=right><span id="redtype5">Resource Type: </span></td>



<td style="width: 40%" align=left>
<select name='RMap{5}{Type}' 
        size=1 
        id=RT5 
        onchange="handleOtherField('RT5','resOther5','RNO5')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>


<optgroup label="Resource-Dataset">
<option value="Corpus" >Corpus</option>

<option value="Lexicon" >Lexicon</option>

<option value="Grammar/Language Model" >Grammar/Language Model</option>

<option value="Ontology" >Ontology</option>

<option value="Terminology" >Terminology</option>

<option value="Treebank" >Treebank</option>

</optgroup>
<optgroup label="Resource-Tool">
<option value="Corpus Tool" >Corpus Tool</option>

<option value="Aligner" >Aligner</option>

<option value="Annotation Tool" >Annotation Tool</option>

<option value="Language Identifier" >Language Identifier</option>

<option value="Language Modeling Tool" >Language Modeling Tool</option>

<option value="Machine Learning Tool" >Machine Learning Tool</option>

<option value="Machine Translation Tool" >Machine Translation Tool</option>

<option value="Named Entity Recognizer" >Named Entity Recognizer</option>

<option value="Sentiment Analysis Tool" >Sentiment Analysis Tool</option>

<option value="Software Toolkit" >Software Toolkit</option>

<option value="Tagger/Parser" >Tagger/Parser</option>

<option value="Tokenizer" >Tokenizer</option>

<option value="Web Service" >Web Service</option>

<option value="Word Sense Disambiguator" >Word Sense Disambiguator</option>

<option value="Prosodic Analyzer" >Prosodic Analyzer</option>

<option value="Signal Processing/Features Extraction" >Signal Processing/Features Extraction</option>

<option value="Speaker Recognizer" >Speaker Recognizer</option>

<option value="Speech Recognizer/Transcriber" >Speech Recognizer/Transcriber</option>

<option value="Spoken Dialogue Tool" >Spoken Dialogue Tool</option>

<option value="Text-to-Speech Synthesizer" >Text-to-Speech Synthesizer</option>

<option value="Image Analyzer" >Image Analyzer</option>

</optgroup>
<optgroup label="Resource-Guidelines">
<option value="Metadata" >Metadata</option>

<option value="Representation-Annotation Formalism/Guidelines" >Representation-Annotation Formalism/Guidelines</option>

<option value="Representation-Annotation Standards/Best Practices" >Representation-Annotation Standards/Best Practices</option>

<option value="Language Resources/Technologies Infrastructure" >Language Resources/Technologies Infrastructure</option>

</optgroup>
<optgroup label="Evaluation">
<option value="Evaluation Data" >Evaluation Data</option>

<option value="Evaluation Tool" >Evaluation Tool</option>

<option value="Evaluation Package" >Evaluation Package</option>

<option value="Evaluation Methodology/Standards/Guidelines" >Evaluation Methodology/Standards/Guidelines</option>

</optgroup>

<optgroup label="Other">
<option value="Other" >Other - please specify &rarr;</option>

</optgroup>

</select>
</td>


<td style="width: 40%" id=resOther5>
<input type=text
       style="width: 100%;"
       name='RMap{5}{TypeOther}'
       value=""
       size="50"
       id=RNO5>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RNO5").autocomplete({
      source: database_ResourceType_other
    });
  });
</script>

</td>
</tr>




<tr>
<td align="right" valign=center><span id='redsize5'>Size:</span></td>
<td align="left" valign=center>
  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{5}{Size}' value="" size="40" id=RSIZE5 type="text">
      </td>
      <td>
	<select name='RMap{5}{Unit}' 
		size="1" 
		id=RSIZEU5 
		style="width: 100%;">
	  <option value="" selected>-- Please select a value --</option>

	  <option value="KByte  " >KByte  </option>

<option value="MByte  " >MByte  </option>

<option value="GByte  " >GByte  </option>

<option value="sentences  " >sentences  </option>

<option value="words  " >words  </option>

<option value="tokens  " >tokens  </option>

<option value="entries  " >entries  </option>

<option value="lexemes  " >lexemes  </option>

<option value="synsets  " >synsets  </option>

<option value="concepts " >concepts </option>

<option value="rules   " >rules   </option>

<option value="hours  " >hours  </option>

<option value="minutes  " >minutes  </option>

	  <option value="Other" >Other - please specify next to the size value</option>

	</select>
      </td>
    </tr>
  </table>
</td>


<td style="width: 40%" valign=top>
<em>Enter package size - a number and a relevant unit of measurement, e.g., 48Mbyte, 
5500&nbsp;lexemes, 2.1Gbyte.</em>
</td>
</tr>



<tr>

<td align="right"><span id='redstatus5'>Resource Production Status:</span></td>
<td align="left">
<select name='RMap{5}{Status}' 
        size="1" 
        onchange="handleOtherField('RS5','rsOther5','RSO5')"
        id=RS5 
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Newly created-finished" >Newly created-finished</option>

<option value="Newly created-in progress" >Newly created-in progress</option>

<option value="Existing-used" >Existing-used</option>

<option value="Existing-updated" >Existing-updated</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>


<td style="width: 40%" id=rsOther5>
<input type=text
       style="width: 100%;"
       name='RMap{5}{StatusOther}'
       value=""
       size="50"
       id=RSO5>
</td>
</tr>

<tr>
<td align=right><span id='redlang5'>Language(s):</span></td>
<td align="left">
<select name='RMap{5}{Language}' 
        size="1" 
        id=RL5 
        onchange="handleOtherLanguageField('RL5','lang5','LAO5')"
        style="width: 100%;">
<option value="P" >-- Please select a value --</option>
<option value="W" >Enter the language(s) in the box(es) below &darr;</option>
<option value="LI" >Language Independent</option>
<option value="NA" >Not Applicable</option>

</select>
</td>
<td>
</td>
</tr>

<tr>
<td></td>
<td colspan=2>
<span id=lang5>
<table border=0>
<tr>
<td>
<blockquote>
<table border=0>
  <tr>
    <td>
      Language&nbsp;1:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{5}{LanguageOther}'
	     value=""
	     size="50"
	     id=LAO5>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO5").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;2:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{5}{LanguageOther2}'
	     value=""
	     size="50"
	     id=LAO25>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO25").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;3:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{5}{LanguageOther3}'
	     value=""
	     size="50"
	     id=LAO35>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO35").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;4:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{5}{LanguageOther4}'
	     value=""
	     size="50"
	     id=LAO45>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO45").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>


  <tr>
    <td>
      Language&nbsp;5:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{5}{LanguageOther5}'
	     value=""
	     size="50"
	     id=LAO55>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO55").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Other&nbsp;Languages:
    </td>
    <td>
      <textarea style="width: 100%;"
	     name='RMap{5}{LanguageOtherN}'
	     cols="50" rows="3"
	     id=LAON5
		></textarea>
    </td>
  </tr>
</table>
</blockquote>
</td>
<td>
  <i>
    Please enter the languages that apply to your resource.<br>
    Please note that the first 3 boxes have autocompletion.<br>
    If your resource covers more than 3 languages, please use the "Other 
    languages" text area
    <b>and enter the various languages separated by a comma</b>. 
  </i>
</td>
</table>
</span>
</td>
<td width="25%">
</td>
</tr>


<tr>
<td align=right><span id='redmodality5'>Modality:</span></td>
<td align=left>
<select name='RMap{5}{Modality}' 
        size="1" 
        id=RM5 
        onchange="handleOtherField('RM5','rmOther5','RMO5')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Speech" >Speech</option>

<option value="Written" >Written</option>

<option value="Speech/Written" >Speech/Written</option>

<option value="Multimodal/Multimedia" >Multimodal/Multimedia</option>

<option value="Sign Language" >Sign Language</option>

<option value="Modality Independent" >Modality Independent</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>



<td style="width: 40%" id=rmOther5>
<input type=text
       style="width: 100%;"
       name='RMap{5}{ModalityOther}'
       value=""
       size="50"
       id=RMO5>
</td>
</tr>

<tr>
<td align=right><span id='reduse5'>Use of the Resource:</span></td>
<td align=left>
<select name='RMap{5}{Use}' 
        size="1" 
        id=RU5 
        onchange="handleOtherField('RU5','ruOther5','RUO5')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Acquisition" >Acquisition</option>

<option value="Anaphora, Coreference" >Anaphora, Coreference</option>

<option value="Corpus Creation/Annotation" >Corpus Creation/Annotation</option>

<option value="Dialogue" >Dialogue</option>

<option value="Discourse" >Discourse</option>

<option value="Document Classification, Text categorisation" >Document Classification, Text categorisation</option>

<option value="Emotion Recognition/Generation" >Emotion Recognition/Generation</option>

<option value="Evaluation/Validation" >Evaluation/Validation</option>

<option value="Information Extraction, Information Retrieval" >Information Extraction, Information Retrieval</option>

<option value="Knowledge Discovery/Representation" >Knowledge Discovery/Representation</option>

<option value="Language Identification" >Language Identification</option>

<option value="Language Modelling" >Language Modelling</option>

<option value="Lexicon Creation/Annotation" >Lexicon Creation/Annotation</option>

<option value="Machine Learning" >Machine Learning</option>

<option value="Machine Translation, SpeechToSpeech Translation" >Machine Translation, SpeechToSpeech Translation</option>

<option value="Morphological Analysis" >Morphological Analysis</option>

<option value="Multimedia Document Processing" >Multimedia Document Processing</option>

<option value="Named Entity Recognition" >Named Entity Recognition</option>

<option value="Natural Language Generation" >Natural Language Generation</option>

<option value="Opinion Mining/Sentiment Analysis" >Opinion Mining/Sentiment Analysis</option>

<option value="Parsing and Tagging" >Parsing and Tagging</option>

<option value="Person Identification" >Person Identification</option>

<option value="Question Answering" >Question Answering</option>

<option value="Semantic Role Labeling" >Semantic Role Labeling</option>

<option value="Semantic Web" >Semantic Web</option>

<option value="Sign Language Recognition/Generation" >Sign Language Recognition/Generation</option>

<option value="Speech Recognition/Understanding" >Speech Recognition/Understanding</option>

<option value="Speech Synthesis" >Speech Synthesis</option>

<option value="Summarisation" >Summarisation</option>

<option value="Text Mining" >Text Mining</option>

<option value="Textual Entailment and Paraphrasing" >Textual Entailment and Paraphrasing</option>

<option value="Topic Detection and Tracking" >Topic Detection and Tracking</option>

<option value="Voice Control" >Voice Control</option>

<option value="Web Services" >Web Services</option>

<option value="Word Sense Disambiguation" >Word Sense Disambiguation</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=ruOther5>
<input type=text
       style="width: 100%;"
       name='RMap{5}{UseOther}'
       value=""
       size="50"
       id=RUO5>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RUO5").autocomplete({
      source: database_ResourceUse_other
    });
  });
</script>

</td>
</tr>

<tr>
<td align=right><span id='redavailability5'>Resource Availability:</span></td>
<td align=left>
<select name='RMap{5}{Availability}' 
        size="1" 
        id=RA5 
        onchange="handleOtherField('RA5','raOther5','RAO5')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Freely Available" >Freely Available</option>

<option value="From Data Center(s)" >From Data Center(s)</option>

<option value="From Owner" >From Owner</option>

<option value="Not Available" >Not Available</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=raOther5>
<input type=text
       style="width: 100%;"
       name='RMap{5}{AvailabilityOther}'
       value=""
       size="50"
       id=RAO5>
</td>
</tr>



<tr>
<td align="right" valign=center><span id='redlicense5'>License:</span></td>
<td align="left" valign=center>
<input name='RMap{5}{License}' value="" size="40" id=RLIC5 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Licence type, e.g., ELRA, LDC, Gnu, CreativeCommons, OpenSource, etc.</em>
</td>
</tr>




<tr>
<td align=right><span id='redurl5'>Resource URL (if available):</span></td>
<td align=left>
<input name='RMap{5}{URL}' value="" size="40" id=RURL5 style="width: 100%;" type="text">
</td>
</tr>

<tr>
<td align="right" valign=center><span id='reddocumentation5'>Documentation:</span></td>
<td align="left" valign=center>
<input name='RMap{5}{Documentation}' value="" size="40" id=RDOC5 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Is there documentation? In which language? Is the documentation publicly available?</em>
</td>
</tr>




<tr>
<td  align=right valign=top><span id='reddescription5'>Resource Description:</span> </td>
<td colspan="2" align="left">
<textarea name='RMap{5}{Description}' rows="10" cols="70" id=RD5 style="width: 100%;"></textarea>
</td>
</tr>
<!--
<tr>
  <td colspan="3">
The section below applies only if the resource is not available 
at/from established repositories, such as ELRA, LDC, or other data 
centers, as described by the URL entered in the above form. 
ELRA encourages all authors to share the described LRs (data, toosl, services,
etc.), unless already available, to enable their reuse, replicability of experiments,
including evaluation ones, etc.

  </td>
</tr>

<tr>
  <td  align=right valign=top>Do you want to make this LR available through the
<b>"Share Your LRs" initiative?</b></td>
  <td  align="left">
    <input type="checkbox"  id="RLaccepted_5" name="RMap{5}{accepted}" onclick="Resource_manage_upload('RLaccepted_5')" >
  </td>
  <td style="width: 40%" valign=top>
    <em><b>Disclaimer:</b> By clicking the box you agree to share this LR for which rights have been cleared and guarantee that such LR is sharable under the licence you mentioned above.</em>
  </td>
</tr>


<tr>
  <td align=right valign=top><span id="filed_RLaccepted_5">You can upload your LR here if it is smaller than <b>20Mb</b>.</span></td>
  <td align="left">
    <span id="filet_RLaccepted_5">
      <input type="file" name="RMap{5}{FILE}" /> &nbsp;&nbsp;&nbsp;
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>


<tr>
  <td align=right><span id="urld_RLaccepted_5">Otherwise please provide an URL from where the LR can be downloaded:</span></td>
  <td align=left>
    <span id="urlt_RLaccepted_5">
      <input name='RMap{5}{FILEURL}' value="" size="40" id=RURL5 style="width: 100%;" type="text">
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>

-->


</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</div><div id=r6>

<table width="95%">
<tr>
<td>#6</td>

<td style="width: 100%">
<table style="width: 100%" border="1">
<tr>
<td style="width: 100%">
<table cellpadding=5 style="width: 100%">

<tr>
<td align="right">Resource Name:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{6}{Name}' 
               type="text"
               value="" 
               size="40" 
               id=RN6> 


	<script type="text/javascript">
	  jQuery(function(){
	    jQuery("#RN6").keyup(function(){setDefaultValid(6)});
            jQuery("#RN6").autocomplete({
              source: database_ResourceName,
              select: function(event, ui) { f = selectResourceNameUI("#RN6", event, ui); setDefaultValid(6); return f;}});
	  });
	</script>
      </td>
     <!-- <td>
	<input type=button onClick="setDefaultResourceData(6)" value='Load Default Values' id="setDefaultButton_3" disabled>
      </td>
      -->
    </tr>
  </table>


</td>
<!--
<td>
  <i>If the button "Load default values" is enabled you can prefill some fields with default values available in the LreMap for this resource.</i>
</td> -->
</tr>

<tr>
<td align="right">ISLRN Field:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{6}{ISLRN}' 
               type="text"
               value="" 
               size="20" 
               id=RISLRN6
	       title="Format: xxx-xxx-xxx-xxx-x"> 
      </td>
      <td>
	&nbsp;&nbsp;&nbsp;
	<input type=checkbox onClick="disableResourceILSRN(6)" value='1'
	       id="disableResourceISLRN6" name='RMap{6}{ISLRNstatus}'
	       > No ISLRN / I don't know
	<script type="text/javascript">
	  jQuery(function(){
	    disableResourceILSRN(6);
	  });
	</script>
      </td>
    </tr>
  </table>
</td>
<td>
  <i>The ISLRN is a unique and universal identification schema for LRs
    which provides LRs with unique identifier using a standardised
    nomenclature.  It also ensures that LRs are correctly identified,
    and consequently, recognised with proper references for their
    usage in applications in R&D projects, products evaluation and
    benchmark as well as in documents and scientific
    papers. Visit <a href="http://www.islrn.org"
    target=_blank>http://www.islrn.org</a>.</i>
</td>
</tr>



<tr>
<td style="width: 20%" align=right><span id="redtype6">Resource Type: </span></td>



<td style="width: 40%" align=left>
<select name='RMap{6}{Type}' 
        size=1 
        id=RT6 
        onchange="handleOtherField('RT6','resOther6','RNO6')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>


<optgroup label="Resource-Dataset">
<option value="Corpus" >Corpus</option>

<option value="Lexicon" >Lexicon</option>

<option value="Grammar/Language Model" >Grammar/Language Model</option>

<option value="Ontology" >Ontology</option>

<option value="Terminology" >Terminology</option>

<option value="Treebank" >Treebank</option>

</optgroup>
<optgroup label="Resource-Tool">
<option value="Corpus Tool" >Corpus Tool</option>

<option value="Aligner" >Aligner</option>

<option value="Annotation Tool" >Annotation Tool</option>

<option value="Language Identifier" >Language Identifier</option>

<option value="Language Modeling Tool" >Language Modeling Tool</option>

<option value="Machine Learning Tool" >Machine Learning Tool</option>

<option value="Machine Translation Tool" >Machine Translation Tool</option>

<option value="Named Entity Recognizer" >Named Entity Recognizer</option>

<option value="Sentiment Analysis Tool" >Sentiment Analysis Tool</option>

<option value="Software Toolkit" >Software Toolkit</option>

<option value="Tagger/Parser" >Tagger/Parser</option>

<option value="Tokenizer" >Tokenizer</option>

<option value="Web Service" >Web Service</option>

<option value="Word Sense Disambiguator" >Word Sense Disambiguator</option>

<option value="Prosodic Analyzer" >Prosodic Analyzer</option>

<option value="Signal Processing/Features Extraction" >Signal Processing/Features Extraction</option>

<option value="Speaker Recognizer" >Speaker Recognizer</option>

<option value="Speech Recognizer/Transcriber" >Speech Recognizer/Transcriber</option>

<option value="Spoken Dialogue Tool" >Spoken Dialogue Tool</option>

<option value="Text-to-Speech Synthesizer" >Text-to-Speech Synthesizer</option>

<option value="Image Analyzer" >Image Analyzer</option>

</optgroup>
<optgroup label="Resource-Guidelines">
<option value="Metadata" >Metadata</option>

<option value="Representation-Annotation Formalism/Guidelines" >Representation-Annotation Formalism/Guidelines</option>

<option value="Representation-Annotation Standards/Best Practices" >Representation-Annotation Standards/Best Practices</option>

<option value="Language Resources/Technologies Infrastructure" >Language Resources/Technologies Infrastructure</option>

</optgroup>
<optgroup label="Evaluation">
<option value="Evaluation Data" >Evaluation Data</option>

<option value="Evaluation Tool" >Evaluation Tool</option>

<option value="Evaluation Package" >Evaluation Package</option>

<option value="Evaluation Methodology/Standards/Guidelines" >Evaluation Methodology/Standards/Guidelines</option>

</optgroup>

<optgroup label="Other">
<option value="Other" >Other - please specify &rarr;</option>

</optgroup>

</select>
</td>


<td style="width: 40%" id=resOther6>
<input type=text
       style="width: 100%;"
       name='RMap{6}{TypeOther}'
       value=""
       size="50"
       id=RNO6>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RNO6").autocomplete({
      source: database_ResourceType_other
    });
  });
</script>

</td>
</tr>




<tr>
<td align="right" valign=center><span id='redsize6'>Size:</span></td>
<td align="left" valign=center>
  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{6}{Size}' value="" size="40" id=RSIZE6 type="text">
      </td>
      <td>
	<select name='RMap{6}{Unit}' 
		size="1" 
		id=RSIZEU6 
		style="width: 100%;">
	  <option value="" selected>-- Please select a value --</option>

	  <option value="KByte  " >KByte  </option>

<option value="MByte  " >MByte  </option>

<option value="GByte  " >GByte  </option>

<option value="sentences  " >sentences  </option>

<option value="words  " >words  </option>

<option value="tokens  " >tokens  </option>

<option value="entries  " >entries  </option>

<option value="lexemes  " >lexemes  </option>

<option value="synsets  " >synsets  </option>

<option value="concepts " >concepts </option>

<option value="rules   " >rules   </option>

<option value="hours  " >hours  </option>

<option value="minutes  " >minutes  </option>

	  <option value="Other" >Other - please specify next to the size value</option>

	</select>
      </td>
    </tr>
  </table>
</td>


<td style="width: 40%" valign=top>
<em>Enter package size - a number and a relevant unit of measurement, e.g., 48Mbyte, 
5500&nbsp;lexemes, 2.1Gbyte.</em>
</td>
</tr>



<tr>

<td align="right"><span id='redstatus6'>Resource Production Status:</span></td>
<td align="left">
<select name='RMap{6}{Status}' 
        size="1" 
        onchange="handleOtherField('RS6','rsOther6','RSO6')"
        id=RS6 
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Newly created-finished" >Newly created-finished</option>

<option value="Newly created-in progress" >Newly created-in progress</option>

<option value="Existing-used" >Existing-used</option>

<option value="Existing-updated" >Existing-updated</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>


<td style="width: 40%" id=rsOther6>
<input type=text
       style="width: 100%;"
       name='RMap{6}{StatusOther}'
       value=""
       size="50"
       id=RSO6>
</td>
</tr>

<tr>
<td align=right><span id='redlang6'>Language(s):</span></td>
<td align="left">
<select name='RMap{6}{Language}' 
        size="1" 
        id=RL6 
        onchange="handleOtherLanguageField('RL6','lang6','LAO6')"
        style="width: 100%;">
<option value="P" >-- Please select a value --</option>
<option value="W" >Enter the language(s) in the box(es) below &darr;</option>
<option value="LI" >Language Independent</option>
<option value="NA" >Not Applicable</option>

</select>
</td>
<td>
</td>
</tr>

<tr>
<td></td>
<td colspan=2>
<span id=lang6>
<table border=0>
<tr>
<td>
<blockquote>
<table border=0>
  <tr>
    <td>
      Language&nbsp;1:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{6}{LanguageOther}'
	     value=""
	     size="50"
	     id=LAO6>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO6").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;2:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{6}{LanguageOther2}'
	     value=""
	     size="50"
	     id=LAO26>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO26").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;3:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{6}{LanguageOther3}'
	     value=""
	     size="50"
	     id=LAO36>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO36").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;4:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{6}{LanguageOther4}'
	     value=""
	     size="50"
	     id=LAO46>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO46").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>


  <tr>
    <td>
      Language&nbsp;5:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{6}{LanguageOther5}'
	     value=""
	     size="50"
	     id=LAO56>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO56").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Other&nbsp;Languages:
    </td>
    <td>
      <textarea style="width: 100%;"
	     name='RMap{6}{LanguageOtherN}'
	     cols="50" rows="3"
	     id=LAON6
		></textarea>
    </td>
  </tr>
</table>
</blockquote>
</td>
<td>
  <i>
    Please enter the languages that apply to your resource.<br>
    Please note that the first 3 boxes have autocompletion.<br>
    If your resource covers more than 3 languages, please use the "Other 
    languages" text area
    <b>and enter the various languages separated by a comma</b>. 
  </i>
</td>
</table>
</span>
</td>
<td width="25%">
</td>
</tr>


<tr>
<td align=right><span id='redmodality6'>Modality:</span></td>
<td align=left>
<select name='RMap{6}{Modality}' 
        size="1" 
        id=RM6 
        onchange="handleOtherField('RM6','rmOther6','RMO6')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Speech" >Speech</option>

<option value="Written" >Written</option>

<option value="Speech/Written" >Speech/Written</option>

<option value="Multimodal/Multimedia" >Multimodal/Multimedia</option>

<option value="Sign Language" >Sign Language</option>

<option value="Modality Independent" >Modality Independent</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>



<td style="width: 40%" id=rmOther6>
<input type=text
       style="width: 100%;"
       name='RMap{6}{ModalityOther}'
       value=""
       size="50"
       id=RMO6>
</td>
</tr>

<tr>
<td align=right><span id='reduse6'>Use of the Resource:</span></td>
<td align=left>
<select name='RMap{6}{Use}' 
        size="1" 
        id=RU6 
        onchange="handleOtherField('RU6','ruOther6','RUO6')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Acquisition" >Acquisition</option>

<option value="Anaphora, Coreference" >Anaphora, Coreference</option>

<option value="Corpus Creation/Annotation" >Corpus Creation/Annotation</option>

<option value="Dialogue" >Dialogue</option>

<option value="Discourse" >Discourse</option>

<option value="Document Classification, Text categorisation" >Document Classification, Text categorisation</option>

<option value="Emotion Recognition/Generation" >Emotion Recognition/Generation</option>

<option value="Evaluation/Validation" >Evaluation/Validation</option>

<option value="Information Extraction, Information Retrieval" >Information Extraction, Information Retrieval</option>

<option value="Knowledge Discovery/Representation" >Knowledge Discovery/Representation</option>

<option value="Language Identification" >Language Identification</option>

<option value="Language Modelling" >Language Modelling</option>

<option value="Lexicon Creation/Annotation" >Lexicon Creation/Annotation</option>

<option value="Machine Learning" >Machine Learning</option>

<option value="Machine Translation, SpeechToSpeech Translation" >Machine Translation, SpeechToSpeech Translation</option>

<option value="Morphological Analysis" >Morphological Analysis</option>

<option value="Multimedia Document Processing" >Multimedia Document Processing</option>

<option value="Named Entity Recognition" >Named Entity Recognition</option>

<option value="Natural Language Generation" >Natural Language Generation</option>

<option value="Opinion Mining/Sentiment Analysis" >Opinion Mining/Sentiment Analysis</option>

<option value="Parsing and Tagging" >Parsing and Tagging</option>

<option value="Person Identification" >Person Identification</option>

<option value="Question Answering" >Question Answering</option>

<option value="Semantic Role Labeling" >Semantic Role Labeling</option>

<option value="Semantic Web" >Semantic Web</option>

<option value="Sign Language Recognition/Generation" >Sign Language Recognition/Generation</option>

<option value="Speech Recognition/Understanding" >Speech Recognition/Understanding</option>

<option value="Speech Synthesis" >Speech Synthesis</option>

<option value="Summarisation" >Summarisation</option>

<option value="Text Mining" >Text Mining</option>

<option value="Textual Entailment and Paraphrasing" >Textual Entailment and Paraphrasing</option>

<option value="Topic Detection and Tracking" >Topic Detection and Tracking</option>

<option value="Voice Control" >Voice Control</option>

<option value="Web Services" >Web Services</option>

<option value="Word Sense Disambiguation" >Word Sense Disambiguation</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=ruOther6>
<input type=text
       style="width: 100%;"
       name='RMap{6}{UseOther}'
       value=""
       size="50"
       id=RUO6>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RUO6").autocomplete({
      source: database_ResourceUse_other
    });
  });
</script>

</td>
</tr>

<tr>
<td align=right><span id='redavailability6'>Resource Availability:</span></td>
<td align=left>
<select name='RMap{6}{Availability}' 
        size="1" 
        id=RA6 
        onchange="handleOtherField('RA6','raOther6','RAO6')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Freely Available" >Freely Available</option>

<option value="From Data Center(s)" >From Data Center(s)</option>

<option value="From Owner" >From Owner</option>

<option value="Not Available" >Not Available</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=raOther6>
<input type=text
       style="width: 100%;"
       name='RMap{6}{AvailabilityOther}'
       value=""
       size="50"
       id=RAO6>
</td>
</tr>



<tr>
<td align="right" valign=center><span id='redlicense6'>License:</span></td>
<td align="left" valign=center>
<input name='RMap{6}{License}' value="" size="40" id=RLIC6 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Licence type, e.g., ELRA, LDC, Gnu, CreativeCommons, OpenSource, etc.</em>
</td>
</tr>




<tr>
<td align=right><span id='redurl6'>Resource URL (if available):</span></td>
<td align=left>
<input name='RMap{6}{URL}' value="" size="40" id=RURL6 style="width: 100%;" type="text">
</td>
</tr>

<tr>
<td align="right" valign=center><span id='reddocumentation6'>Documentation:</span></td>
<td align="left" valign=center>
<input name='RMap{6}{Documentation}' value="" size="40" id=RDOC6 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Is there documentation? In which language? Is the documentation publicly available?</em>
</td>
</tr>




<tr>
<td  align=right valign=top><span id='reddescription6'>Resource Description:</span> </td>
<td colspan="2" align="left">
<textarea name='RMap{6}{Description}' rows="10" cols="70" id=RD6 style="width: 100%;"></textarea>
</td>
</tr>
<!--
<tr>
  <td colspan="3">
The section below applies only if the resource is not available 
at/from established repositories, such as ELRA, LDC, or other data 
centers, as described by the URL entered in the above form. 
ELRA encourages all authors to share the described LRs (data, toosl, services,
etc.), unless already available, to enable their reuse, replicability of experiments,
including evaluation ones, etc.

  </td>
</tr>

<tr>
  <td  align=right valign=top>Do you want to make this LR available through the
<b>"Share Your LRs" initiative?</b></td>
  <td  align="left">
    <input type="checkbox"  id="RLaccepted_6" name="RMap{6}{accepted}" onclick="Resource_manage_upload('RLaccepted_6')" >
  </td>
  <td style="width: 40%" valign=top>
    <em><b>Disclaimer:</b> By clicking the box you agree to share this LR for which rights have been cleared and guarantee that such LR is sharable under the licence you mentioned above.</em>
  </td>
</tr>


<tr>
  <td align=right valign=top><span id="filed_RLaccepted_6">You can upload your LR here if it is smaller than <b>20Mb</b>.</span></td>
  <td align="left">
    <span id="filet_RLaccepted_6">
      <input type="file" name="RMap{6}{FILE}" /> &nbsp;&nbsp;&nbsp;
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>


<tr>
  <td align=right><span id="urld_RLaccepted_6">Otherwise please provide an URL from where the LR can be downloaded:</span></td>
  <td align=left>
    <span id="urlt_RLaccepted_6">
      <input name='RMap{6}{FILEURL}' value="" size="40" id=RURL6 style="width: 100%;" type="text">
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>


-->

</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</div><div id=r7>

<table width="95%">
<tr>
<td>#7</td>

<td style="width: 100%">
<table style="width: 100%" border="1">
<tr>
<td style="width: 100%">
<table cellpadding=5 style="width: 100%">

<tr>
<td align="right">Resource Name:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{7}{Name}' 
               type="text"
               value="" 
               size="40" 
               id=RN7> 


	<script type="text/javascript">
	  jQuery(function(){
	    jQuery("#RN7").keyup(function(){setDefaultValid(7)});
            jQuery("#RN7").autocomplete({
              source: database_ResourceName,
              select: function(event, ui) { f = selectResourceNameUI("#RN7", event, ui); setDefaultValid(7); return f;}});
	  });
	</script>
      </td>
     <!-- <td>
	<input type=button onClick="setDefaultResourceData(7)" value='Load Default Values' id="setDefaultButton_3" disabled>
      </td>
      -->
    </tr>
  </table>


</td>
<!--
<td>
  <i>If the button "Load default values" is enabled you can prefill some fields with default values available in the LreMap for this resource.</i>
</td> -->
</tr>

<tr>
<td align="right">ISLRN Field:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{7}{ISLRN}' 
               type="text"
               value="" 
               size="20" 
               id=RISLRN7
	       title="Format: xxx-xxx-xxx-xxx-x"> 
      </td>
      <td>
	&nbsp;&nbsp;&nbsp;
	<input type=checkbox onClick="disableResourceILSRN(7)" value='1'
	       id="disableResourceISLRN7" name='RMap{7}{ISLRNstatus}'
	       > No ISLRN / I don't know
	<script type="text/javascript">
	  jQuery(function(){
	    disableResourceILSRN(7);
	  });
	</script>
      </td>
    </tr>
  </table>
</td>
<td>
  <i>The ISLRN is a unique and universal identification schema for LRs
    which provides LRs with unique identifier using a standardised
    nomenclature.  It also ensures that LRs are correctly identified,
    and consequently, recognised with proper references for their
    usage in applications in R&D projects, products evaluation and
    benchmark as well as in documents and scientific
    papers. Visit <a href="http://www.islrn.org"
    target=_blank>http://www.islrn.org</a>.</i>
</td>
</tr>



<tr>
<td style="width: 20%" align=right><span id="redtype7">Resource Type: </span></td>



<td style="width: 40%" align=left>
<select name='RMap{7}{Type}' 
        size=1 
        id=RT7 
        onchange="handleOtherField('RT7','resOther7','RNO7')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>


<optgroup label="Resource-Dataset">
<option value="Corpus" >Corpus</option>

<option value="Lexicon" >Lexicon</option>

<option value="Grammar/Language Model" >Grammar/Language Model</option>

<option value="Ontology" >Ontology</option>

<option value="Terminology" >Terminology</option>

<option value="Treebank" >Treebank</option>

</optgroup>
<optgroup label="Resource-Tool">
<option value="Corpus Tool" >Corpus Tool</option>

<option value="Aligner" >Aligner</option>

<option value="Annotation Tool" >Annotation Tool</option>

<option value="Language Identifier" >Language Identifier</option>

<option value="Language Modeling Tool" >Language Modeling Tool</option>

<option value="Machine Learning Tool" >Machine Learning Tool</option>

<option value="Machine Translation Tool" >Machine Translation Tool</option>

<option value="Named Entity Recognizer" >Named Entity Recognizer</option>

<option value="Sentiment Analysis Tool" >Sentiment Analysis Tool</option>

<option value="Software Toolkit" >Software Toolkit</option>

<option value="Tagger/Parser" >Tagger/Parser</option>

<option value="Tokenizer" >Tokenizer</option>

<option value="Web Service" >Web Service</option>

<option value="Word Sense Disambiguator" >Word Sense Disambiguator</option>

<option value="Prosodic Analyzer" >Prosodic Analyzer</option>

<option value="Signal Processing/Features Extraction" >Signal Processing/Features Extraction</option>

<option value="Speaker Recognizer" >Speaker Recognizer</option>

<option value="Speech Recognizer/Transcriber" >Speech Recognizer/Transcriber</option>

<option value="Spoken Dialogue Tool" >Spoken Dialogue Tool</option>

<option value="Text-to-Speech Synthesizer" >Text-to-Speech Synthesizer</option>

<option value="Image Analyzer" >Image Analyzer</option>

</optgroup>
<optgroup label="Resource-Guidelines">
<option value="Metadata" >Metadata</option>

<option value="Representation-Annotation Formalism/Guidelines" >Representation-Annotation Formalism/Guidelines</option>

<option value="Representation-Annotation Standards/Best Practices" >Representation-Annotation Standards/Best Practices</option>

<option value="Language Resources/Technologies Infrastructure" >Language Resources/Technologies Infrastructure</option>

</optgroup>
<optgroup label="Evaluation">
<option value="Evaluation Data" >Evaluation Data</option>

<option value="Evaluation Tool" >Evaluation Tool</option>

<option value="Evaluation Package" >Evaluation Package</option>

<option value="Evaluation Methodology/Standards/Guidelines" >Evaluation Methodology/Standards/Guidelines</option>

</optgroup>

<optgroup label="Other">
<option value="Other" >Other - please specify &rarr;</option>

</optgroup>

</select>
</td>


<td style="width: 40%" id=resOther7>
<input type=text
       style="width: 100%;"
       name='RMap{7}{TypeOther}'
       value=""
       size="50"
       id=RNO7>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RNO7").autocomplete({
      source: database_ResourceType_other
    });
  });
</script>

</td>
</tr>




<tr>
<td align="right" valign=center><span id='redsize7'>Size:</span></td>
<td align="left" valign=center>
  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{7}{Size}' value="" size="40" id=RSIZE7 type="text">
      </td>
      <td>
	<select name='RMap{7}{Unit}' 
		size="1" 
		id=RSIZEU7 
		style="width: 100%;">
	  <option value="" selected>-- Please select a value --</option>

	  <option value="KByte  " >KByte  </option>

<option value="MByte  " >MByte  </option>

<option value="GByte  " >GByte  </option>

<option value="sentences  " >sentences  </option>

<option value="words  " >words  </option>

<option value="tokens  " >tokens  </option>

<option value="entries  " >entries  </option>

<option value="lexemes  " >lexemes  </option>

<option value="synsets  " >synsets  </option>

<option value="concepts " >concepts </option>

<option value="rules   " >rules   </option>

<option value="hours  " >hours  </option>

<option value="minutes  " >minutes  </option>

	  <option value="Other" >Other - please specify next to the size value</option>

	</select>
      </td>
    </tr>
  </table>
</td>


<td style="width: 40%" valign=top>
<em>Enter package size - a number and a relevant unit of measurement, e.g., 48Mbyte, 
5500&nbsp;lexemes, 2.1Gbyte.</em>
</td>
</tr>



<tr>

<td align="right"><span id='redstatus7'>Resource Production Status:</span></td>
<td align="left">
<select name='RMap{7}{Status}' 
        size="1" 
        onchange="handleOtherField('RS7','rsOther7','RSO7')"
        id=RS7 
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Newly created-finished" >Newly created-finished</option>

<option value="Newly created-in progress" >Newly created-in progress</option>

<option value="Existing-used" >Existing-used</option>

<option value="Existing-updated" >Existing-updated</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>


<td style="width: 40%" id=rsOther7>
<input type=text
       style="width: 100%;"
       name='RMap{7}{StatusOther}'
       value=""
       size="50"
       id=RSO7>
</td>
</tr>

<tr>
<td align=right><span id='redlang7'>Language(s):</span></td>
<td align="left">
<select name='RMap{7}{Language}' 
        size="1" 
        id=RL7 
        onchange="handleOtherLanguageField('RL7','lang7','LAO7')"
        style="width: 100%;">
<option value="P" >-- Please select a value --</option>
<option value="W" >Enter the language(s) in the box(es) below &darr;</option>
<option value="LI" >Language Independent</option>
<option value="NA" >Not Applicable</option>

</select>
</td>
<td>
</td>
</tr>

<tr>
<td></td>
<td colspan=2>
<span id=lang7>
<table border=0>
<tr>
<td>
<blockquote>
<table border=0>
  <tr>
    <td>
      Language&nbsp;1:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{7}{LanguageOther}'
	     value=""
	     size="50"
	     id=LAO7>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO7").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;2:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{7}{LanguageOther2}'
	     value=""
	     size="50"
	     id=LAO27>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO27").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;3:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{7}{LanguageOther3}'
	     value=""
	     size="50"
	     id=LAO37>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO37").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;4:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{7}{LanguageOther4}'
	     value=""
	     size="50"
	     id=LAO47>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO47").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>


  <tr>
    <td>
      Language&nbsp;5:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{7}{LanguageOther5}'
	     value=""
	     size="50"
	     id=LAO57>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO57").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Other&nbsp;Languages:
    </td>
    <td>
      <textarea style="width: 100%;"
	     name='RMap{7}{LanguageOtherN}'
	     cols="50" rows="3"
	     id=LAON7
		></textarea>
    </td>
  </tr>
</table>
</blockquote>
</td>
<td>
  <i>
    Please enter the languages that apply to your resource.<br>
    Please note that the first 3 boxes have autocompletion.<br>
    If your resource covers more than 3 languages, please use the "Other 
    languages" text area
    <b>and enter the various languages separated by a comma</b>. 
  </i>
</td>
</table>
</span>
</td>
<td width="25%">
</td>
</tr>


<tr>
<td align=right><span id='redmodality7'>Modality:</span></td>
<td align=left>
<select name='RMap{7}{Modality}' 
        size="1" 
        id=RM7 
        onchange="handleOtherField('RM7','rmOther7','RMO7')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Speech" >Speech</option>

<option value="Written" >Written</option>

<option value="Speech/Written" >Speech/Written</option>

<option value="Multimodal/Multimedia" >Multimodal/Multimedia</option>

<option value="Sign Language" >Sign Language</option>

<option value="Modality Independent" >Modality Independent</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>



<td style="width: 40%" id=rmOther7>
<input type=text
       style="width: 100%;"
       name='RMap{7}{ModalityOther}'
       value=""
       size="50"
       id=RMO7>
</td>
</tr>

<tr>
<td align=right><span id='reduse7'>Use of the Resource:</span></td>
<td align=left>
<select name='RMap{7}{Use}' 
        size="1" 
        id=RU7 
        onchange="handleOtherField('RU7','ruOther7','RUO7')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Acquisition" >Acquisition</option>

<option value="Anaphora, Coreference" >Anaphora, Coreference</option>

<option value="Corpus Creation/Annotation" >Corpus Creation/Annotation</option>

<option value="Dialogue" >Dialogue</option>

<option value="Discourse" >Discourse</option>

<option value="Document Classification, Text categorisation" >Document Classification, Text categorisation</option>

<option value="Emotion Recognition/Generation" >Emotion Recognition/Generation</option>

<option value="Evaluation/Validation" >Evaluation/Validation</option>

<option value="Information Extraction, Information Retrieval" >Information Extraction, Information Retrieval</option>

<option value="Knowledge Discovery/Representation" >Knowledge Discovery/Representation</option>

<option value="Language Identification" >Language Identification</option>

<option value="Language Modelling" >Language Modelling</option>

<option value="Lexicon Creation/Annotation" >Lexicon Creation/Annotation</option>

<option value="Machine Learning" >Machine Learning</option>

<option value="Machine Translation, SpeechToSpeech Translation" >Machine Translation, SpeechToSpeech Translation</option>

<option value="Morphological Analysis" >Morphological Analysis</option>

<option value="Multimedia Document Processing" >Multimedia Document Processing</option>

<option value="Named Entity Recognition" >Named Entity Recognition</option>

<option value="Natural Language Generation" >Natural Language Generation</option>

<option value="Opinion Mining/Sentiment Analysis" >Opinion Mining/Sentiment Analysis</option>

<option value="Parsing and Tagging" >Parsing and Tagging</option>

<option value="Person Identification" >Person Identification</option>

<option value="Question Answering" >Question Answering</option>

<option value="Semantic Role Labeling" >Semantic Role Labeling</option>

<option value="Semantic Web" >Semantic Web</option>

<option value="Sign Language Recognition/Generation" >Sign Language Recognition/Generation</option>

<option value="Speech Recognition/Understanding" >Speech Recognition/Understanding</option>

<option value="Speech Synthesis" >Speech Synthesis</option>

<option value="Summarisation" >Summarisation</option>

<option value="Text Mining" >Text Mining</option>

<option value="Textual Entailment and Paraphrasing" >Textual Entailment and Paraphrasing</option>

<option value="Topic Detection and Tracking" >Topic Detection and Tracking</option>

<option value="Voice Control" >Voice Control</option>

<option value="Web Services" >Web Services</option>

<option value="Word Sense Disambiguation" >Word Sense Disambiguation</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=ruOther7>
<input type=text
       style="width: 100%;"
       name='RMap{7}{UseOther}'
       value=""
       size="50"
       id=RUO7>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RUO7").autocomplete({
      source: database_ResourceUse_other
    });
  });
</script>

</td>
</tr>

<tr>
<td align=right><span id='redavailability7'>Resource Availability:</span></td>
<td align=left>
<select name='RMap{7}{Availability}' 
        size="1" 
        id=RA7 
        onchange="handleOtherField('RA7','raOther7','RAO7')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Freely Available" >Freely Available</option>

<option value="From Data Center(s)" >From Data Center(s)</option>

<option value="From Owner" >From Owner</option>

<option value="Not Available" >Not Available</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=raOther7>
<input type=text
       style="width: 100%;"
       name='RMap{7}{AvailabilityOther}'
       value=""
       size="50"
       id=RAO7>
</td>
</tr>



<tr>
<td align="right" valign=center><span id='redlicense7'>License:</span></td>
<td align="left" valign=center>
<input name='RMap{7}{License}' value="" size="40" id=RLIC7 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Licence type, e.g., ELRA, LDC, Gnu, CreativeCommons, OpenSource, etc.</em>
</td>
</tr>




<tr>
<td align=right><span id='redurl7'>Resource URL (if available):</span></td>
<td align=left>
<input name='RMap{7}{URL}' value="" size="40" id=RURL7 style="width: 100%;" type="text">
</td>
</tr>

<tr>
<td align="right" valign=center><span id='reddocumentation7'>Documentation:</span></td>
<td align="left" valign=center>
<input name='RMap{7}{Documentation}' value="" size="40" id=RDOC7 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Is there documentation? In which language? Is the documentation publicly available?</em>
</td>
</tr>




<tr>
<td  align=right valign=top><span id='reddescription7'>Resource Description:</span> </td>
<td colspan="2" align="left">
<textarea name='RMap{7}{Description}' rows="10" cols="70" id=RD7 style="width: 100%;"></textarea>
</td>
</tr>
<!--
<tr>
  <td colspan="3">
The section below applies only if the resource is not available 
at/from established repositories, such as ELRA, LDC, or other data 
centers, as described by the URL entered in the above form. 
ELRA encourages all authors to share the described LRs (data, toosl, services,
etc.), unless already available, to enable their reuse, replicability of experiments,
including evaluation ones, etc.

  </td>
</tr>

<tr>
  <td  align=right valign=top>Do you want to make this LR available through the
<b>"Share Your LRs" initiative?</b></td>
  <td  align="left">
    <input type="checkbox"  id="RLaccepted_7" name="RMap{7}{accepted}" onclick="Resource_manage_upload('RLaccepted_7')" >
  </td>
  <td style="width: 40%" valign=top>
    <em><b>Disclaimer:</b> By clicking the box you agree to share this LR for which rights have been cleared and guarantee that such LR is sharable under the licence you mentioned above.</em>
  </td>
</tr>


<tr>
  <td align=right valign=top><span id="filed_RLaccepted_7">You can upload your LR here if it is smaller than <b>20Mb</b>.</span></td>
  <td align="left">
    <span id="filet_RLaccepted_7">
      <input type="file" name="RMap{7}{FILE}" /> &nbsp;&nbsp;&nbsp;
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>


<tr>
  <td align=right><span id="urld_RLaccepted_7">Otherwise please provide an URL from where the LR can be downloaded:</span></td>
  <td align=left>
    <span id="urlt_RLaccepted_7">
      <input name='RMap{7}{FILEURL}' value="" size="40" id=RURL7 style="width: 100%;" type="text">
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>


-->

</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</div><div id=r8>

<table width="95%">
<tr>
<td>#8</td>

<td style="width: 100%">
<table style="width: 100%" border="1">
<tr>
<td style="width: 100%">
<table cellpadding=5 style="width: 100%">

<tr>
<td align="right">Resource Name:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{8}{Name}' 
               type="text"
               value="" 
               size="40" 
               id=RN8> 


	<script type="text/javascript">
	  jQuery(function(){
	    jQuery("#RN8").keyup(function(){setDefaultValid(8)});
            jQuery("#RN8").autocomplete({
              source: database_ResourceName,
              select: function(event, ui) { f = selectResourceNameUI("#RN8", event, ui); setDefaultValid(8); return f;}});
	  });
	</script>
      </td>
     <!-- <td>
	<input type=button onClick="setDefaultResourceData(8)" value='Load Default Values' id="setDefaultButton_3" disabled>
      </td>
      -->
    </tr>
  </table>


</td>
<!--
<td>
  <i>If the button "Load default values" is enabled you can prefill some fields with default values available in the LreMap for this resource.</i>
</td> -->
</tr>

<tr>
<td align="right">ISLRN Field:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{8}{ISLRN}' 
               type="text"
               value="" 
               size="20" 
               id=RISLRN8
	       title="Format: xxx-xxx-xxx-xxx-x"> 
      </td>
      <td>
	&nbsp;&nbsp;&nbsp;
	<input type=checkbox onClick="disableResourceILSRN(8)" value='1'
	       id="disableResourceISLRN8" name='RMap{8}{ISLRNstatus}'
	       > No ISLRN / I don't know
	<script type="text/javascript">
	  jQuery(function(){
	    disableResourceILSRN(8);
	  });
	</script>
      </td>
    </tr>
  </table>
</td>
<td>
  <i>The ISLRN is a unique and universal identification schema for LRs
    which provides LRs with unique identifier using a standardised
    nomenclature.  It also ensures that LRs are correctly identified,
    and consequently, recognised with proper references for their
    usage in applications in R&D projects, products evaluation and
    benchmark as well as in documents and scientific
    papers. Visit <a href="http://www.islrn.org"
    target=_blank>http://www.islrn.org</a>.</i>
</td>
</tr>



<tr>
<td style="width: 20%" align=right><span id="redtype8">Resource Type: </span></td>



<td style="width: 40%" align=left>
<select name='RMap{8}{Type}' 
        size=1 
        id=RT8 
        onchange="handleOtherField('RT8','resOther8','RNO8')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>


<optgroup label="Resource-Dataset">
<option value="Corpus" >Corpus</option>

<option value="Lexicon" >Lexicon</option>

<option value="Grammar/Language Model" >Grammar/Language Model</option>

<option value="Ontology" >Ontology</option>

<option value="Terminology" >Terminology</option>

<option value="Treebank" >Treebank</option>

</optgroup>
<optgroup label="Resource-Tool">
<option value="Corpus Tool" >Corpus Tool</option>

<option value="Aligner" >Aligner</option>

<option value="Annotation Tool" >Annotation Tool</option>

<option value="Language Identifier" >Language Identifier</option>

<option value="Language Modeling Tool" >Language Modeling Tool</option>

<option value="Machine Learning Tool" >Machine Learning Tool</option>

<option value="Machine Translation Tool" >Machine Translation Tool</option>

<option value="Named Entity Recognizer" >Named Entity Recognizer</option>

<option value="Sentiment Analysis Tool" >Sentiment Analysis Tool</option>

<option value="Software Toolkit" >Software Toolkit</option>

<option value="Tagger/Parser" >Tagger/Parser</option>

<option value="Tokenizer" >Tokenizer</option>

<option value="Web Service" >Web Service</option>

<option value="Word Sense Disambiguator" >Word Sense Disambiguator</option>

<option value="Prosodic Analyzer" >Prosodic Analyzer</option>

<option value="Signal Processing/Features Extraction" >Signal Processing/Features Extraction</option>

<option value="Speaker Recognizer" >Speaker Recognizer</option>

<option value="Speech Recognizer/Transcriber" >Speech Recognizer/Transcriber</option>

<option value="Spoken Dialogue Tool" >Spoken Dialogue Tool</option>

<option value="Text-to-Speech Synthesizer" >Text-to-Speech Synthesizer</option>

<option value="Image Analyzer" >Image Analyzer</option>

</optgroup>
<optgroup label="Resource-Guidelines">
<option value="Metadata" >Metadata</option>

<option value="Representation-Annotation Formalism/Guidelines" >Representation-Annotation Formalism/Guidelines</option>

<option value="Representation-Annotation Standards/Best Practices" >Representation-Annotation Standards/Best Practices</option>

<option value="Language Resources/Technologies Infrastructure" >Language Resources/Technologies Infrastructure</option>

</optgroup>
<optgroup label="Evaluation">
<option value="Evaluation Data" >Evaluation Data</option>

<option value="Evaluation Tool" >Evaluation Tool</option>

<option value="Evaluation Package" >Evaluation Package</option>

<option value="Evaluation Methodology/Standards/Guidelines" >Evaluation Methodology/Standards/Guidelines</option>

</optgroup>

<optgroup label="Other">
<option value="Other" >Other - please specify &rarr;</option>

</optgroup>

</select>
</td>


<td style="width: 40%" id=resOther8>
<input type=text
       style="width: 100%;"
       name='RMap{8}{TypeOther}'
       value=""
       size="50"
       id=RNO8>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RNO8").autocomplete({
      source: database_ResourceType_other
    });
  });
</script>

</td>
</tr>




<tr>
<td align="right" valign=center><span id='redsize8'>Size:</span></td>
<td align="left" valign=center>
  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{8}{Size}' value="" size="40" id=RSIZE8 type="text">
      </td>
      <td>
	<select name='RMap{8}{Unit}' 
		size="1" 
		id=RSIZEU8 
		style="width: 100%;">
	  <option value="" selected>-- Please select a value --</option>

	  <option value="KByte  " >KByte  </option>

<option value="MByte  " >MByte  </option>

<option value="GByte  " >GByte  </option>

<option value="sentences  " >sentences  </option>

<option value="words  " >words  </option>

<option value="tokens  " >tokens  </option>

<option value="entries  " >entries  </option>

<option value="lexemes  " >lexemes  </option>

<option value="synsets  " >synsets  </option>

<option value="concepts " >concepts </option>

<option value="rules   " >rules   </option>

<option value="hours  " >hours  </option>

<option value="minutes  " >minutes  </option>

	  <option value="Other" >Other - please specify next to the size value</option>

	</select>
      </td>
    </tr>
  </table>
</td>


<td style="width: 40%" valign=top>
<em>Enter package size - a number and a relevant unit of measurement, e.g., 48Mbyte, 
5500&nbsp;lexemes, 2.1Gbyte.</em>
</td>
</tr>



<tr>

<td align="right"><span id='redstatus8'>Resource Production Status:</span></td>
<td align="left">
<select name='RMap{8}{Status}' 
        size="1" 
        onchange="handleOtherField('RS8','rsOther8','RSO8')"
        id=RS8 
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Newly created-finished" >Newly created-finished</option>

<option value="Newly created-in progress" >Newly created-in progress</option>

<option value="Existing-used" >Existing-used</option>

<option value="Existing-updated" >Existing-updated</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>


<td style="width: 40%" id=rsOther8>
<input type=text
       style="width: 100%;"
       name='RMap{8}{StatusOther}'
       value=""
       size="50"
       id=RSO8>
</td>
</tr>

<tr>
<td align=right><span id='redlang8'>Language(s):</span></td>
<td align="left">
<select name='RMap{8}{Language}' 
        size="1" 
        id=RL8 
        onchange="handleOtherLanguageField('RL8','lang8','LAO8')"
        style="width: 100%;">
<option value="P" >-- Please select a value --</option>
<option value="W" >Enter the language(s) in the box(es) below &darr;</option>
<option value="LI" >Language Independent</option>
<option value="NA" >Not Applicable</option>

</select>
</td>
<td>
</td>
</tr>

<tr>
<td></td>
<td colspan=2>
<span id=lang8>
<table border=0>
<tr>
<td>
<blockquote>
<table border=0>
  <tr>
    <td>
      Language&nbsp;1:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{8}{LanguageOther}'
	     value=""
	     size="50"
	     id=LAO8>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO8").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;2:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{8}{LanguageOther2}'
	     value=""
	     size="50"
	     id=LAO28>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO28").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;3:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{8}{LanguageOther3}'
	     value=""
	     size="50"
	     id=LAO38>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO38").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;4:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{8}{LanguageOther4}'
	     value=""
	     size="50"
	     id=LAO48>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO48").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>


  <tr>
    <td>
      Language&nbsp;5:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{8}{LanguageOther5}'
	     value=""
	     size="50"
	     id=LAO58>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO58").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Other&nbsp;Languages:
    </td>
    <td>
      <textarea style="width: 100%;"
	     name='RMap{8}{LanguageOtherN}'
	     cols="50" rows="3"
	     id=LAON8
		></textarea>
    </td>
  </tr>
</table>
</blockquote>
</td>
<td>
  <i>
    Please enter the languages that apply to your resource.<br>
    Please note that the first 3 boxes have autocompletion.<br>
    If your resource covers more than 3 languages, please use the "Other 
    languages" text area
    <b>and enter the various languages separated by a comma</b>. 
  </i>
</td>
</table>
</span>
</td>
<td width="25%">
</td>
</tr>


<tr>
<td align=right><span id='redmodality8'>Modality:</span></td>
<td align=left>
<select name='RMap{8}{Modality}' 
        size="1" 
        id=RM8 
        onchange="handleOtherField('RM8','rmOther8','RMO8')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Speech" >Speech</option>

<option value="Written" >Written</option>

<option value="Speech/Written" >Speech/Written</option>

<option value="Multimodal/Multimedia" >Multimodal/Multimedia</option>

<option value="Sign Language" >Sign Language</option>

<option value="Modality Independent" >Modality Independent</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>



<td style="width: 40%" id=rmOther8>
<input type=text
       style="width: 100%;"
       name='RMap{8}{ModalityOther}'
       value=""
       size="50"
       id=RMO8>
</td>
</tr>

<tr>
<td align=right><span id='reduse8'>Use of the Resource:</span></td>
<td align=left>
<select name='RMap{8}{Use}' 
        size="1" 
        id=RU8 
        onchange="handleOtherField('RU8','ruOther8','RUO8')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Acquisition" >Acquisition</option>

<option value="Anaphora, Coreference" >Anaphora, Coreference</option>

<option value="Corpus Creation/Annotation" >Corpus Creation/Annotation</option>

<option value="Dialogue" >Dialogue</option>

<option value="Discourse" >Discourse</option>

<option value="Document Classification, Text categorisation" >Document Classification, Text categorisation</option>

<option value="Emotion Recognition/Generation" >Emotion Recognition/Generation</option>

<option value="Evaluation/Validation" >Evaluation/Validation</option>

<option value="Information Extraction, Information Retrieval" >Information Extraction, Information Retrieval</option>

<option value="Knowledge Discovery/Representation" >Knowledge Discovery/Representation</option>

<option value="Language Identification" >Language Identification</option>

<option value="Language Modelling" >Language Modelling</option>

<option value="Lexicon Creation/Annotation" >Lexicon Creation/Annotation</option>

<option value="Machine Learning" >Machine Learning</option>

<option value="Machine Translation, SpeechToSpeech Translation" >Machine Translation, SpeechToSpeech Translation</option>

<option value="Morphological Analysis" >Morphological Analysis</option>

<option value="Multimedia Document Processing" >Multimedia Document Processing</option>

<option value="Named Entity Recognition" >Named Entity Recognition</option>

<option value="Natural Language Generation" >Natural Language Generation</option>

<option value="Opinion Mining/Sentiment Analysis" >Opinion Mining/Sentiment Analysis</option>

<option value="Parsing and Tagging" >Parsing and Tagging</option>

<option value="Person Identification" >Person Identification</option>

<option value="Question Answering" >Question Answering</option>

<option value="Semantic Role Labeling" >Semantic Role Labeling</option>

<option value="Semantic Web" >Semantic Web</option>

<option value="Sign Language Recognition/Generation" >Sign Language Recognition/Generation</option>

<option value="Speech Recognition/Understanding" >Speech Recognition/Understanding</option>

<option value="Speech Synthesis" >Speech Synthesis</option>

<option value="Summarisation" >Summarisation</option>

<option value="Text Mining" >Text Mining</option>

<option value="Textual Entailment and Paraphrasing" >Textual Entailment and Paraphrasing</option>

<option value="Topic Detection and Tracking" >Topic Detection and Tracking</option>

<option value="Voice Control" >Voice Control</option>

<option value="Web Services" >Web Services</option>

<option value="Word Sense Disambiguation" >Word Sense Disambiguation</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=ruOther8>
<input type=text
       style="width: 100%;"
       name='RMap{8}{UseOther}'
       value=""
       size="50"
       id=RUO8>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RUO8").autocomplete({
      source: database_ResourceUse_other
    });
  });
</script>

</td>
</tr>

<tr>
<td align=right><span id='redavailability8'>Resource Availability:</span></td>
<td align=left>
<select name='RMap{8}{Availability}' 
        size="1" 
        id=RA8 
        onchange="handleOtherField('RA8','raOther8','RAO8')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Freely Available" >Freely Available</option>

<option value="From Data Center(s)" >From Data Center(s)</option>

<option value="From Owner" >From Owner</option>

<option value="Not Available" >Not Available</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=raOther8>
<input type=text
       style="width: 100%;"
       name='RMap{8}{AvailabilityOther}'
       value=""
       size="50"
       id=RAO8>
</td>
</tr>



<tr>
<td align="right" valign=center><span id='redlicense8'>License:</span></td>
<td align="left" valign=center>
<input name='RMap{8}{License}' value="" size="40" id=RLIC8 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Licence type, e.g., ELRA, LDC, Gnu, CreativeCommons, OpenSource, etc.</em>
</td>
</tr>




<tr>
<td align=right><span id='redurl8'>Resource URL (if available):</span></td>
<td align=left>
<input name='RMap{8}{URL}' value="" size="40" id=RURL8 style="width: 100%;" type="text">
</td>
</tr>

<tr>
<td align="right" valign=center><span id='reddocumentation8'>Documentation:</span></td>
<td align="left" valign=center>
<input name='RMap{8}{Documentation}' value="" size="40" id=RDOC8 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Is there documentation? In which language? Is the documentation publicly available?</em>
</td>
</tr>




<tr>
<td  align=right valign=top><span id='reddescription8'>Resource Description:</span> </td>
<td colspan="2" align="left">
<textarea name='RMap{8}{Description}' rows="10" cols="70" id=RD8 style="width: 100%;"></textarea>
</td>
</tr>
<!--
<tr>
  <td colspan="3">
The section below applies only if the resource is not available 
at/from established repositories, such as ELRA, LDC, or other data 
centers, as described by the URL entered in the above form. 
ELRA encourages all authors to share the described LRs (data, toosl, services,
etc.), unless already available, to enable their reuse, replicability of experiments,
including evaluation ones, etc.

  </td>
</tr>

<tr>
  <td  align=right valign=top>Do you want to make this LR available through the
<b>"Share Your LRs" initiative?</b></td>
  <td  align="left">
    <input type="checkbox"  id="RLaccepted_8" name="RMap{8}{accepted}" onclick="Resource_manage_upload('RLaccepted_8')" >
  </td>
  <td style="width: 40%" valign=top>
    <em><b>Disclaimer:</b> By clicking the box you agree to share this LR for which rights have been cleared and guarantee that such LR is sharable under the licence you mentioned above.</em>
  </td>
</tr>


<tr>
  <td align=right valign=top><span id="filed_RLaccepted_8">You can upload your LR here if it is smaller than <b>20Mb</b>.</span></td>
  <td align="left">
    <span id="filet_RLaccepted_8">
      <input type="file" name="RMap{8}{FILE}" /> &nbsp;&nbsp;&nbsp;
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>


<tr>
  <td align=right><span id="urld_RLaccepted_8">Otherwise please provide an URL from where the LR can be downloaded:</span></td>
  <td align=left>
    <span id="urlt_RLaccepted_8">
      <input name='RMap{8}{FILEURL}' value="" size="40" id=RURL8 style="width: 100%;" type="text">
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>


-->

</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</div><div id=r9>

<table width="95%">
<tr>
<td>#9</td>

<td style="width: 100%">
<table style="width: 100%" border="1">
<tr>
<td style="width: 100%">
<table cellpadding=5 style="width: 100%">

<tr>
<td align="right">Resource Name:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{9}{Name}' 
               type="text"
               value="" 
               size="40" 
               id=RN9> 


	<script type="text/javascript">
	  jQuery(function(){
	    jQuery("#RN9").keyup(function(){setDefaultValid(9)});
            jQuery("#RN9").autocomplete({
              source: database_ResourceName,
              select: function(event, ui) { f = selectResourceNameUI("#RN9", event, ui); setDefaultValid(9); return f;}});
	  });
	</script>
      </td>
      <!-- <td>
	<input type=button onClick="setDefaultResourceData(9)" value='Load Default Values' id="setDefaultButton_3" disabled>
      </td>
      -->
    </tr>
  </table>


</td>
<!--
<td>
  <i>If the button "Load default values" is enabled you can prefill some fields with default values available in the LreMap for this resource.</i>
</td> -->
</tr>

<tr>
<td align="right">ISLRN Field:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{9}{ISLRN}' 
               type="text"
               value="" 
               size="20" 
               id=RISLRN9
	       title="Format: xxx-xxx-xxx-xxx-x"> 
      </td>
      <td>
	&nbsp;&nbsp;&nbsp;
	<input type=checkbox onClick="disableResourceILSRN(9)" value='1'
	       id="disableResourceISLRN9" name='RMap{9}{ISLRNstatus}'
	       > No ISLRN / I don't know
	<script type="text/javascript">
	  jQuery(function(){
	    disableResourceILSRN(9);
	  });
	</script>
      </td>
    </tr>
  </table>
</td>
<td>
  <i>The ISLRN is a unique and universal identification schema for LRs
    which provides LRs with unique identifier using a standardised
    nomenclature.  It also ensures that LRs are correctly identified,
    and consequently, recognised with proper references for their
    usage in applications in R&D projects, products evaluation and
    benchmark as well as in documents and scientific
    papers. Visit <a href="http://www.islrn.org"
    target=_blank>http://www.islrn.org</a>.</i>
</td>
</tr>



<tr>
<td style="width: 20%" align=right><span id="redtype9">Resource Type: </span></td>



<td style="width: 40%" align=left>
<select name='RMap{9}{Type}' 
        size=1 
        id=RT9 
        onchange="handleOtherField('RT9','resOther9','RNO9')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>


<optgroup label="Resource-Dataset">
<option value="Corpus" >Corpus</option>

<option value="Lexicon" >Lexicon</option>

<option value="Grammar/Language Model" >Grammar/Language Model</option>

<option value="Ontology" >Ontology</option>

<option value="Terminology" >Terminology</option>

<option value="Treebank" >Treebank</option>

</optgroup>
<optgroup label="Resource-Tool">
<option value="Corpus Tool" >Corpus Tool</option>

<option value="Aligner" >Aligner</option>

<option value="Annotation Tool" >Annotation Tool</option>

<option value="Language Identifier" >Language Identifier</option>

<option value="Language Modeling Tool" >Language Modeling Tool</option>

<option value="Machine Learning Tool" >Machine Learning Tool</option>

<option value="Machine Translation Tool" >Machine Translation Tool</option>

<option value="Named Entity Recognizer" >Named Entity Recognizer</option>

<option value="Sentiment Analysis Tool" >Sentiment Analysis Tool</option>

<option value="Software Toolkit" >Software Toolkit</option>

<option value="Tagger/Parser" >Tagger/Parser</option>

<option value="Tokenizer" >Tokenizer</option>

<option value="Web Service" >Web Service</option>

<option value="Word Sense Disambiguator" >Word Sense Disambiguator</option>

<option value="Prosodic Analyzer" >Prosodic Analyzer</option>

<option value="Signal Processing/Features Extraction" >Signal Processing/Features Extraction</option>

<option value="Speaker Recognizer" >Speaker Recognizer</option>

<option value="Speech Recognizer/Transcriber" >Speech Recognizer/Transcriber</option>

<option value="Spoken Dialogue Tool" >Spoken Dialogue Tool</option>

<option value="Text-to-Speech Synthesizer" >Text-to-Speech Synthesizer</option>

<option value="Image Analyzer" >Image Analyzer</option>

</optgroup>
<optgroup label="Resource-Guidelines">
<option value="Metadata" >Metadata</option>

<option value="Representation-Annotation Formalism/Guidelines" >Representation-Annotation Formalism/Guidelines</option>

<option value="Representation-Annotation Standards/Best Practices" >Representation-Annotation Standards/Best Practices</option>

<option value="Language Resources/Technologies Infrastructure" >Language Resources/Technologies Infrastructure</option>

</optgroup>
<optgroup label="Evaluation">
<option value="Evaluation Data" >Evaluation Data</option>

<option value="Evaluation Tool" >Evaluation Tool</option>

<option value="Evaluation Package" >Evaluation Package</option>

<option value="Evaluation Methodology/Standards/Guidelines" >Evaluation Methodology/Standards/Guidelines</option>

</optgroup>

<optgroup label="Other">
<option value="Other" >Other - please specify &rarr;</option>

</optgroup>

</select>
</td>


<td style="width: 40%" id=resOther9>
<input type=text
       style="width: 100%;"
       name='RMap{9}{TypeOther}'
       value=""
       size="50"
       id=RNO9>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RNO9").autocomplete({
      source: database_ResourceType_other
    });
  });
</script>

</td>
</tr>




<tr>
<td align="right" valign=center><span id='redsize9'>Size:</span></td>
<td align="left" valign=center>
  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{9}{Size}' value="" size="40" id=RSIZE9 type="text">
      </td>
      <td>
	<select name='RMap{9}{Unit}' 
		size="1" 
		id=RSIZEU9 
		style="width: 100%;">
	  <option value="" selected>-- Please select a value --</option>

	  <option value="KByte  " >KByte  </option>

<option value="MByte  " >MByte  </option>

<option value="GByte  " >GByte  </option>

<option value="sentences  " >sentences  </option>

<option value="words  " >words  </option>

<option value="tokens  " >tokens  </option>

<option value="entries  " >entries  </option>

<option value="lexemes  " >lexemes  </option>

<option value="synsets  " >synsets  </option>

<option value="concepts " >concepts </option>

<option value="rules   " >rules   </option>

<option value="hours  " >hours  </option>

<option value="minutes  " >minutes  </option>

	  <option value="Other" >Other - please specify next to the size value</option>

	</select>
      </td>
    </tr>
  </table>
</td>


<td style="width: 40%" valign=top>
<em>Enter package size - a number and a relevant unit of measurement, e.g., 48Mbyte, 
5500&nbsp;lexemes, 2.1Gbyte.</em>
</td>
</tr>



<tr>

<td align="right"><span id='redstatus9'>Resource Production Status:</span></td>
<td align="left">
<select name='RMap{9}{Status}' 
        size="1" 
        onchange="handleOtherField('RS9','rsOther9','RSO9')"
        id=RS9 
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Newly created-finished" >Newly created-finished</option>

<option value="Newly created-in progress" >Newly created-in progress</option>

<option value="Existing-used" >Existing-used</option>

<option value="Existing-updated" >Existing-updated</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>


<td style="width: 40%" id=rsOther9>
<input type=text
       style="width: 100%;"
       name='RMap{9}{StatusOther}'
       value=""
       size="50"
       id=RSO9>
</td>
</tr>

<tr>
<td align=right><span id='redlang9'>Language(s):</span></td>
<td align="left">
<select name='RMap{9}{Language}' 
        size="1" 
        id=RL9 
        onchange="handleOtherLanguageField('RL9','lang9','LAO9')"
        style="width: 100%;">
<option value="P" >-- Please select a value --</option>
<option value="W" >Enter the language(s) in the box(es) below &darr;</option>
<option value="LI" >Language Independent</option>
<option value="NA" >Not Applicable</option>

</select>
</td>
<td>
</td>
</tr>

<tr>
<td></td>
<td colspan=2>
<span id=lang9>
<table border=0>
<tr>
<td>
<blockquote>
<table border=0>
  <tr>
    <td>
      Language&nbsp;1:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{9}{LanguageOther}'
	     value=""
	     size="50"
	     id=LAO9>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO9").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;2:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{9}{LanguageOther2}'
	     value=""
	     size="50"
	     id=LAO29>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO29").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;3:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{9}{LanguageOther3}'
	     value=""
	     size="50"
	     id=LAO39>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO39").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;4:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{9}{LanguageOther4}'
	     value=""
	     size="50"
	     id=LAO49>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO49").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>


  <tr>
    <td>
      Language&nbsp;5:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{9}{LanguageOther5}'
	     value=""
	     size="50"
	     id=LAO59>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO59").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Other&nbsp;Languages:
    </td>
    <td>
      <textarea style="width: 100%;"
	     name='RMap{9}{LanguageOtherN}'
	     cols="50" rows="3"
	     id=LAON9
		></textarea>
    </td>
  </tr>
</table>
</blockquote>
</td>
<td>
  <i>
    Please enter the languages that apply to your resource.<br>
    Please note that the first 3 boxes have autocompletion.<br>
    If your resource covers more than 3 languages, please use the "Other 
    languages" text area
    <b>and enter the various languages separated by a comma</b>. 
  </i>
</td>
</table>
</span>
</td>
<td width="25%">
</td>
</tr>


<tr>
<td align=right><span id='redmodality9'>Modality:</span></td>
<td align=left>
<select name='RMap{9}{Modality}' 
        size="1" 
        id=RM9 
        onchange="handleOtherField('RM9','rmOther9','RMO9')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Speech" >Speech</option>

<option value="Written" >Written</option>

<option value="Speech/Written" >Speech/Written</option>

<option value="Multimodal/Multimedia" >Multimodal/Multimedia</option>

<option value="Sign Language" >Sign Language</option>

<option value="Modality Independent" >Modality Independent</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>



<td style="width: 40%" id=rmOther9>
<input type=text
       style="width: 100%;"
       name='RMap{9}{ModalityOther}'
       value=""
       size="50"
       id=RMO9>
</td>
</tr>

<tr>
<td align=right><span id='reduse9'>Use of the Resource:</span></td>
<td align=left>
<select name='RMap{9}{Use}' 
        size="1" 
        id=RU9 
        onchange="handleOtherField('RU9','ruOther9','RUO9')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Acquisition" >Acquisition</option>

<option value="Anaphora, Coreference" >Anaphora, Coreference</option>

<option value="Corpus Creation/Annotation" >Corpus Creation/Annotation</option>

<option value="Dialogue" >Dialogue</option>

<option value="Discourse" >Discourse</option>

<option value="Document Classification, Text categorisation" >Document Classification, Text categorisation</option>

<option value="Emotion Recognition/Generation" >Emotion Recognition/Generation</option>

<option value="Evaluation/Validation" >Evaluation/Validation</option>

<option value="Information Extraction, Information Retrieval" >Information Extraction, Information Retrieval</option>

<option value="Knowledge Discovery/Representation" >Knowledge Discovery/Representation</option>

<option value="Language Identification" >Language Identification</option>

<option value="Language Modelling" >Language Modelling</option>

<option value="Lexicon Creation/Annotation" >Lexicon Creation/Annotation</option>

<option value="Machine Learning" >Machine Learning</option>

<option value="Machine Translation, SpeechToSpeech Translation" >Machine Translation, SpeechToSpeech Translation</option>

<option value="Morphological Analysis" >Morphological Analysis</option>

<option value="Multimedia Document Processing" >Multimedia Document Processing</option>

<option value="Named Entity Recognition" >Named Entity Recognition</option>

<option value="Natural Language Generation" >Natural Language Generation</option>

<option value="Opinion Mining/Sentiment Analysis" >Opinion Mining/Sentiment Analysis</option>

<option value="Parsing and Tagging" >Parsing and Tagging</option>

<option value="Person Identification" >Person Identification</option>

<option value="Question Answering" >Question Answering</option>

<option value="Semantic Role Labeling" >Semantic Role Labeling</option>

<option value="Semantic Web" >Semantic Web</option>

<option value="Sign Language Recognition/Generation" >Sign Language Recognition/Generation</option>

<option value="Speech Recognition/Understanding" >Speech Recognition/Understanding</option>

<option value="Speech Synthesis" >Speech Synthesis</option>

<option value="Summarisation" >Summarisation</option>

<option value="Text Mining" >Text Mining</option>

<option value="Textual Entailment and Paraphrasing" >Textual Entailment and Paraphrasing</option>

<option value="Topic Detection and Tracking" >Topic Detection and Tracking</option>

<option value="Voice Control" >Voice Control</option>

<option value="Web Services" >Web Services</option>

<option value="Word Sense Disambiguation" >Word Sense Disambiguation</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=ruOther9>
<input type=text
       style="width: 100%;"
       name='RMap{9}{UseOther}'
       value=""
       size="50"
       id=RUO9>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RUO9").autocomplete({
      source: database_ResourceUse_other
    });
  });
</script>

</td>
</tr>

<tr>
<td align=right><span id='redavailability9'>Resource Availability:</span></td>
<td align=left>
<select name='RMap{9}{Availability}' 
        size="1" 
        id=RA9 
        onchange="handleOtherField('RA9','raOther9','RAO9')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Freely Available" >Freely Available</option>

<option value="From Data Center(s)" >From Data Center(s)</option>

<option value="From Owner" >From Owner</option>

<option value="Not Available" >Not Available</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=raOther9>
<input type=text
       style="width: 100%;"
       name='RMap{9}{AvailabilityOther}'
       value=""
       size="50"
       id=RAO9>
</td>
</tr>



<tr>
<td align="right" valign=center><span id='redlicense9'>License:</span></td>
<td align="left" valign=center>
<input name='RMap{9}{License}' value="" size="40" id=RLIC9 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Licence type, e.g., ELRA, LDC, Gnu, CreativeCommons, OpenSource, etc.</em>
</td>
</tr>




<tr>
<td align=right><span id='redurl9'>Resource URL (if available):</span></td>
<td align=left>
<input name='RMap{9}{URL}' value="" size="40" id=RURL9 style="width: 100%;" type="text">
</td>
</tr>

<tr>
<td align="right" valign=center><span id='reddocumentation9'>Documentation:</span></td>
<td align="left" valign=center>
<input name='RMap{9}{Documentation}' value="" size="40" id=RDOC9 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Is there documentation? In which language? Is the documentation publicly available?</em>
</td>
</tr>




<tr>
<td  align=right valign=top><span id='reddescription9'>Resource Description:</span> </td>
<td colspan="2" align="left">
<textarea name='RMap{9}{Description}' rows="10" cols="70" id=RD9 style="width: 100%;"></textarea>
</td>
</tr>
<!--
<tr>
  <td colspan="3">
The section below applies only if the resource is not available 
at/from established repositories, such as ELRA, LDC, or other data 
centers, as described by the URL entered in the above form. 
ELRA encourages all authors to share the described LRs (data, toosl, services,
etc.), unless already available, to enable their reuse, replicability of experiments,
including evaluation ones, etc.

  </td>
</tr>

<tr>
  <td  align=right valign=top>Do you want to make this LR available through the
<b>"Share Your LRs" initiative?</b></td>
  <td  align="left">
    <input type="checkbox"  id="RLaccepted_9" name="RMap{9}{accepted}" onclick="Resource_manage_upload('RLaccepted_9')" >
  </td>
  <td style="width: 40%" valign=top>
    <em><b>Disclaimer:</b> By clicking the box you agree to share this LR for which rights have been cleared and guarantee that such LR is sharable under the licence you mentioned above.</em>
  </td>
</tr>


<tr>
  <td align=right valign=top><span id="filed_RLaccepted_9">You can upload your LR here if it is smaller than <b>20Mb</b>.</span></td>
  <td align="left">
    <span id="filet_RLaccepted_9">
      <input type="file" name="RMap{9}{FILE}" /> &nbsp;&nbsp;&nbsp;
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>


<tr>
  <td align=right><span id="urld_RLaccepted_9">Otherwise please provide an URL from where the LR can be downloaded:</span></td>
  <td align=left>
    <span id="urlt_RLaccepted_9">
      <input name='RMap{9}{FILEURL}' value="" size="40" id=RURL9 style="width: 100%;" type="text">
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>


-->

</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</div><div id=r10>

<table width="95%">
<tr>
<td>#10</td>

<td style="width: 100%">
<table style="width: 100%" border="1">
<tr>
<td style="width: 100%">
<table cellpadding=5 style="width: 100%">

<tr>
<td align="right">Resource Name:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{10}{Name}' 
               type="text"
               value="" 
               size="40" 
               id=RN10> 


	<script type="text/javascript">
	  jQuery(function(){
	    jQuery("#RN10").keyup(function(){setDefaultValid(10)});
            jQuery("#RN10").autocomplete({
              source: database_ResourceName,
              select: function(event, ui) { f = selectResourceNameUI("#RN10", event, ui); setDefaultValid(10); return f;}});
	  });
	</script>
      </td>
      <!-- <td>
	<input type=button onClick="setDefaultResourceData(10)" value='Load Default Values' id="setDefaultButton_3" disabled>
      </td>
      -->
    </tr>
  </table>


</td>
<!--
<td>
  <i>If the button "Load default values" is enabled you can prefill some fields with default values available in the LreMap for this resource.</i>
</td> -->
</tr>

<tr>
<td align="right">ISLRN Field:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{10}{ISLRN}' 
               type="text"
               value="" 
               size="20" 
               id=RISLRN10
	       title="Format: xxx-xxx-xxx-xxx-x"> 
      </td>
      <td>
	&nbsp;&nbsp;&nbsp;
	<input type=checkbox onClick="disableResourceILSRN(10)" value='1'
	       id="disableResourceISLRN10" name='RMap{10}{ISLRNstatus}'
	       > No ISLRN / I don't know
	<script type="text/javascript">
	  jQuery(function(){
	    disableResourceILSRN(10);
	  });
	</script>
      </td>
    </tr>
  </table>
</td>
<td>
  <i>The ISLRN is a unique and universal identification schema for LRs
    which provides LRs with unique identifier using a standardised
    nomenclature.  It also ensures that LRs are correctly identified,
    and consequently, recognised with proper references for their
    usage in applications in R&D projects, products evaluation and
    benchmark as well as in documents and scientific
    papers. Visit <a href="http://www.islrn.org"
    target=_blank>http://www.islrn.org</a>.</i>
</td>
</tr>



<tr>
<td style="width: 20%" align=right><span id="redtype10">Resource Type: </span></td>



<td style="width: 40%" align=left>
<select name='RMap{10}{Type}' 
        size=1 
        id=RT10 
        onchange="handleOtherField('RT10','resOther10','RNO10')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>


<optgroup label="Resource-Dataset">
<option value="Corpus" >Corpus</option>

<option value="Lexicon" >Lexicon</option>

<option value="Grammar/Language Model" >Grammar/Language Model</option>

<option value="Ontology" >Ontology</option>

<option value="Terminology" >Terminology</option>

<option value="Treebank" >Treebank</option>

</optgroup>
<optgroup label="Resource-Tool">
<option value="Corpus Tool" >Corpus Tool</option>

<option value="Aligner" >Aligner</option>

<option value="Annotation Tool" >Annotation Tool</option>

<option value="Language Identifier" >Language Identifier</option>

<option value="Language Modeling Tool" >Language Modeling Tool</option>

<option value="Machine Learning Tool" >Machine Learning Tool</option>

<option value="Machine Translation Tool" >Machine Translation Tool</option>

<option value="Named Entity Recognizer" >Named Entity Recognizer</option>

<option value="Sentiment Analysis Tool" >Sentiment Analysis Tool</option>

<option value="Software Toolkit" >Software Toolkit</option>

<option value="Tagger/Parser" >Tagger/Parser</option>

<option value="Tokenizer" >Tokenizer</option>

<option value="Web Service" >Web Service</option>

<option value="Word Sense Disambiguator" >Word Sense Disambiguator</option>

<option value="Prosodic Analyzer" >Prosodic Analyzer</option>

<option value="Signal Processing/Features Extraction" >Signal Processing/Features Extraction</option>

<option value="Speaker Recognizer" >Speaker Recognizer</option>

<option value="Speech Recognizer/Transcriber" >Speech Recognizer/Transcriber</option>

<option value="Spoken Dialogue Tool" >Spoken Dialogue Tool</option>

<option value="Text-to-Speech Synthesizer" >Text-to-Speech Synthesizer</option>

<option value="Image Analyzer" >Image Analyzer</option>

</optgroup>
<optgroup label="Resource-Guidelines">
<option value="Metadata" >Metadata</option>

<option value="Representation-Annotation Formalism/Guidelines" >Representation-Annotation Formalism/Guidelines</option>

<option value="Representation-Annotation Standards/Best Practices" >Representation-Annotation Standards/Best Practices</option>

<option value="Language Resources/Technologies Infrastructure" >Language Resources/Technologies Infrastructure</option>

</optgroup>
<optgroup label="Evaluation">
<option value="Evaluation Data" >Evaluation Data</option>

<option value="Evaluation Tool" >Evaluation Tool</option>

<option value="Evaluation Package" >Evaluation Package</option>

<option value="Evaluation Methodology/Standards/Guidelines" >Evaluation Methodology/Standards/Guidelines</option>

</optgroup>

<optgroup label="Other">
<option value="Other" >Other - please specify &rarr;</option>

</optgroup>

</select>
</td>


<td style="width: 40%" id=resOther10>
<input type=text
       style="width: 100%;"
       name='RMap{10}{TypeOther}'
       value=""
       size="50"
       id=RNO10>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RNO10").autocomplete({
      source: database_ResourceType_other
    });
  });
</script>

</td>
</tr>




<tr>
<td align="right" valign=center><span id='redsize10'>Size:</span></td>
<td align="left" valign=center>
  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{10}{Size}' value="" size="40" id=RSIZE10 type="text">
      </td>
      <td>
	<select name='RMap{10}{Unit}' 
		size="1" 
		id=RSIZEU10 
		style="width: 100%;">
	  <option value="" selected>-- Please select a value --</option>

	  <option value="KByte  " >KByte  </option>

<option value="MByte  " >MByte  </option>

<option value="GByte  " >GByte  </option>

<option value="sentences  " >sentences  </option>

<option value="words  " >words  </option>

<option value="tokens  " >tokens  </option>

<option value="entries  " >entries  </option>

<option value="lexemes  " >lexemes  </option>

<option value="synsets  " >synsets  </option>

<option value="concepts " >concepts </option>

<option value="rules   " >rules   </option>

<option value="hours  " >hours  </option>

<option value="minutes  " >minutes  </option>

	  <option value="Other" >Other - please specify next to the size value</option>

	</select>
      </td>
    </tr>
  </table>
</td>


<td style="width: 40%" valign=top>
<em>Enter package size - a number and a relevant unit of measurement, e.g., 48Mbyte, 
5500&nbsp;lexemes, 2.1Gbyte.</em>
</td>
</tr>



<tr>

<td align="right"><span id='redstatus10'>Resource Production Status:</span></td>
<td align="left">
<select name='RMap{10}{Status}' 
        size="1" 
        onchange="handleOtherField('RS10','rsOther10','RSO10')"
        id=RS10 
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Newly created-finished" >Newly created-finished</option>

<option value="Newly created-in progress" >Newly created-in progress</option>

<option value="Existing-used" >Existing-used</option>

<option value="Existing-updated" >Existing-updated</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>


<td style="width: 40%" id=rsOther10>
<input type=text
       style="width: 100%;"
       name='RMap{10}{StatusOther}'
       value=""
       size="50"
       id=RSO10>
</td>
</tr>

<tr>
<td align=right><span id='redlang10'>Language(s):</span></td>
<td align="left">
<select name='RMap{10}{Language}' 
        size="1" 
        id=RL10 
        onchange="handleOtherLanguageField('RL10','lang10','LAO10')"
        style="width: 100%;">
<option value="P" >-- Please select a value --</option>
<option value="W" >Enter the language(s) in the box(es) below &darr;</option>
<option value="LI" >Language Independent</option>
<option value="NA" >Not Applicable</option>

</select>
</td>
<td>
</td>
</tr>

<tr>
<td></td>
<td colspan=2>
<span id=lang10>
<table border=0>
<tr>
<td>
<blockquote>
<table border=0>
  <tr>
    <td>
      Language&nbsp;1:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{10}{LanguageOther}'
	     value=""
	     size="50"
	     id=LAO10>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO10").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;2:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{10}{LanguageOther2}'
	     value=""
	     size="50"
	     id=LAO210>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO210").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;3:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{10}{LanguageOther3}'
	     value=""
	     size="50"
	     id=LAO310>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO310").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;4:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{10}{LanguageOther4}'
	     value=""
	     size="50"
	     id=LAO410>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO410").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>


  <tr>
    <td>
      Language&nbsp;5:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{10}{LanguageOther5}'
	     value=""
	     size="50"
	     id=LAO510>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO510").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Other&nbsp;Languages:
    </td>
    <td>
      <textarea style="width: 100%;"
	     name='RMap{10}{LanguageOtherN}'
	     cols="50" rows="3"
	     id=LAON10
		></textarea>
    </td>
  </tr>
</table>
</blockquote>
</td>
<td>
  <i>
    Please enter the languages that apply to your resource.<br>
    Please note that the first 3 boxes have autocompletion.<br>
    If your resource covers more than 3 languages, please use the "Other 
    languages" text area
    <b>and enter the various languages separated by a comma</b>. 
  </i>
</td>
</table>
</span>
</td>
<td width="25%">
</td>
</tr>


<tr>
<td align=right><span id='redmodality10'>Modality:</span></td>
<td align=left>
<select name='RMap{10}{Modality}' 
        size="1" 
        id=RM10 
        onchange="handleOtherField('RM10','rmOther10','RMO10')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Speech" >Speech</option>

<option value="Written" >Written</option>

<option value="Speech/Written" >Speech/Written</option>

<option value="Multimodal/Multimedia" >Multimodal/Multimedia</option>

<option value="Sign Language" >Sign Language</option>

<option value="Modality Independent" >Modality Independent</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>



<td style="width: 40%" id=rmOther10>
<input type=text
       style="width: 100%;"
       name='RMap{10}{ModalityOther}'
       value=""
       size="50"
       id=RMO10>
</td>
</tr>

<tr>
<td align=right><span id='reduse10'>Use of the Resource:</span></td>
<td align=left>
<select name='RMap{10}{Use}' 
        size="1" 
        id=RU10 
        onchange="handleOtherField('RU10','ruOther10','RUO10')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Acquisition" >Acquisition</option>

<option value="Anaphora, Coreference" >Anaphora, Coreference</option>

<option value="Corpus Creation/Annotation" >Corpus Creation/Annotation</option>

<option value="Dialogue" >Dialogue</option>

<option value="Discourse" >Discourse</option>

<option value="Document Classification, Text categorisation" >Document Classification, Text categorisation</option>

<option value="Emotion Recognition/Generation" >Emotion Recognition/Generation</option>

<option value="Evaluation/Validation" >Evaluation/Validation</option>

<option value="Information Extraction, Information Retrieval" >Information Extraction, Information Retrieval</option>

<option value="Knowledge Discovery/Representation" >Knowledge Discovery/Representation</option>

<option value="Language Identification" >Language Identification</option>

<option value="Language Modelling" >Language Modelling</option>

<option value="Lexicon Creation/Annotation" >Lexicon Creation/Annotation</option>

<option value="Machine Learning" >Machine Learning</option>

<option value="Machine Translation, SpeechToSpeech Translation" >Machine Translation, SpeechToSpeech Translation</option>

<option value="Morphological Analysis" >Morphological Analysis</option>

<option value="Multimedia Document Processing" >Multimedia Document Processing</option>

<option value="Named Entity Recognition" >Named Entity Recognition</option>

<option value="Natural Language Generation" >Natural Language Generation</option>

<option value="Opinion Mining/Sentiment Analysis" >Opinion Mining/Sentiment Analysis</option>

<option value="Parsing and Tagging" >Parsing and Tagging</option>

<option value="Person Identification" >Person Identification</option>

<option value="Question Answering" >Question Answering</option>

<option value="Semantic Role Labeling" >Semantic Role Labeling</option>

<option value="Semantic Web" >Semantic Web</option>

<option value="Sign Language Recognition/Generation" >Sign Language Recognition/Generation</option>

<option value="Speech Recognition/Understanding" >Speech Recognition/Understanding</option>

<option value="Speech Synthesis" >Speech Synthesis</option>

<option value="Summarisation" >Summarisation</option>

<option value="Text Mining" >Text Mining</option>

<option value="Textual Entailment and Paraphrasing" >Textual Entailment and Paraphrasing</option>

<option value="Topic Detection and Tracking" >Topic Detection and Tracking</option>

<option value="Voice Control" >Voice Control</option>

<option value="Web Services" >Web Services</option>

<option value="Word Sense Disambiguation" >Word Sense Disambiguation</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=ruOther10>
<input type=text
       style="width: 100%;"
       name='RMap{10}{UseOther}'
       value=""
       size="50"
       id=RUO10>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RUO10").autocomplete({
      source: database_ResourceUse_other
    });
  });
</script>

</td>
</tr>

<tr>
<td align=right><span id='redavailability10'>Resource Availability:</span></td>
<td align=left>
<select name='RMap{10}{Availability}' 
        size="1" 
        id=RA10 
        onchange="handleOtherField('RA10','raOther10','RAO10')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Freely Available" >Freely Available</option>

<option value="From Data Center(s)" >From Data Center(s)</option>

<option value="From Owner" >From Owner</option>

<option value="Not Available" >Not Available</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=raOther10>
<input type=text
       style="width: 100%;"
       name='RMap{10}{AvailabilityOther}'
       value=""
       size="50"
       id=RAO10>
</td>
</tr>



<tr>
<td align="right" valign=center><span id='redlicense10'>License:</span></td>
<td align="left" valign=center>
<input name='RMap{10}{License}' value="" size="40" id=RLIC10 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Licence type, e.g., ELRA, LDC, Gnu, CreativeCommons, OpenSource, etc.</em>
</td>
</tr>




<tr>
<td align=right><span id='redurl10'>Resource URL (if available):</span></td>
<td align=left>
<input name='RMap{10}{URL}' value="" size="40" id=RURL10 style="width: 100%;" type="text">
</td>
</tr>

<tr>
<td align="right" valign=center><span id='reddocumentation10'>Documentation:</span></td>
<td align="left" valign=center>
<input name='RMap{10}{Documentation}' value="" size="40" id=RDOC10 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Is there documentation? In which language? Is the documentation publicly available?</em>
</td>
</tr>




<tr>
<td  align=right valign=top><span id='reddescription10'>Resource Description:</span> </td>
<td colspan="2" align="left">
<textarea name='RMap{10}{Description}' rows="10" cols="70" id=RD10 style="width: 100%;"></textarea>
</td>
</tr>
<!--
<tr>
  <td colspan="3">
The section below applies only if the resource is not available 
at/from established repositories, such as ELRA, LDC, or other data 
centers, as described by the URL entered in the above form. 
ELRA encourages all authors to share the described LRs (data, toosl, services,
etc.), unless already available, to enable their reuse, replicability of experiments,
including evaluation ones, etc.

  </td>
</tr>

<tr>
  <td  align=right valign=top>Do you want to make this LR available through the
<b>"Share Your LRs" initiative?</b></td>
  <td  align="left">
    <input type="checkbox"  id="RLaccepted_10" name="RMap{10}{accepted}" onclick="Resource_manage_upload('RLaccepted_10')" >
  </td>
  <td style="width: 40%" valign=top>
    <em><b>Disclaimer:</b> By clicking the box you agree to share this LR for which rights have been cleared and guarantee that such LR is sharable under the licence you mentioned above.</em>
  </td>
</tr>


<tr>
  <td align=right valign=top><span id="filed_RLaccepted_10">You can upload your LR here if it is smaller than <b>20Mb</b>.</span></td>
  <td align="left">
    <span id="filet_RLaccepted_10">
      <input type="file" name="RMap{10}{FILE}" /> &nbsp;&nbsp;&nbsp;
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>


<tr>
  <td align=right><span id="urld_RLaccepted_10">Otherwise please provide an URL from where the LR can be downloaded:</span></td>
  <td align=left>
    <span id="urlt_RLaccepted_10">
      <input name='RMap{10}{FILEURL}' value="" size="40" id=RURL10 style="width: 100%;" type="text">
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>


-->

</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</div><div id=r11>

<table width="95%">
<tr>
<td>#11</td>

<td style="width: 100%">
<table style="width: 100%" border="1">
<tr>
<td style="width: 100%">
<table cellpadding=5 style="width: 100%">

<tr>
<td align="right">Resource Name:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{11}{Name}' 
               type="text"
               value="" 
               size="40" 
               id=RN11> 


	<script type="text/javascript">
	  jQuery(function(){
	    jQuery("#RN11").keyup(function(){setDefaultValid(11)});
            jQuery("#RN11").autocomplete({
              source: database_ResourceName,
              select: function(event, ui) { f = selectResourceNameUI("#RN11", event, ui); setDefaultValid(11); return f;}});
	  });
	</script>
      </td>
     <!-- <td>
	<input type=button onClick="setDefaultResourceData(11)" value='Load Default Values' id="setDefaultButton_3" disabled>
      </td>
      -->
    </tr>
  </table>


</td>
<!--
<td>
  <i>If the button "Load default values" is enabled you can prefill some fields with default values available in the LreMap for this resource.</i>
</td> -->
</tr>

<tr>
<td align="right">ISLRN Field:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{11}{ISLRN}' 
               type="text"
               value="" 
               size="20" 
               id=RISLRN11
	       title="Format: xxx-xxx-xxx-xxx-x"> 
      </td>
      <td>
	&nbsp;&nbsp;&nbsp;
	<input type=checkbox onClick="disableResourceILSRN(11)" value='1'
	       id="disableResourceISLRN11" name='RMap{11}{ISLRNstatus}'
	       > No ISLRN / I don't know
	<script type="text/javascript">
	  jQuery(function(){
	    disableResourceILSRN(11);
	  });
	</script>
      </td>
    </tr>
  </table>
</td>
<td>
  <i>The ISLRN is a unique and universal identification schema for LRs
    which provides LRs with unique identifier using a standardised
    nomenclature.  It also ensures that LRs are correctly identified,
    and consequently, recognised with proper references for their
    usage in applications in R&D projects, products evaluation and
    benchmark as well as in documents and scientific
    papers. Visit <a href="http://www.islrn.org"
    target=_blank>http://www.islrn.org</a>.</i>
</td>
</tr>



<tr>
<td style="width: 20%" align=right><span id="redtype11">Resource Type: </span></td>



<td style="width: 40%" align=left>
<select name='RMap{11}{Type}' 
        size=1 
        id=RT11 
        onchange="handleOtherField('RT11','resOther11','RNO11')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>


<optgroup label="Resource-Dataset">
<option value="Corpus" >Corpus</option>

<option value="Lexicon" >Lexicon</option>

<option value="Grammar/Language Model" >Grammar/Language Model</option>

<option value="Ontology" >Ontology</option>

<option value="Terminology" >Terminology</option>

<option value="Treebank" >Treebank</option>

</optgroup>
<optgroup label="Resource-Tool">
<option value="Corpus Tool" >Corpus Tool</option>

<option value="Aligner" >Aligner</option>

<option value="Annotation Tool" >Annotation Tool</option>

<option value="Language Identifier" >Language Identifier</option>

<option value="Language Modeling Tool" >Language Modeling Tool</option>

<option value="Machine Learning Tool" >Machine Learning Tool</option>

<option value="Machine Translation Tool" >Machine Translation Tool</option>

<option value="Named Entity Recognizer" >Named Entity Recognizer</option>

<option value="Sentiment Analysis Tool" >Sentiment Analysis Tool</option>

<option value="Software Toolkit" >Software Toolkit</option>

<option value="Tagger/Parser" >Tagger/Parser</option>

<option value="Tokenizer" >Tokenizer</option>

<option value="Web Service" >Web Service</option>

<option value="Word Sense Disambiguator" >Word Sense Disambiguator</option>

<option value="Prosodic Analyzer" >Prosodic Analyzer</option>

<option value="Signal Processing/Features Extraction" >Signal Processing/Features Extraction</option>

<option value="Speaker Recognizer" >Speaker Recognizer</option>

<option value="Speech Recognizer/Transcriber" >Speech Recognizer/Transcriber</option>

<option value="Spoken Dialogue Tool" >Spoken Dialogue Tool</option>

<option value="Text-to-Speech Synthesizer" >Text-to-Speech Synthesizer</option>

<option value="Image Analyzer" >Image Analyzer</option>

</optgroup>
<optgroup label="Resource-Guidelines">
<option value="Metadata" >Metadata</option>

<option value="Representation-Annotation Formalism/Guidelines" >Representation-Annotation Formalism/Guidelines</option>

<option value="Representation-Annotation Standards/Best Practices" >Representation-Annotation Standards/Best Practices</option>

<option value="Language Resources/Technologies Infrastructure" >Language Resources/Technologies Infrastructure</option>

</optgroup>
<optgroup label="Evaluation">
<option value="Evaluation Data" >Evaluation Data</option>

<option value="Evaluation Tool" >Evaluation Tool</option>

<option value="Evaluation Package" >Evaluation Package</option>

<option value="Evaluation Methodology/Standards/Guidelines" >Evaluation Methodology/Standards/Guidelines</option>

</optgroup>

<optgroup label="Other">
<option value="Other" >Other - please specify &rarr;</option>

</optgroup>

</select>
</td>


<td style="width: 40%" id=resOther11>
<input type=text
       style="width: 100%;"
       name='RMap{11}{TypeOther}'
       value=""
       size="50"
       id=RNO11>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RNO11").autocomplete({
      source: database_ResourceType_other
    });
  });
</script>

</td>
</tr>




<tr>
<td align="right" valign=center><span id='redsize11'>Size:</span></td>
<td align="left" valign=center>
  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{11}{Size}' value="" size="40" id=RSIZE11 type="text">
      </td>
      <td>
	<select name='RMap{11}{Unit}' 
		size="1" 
		id=RSIZEU11 
		style="width: 100%;">
	  <option value="" selected>-- Please select a value --</option>

	  <option value="KByte  " >KByte  </option>

<option value="MByte  " >MByte  </option>

<option value="GByte  " >GByte  </option>

<option value="sentences  " >sentences  </option>

<option value="words  " >words  </option>

<option value="tokens  " >tokens  </option>

<option value="entries  " >entries  </option>

<option value="lexemes  " >lexemes  </option>

<option value="synsets  " >synsets  </option>

<option value="concepts " >concepts </option>

<option value="rules   " >rules   </option>

<option value="hours  " >hours  </option>

<option value="minutes  " >minutes  </option>

	  <option value="Other" >Other - please specify next to the size value</option>

	</select>
      </td>
    </tr>
  </table>
</td>


<td style="width: 40%" valign=top>
<em>Enter package size - a number and a relevant unit of measurement, e.g., 48Mbyte, 
5500&nbsp;lexemes, 2.1Gbyte.</em>
</td>
</tr>



<tr>

<td align="right"><span id='redstatus11'>Resource Production Status:</span></td>
<td align="left">
<select name='RMap{11}{Status}' 
        size="1" 
        onchange="handleOtherField('RS11','rsOther11','RSO11')"
        id=RS11 
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Newly created-finished" >Newly created-finished</option>

<option value="Newly created-in progress" >Newly created-in progress</option>

<option value="Existing-used" >Existing-used</option>

<option value="Existing-updated" >Existing-updated</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>


<td style="width: 40%" id=rsOther11>
<input type=text
       style="width: 100%;"
       name='RMap{11}{StatusOther}'
       value=""
       size="50"
       id=RSO11>
</td>
</tr>

<tr>
<td align=right><span id='redlang11'>Language(s):</span></td>
<td align="left">
<select name='RMap{11}{Language}' 
        size="1" 
        id=RL11 
        onchange="handleOtherLanguageField('RL11','lang11','LAO11')"
        style="width: 100%;">
<option value="P" >-- Please select a value --</option>
<option value="W" >Enter the language(s) in the box(es) below &darr;</option>
<option value="LI" >Language Independent</option>
<option value="NA" >Not Applicable</option>

</select>
</td>
<td>
</td>
</tr>

<tr>
<td></td>
<td colspan=2>
<span id=lang11>
<table border=0>
<tr>
<td>
<blockquote>
<table border=0>
  <tr>
    <td>
      Language&nbsp;1:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{11}{LanguageOther}'
	     value=""
	     size="50"
	     id=LAO11>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO11").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;2:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{11}{LanguageOther2}'
	     value=""
	     size="50"
	     id=LAO211>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO211").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;3:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{11}{LanguageOther3}'
	     value=""
	     size="50"
	     id=LAO311>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO311").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;4:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{11}{LanguageOther4}'
	     value=""
	     size="50"
	     id=LAO411>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO411").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>


  <tr>
    <td>
      Language&nbsp;5:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{11}{LanguageOther5}'
	     value=""
	     size="50"
	     id=LAO511>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO511").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Other&nbsp;Languages:
    </td>
    <td>
      <textarea style="width: 100%;"
	     name='RMap{11}{LanguageOtherN}'
	     cols="50" rows="3"
	     id=LAON11
		></textarea>
    </td>
  </tr>
</table>
</blockquote>
</td>
<td>
  <i>
    Please enter the languages that apply to your resource.<br>
    Please note that the first 3 boxes have autocompletion.<br>
    If your resource covers more than 3 languages, please use the "Other 
    languages" text area
    <b>and enter the various languages separated by a comma</b>. 
  </i>
</td>
</table>
</span>
</td>
<td width="25%">
</td>
</tr>


<tr>
<td align=right><span id='redmodality11'>Modality:</span></td>
<td align=left>
<select name='RMap{11}{Modality}' 
        size="1" 
        id=RM11 
        onchange="handleOtherField('RM11','rmOther11','RMO11')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Speech" >Speech</option>

<option value="Written" >Written</option>

<option value="Speech/Written" >Speech/Written</option>

<option value="Multimodal/Multimedia" >Multimodal/Multimedia</option>

<option value="Sign Language" >Sign Language</option>

<option value="Modality Independent" >Modality Independent</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>



<td style="width: 40%" id=rmOther11>
<input type=text
       style="width: 100%;"
       name='RMap{11}{ModalityOther}'
       value=""
       size="50"
       id=RMO11>
</td>
</tr>

<tr>
<td align=right><span id='reduse11'>Use of the Resource:</span></td>
<td align=left>
<select name='RMap{11}{Use}' 
        size="1" 
        id=RU11 
        onchange="handleOtherField('RU11','ruOther11','RUO11')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Acquisition" >Acquisition</option>

<option value="Anaphora, Coreference" >Anaphora, Coreference</option>

<option value="Corpus Creation/Annotation" >Corpus Creation/Annotation</option>

<option value="Dialogue" >Dialogue</option>

<option value="Discourse" >Discourse</option>

<option value="Document Classification, Text categorisation" >Document Classification, Text categorisation</option>

<option value="Emotion Recognition/Generation" >Emotion Recognition/Generation</option>

<option value="Evaluation/Validation" >Evaluation/Validation</option>

<option value="Information Extraction, Information Retrieval" >Information Extraction, Information Retrieval</option>

<option value="Knowledge Discovery/Representation" >Knowledge Discovery/Representation</option>

<option value="Language Identification" >Language Identification</option>

<option value="Language Modelling" >Language Modelling</option>

<option value="Lexicon Creation/Annotation" >Lexicon Creation/Annotation</option>

<option value="Machine Learning" >Machine Learning</option>

<option value="Machine Translation, SpeechToSpeech Translation" >Machine Translation, SpeechToSpeech Translation</option>

<option value="Morphological Analysis" >Morphological Analysis</option>

<option value="Multimedia Document Processing" >Multimedia Document Processing</option>

<option value="Named Entity Recognition" >Named Entity Recognition</option>

<option value="Natural Language Generation" >Natural Language Generation</option>

<option value="Opinion Mining/Sentiment Analysis" >Opinion Mining/Sentiment Analysis</option>

<option value="Parsing and Tagging" >Parsing and Tagging</option>

<option value="Person Identification" >Person Identification</option>

<option value="Question Answering" >Question Answering</option>

<option value="Semantic Role Labeling" >Semantic Role Labeling</option>

<option value="Semantic Web" >Semantic Web</option>

<option value="Sign Language Recognition/Generation" >Sign Language Recognition/Generation</option>

<option value="Speech Recognition/Understanding" >Speech Recognition/Understanding</option>

<option value="Speech Synthesis" >Speech Synthesis</option>

<option value="Summarisation" >Summarisation</option>

<option value="Text Mining" >Text Mining</option>

<option value="Textual Entailment and Paraphrasing" >Textual Entailment and Paraphrasing</option>

<option value="Topic Detection and Tracking" >Topic Detection and Tracking</option>

<option value="Voice Control" >Voice Control</option>

<option value="Web Services" >Web Services</option>

<option value="Word Sense Disambiguation" >Word Sense Disambiguation</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=ruOther11>
<input type=text
       style="width: 100%;"
       name='RMap{11}{UseOther}'
       value=""
       size="50"
       id=RUO11>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RUO11").autocomplete({
      source: database_ResourceUse_other
    });
  });
</script>

</td>
</tr>

<tr>
<td align=right><span id='redavailability11'>Resource Availability:</span></td>
<td align=left>
<select name='RMap{11}{Availability}' 
        size="1" 
        id=RA11 
        onchange="handleOtherField('RA11','raOther11','RAO11')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Freely Available" >Freely Available</option>

<option value="From Data Center(s)" >From Data Center(s)</option>

<option value="From Owner" >From Owner</option>

<option value="Not Available" >Not Available</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=raOther11>
<input type=text
       style="width: 100%;"
       name='RMap{11}{AvailabilityOther}'
       value=""
       size="50"
       id=RAO11>
</td>
</tr>



<tr>
<td align="right" valign=center><span id='redlicense11'>License:</span></td>
<td align="left" valign=center>
<input name='RMap{11}{License}' value="" size="40" id=RLIC11 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Licence type, e.g., ELRA, LDC, Gnu, CreativeCommons, OpenSource, etc.</em>
</td>
</tr>




<tr>
<td align=right><span id='redurl11'>Resource URL (if available):</span></td>
<td align=left>
<input name='RMap{11}{URL}' value="" size="40" id=RURL11 style="width: 100%;" type="text">
</td>
</tr>

<tr>
<td align="right" valign=center><span id='reddocumentation11'>Documentation:</span></td>
<td align="left" valign=center>
<input name='RMap{11}{Documentation}' value="" size="40" id=RDOC11 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Is there documentation? In which language? Is the documentation publicly available?</em>
</td>
</tr>




<tr>
<td  align=right valign=top><span id='reddescription11'>Resource Description:</span> </td>
<td colspan="2" align="left">
<textarea name='RMap{11}{Description}' rows="10" cols="70" id=RD11 style="width: 100%;"></textarea>
</td>
</tr>
<!--
<tr>
  <td colspan="3">
The section below applies only if the resource is not available 
at/from established repositories, such as ELRA, LDC, or other data 
centers, as described by the URL entered in the above form. 
ELRA encourages all authors to share the described LRs (data, toosl, services,
etc.), unless already available, to enable their reuse, replicability of experiments,
including evaluation ones, etc.

  </td>
</tr>

<tr>
  <td  align=right valign=top>Do you want to make this LR available through the
<b>"Share Your LRs" initiative?</b></td>
  <td  align="left">
    <input type="checkbox"  id="RLaccepted_11" name="RMap{11}{accepted}" onclick="Resource_manage_upload('RLaccepted_11')" >
  </td>
  <td style="width: 40%" valign=top>
    <em><b>Disclaimer:</b> By clicking the box you agree to share this LR for which rights have been cleared and guarantee that such LR is sharable under the licence you mentioned above.</em>
  </td>
</tr>


<tr>
  <td align=right valign=top><span id="filed_RLaccepted_11">You can upload your LR here if it is smaller than <b>20Mb</b>.</span></td>
  <td align="left">
    <span id="filet_RLaccepted_11">
      <input type="file" name="RMap{11}{FILE}" /> &nbsp;&nbsp;&nbsp;
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>


<tr>
  <td align=right><span id="urld_RLaccepted_11">Otherwise please provide an URL from where the LR can be downloaded:</span></td>
  <td align=left>
    <span id="urlt_RLaccepted_11">
      <input name='RMap{11}{FILEURL}' value="" size="40" id=RURL11 style="width: 100%;" type="text">
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>


-->

</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</div><div id=r12>

<table width="95%">
<tr>
<td>#12</td>

<td style="width: 100%">
<table style="width: 100%" border="1">
<tr>
<td style="width: 100%">
<table cellpadding=5 style="width: 100%">

<tr>
<td align="right">Resource Name:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{12}{Name}' 
               type="text"
               value="" 
               size="40" 
               id=RN12> 


	<script type="text/javascript">
	  jQuery(function(){
	    jQuery("#RN12").keyup(function(){setDefaultValid(12)});
            jQuery("#RN12").autocomplete({
              source: database_ResourceName,
              select: function(event, ui) { f = selectResourceNameUI("#RN12", event, ui); setDefaultValid(12); return f;}});
	  });
	</script>
      </td>
     <!-- <td>
	<input type=button onClick="setDefaultResourceData(12)" value='Load Default Values' id="setDefaultButton_3" disabled>
      </td>
      -->
    </tr>
  </table>


</td>
<!--
<td>
  <i>If the button "Load default values" is enabled you can prefill some fields with default values available in the LreMap for this resource.</i>
</td> -->
</tr>

<tr>
<td align="right">ISLRN Field:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{12}{ISLRN}' 
               type="text"
               value="" 
               size="20" 
               id=RISLRN12
	       title="Format: xxx-xxx-xxx-xxx-x"> 
      </td>
      <td>
	&nbsp;&nbsp;&nbsp;
	<input type=checkbox onClick="disableResourceILSRN(12)" value='1'
	       id="disableResourceISLRN12" name='RMap{12}{ISLRNstatus}'
	       > No ISLRN / I don't know
	<script type="text/javascript">
	  jQuery(function(){
	    disableResourceILSRN(12);
	  });
	</script>
      </td>
    </tr>
  </table>
</td>
<td>
  <i>The ISLRN is a unique and universal identification schema for LRs
    which provides LRs with unique identifier using a standardised
    nomenclature.  It also ensures that LRs are correctly identified,
    and consequently, recognised with proper references for their
    usage in applications in R&D projects, products evaluation and
    benchmark as well as in documents and scientific
    papers. Visit <a href="http://www.islrn.org"
    target=_blank>http://www.islrn.org</a>.</i>
</td>
</tr>



<tr>
<td style="width: 20%" align=right><span id="redtype12">Resource Type: </span></td>



<td style="width: 40%" align=left>
<select name='RMap{12}{Type}' 
        size=1 
        id=RT12 
        onchange="handleOtherField('RT12','resOther12','RNO12')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>


<optgroup label="Resource-Dataset">
<option value="Corpus" >Corpus</option>

<option value="Lexicon" >Lexicon</option>

<option value="Grammar/Language Model" >Grammar/Language Model</option>

<option value="Ontology" >Ontology</option>

<option value="Terminology" >Terminology</option>

<option value="Treebank" >Treebank</option>

</optgroup>
<optgroup label="Resource-Tool">
<option value="Corpus Tool" >Corpus Tool</option>

<option value="Aligner" >Aligner</option>

<option value="Annotation Tool" >Annotation Tool</option>

<option value="Language Identifier" >Language Identifier</option>

<option value="Language Modeling Tool" >Language Modeling Tool</option>

<option value="Machine Learning Tool" >Machine Learning Tool</option>

<option value="Machine Translation Tool" >Machine Translation Tool</option>

<option value="Named Entity Recognizer" >Named Entity Recognizer</option>

<option value="Sentiment Analysis Tool" >Sentiment Analysis Tool</option>

<option value="Software Toolkit" >Software Toolkit</option>

<option value="Tagger/Parser" >Tagger/Parser</option>

<option value="Tokenizer" >Tokenizer</option>

<option value="Web Service" >Web Service</option>

<option value="Word Sense Disambiguator" >Word Sense Disambiguator</option>

<option value="Prosodic Analyzer" >Prosodic Analyzer</option>

<option value="Signal Processing/Features Extraction" >Signal Processing/Features Extraction</option>

<option value="Speaker Recognizer" >Speaker Recognizer</option>

<option value="Speech Recognizer/Transcriber" >Speech Recognizer/Transcriber</option>

<option value="Spoken Dialogue Tool" >Spoken Dialogue Tool</option>

<option value="Text-to-Speech Synthesizer" >Text-to-Speech Synthesizer</option>

<option value="Image Analyzer" >Image Analyzer</option>

</optgroup>
<optgroup label="Resource-Guidelines">
<option value="Metadata" >Metadata</option>

<option value="Representation-Annotation Formalism/Guidelines" >Representation-Annotation Formalism/Guidelines</option>

<option value="Representation-Annotation Standards/Best Practices" >Representation-Annotation Standards/Best Practices</option>

<option value="Language Resources/Technologies Infrastructure" >Language Resources/Technologies Infrastructure</option>

</optgroup>
<optgroup label="Evaluation">
<option value="Evaluation Data" >Evaluation Data</option>

<option value="Evaluation Tool" >Evaluation Tool</option>

<option value="Evaluation Package" >Evaluation Package</option>

<option value="Evaluation Methodology/Standards/Guidelines" >Evaluation Methodology/Standards/Guidelines</option>

</optgroup>

<optgroup label="Other">
<option value="Other" >Other - please specify &rarr;</option>

</optgroup>

</select>
</td>


<td style="width: 40%" id=resOther12>
<input type=text
       style="width: 100%;"
       name='RMap{12}{TypeOther}'
       value=""
       size="50"
       id=RNO12>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RNO12").autocomplete({
      source: database_ResourceType_other
    });
  });
</script>

</td>
</tr>




<tr>
<td align="right" valign=center><span id='redsize12'>Size:</span></td>
<td align="left" valign=center>
  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{12}{Size}' value="" size="40" id=RSIZE12 type="text">
      </td>
      <td>
	<select name='RMap{12}{Unit}' 
		size="1" 
		id=RSIZEU12 
		style="width: 100%;">
	  <option value="" selected>-- Please select a value --</option>

	  <option value="KByte  " >KByte  </option>

<option value="MByte  " >MByte  </option>

<option value="GByte  " >GByte  </option>

<option value="sentences  " >sentences  </option>

<option value="words  " >words  </option>

<option value="tokens  " >tokens  </option>

<option value="entries  " >entries  </option>

<option value="lexemes  " >lexemes  </option>

<option value="synsets  " >synsets  </option>

<option value="concepts " >concepts </option>

<option value="rules   " >rules   </option>

<option value="hours  " >hours  </option>

<option value="minutes  " >minutes  </option>

	  <option value="Other" >Other - please specify next to the size value</option>

	</select>
      </td>
    </tr>
  </table>
</td>


<td style="width: 40%" valign=top>
<em>Enter package size - a number and a relevant unit of measurement, e.g., 48Mbyte, 
5500&nbsp;lexemes, 2.1Gbyte.</em>
</td>
</tr>



<tr>

<td align="right"><span id='redstatus12'>Resource Production Status:</span></td>
<td align="left">
<select name='RMap{12}{Status}' 
        size="1" 
        onchange="handleOtherField('RS12','rsOther12','RSO12')"
        id=RS12 
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Newly created-finished" >Newly created-finished</option>

<option value="Newly created-in progress" >Newly created-in progress</option>

<option value="Existing-used" >Existing-used</option>

<option value="Existing-updated" >Existing-updated</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>


<td style="width: 40%" id=rsOther12>
<input type=text
       style="width: 100%;"
       name='RMap{12}{StatusOther}'
       value=""
       size="50"
       id=RSO12>
</td>
</tr>

<tr>
<td align=right><span id='redlang12'>Language(s):</span></td>
<td align="left">
<select name='RMap{12}{Language}' 
        size="1" 
        id=RL12 
        onchange="handleOtherLanguageField('RL12','lang12','LAO12')"
        style="width: 100%;">
<option value="P" >-- Please select a value --</option>
<option value="W" >Enter the language(s) in the box(es) below &darr;</option>
<option value="LI" >Language Independent</option>
<option value="NA" >Not Applicable</option>

</select>
</td>
<td>
</td>
</tr>

<tr>
<td></td>
<td colspan=2>
<span id=lang12>
<table border=0>
<tr>
<td>
<blockquote>
<table border=0>
  <tr>
    <td>
      Language&nbsp;1:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{12}{LanguageOther}'
	     value=""
	     size="50"
	     id=LAO12>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO12").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;2:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{12}{LanguageOther2}'
	     value=""
	     size="50"
	     id=LAO212>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO212").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;3:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{12}{LanguageOther3}'
	     value=""
	     size="50"
	     id=LAO312>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO312").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;4:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{12}{LanguageOther4}'
	     value=""
	     size="50"
	     id=LAO412>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO412").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>


  <tr>
    <td>
      Language&nbsp;5:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{12}{LanguageOther5}'
	     value=""
	     size="50"
	     id=LAO512>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO512").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Other&nbsp;Languages:
    </td>
    <td>
      <textarea style="width: 100%;"
	     name='RMap{12}{LanguageOtherN}'
	     cols="50" rows="3"
	     id=LAON12
		></textarea>
    </td>
  </tr>
</table>
</blockquote>
</td>
<td>
  <i>
    Please enter the languages that apply to your resource.<br>
    Please note that the first 3 boxes have autocompletion.<br>
    If your resource covers more than 3 languages, please use the "Other 
    languages" text area
    <b>and enter the various languages separated by a comma</b>. 
  </i>
</td>
</table>
</span>
</td>
<td width="25%">
</td>
</tr>


<tr>
<td align=right><span id='redmodality12'>Modality:</span></td>
<td align=left>
<select name='RMap{12}{Modality}' 
        size="1" 
        id=RM12 
        onchange="handleOtherField('RM12','rmOther12','RMO12')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Speech" >Speech</option>

<option value="Written" >Written</option>

<option value="Speech/Written" >Speech/Written</option>

<option value="Multimodal/Multimedia" >Multimodal/Multimedia</option>

<option value="Sign Language" >Sign Language</option>

<option value="Modality Independent" >Modality Independent</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>



<td style="width: 40%" id=rmOther12>
<input type=text
       style="width: 100%;"
       name='RMap{12}{ModalityOther}'
       value=""
       size="50"
       id=RMO12>
</td>
</tr>

<tr>
<td align=right><span id='reduse12'>Use of the Resource:</span></td>
<td align=left>
<select name='RMap{12}{Use}' 
        size="1" 
        id=RU12 
        onchange="handleOtherField('RU12','ruOther12','RUO12')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Acquisition" >Acquisition</option>

<option value="Anaphora, Coreference" >Anaphora, Coreference</option>

<option value="Corpus Creation/Annotation" >Corpus Creation/Annotation</option>

<option value="Dialogue" >Dialogue</option>

<option value="Discourse" >Discourse</option>

<option value="Document Classification, Text categorisation" >Document Classification, Text categorisation</option>

<option value="Emotion Recognition/Generation" >Emotion Recognition/Generation</option>

<option value="Evaluation/Validation" >Evaluation/Validation</option>

<option value="Information Extraction, Information Retrieval" >Information Extraction, Information Retrieval</option>

<option value="Knowledge Discovery/Representation" >Knowledge Discovery/Representation</option>

<option value="Language Identification" >Language Identification</option>

<option value="Language Modelling" >Language Modelling</option>

<option value="Lexicon Creation/Annotation" >Lexicon Creation/Annotation</option>

<option value="Machine Learning" >Machine Learning</option>

<option value="Machine Translation, SpeechToSpeech Translation" >Machine Translation, SpeechToSpeech Translation</option>

<option value="Morphological Analysis" >Morphological Analysis</option>

<option value="Multimedia Document Processing" >Multimedia Document Processing</option>

<option value="Named Entity Recognition" >Named Entity Recognition</option>

<option value="Natural Language Generation" >Natural Language Generation</option>

<option value="Opinion Mining/Sentiment Analysis" >Opinion Mining/Sentiment Analysis</option>

<option value="Parsing and Tagging" >Parsing and Tagging</option>

<option value="Person Identification" >Person Identification</option>

<option value="Question Answering" >Question Answering</option>

<option value="Semantic Role Labeling" >Semantic Role Labeling</option>

<option value="Semantic Web" >Semantic Web</option>

<option value="Sign Language Recognition/Generation" >Sign Language Recognition/Generation</option>

<option value="Speech Recognition/Understanding" >Speech Recognition/Understanding</option>

<option value="Speech Synthesis" >Speech Synthesis</option>

<option value="Summarisation" >Summarisation</option>

<option value="Text Mining" >Text Mining</option>

<option value="Textual Entailment and Paraphrasing" >Textual Entailment and Paraphrasing</option>

<option value="Topic Detection and Tracking" >Topic Detection and Tracking</option>

<option value="Voice Control" >Voice Control</option>

<option value="Web Services" >Web Services</option>

<option value="Word Sense Disambiguation" >Word Sense Disambiguation</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=ruOther12>
<input type=text
       style="width: 100%;"
       name='RMap{12}{UseOther}'
       value=""
       size="50"
       id=RUO12>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RUO12").autocomplete({
      source: database_ResourceUse_other
    });
  });
</script>

</td>
</tr>

<tr>
<td align=right><span id='redavailability12'>Resource Availability:</span></td>
<td align=left>
<select name='RMap{12}{Availability}' 
        size="1" 
        id=RA12 
        onchange="handleOtherField('RA12','raOther12','RAO12')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Freely Available" >Freely Available</option>

<option value="From Data Center(s)" >From Data Center(s)</option>

<option value="From Owner" >From Owner</option>

<option value="Not Available" >Not Available</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=raOther12>
<input type=text
       style="width: 100%;"
       name='RMap{12}{AvailabilityOther}'
       value=""
       size="50"
       id=RAO12>
</td>
</tr>



<tr>
<td align="right" valign=center><span id='redlicense12'>License:</span></td>
<td align="left" valign=center>
<input name='RMap{12}{License}' value="" size="40" id=RLIC12 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Licence type, e.g., ELRA, LDC, Gnu, CreativeCommons, OpenSource, etc.</em>
</td>
</tr>




<tr>
<td align=right><span id='redurl12'>Resource URL (if available):</span></td>
<td align=left>
<input name='RMap{12}{URL}' value="" size="40" id=RURL12 style="width: 100%;" type="text">
</td>
</tr>

<tr>
<td align="right" valign=center><span id='reddocumentation12'>Documentation:</span></td>
<td align="left" valign=center>
<input name='RMap{12}{Documentation}' value="" size="40" id=RDOC12 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Is there documentation? In which language? Is the documentation publicly available?</em>
</td>
</tr>




<tr>
<td  align=right valign=top><span id='reddescription12'>Resource Description:</span> </td>
<td colspan="2" align="left">
<textarea name='RMap{12}{Description}' rows="10" cols="70" id=RD12 style="width: 100%;"></textarea>
</td>
</tr>
<!--
<tr>
  <td colspan="3">
The section below applies only if the resource is not available 
at/from established repositories, such as ELRA, LDC, or other data 
centers, as described by the URL entered in the above form. 
ELRA encourages all authors to share the described LRs (data, toosl, services,
etc.), unless already available, to enable their reuse, replicability of experiments,
including evaluation ones, etc.

  </td>
</tr>

<tr>
  <td  align=right valign=top>Do you want to make this LR available through the
<b>"Share Your LRs" initiative?</b></td>
  <td  align="left">
    <input type="checkbox"  id="RLaccepted_12" name="RMap{12}{accepted}" onclick="Resource_manage_upload('RLaccepted_12')" >
  </td>
  <td style="width: 40%" valign=top>
    <em><b>Disclaimer:</b> By clicking the box you agree to share this LR for which rights have been cleared and guarantee that such LR is sharable under the licence you mentioned above.</em>
  </td>
</tr>


<tr>
  <td align=right valign=top><span id="filed_RLaccepted_12">You can upload your LR here if it is smaller than <b>20Mb</b>.</span></td>
  <td align="left">
    <span id="filet_RLaccepted_12">
      <input type="file" name="RMap{12}{FILE}" /> &nbsp;&nbsp;&nbsp;
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>


<tr>
  <td align=right><span id="urld_RLaccepted_12">Otherwise please provide an URL from where the LR can be downloaded:</span></td>
  <td align=left>
    <span id="urlt_RLaccepted_12">
      <input name='RMap{12}{FILEURL}' value="" size="40" id=RURL12 style="width: 100%;" type="text">
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>


-->

</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</div><div id=r13>

<table width="95%">
<tr>
<td>#13</td>

<td style="width: 100%">
<table style="width: 100%" border="1">
<tr>
<td style="width: 100%">
<table cellpadding=5 style="width: 100%">

<tr>
<td align="right">Resource Name:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{13}{Name}' 
               type="text"
               value="" 
               size="40" 
               id=RN13> 


	<script type="text/javascript">
	  jQuery(function(){
	    jQuery("#RN13").keyup(function(){setDefaultValid(13)});
            jQuery("#RN13").autocomplete({
              source: database_ResourceName,
              select: function(event, ui) { f = selectResourceNameUI("#RN13", event, ui); setDefaultValid(13); return f;}});
	  });
	</script>
      </td>
     <!-- <td>
	<input type=button onClick="setDefaultResourceData(13)" value='Load Default Values' id="setDefaultButton_3" disabled>
      </td>
      -->
    </tr>
  </table>


</td>
<!--
<td>
  <i>If the button "Load default values" is enabled you can prefill some fields with default values available in the LreMap for this resource.</i>
</td> -->
</tr>

<tr>
<td align="right">ISLRN Field:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{13}{ISLRN}' 
               type="text"
               value="" 
               size="20" 
               id=RISLRN13
	       title="Format: xxx-xxx-xxx-xxx-x"> 
      </td>
      <td>
	&nbsp;&nbsp;&nbsp;
	<input type=checkbox onClick="disableResourceILSRN(13)" value='1'
	       id="disableResourceISLRN13" name='RMap{13}{ISLRNstatus}'
	       > No ISLRN / I don't know
	<script type="text/javascript">
	  jQuery(function(){
	    disableResourceILSRN(13);
	  });
	</script>
      </td>
    </tr>
  </table>
</td>
<td>
  <i>The ISLRN is a unique and universal identification schema for LRs
    which provides LRs with unique identifier using a standardised
    nomenclature.  It also ensures that LRs are correctly identified,
    and consequently, recognised with proper references for their
    usage in applications in R&D projects, products evaluation and
    benchmark as well as in documents and scientific
    papers. Visit <a href="http://www.islrn.org"
    target=_blank>http://www.islrn.org</a>.</i>
</td>
</tr>



<tr>
<td style="width: 20%" align=right><span id="redtype13">Resource Type: </span></td>



<td style="width: 40%" align=left>
<select name='RMap{13}{Type}' 
        size=1 
        id=RT13 
        onchange="handleOtherField('RT13','resOther13','RNO13')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>


<optgroup label="Resource-Dataset">
<option value="Corpus" >Corpus</option>

<option value="Lexicon" >Lexicon</option>

<option value="Grammar/Language Model" >Grammar/Language Model</option>

<option value="Ontology" >Ontology</option>

<option value="Terminology" >Terminology</option>

<option value="Treebank" >Treebank</option>

</optgroup>
<optgroup label="Resource-Tool">
<option value="Corpus Tool" >Corpus Tool</option>

<option value="Aligner" >Aligner</option>

<option value="Annotation Tool" >Annotation Tool</option>

<option value="Language Identifier" >Language Identifier</option>

<option value="Language Modeling Tool" >Language Modeling Tool</option>

<option value="Machine Learning Tool" >Machine Learning Tool</option>

<option value="Machine Translation Tool" >Machine Translation Tool</option>

<option value="Named Entity Recognizer" >Named Entity Recognizer</option>

<option value="Sentiment Analysis Tool" >Sentiment Analysis Tool</option>

<option value="Software Toolkit" >Software Toolkit</option>

<option value="Tagger/Parser" >Tagger/Parser</option>

<option value="Tokenizer" >Tokenizer</option>

<option value="Web Service" >Web Service</option>

<option value="Word Sense Disambiguator" >Word Sense Disambiguator</option>

<option value="Prosodic Analyzer" >Prosodic Analyzer</option>

<option value="Signal Processing/Features Extraction" >Signal Processing/Features Extraction</option>

<option value="Speaker Recognizer" >Speaker Recognizer</option>

<option value="Speech Recognizer/Transcriber" >Speech Recognizer/Transcriber</option>

<option value="Spoken Dialogue Tool" >Spoken Dialogue Tool</option>

<option value="Text-to-Speech Synthesizer" >Text-to-Speech Synthesizer</option>

<option value="Image Analyzer" >Image Analyzer</option>

</optgroup>
<optgroup label="Resource-Guidelines">
<option value="Metadata" >Metadata</option>

<option value="Representation-Annotation Formalism/Guidelines" >Representation-Annotation Formalism/Guidelines</option>

<option value="Representation-Annotation Standards/Best Practices" >Representation-Annotation Standards/Best Practices</option>

<option value="Language Resources/Technologies Infrastructure" >Language Resources/Technologies Infrastructure</option>

</optgroup>
<optgroup label="Evaluation">
<option value="Evaluation Data" >Evaluation Data</option>

<option value="Evaluation Tool" >Evaluation Tool</option>

<option value="Evaluation Package" >Evaluation Package</option>

<option value="Evaluation Methodology/Standards/Guidelines" >Evaluation Methodology/Standards/Guidelines</option>

</optgroup>

<optgroup label="Other">
<option value="Other" >Other - please specify &rarr;</option>

</optgroup>

</select>
</td>


<td style="width: 40%" id=resOther13>
<input type=text
       style="width: 100%;"
       name='RMap{13}{TypeOther}'
       value=""
       size="50"
       id=RNO13>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RNO13").autocomplete({
      source: database_ResourceType_other
    });
  });
</script>

</td>
</tr>




<tr>
<td align="right" valign=center><span id='redsize13'>Size:</span></td>
<td align="left" valign=center>
  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{13}{Size}' value="" size="40" id=RSIZE13 type="text">
      </td>
      <td>
	<select name='RMap{13}{Unit}' 
		size="1" 
		id=RSIZEU13 
		style="width: 100%;">
	  <option value="" selected>-- Please select a value --</option>

	  <option value="KByte  " >KByte  </option>

<option value="MByte  " >MByte  </option>

<option value="GByte  " >GByte  </option>

<option value="sentences  " >sentences  </option>

<option value="words  " >words  </option>

<option value="tokens  " >tokens  </option>

<option value="entries  " >entries  </option>

<option value="lexemes  " >lexemes  </option>

<option value="synsets  " >synsets  </option>

<option value="concepts " >concepts </option>

<option value="rules   " >rules   </option>

<option value="hours  " >hours  </option>

<option value="minutes  " >minutes  </option>

	  <option value="Other" >Other - please specify next to the size value</option>

	</select>
      </td>
    </tr>
  </table>
</td>


<td style="width: 40%" valign=top>
<em>Enter package size - a number and a relevant unit of measurement, e.g., 48Mbyte, 
5500&nbsp;lexemes, 2.1Gbyte.</em>
</td>
</tr>



<tr>

<td align="right"><span id='redstatus13'>Resource Production Status:</span></td>
<td align="left">
<select name='RMap{13}{Status}' 
        size="1" 
        onchange="handleOtherField('RS13','rsOther13','RSO13')"
        id=RS13 
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Newly created-finished" >Newly created-finished</option>

<option value="Newly created-in progress" >Newly created-in progress</option>

<option value="Existing-used" >Existing-used</option>

<option value="Existing-updated" >Existing-updated</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>


<td style="width: 40%" id=rsOther13>
<input type=text
       style="width: 100%;"
       name='RMap{13}{StatusOther}'
       value=""
       size="50"
       id=RSO13>
</td>
</tr>

<tr>
<td align=right><span id='redlang13'>Language(s):</span></td>
<td align="left">
<select name='RMap{13}{Language}' 
        size="1" 
        id=RL13 
        onchange="handleOtherLanguageField('RL13','lang13','LAO13')"
        style="width: 100%;">
<option value="P" >-- Please select a value --</option>
<option value="W" >Enter the language(s) in the box(es) below &darr;</option>
<option value="LI" >Language Independent</option>
<option value="NA" >Not Applicable</option>

</select>
</td>
<td>
</td>
</tr>

<tr>
<td></td>
<td colspan=2>
<span id=lang13>
<table border=0>
<tr>
<td>
<blockquote>
<table border=0>
  <tr>
    <td>
      Language&nbsp;1:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{13}{LanguageOther}'
	     value=""
	     size="50"
	     id=LAO13>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO13").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;2:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{13}{LanguageOther2}'
	     value=""
	     size="50"
	     id=LAO213>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO213").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;3:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{13}{LanguageOther3}'
	     value=""
	     size="50"
	     id=LAO313>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO313").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;4:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{13}{LanguageOther4}'
	     value=""
	     size="50"
	     id=LAO413>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO413").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>


  <tr>
    <td>
      Language&nbsp;5:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{13}{LanguageOther5}'
	     value=""
	     size="50"
	     id=LAO513>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO513").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Other&nbsp;Languages:
    </td>
    <td>
      <textarea style="width: 100%;"
	     name='RMap{13}{LanguageOtherN}'
	     cols="50" rows="3"
	     id=LAON13
		></textarea>
    </td>
  </tr>
</table>
</blockquote>
</td>
<td>
  <i>
    Please enter the languages that apply to your resource.<br>
    Please note that the first 3 boxes have autocompletion.<br>
    If your resource covers more than 3 languages, please use the "Other 
    languages" text area
    <b>and enter the various languages separated by a comma</b>. 
  </i>
</td>
</table>
</span>
</td>
<td width="25%">
</td>
</tr>


<tr>
<td align=right><span id='redmodality13'>Modality:</span></td>
<td align=left>
<select name='RMap{13}{Modality}' 
        size="1" 
        id=RM13 
        onchange="handleOtherField('RM13','rmOther13','RMO13')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Speech" >Speech</option>

<option value="Written" >Written</option>

<option value="Speech/Written" >Speech/Written</option>

<option value="Multimodal/Multimedia" >Multimodal/Multimedia</option>

<option value="Sign Language" >Sign Language</option>

<option value="Modality Independent" >Modality Independent</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>



<td style="width: 40%" id=rmOther13>
<input type=text
       style="width: 100%;"
       name='RMap{13}{ModalityOther}'
       value=""
       size="50"
       id=RMO13>
</td>
</tr>

<tr>
<td align=right><span id='reduse13'>Use of the Resource:</span></td>
<td align=left>
<select name='RMap{13}{Use}' 
        size="1" 
        id=RU13 
        onchange="handleOtherField('RU13','ruOther13','RUO13')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Acquisition" >Acquisition</option>

<option value="Anaphora, Coreference" >Anaphora, Coreference</option>

<option value="Corpus Creation/Annotation" >Corpus Creation/Annotation</option>

<option value="Dialogue" >Dialogue</option>

<option value="Discourse" >Discourse</option>

<option value="Document Classification, Text categorisation" >Document Classification, Text categorisation</option>

<option value="Emotion Recognition/Generation" >Emotion Recognition/Generation</option>

<option value="Evaluation/Validation" >Evaluation/Validation</option>

<option value="Information Extraction, Information Retrieval" >Information Extraction, Information Retrieval</option>

<option value="Knowledge Discovery/Representation" >Knowledge Discovery/Representation</option>

<option value="Language Identification" >Language Identification</option>

<option value="Language Modelling" >Language Modelling</option>

<option value="Lexicon Creation/Annotation" >Lexicon Creation/Annotation</option>

<option value="Machine Learning" >Machine Learning</option>

<option value="Machine Translation, SpeechToSpeech Translation" >Machine Translation, SpeechToSpeech Translation</option>

<option value="Morphological Analysis" >Morphological Analysis</option>

<option value="Multimedia Document Processing" >Multimedia Document Processing</option>

<option value="Named Entity Recognition" >Named Entity Recognition</option>

<option value="Natural Language Generation" >Natural Language Generation</option>

<option value="Opinion Mining/Sentiment Analysis" >Opinion Mining/Sentiment Analysis</option>

<option value="Parsing and Tagging" >Parsing and Tagging</option>

<option value="Person Identification" >Person Identification</option>

<option value="Question Answering" >Question Answering</option>

<option value="Semantic Role Labeling" >Semantic Role Labeling</option>

<option value="Semantic Web" >Semantic Web</option>

<option value="Sign Language Recognition/Generation" >Sign Language Recognition/Generation</option>

<option value="Speech Recognition/Understanding" >Speech Recognition/Understanding</option>

<option value="Speech Synthesis" >Speech Synthesis</option>

<option value="Summarisation" >Summarisation</option>

<option value="Text Mining" >Text Mining</option>

<option value="Textual Entailment and Paraphrasing" >Textual Entailment and Paraphrasing</option>

<option value="Topic Detection and Tracking" >Topic Detection and Tracking</option>

<option value="Voice Control" >Voice Control</option>

<option value="Web Services" >Web Services</option>

<option value="Word Sense Disambiguation" >Word Sense Disambiguation</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=ruOther13>
<input type=text
       style="width: 100%;"
       name='RMap{13}{UseOther}'
       value=""
       size="50"
       id=RUO13>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RUO13").autocomplete({
      source: database_ResourceUse_other
    });
  });
</script>

</td>
</tr>

<tr>
<td align=right><span id='redavailability13'>Resource Availability:</span></td>
<td align=left>
<select name='RMap{13}{Availability}' 
        size="1" 
        id=RA13 
        onchange="handleOtherField('RA13','raOther13','RAO13')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Freely Available" >Freely Available</option>

<option value="From Data Center(s)" >From Data Center(s)</option>

<option value="From Owner" >From Owner</option>

<option value="Not Available" >Not Available</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=raOther13>
<input type=text
       style="width: 100%;"
       name='RMap{13}{AvailabilityOther}'
       value=""
       size="50"
       id=RAO13>
</td>
</tr>



<tr>
<td align="right" valign=center><span id='redlicense13'>License:</span></td>
<td align="left" valign=center>
<input name='RMap{13}{License}' value="" size="40" id=RLIC13 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Licence type, e.g., ELRA, LDC, Gnu, CreativeCommons, OpenSource, etc.</em>
</td>
</tr>




<tr>
<td align=right><span id='redurl13'>Resource URL (if available):</span></td>
<td align=left>
<input name='RMap{13}{URL}' value="" size="40" id=RURL13 style="width: 100%;" type="text">
</td>
</tr>

<tr>
<td align="right" valign=center><span id='reddocumentation13'>Documentation:</span></td>
<td align="left" valign=center>
<input name='RMap{13}{Documentation}' value="" size="40" id=RDOC13 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Is there documentation? In which language? Is the documentation publicly available?</em>
</td>
</tr>




<tr>
<td  align=right valign=top><span id='reddescription13'>Resource Description:</span> </td>
<td colspan="2" align="left">
<textarea name='RMap{13}{Description}' rows="10" cols="70" id=RD13 style="width: 100%;"></textarea>
</td>
</tr>
<!--
<tr>
  <td colspan="3">
The section below applies only if the resource is not available 
at/from established repositories, such as ELRA, LDC, or other data 
centers, as described by the URL entered in the above form. 
ELRA encourages all authors to share the described LRs (data, toosl, services,
etc.), unless already available, to enable their reuse, replicability of experiments,
including evaluation ones, etc.

  </td>
</tr>

<tr>
  <td  align=right valign=top>Do you want to make this LR available through the
<b>"Share Your LRs" initiative?</b></td>
  <td  align="left">
    <input type="checkbox"  id="RLaccepted_13" name="RMap{13}{accepted}" onclick="Resource_manage_upload('RLaccepted_13')" >
  </td>
  <td style="width: 40%" valign=top>
    <em><b>Disclaimer:</b> By clicking the box you agree to share this LR for which rights have been cleared and guarantee that such LR is sharable under the licence you mentioned above.</em>
  </td>
</tr>


<tr>
  <td align=right valign=top><span id="filed_RLaccepted_13">You can upload your LR here if it is smaller than <b>20Mb</b>.</span></td>
  <td align="left">
    <span id="filet_RLaccepted_13">
      <input type="file" name="RMap{13}{FILE}" /> &nbsp;&nbsp;&nbsp;
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>


<tr>
  <td align=right><span id="urld_RLaccepted_13">Otherwise please provide an URL from where the LR can be downloaded:</span></td>
  <td align=left>
    <span id="urlt_RLaccepted_13">
      <input name='RMap{13}{FILEURL}' value="" size="40" id=RURL13 style="width: 100%;" type="text">
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>


-->

</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</div><div id=r14>

<table width="95%">
<tr>
<td>#14</td>

<td style="width: 100%">
<table style="width: 100%" border="1">
<tr>
<td style="width: 100%">
<table cellpadding=5 style="width: 100%">

<tr>
<td align="right">Resource Name:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{14}{Name}' 
               type="text"
               value="" 
               size="40" 
               id=RN14> 


	<script type="text/javascript">
	  jQuery(function(){
	    jQuery("#RN14").keyup(function(){setDefaultValid(14)});
            jQuery("#RN14").autocomplete({
              source: database_ResourceName,
              select: function(event, ui) { f = selectResourceNameUI("#RN14", event, ui); setDefaultValid(14); return f;}});
	  });
	</script>
      </td>
   <!-- <td>
	<input type=button onClick="setDefaultResourceData(14)" value='Load Default Values' id="setDefaultButton_3" disabled>
      </td>
      -->
    </tr>
  </table>


</td>
<!--
<td>
  <i>If the button "Load default values" is enabled you can prefill some fields with default values available in the LreMap for this resource.</i>
</td> -->
</tr>

<tr>
<td align="right">ISLRN Field:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{14}{ISLRN}' 
               type="text"
               value="" 
               size="20" 
               id=RISLRN14
	       title="Format: xxx-xxx-xxx-xxx-x"> 
      </td>
      <td>
	&nbsp;&nbsp;&nbsp;
	<input type=checkbox onClick="disableResourceILSRN(14)" value='1'
	       id="disableResourceISLRN14" name='RMap{14}{ISLRNstatus}'
	       > No ISLRN / I don't know
	<script type="text/javascript">
	  jQuery(function(){
	    disableResourceILSRN(14);
	  });
	</script>
      </td>
    </tr>
  </table>
</td>
<td>
  <i>The ISLRN is a unique and universal identification schema for LRs
    which provides LRs with unique identifier using a standardised
    nomenclature.  It also ensures that LRs are correctly identified,
    and consequently, recognised with proper references for their
    usage in applications in R&D projects, products evaluation and
    benchmark as well as in documents and scientific
    papers. Visit <a href="http://www.islrn.org"
    target=_blank>http://www.islrn.org</a>.</i>
</td>
</tr>



<tr>
<td style="width: 20%" align=right><span id="redtype14">Resource Type: </span></td>



<td style="width: 40%" align=left>
<select name='RMap{14}{Type}' 
        size=1 
        id=RT14 
        onchange="handleOtherField('RT14','resOther14','RNO14')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>


<optgroup label="Resource-Dataset">
<option value="Corpus" >Corpus</option>

<option value="Lexicon" >Lexicon</option>

<option value="Grammar/Language Model" >Grammar/Language Model</option>

<option value="Ontology" >Ontology</option>

<option value="Terminology" >Terminology</option>

<option value="Treebank" >Treebank</option>

</optgroup>
<optgroup label="Resource-Tool">
<option value="Corpus Tool" >Corpus Tool</option>

<option value="Aligner" >Aligner</option>

<option value="Annotation Tool" >Annotation Tool</option>

<option value="Language Identifier" >Language Identifier</option>

<option value="Language Modeling Tool" >Language Modeling Tool</option>

<option value="Machine Learning Tool" >Machine Learning Tool</option>

<option value="Machine Translation Tool" >Machine Translation Tool</option>

<option value="Named Entity Recognizer" >Named Entity Recognizer</option>

<option value="Sentiment Analysis Tool" >Sentiment Analysis Tool</option>

<option value="Software Toolkit" >Software Toolkit</option>

<option value="Tagger/Parser" >Tagger/Parser</option>

<option value="Tokenizer" >Tokenizer</option>

<option value="Web Service" >Web Service</option>

<option value="Word Sense Disambiguator" >Word Sense Disambiguator</option>

<option value="Prosodic Analyzer" >Prosodic Analyzer</option>

<option value="Signal Processing/Features Extraction" >Signal Processing/Features Extraction</option>

<option value="Speaker Recognizer" >Speaker Recognizer</option>

<option value="Speech Recognizer/Transcriber" >Speech Recognizer/Transcriber</option>

<option value="Spoken Dialogue Tool" >Spoken Dialogue Tool</option>

<option value="Text-to-Speech Synthesizer" >Text-to-Speech Synthesizer</option>

<option value="Image Analyzer" >Image Analyzer</option>

</optgroup>
<optgroup label="Resource-Guidelines">
<option value="Metadata" >Metadata</option>

<option value="Representation-Annotation Formalism/Guidelines" >Representation-Annotation Formalism/Guidelines</option>

<option value="Representation-Annotation Standards/Best Practices" >Representation-Annotation Standards/Best Practices</option>

<option value="Language Resources/Technologies Infrastructure" >Language Resources/Technologies Infrastructure</option>

</optgroup>
<optgroup label="Evaluation">
<option value="Evaluation Data" >Evaluation Data</option>

<option value="Evaluation Tool" >Evaluation Tool</option>

<option value="Evaluation Package" >Evaluation Package</option>

<option value="Evaluation Methodology/Standards/Guidelines" >Evaluation Methodology/Standards/Guidelines</option>

</optgroup>

<optgroup label="Other">
<option value="Other" >Other - please specify &rarr;</option>

</optgroup>

</select>
</td>


<td style="width: 40%" id=resOther14>
<input type=text
       style="width: 100%;"
       name='RMap{14}{TypeOther}'
       value=""
       size="50"
       id=RNO14>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RNO14").autocomplete({
      source: database_ResourceType_other
    });
  });
</script>

</td>
</tr>




<tr>
<td align="right" valign=center><span id='redsize14'>Size:</span></td>
<td align="left" valign=center>
  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{14}{Size}' value="" size="40" id=RSIZE14 type="text">
      </td>
      <td>
	<select name='RMap{14}{Unit}' 
		size="1" 
		id=RSIZEU14 
		style="width: 100%;">
	  <option value="" selected>-- Please select a value --</option>

	  <option value="KByte  " >KByte  </option>

<option value="MByte  " >MByte  </option>

<option value="GByte  " >GByte  </option>

<option value="sentences  " >sentences  </option>

<option value="words  " >words  </option>

<option value="tokens  " >tokens  </option>

<option value="entries  " >entries  </option>

<option value="lexemes  " >lexemes  </option>

<option value="synsets  " >synsets  </option>

<option value="concepts " >concepts </option>

<option value="rules   " >rules   </option>

<option value="hours  " >hours  </option>

<option value="minutes  " >minutes  </option>

	  <option value="Other" >Other - please specify next to the size value</option>

	</select>
      </td>
    </tr>
  </table>
</td>


<td style="width: 40%" valign=top>
<em>Enter package size - a number and a relevant unit of measurement, e.g., 48Mbyte, 
5500&nbsp;lexemes, 2.1Gbyte.</em>
</td>
</tr>



<tr>

<td align="right"><span id='redstatus14'>Resource Production Status:</span></td>
<td align="left">
<select name='RMap{14}{Status}' 
        size="1" 
        onchange="handleOtherField('RS14','rsOther14','RSO14')"
        id=RS14 
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Newly created-finished" >Newly created-finished</option>

<option value="Newly created-in progress" >Newly created-in progress</option>

<option value="Existing-used" >Existing-used</option>

<option value="Existing-updated" >Existing-updated</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>


<td style="width: 40%" id=rsOther14>
<input type=text
       style="width: 100%;"
       name='RMap{14}{StatusOther}'
       value=""
       size="50"
       id=RSO14>
</td>
</tr>

<tr>
<td align=right><span id='redlang14'>Language(s):</span></td>
<td align="left">
<select name='RMap{14}{Language}' 
        size="1" 
        id=RL14 
        onchange="handleOtherLanguageField('RL14','lang14','LAO14')"
        style="width: 100%;">
<option value="P" >-- Please select a value --</option>
<option value="W" >Enter the language(s) in the box(es) below &darr;</option>
<option value="LI" >Language Independent</option>
<option value="NA" >Not Applicable</option>

</select>
</td>
<td>
</td>
</tr>

<tr>
<td></td>
<td colspan=2>
<span id=lang14>
<table border=0>
<tr>
<td>
<blockquote>
<table border=0>
  <tr>
    <td>
      Language&nbsp;1:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{14}{LanguageOther}'
	     value=""
	     size="50"
	     id=LAO14>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO14").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;2:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{14}{LanguageOther2}'
	     value=""
	     size="50"
	     id=LAO214>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO214").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;3:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{14}{LanguageOther3}'
	     value=""
	     size="50"
	     id=LAO314>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO314").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;4:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{14}{LanguageOther4}'
	     value=""
	     size="50"
	     id=LAO414>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO414").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>


  <tr>
    <td>
      Language&nbsp;5:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{14}{LanguageOther5}'
	     value=""
	     size="50"
	     id=LAO514>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO514").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Other&nbsp;Languages:
    </td>
    <td>
      <textarea style="width: 100%;"
	     name='RMap{14}{LanguageOtherN}'
	     cols="50" rows="3"
	     id=LAON14
		></textarea>
    </td>
  </tr>
</table>
</blockquote>
</td>
<td>
  <i>
    Please enter the languages that apply to your resource.<br>
    Please note that the first 3 boxes have autocompletion.<br>
    If your resource covers more than 3 languages, please use the "Other 
    languages" text area
    <b>and enter the various languages separated by a comma</b>. 
  </i>
</td>
</table>
</span>
</td>
<td width="25%">
</td>
</tr>


<tr>
<td align=right><span id='redmodality14'>Modality:</span></td>
<td align=left>
<select name='RMap{14}{Modality}' 
        size="1" 
        id=RM14 
        onchange="handleOtherField('RM14','rmOther14','RMO14')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Speech" >Speech</option>

<option value="Written" >Written</option>

<option value="Speech/Written" >Speech/Written</option>

<option value="Multimodal/Multimedia" >Multimodal/Multimedia</option>

<option value="Sign Language" >Sign Language</option>

<option value="Modality Independent" >Modality Independent</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>



<td style="width: 40%" id=rmOther14>
<input type=text
       style="width: 100%;"
       name='RMap{14}{ModalityOther}'
       value=""
       size="50"
       id=RMO14>
</td>
</tr>

<tr>
<td align=right><span id='reduse14'>Use of the Resource:</span></td>
<td align=left>
<select name='RMap{14}{Use}' 
        size="1" 
        id=RU14 
        onchange="handleOtherField('RU14','ruOther14','RUO14')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Acquisition" >Acquisition</option>

<option value="Anaphora, Coreference" >Anaphora, Coreference</option>

<option value="Corpus Creation/Annotation" >Corpus Creation/Annotation</option>

<option value="Dialogue" >Dialogue</option>

<option value="Discourse" >Discourse</option>

<option value="Document Classification, Text categorisation" >Document Classification, Text categorisation</option>

<option value="Emotion Recognition/Generation" >Emotion Recognition/Generation</option>

<option value="Evaluation/Validation" >Evaluation/Validation</option>

<option value="Information Extraction, Information Retrieval" >Information Extraction, Information Retrieval</option>

<option value="Knowledge Discovery/Representation" >Knowledge Discovery/Representation</option>

<option value="Language Identification" >Language Identification</option>

<option value="Language Modelling" >Language Modelling</option>

<option value="Lexicon Creation/Annotation" >Lexicon Creation/Annotation</option>

<option value="Machine Learning" >Machine Learning</option>

<option value="Machine Translation, SpeechToSpeech Translation" >Machine Translation, SpeechToSpeech Translation</option>

<option value="Morphological Analysis" >Morphological Analysis</option>

<option value="Multimedia Document Processing" >Multimedia Document Processing</option>

<option value="Named Entity Recognition" >Named Entity Recognition</option>

<option value="Natural Language Generation" >Natural Language Generation</option>

<option value="Opinion Mining/Sentiment Analysis" >Opinion Mining/Sentiment Analysis</option>

<option value="Parsing and Tagging" >Parsing and Tagging</option>

<option value="Person Identification" >Person Identification</option>

<option value="Question Answering" >Question Answering</option>

<option value="Semantic Role Labeling" >Semantic Role Labeling</option>

<option value="Semantic Web" >Semantic Web</option>

<option value="Sign Language Recognition/Generation" >Sign Language Recognition/Generation</option>

<option value="Speech Recognition/Understanding" >Speech Recognition/Understanding</option>

<option value="Speech Synthesis" >Speech Synthesis</option>

<option value="Summarisation" >Summarisation</option>

<option value="Text Mining" >Text Mining</option>

<option value="Textual Entailment and Paraphrasing" >Textual Entailment and Paraphrasing</option>

<option value="Topic Detection and Tracking" >Topic Detection and Tracking</option>

<option value="Voice Control" >Voice Control</option>

<option value="Web Services" >Web Services</option>

<option value="Word Sense Disambiguation" >Word Sense Disambiguation</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=ruOther14>
<input type=text
       style="width: 100%;"
       name='RMap{14}{UseOther}'
       value=""
       size="50"
       id=RUO14>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RUO14").autocomplete({
      source: database_ResourceUse_other
    });
  });
</script>

</td>
</tr>

<tr>
<td align=right><span id='redavailability14'>Resource Availability:</span></td>
<td align=left>
<select name='RMap{14}{Availability}' 
        size="1" 
        id=RA14 
        onchange="handleOtherField('RA14','raOther14','RAO14')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Freely Available" >Freely Available</option>

<option value="From Data Center(s)" >From Data Center(s)</option>

<option value="From Owner" >From Owner</option>

<option value="Not Available" >Not Available</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=raOther14>
<input type=text
       style="width: 100%;"
       name='RMap{14}{AvailabilityOther}'
       value=""
       size="50"
       id=RAO14>
</td>
</tr>



<tr>
<td align="right" valign=center><span id='redlicense14'>License:</span></td>
<td align="left" valign=center>
<input name='RMap{14}{License}' value="" size="40" id=RLIC14 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Licence type, e.g., ELRA, LDC, Gnu, CreativeCommons, OpenSource, etc.</em>
</td>
</tr>




<tr>
<td align=right><span id='redurl14'>Resource URL (if available):</span></td>
<td align=left>
<input name='RMap{14}{URL}' value="" size="40" id=RURL14 style="width: 100%;" type="text">
</td>
</tr>

<tr>
<td align="right" valign=center><span id='reddocumentation14'>Documentation:</span></td>
<td align="left" valign=center>
<input name='RMap{14}{Documentation}' value="" size="40" id=RDOC14 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Is there documentation? In which language? Is the documentation publicly available?</em>
</td>
</tr>




<tr>
<td  align=right valign=top><span id='reddescription14'>Resource Description:</span> </td>
<td colspan="2" align="left">
<textarea name='RMap{14}{Description}' rows="10" cols="70" id=RD14 style="width: 100%;"></textarea>
</td>
</tr>
<!--
<tr>
  <td colspan="3">
The section below applies only if the resource is not available 
at/from established repositories, such as ELRA, LDC, or other data 
centers, as described by the URL entered in the above form. 
ELRA encourages all authors to share the described LRs (data, toosl, services,
etc.), unless already available, to enable their reuse, replicability of experiments,
including evaluation ones, etc.

  </td>
</tr>

<tr>
  <td  align=right valign=top>Do you want to make this LR available through the
<b>"Share Your LRs" initiative?</b></td>
  <td  align="left">
    <input type="checkbox"  id="RLaccepted_14" name="RMap{14}{accepted}" onclick="Resource_manage_upload('RLaccepted_14')" >
  </td>
  <td style="width: 40%" valign=top>
    <em><b>Disclaimer:</b> By clicking the box you agree to share this LR for which rights have been cleared and guarantee that such LR is sharable under the licence you mentioned above.</em>
  </td>
</tr>


<tr>
  <td align=right valign=top><span id="filed_RLaccepted_14">You can upload your LR here if it is smaller than <b>20Mb</b>.</span></td>
  <td align="left">
    <span id="filet_RLaccepted_14">
      <input type="file" name="RMap{14}{FILE}" /> &nbsp;&nbsp;&nbsp;
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>


<tr>
  <td align=right><span id="urld_RLaccepted_14">Otherwise please provide an URL from where the LR can be downloaded:</span></td>
  <td align=left>
    <span id="urlt_RLaccepted_14">
      <input name='RMap{14}{FILEURL}' value="" size="40" id=RURL14 style="width: 100%;" type="text">
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>


-->

</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</div><div id=r15>

<table width="95%">
<tr>
<td>#15</td>

<td style="width: 100%">
<table style="width: 100%" border="1">
<tr>
<td style="width: 100%">
<table cellpadding=5 style="width: 100%">

<tr>
<td align="right">Resource Name:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{15}{Name}' 
               type="text"
               value="" 
               size="40" 
               id=RN15> 


	<script type="text/javascript">
	  jQuery(function(){
	    jQuery("#RN15").keyup(function(){setDefaultValid(15)});
            jQuery("#RN15").autocomplete({
              source: database_ResourceName,
              select: function(event, ui) { f = selectResourceNameUI("#RN15", event, ui); setDefaultValid(15); return f;}});
	  });
	</script>
      </td>
      <!-- <td>
	<input type=button onClick="setDefaultResourceData(15)" value='Load Default Values' id="setDefaultButton_3" disabled>
      </td>
      -->
    </tr>
  </table>


</td>
<!--
<td>
  <i>If the button "Load default values" is enabled you can prefill some fields with default values available in the LreMap for this resource.</i>
</td> -->
</tr>

<tr>
<td align="right">ISLRN Field:</td>
<td align="left">

  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{15}{ISLRN}' 
               type="text"
               value="" 
               size="20" 
               id=RISLRN15
	       title="Format: xxx-xxx-xxx-xxx-x"> 
      </td>
      <td>
	&nbsp;&nbsp;&nbsp;
	<input type=checkbox onClick="disableResourceILSRN(15)" value='1'
	       id="disableResourceISLRN15" name='RMap{15}{ISLRNstatus}'
	       > No ISLRN / I don't know
	<script type="text/javascript">
	  jQuery(function(){
	    disableResourceILSRN(15);
	  });
	</script>
      </td>
    </tr>
  </table>
</td>
<td>
  <i>The ISLRN is a unique and universal identification schema for LRs
    which provides LRs with unique identifier using a standardised
    nomenclature.  It also ensures that LRs are correctly identified,
    and consequently, recognised with proper references for their
    usage in applications in R&D projects, products evaluation and
    benchmark as well as in documents and scientific
    papers. Visit <a href="http://www.islrn.org"
    target=_blank>http://www.islrn.org</a>.</i>
</td>
</tr>



<tr>
<td style="width: 20%" align=right><span id="redtype15">Resource Type: </span></td>



<td style="width: 40%" align=left>
<select name='RMap{15}{Type}' 
        size=1 
        id=RT15 
        onchange="handleOtherField('RT15','resOther15','RNO15')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>


<optgroup label="Resource-Dataset">
<option value="Corpus" >Corpus</option>

<option value="Lexicon" >Lexicon</option>

<option value="Grammar/Language Model" >Grammar/Language Model</option>

<option value="Ontology" >Ontology</option>

<option value="Terminology" >Terminology</option>

<option value="Treebank" >Treebank</option>

</optgroup>
<optgroup label="Resource-Tool">
<option value="Corpus Tool" >Corpus Tool</option>

<option value="Aligner" >Aligner</option>

<option value="Annotation Tool" >Annotation Tool</option>

<option value="Language Identifier" >Language Identifier</option>

<option value="Language Modeling Tool" >Language Modeling Tool</option>

<option value="Machine Learning Tool" >Machine Learning Tool</option>

<option value="Machine Translation Tool" >Machine Translation Tool</option>

<option value="Named Entity Recognizer" >Named Entity Recognizer</option>

<option value="Sentiment Analysis Tool" >Sentiment Analysis Tool</option>

<option value="Software Toolkit" >Software Toolkit</option>

<option value="Tagger/Parser" >Tagger/Parser</option>

<option value="Tokenizer" >Tokenizer</option>

<option value="Web Service" >Web Service</option>

<option value="Word Sense Disambiguator" >Word Sense Disambiguator</option>

<option value="Prosodic Analyzer" >Prosodic Analyzer</option>

<option value="Signal Processing/Features Extraction" >Signal Processing/Features Extraction</option>

<option value="Speaker Recognizer" >Speaker Recognizer</option>

<option value="Speech Recognizer/Transcriber" >Speech Recognizer/Transcriber</option>

<option value="Spoken Dialogue Tool" >Spoken Dialogue Tool</option>

<option value="Text-to-Speech Synthesizer" >Text-to-Speech Synthesizer</option>

<option value="Image Analyzer" >Image Analyzer</option>

</optgroup>
<optgroup label="Resource-Guidelines">
<option value="Metadata" >Metadata</option>

<option value="Representation-Annotation Formalism/Guidelines" >Representation-Annotation Formalism/Guidelines</option>

<option value="Representation-Annotation Standards/Best Practices" >Representation-Annotation Standards/Best Practices</option>

<option value="Language Resources/Technologies Infrastructure" >Language Resources/Technologies Infrastructure</option>

</optgroup>
<optgroup label="Evaluation">
<option value="Evaluation Data" >Evaluation Data</option>

<option value="Evaluation Tool" >Evaluation Tool</option>

<option value="Evaluation Package" >Evaluation Package</option>

<option value="Evaluation Methodology/Standards/Guidelines" >Evaluation Methodology/Standards/Guidelines</option>

</optgroup>

<optgroup label="Other">
<option value="Other" >Other - please specify &rarr;</option>

</optgroup>

</select>
</td>


<td style="width: 40%" id=resOther15>
<input type=text
       style="width: 100%;"
       name='RMap{15}{TypeOther}'
       value=""
       size="50"
       id=RNO15>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RNO15").autocomplete({
      source: database_ResourceType_other
    });
  });
</script>

</td>
</tr>




<tr>
<td align="right" valign=center><span id='redsize15'>Size:</span></td>
<td align="left" valign=center>
  <table border=0 style="padding-left: 0">
    <tr>
      <td style="padding-left: 0">
	<input name='RMap{15}{Size}' value="" size="40" id=RSIZE15 type="text">
      </td>
      <td>
	<select name='RMap{15}{Unit}' 
		size="1" 
		id=RSIZEU15 
		style="width: 100%;">
	  <option value="" selected>-- Please select a value --</option>

	  <option value="KByte  " >KByte  </option>

<option value="MByte  " >MByte  </option>

<option value="GByte  " >GByte  </option>

<option value="sentences  " >sentences  </option>

<option value="words  " >words  </option>

<option value="tokens  " >tokens  </option>

<option value="entries  " >entries  </option>

<option value="lexemes  " >lexemes  </option>

<option value="synsets  " >synsets  </option>

<option value="concepts " >concepts </option>

<option value="rules   " >rules   </option>

<option value="hours  " >hours  </option>

<option value="minutes  " >minutes  </option>

	  <option value="Other" >Other - please specify next to the size value</option>

	</select>
      </td>
    </tr>
  </table>
</td>


<td style="width: 40%" valign=top>
<em>Enter package size - a number and a relevant unit of measurement, e.g., 48Mbyte, 
5500&nbsp;lexemes, 2.1Gbyte.</em>
</td>
</tr>



<tr>

<td align="right"><span id='redstatus15'>Resource Production Status:</span></td>
<td align="left">
<select name='RMap{15}{Status}' 
        size="1" 
        onchange="handleOtherField('RS15','rsOther15','RSO15')"
        id=RS15 
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Newly created-finished" >Newly created-finished</option>

<option value="Newly created-in progress" >Newly created-in progress</option>

<option value="Existing-used" >Existing-used</option>

<option value="Existing-updated" >Existing-updated</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>


<td style="width: 40%" id=rsOther15>
<input type=text
       style="width: 100%;"
       name='RMap{15}{StatusOther}'
       value=""
       size="50"
       id=RSO15>
</td>
</tr>

<tr>
<td align=right><span id='redlang15'>Language(s):</span></td>
<td align="left">
<select name='RMap{15}{Language}' 
        size="1" 
        id=RL15 
        onchange="handleOtherLanguageField('RL15','lang15','LAO15')"
        style="width: 100%;">
<option value="P" >-- Please select a value --</option>
<option value="W" >Enter the language(s) in the box(es) below &darr;</option>
<option value="LI" >Language Independent</option>
<option value="NA" >Not Applicable</option>

</select>
</td>
<td>
</td>
</tr>

<tr>
<td></td>
<td colspan=2>
<span id=lang15>
<table border=0>
<tr>
<td>
<blockquote>
<table border=0>
  <tr>
    <td>
      Language&nbsp;1:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{15}{LanguageOther}'
	     value=""
	     size="50"
	     id=LAO15>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO15").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;2:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{15}{LanguageOther2}'
	     value=""
	     size="50"
	     id=LAO215>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO215").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;3:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{15}{LanguageOther3}'
	     value=""
	     size="50"
	     id=LAO315>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO315").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Language&nbsp;4:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{15}{LanguageOther4}'
	     value=""
	     size="50"
	     id=LAO415>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO415").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>


  <tr>
    <td>
      Language&nbsp;5:
    </td>
    <td>
      <input type=text
	     style="width: 100%;"
	     name='RMap{15}{LanguageOther5}'
	     value=""
	     size="50"
	     id=LAO515>
<script type="text/javascript">
  jQuery(function(){
    jQuery("#LAO515").autocomplete({
      source: database_ResourceLang
    });
  });
</script>

    </td>
  </tr>

  <tr>
    <td>
      Other&nbsp;Languages:
    </td>
    <td>
      <textarea style="width: 100%;"
	     name='RMap{15}{LanguageOtherN}'
	     cols="50" rows="3"
	     id=LAON15
		></textarea>
    </td>
  </tr>
</table>
</blockquote>
</td>
<td>
  <i>
    Please enter the languages that apply to your resource.<br>
    Please note that the first 3 boxes have autocompletion.<br>
    If your resource covers more than 3 languages, please use the "Other 
    languages" text area
    <b>and enter the various languages separated by a comma</b>. 
  </i>
</td>
</table>
</span>
</td>
<td width="25%">
</td>
</tr>


<tr>
<td align=right><span id='redmodality15'>Modality:</span></td>
<td align=left>
<select name='RMap{15}{Modality}' 
        size="1" 
        id=RM15 
        onchange="handleOtherField('RM15','rmOther15','RMO15')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Speech" >Speech</option>

<option value="Written" >Written</option>

<option value="Speech/Written" >Speech/Written</option>

<option value="Multimodal/Multimedia" >Multimodal/Multimedia</option>

<option value="Sign Language" >Sign Language</option>

<option value="Modality Independent" >Modality Independent</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>



<td style="width: 40%" id=rmOther15>
<input type=text
       style="width: 100%;"
       name='RMap{15}{ModalityOther}'
       value=""
       size="50"
       id=RMO15>
</td>
</tr>

<tr>
<td align=right><span id='reduse15'>Use of the Resource:</span></td>
<td align=left>
<select name='RMap{15}{Use}' 
        size="1" 
        id=RU15 
        onchange="handleOtherField('RU15','ruOther15','RUO15')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Acquisition" >Acquisition</option>

<option value="Anaphora, Coreference" >Anaphora, Coreference</option>

<option value="Corpus Creation/Annotation" >Corpus Creation/Annotation</option>

<option value="Dialogue" >Dialogue</option>

<option value="Discourse" >Discourse</option>

<option value="Document Classification, Text categorisation" >Document Classification, Text categorisation</option>

<option value="Emotion Recognition/Generation" >Emotion Recognition/Generation</option>

<option value="Evaluation/Validation" >Evaluation/Validation</option>

<option value="Information Extraction, Information Retrieval" >Information Extraction, Information Retrieval</option>

<option value="Knowledge Discovery/Representation" >Knowledge Discovery/Representation</option>

<option value="Language Identification" >Language Identification</option>

<option value="Language Modelling" >Language Modelling</option>

<option value="Lexicon Creation/Annotation" >Lexicon Creation/Annotation</option>

<option value="Machine Learning" >Machine Learning</option>

<option value="Machine Translation, SpeechToSpeech Translation" >Machine Translation, SpeechToSpeech Translation</option>

<option value="Morphological Analysis" >Morphological Analysis</option>

<option value="Multimedia Document Processing" >Multimedia Document Processing</option>

<option value="Named Entity Recognition" >Named Entity Recognition</option>

<option value="Natural Language Generation" >Natural Language Generation</option>

<option value="Opinion Mining/Sentiment Analysis" >Opinion Mining/Sentiment Analysis</option>

<option value="Parsing and Tagging" >Parsing and Tagging</option>

<option value="Person Identification" >Person Identification</option>

<option value="Question Answering" >Question Answering</option>

<option value="Semantic Role Labeling" >Semantic Role Labeling</option>

<option value="Semantic Web" >Semantic Web</option>

<option value="Sign Language Recognition/Generation" >Sign Language Recognition/Generation</option>

<option value="Speech Recognition/Understanding" >Speech Recognition/Understanding</option>

<option value="Speech Synthesis" >Speech Synthesis</option>

<option value="Summarisation" >Summarisation</option>

<option value="Text Mining" >Text Mining</option>

<option value="Textual Entailment and Paraphrasing" >Textual Entailment and Paraphrasing</option>

<option value="Topic Detection and Tracking" >Topic Detection and Tracking</option>

<option value="Voice Control" >Voice Control</option>

<option value="Web Services" >Web Services</option>

<option value="Word Sense Disambiguation" >Word Sense Disambiguation</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=ruOther15>
<input type=text
       style="width: 100%;"
       name='RMap{15}{UseOther}'
       value=""
       size="50"
       id=RUO15>

<script type="text/javascript">
  jQuery(function(){
    jQuery("#RUO15").autocomplete({
      source: database_ResourceUse_other
    });
  });
</script>

</td>
</tr>

<tr>
<td align=right><span id='redavailability15'>Resource Availability:</span></td>
<td align=left>
<select name='RMap{15}{Availability}' 
        size="1" 
        id=RA15 
        onchange="handleOtherField('RA15','raOther15','RAO15')"
        style="width: 100%;">
<option value="" selected>-- Please select a value --</option>

<option value="Freely Available" >Freely Available</option>

<option value="From Data Center(s)" >From Data Center(s)</option>

<option value="From Owner" >From Owner</option>

<option value="Not Available" >Not Available</option>

<option value="Not Applicable" >Not Applicable</option>

<option value="Other" >Other - please specify &rarr;</option>

</select>
</td>



<td style="width: 40%" id=raOther15>
<input type=text
       style="width: 100%;"
       name='RMap{15}{AvailabilityOther}'
       value=""
       size="50"
       id=RAO15>
</td>
</tr>



<tr>
<td align="right" valign=center><span id='redlicense15'>License:</span></td>
<td align="left" valign=center>
<input name='RMap{15}{License}' value="" size="40" id=RLIC15 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Licence type, e.g., ELRA, LDC, Gnu, CreativeCommons, OpenSource, etc.</em>
</td>
</tr>




<tr>
<td align=right><span id='redurl15'>Resource URL (if available):</span></td>
<td align=left>
<input name='RMap{15}{URL}' value="" size="40" id=RURL15 style="width: 100%;" type="text">
</td>
</tr>

<tr>
<td align="right" valign=center><span id='reddocumentation15'>Documentation:</span></td>
<td align="left" valign=center>
<input name='RMap{15}{Documentation}' value="" size="40" id=RDOC15 style="width: 100%;" type="text">
</td>


<td style="width: 40%" valign=top>
<em>Is there documentation? In which language? Is the documentation publicly available?</em>
</td>
</tr>




<tr>
<td  align=right valign=top><span id='reddescription15'>Resource Description:</span> </td>
<td colspan="2" align="left">
<textarea name='RMap{15}{Description}' rows="10" cols="70" id=RD15 style="width: 100%;"></textarea>
</td>
</tr>
<!--
<tr>
  <td colspan="3">
The section below applies only if the resource is not available 
at/from established repositories, such as ELRA, LDC, or other data 
centers, as described by the URL entered in the above form. 
ELRA encourages all authors to share the described LRs (data, toosl, services,
etc.), unless already available, to enable their reuse, replicability of experiments,
including evaluation ones, etc.

  </td>
</tr>

<tr>
  <td  align=right valign=top>Do you want to make this LR available through the
<b>"Share Your LRs" initiative?</b></td>
  <td  align="left">
    <input type="checkbox"  id="RLaccepted_15" name="RMap{15}{accepted}" onclick="Resource_manage_upload('RLaccepted_15')" >
  </td>
  <td style="width: 40%" valign=top>
    <em><b>Disclaimer:</b> By clicking the box you agree to share this LR for which rights have been cleared and guarantee that such LR is sharable under the licence you mentioned above.</em>
  </td>
</tr>


<tr>
  <td align=right valign=top><span id="filed_RLaccepted_15">You can upload your LR here if it is smaller than <b>20Mb</b>.</span></td>
  <td align="left">
    <span id="filet_RLaccepted_15">
      <input type="file" name="RMap{15}{FILE}" /> &nbsp;&nbsp;&nbsp;
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>


<tr>
  <td align=right><span id="urld_RLaccepted_15">Otherwise please provide an URL from where the LR can be downloaded:</span></td>
  <td align=left>
    <span id="urlt_RLaccepted_15">
      <input name='RMap{15}{FILEURL}' value="" size="40" id=RURL15 style="width: 100%;" type="text">
    </span>
  </td>
  <td style="width: 40%" valign=top>
  </td>
</tr>


-->

</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</div>

</div>
<p>

<p>
<blockquote>
<A href="javascript:RmoreLess('more')">More resources</A>
&nbsp;&nbsp;&nbsp;&nbsp;
<A href="javascript:RmoreLess('less')">Fewer resources</A>
</blockquote>



</div>


<script type="text/javascript">
<!---
function handleOtherField(selectID,otherTDID,otherTextID) {
   select = document.getElementById(selectID);
   other = document.getElementById(otherTDID);
   text = document.getElementById(otherTextID);
   for (var i = 0; i < select.options.length; i++) {
      if (select.options[i].selected) {
	  if ((select.options[i].value == "Other") || (select.options[i].value == "User Entered") || (select.options[i].value == "W")) {
	      other.style.display="";
	  }
	  else {
	      other.style.display="none";
	      text.value="";
	  }
	  return;
      }
   }
   other.style.display="none";
   text.value="";
}

function handleOtherLanguageField(selectID,otherDIV,otherTextID) {
   select = document.getElementById(selectID);
   other = document.getElementById(otherDIV);
   text = document.getElementById(otherTextID);
   for (var i = 0; i < select.options.length; i++) {
      if (select.options[i].selected) {
	  if ((select.options[i].value == "Other") || (select.options[i].value == "User Entered") || (select.options[i].value == "W")) {
	      other.style.display="";
	  }
	  else {
	      other.style.display="none";
	      //text.value="";
	  }
	  return;
      }
   }
   other.style.display="none";
   //text.value="";
}


// populate the previous things

minRes = 1;
maxRes = 15;
res = 15;

var selectorIDS = ["RT","RS","RL","RM","RU","RA"];
var otherTextTDS = ["resOther","rsOther","lang","rmOther","ruOther","raOther"];
var otherTextBox = ["RNO","RSO","LAO","RMO","RUO","RAO"];
var allTextBoxes = ["RNO","RSO","LAO","RMO","RUO","RAO","RN","RURL","RD","RSIZE","RLIC","RDOC"];

function handleTextBoxes(thisOne) {
    for (ijk=0; ijk < selectorIDS.length; ijk++) { 
	selectID = selectorIDS[ijk]+thisOne;
	otherTDID = otherTextTDS[ijk]+thisOne;
        otherTextID = otherTextBox[ijk]+thisOne;
        handleOtherField(selectID,otherTDID,otherTextID);
    }
}


function checkResEmpty(thisOne) {
   slot = document.getElementById("RN" + thisOne);
   if (slot.value) {
      return false;
   }
  return true;
}


function clearResOut(thisOne) {
    for (ijk=0; ijk < allTextBoxes.length; ijk++) { 
       slot = document.getElementById(allTextBoxes[ijk] + thisOne);
       slot.value="";
    }
    for (ijk=0; ijk < selectorIDS.length; ijk++) { 
       select = document.getElementById(selectorIDS[ijk] + thisOne);
       for (var i = 0; i < select.options.length; i++) {
	   select.options[i].selected = false;
       }
    }
}


function RmoreLess(comm) {
  if (!document.getElementById) {
    return;
  }


  if ((comm == "less") && (res > minRes)) {
     handleTextBoxes(res);
     clearResOut(res);
     str = "r" + res;
     which = document.getElementById(str);
     which.style.display="none";
     res = res - 1;
  }
  if ((comm == "more") && (res < maxRes)) {
      res = res + 1;
      str = "r" + res;
      which = document.getElementById(str);
      which.style.display="";
      handleTextBoxes(res);
}
var numres  = document.getElementById("numofres");
numres.value=res;
}

for (rj = minRes; rj < 15; rj++) {
    handleTextBoxes(rj);
}

for (rj = minRes; rj < 15; rj++) {
    Resource_manage_upload('RLaccepted_'+rj);
}

function noresources() {
   which = document.getElementById("noResources");
   that = document.getElementById("resourceBlock");
   if (which.checked) {
      for (rj = minRes; rj < 15; rj++) {
          clearResOut(rj);
      }
      while ((res > minRes) && checkResEmpty(res)) {
           RmoreLess('less');
      }
      that.style.display="none";
   }
   else {
      that.style.display="";
   }
}



while ((res > minRes) && checkResEmpty(res)) {
  RmoreLess('less');
}

// -->
</script>

</table>
</blockquote>
</blockquote>
</p><hr><script type="text/javascript">
	// not to be used anymore. will be obsoleted by end of 2011

	function countWords(fieldname) {
	  var thefield = document.getElementsByName(fieldname);
          var char_count = thefield[0].value.length;
          var fullStr = thefield[0].value + " ";
          var initial_whitespace_rExp = /^[^A-Za-z0-9]+/gi;
          var left_trimmedStr = fullStr.replace(initial_whitespace_rExp, "");
          var non_alphanumerics_rExp = rExp = /[^A-Za-z0-9]+/gi;
          var cleanedStr = left_trimmedStr.replace(non_alphanumerics_rExp, " ");
          var splitString = cleanedStr.split(" ");
          var word_count = splitString.length -1;

          // http://javascript.about.com/library/blcount.htm - use different method, compare use least.
	  var y= thefield[0].value + " ";
	  var r = 0;
	  a=y.replace(/\s/g,' ');
	  a=a.split(' ');
	  for (z=0; z<a.length; z++) {if (a[z].length > 0) r++;}
	  var word_count1 = r;

          word_count = (word_count < word_count1) ? word_count : word_count1;

          if (fullStr.length <2) {
            word_count = 0;
          }
          return word_count;
        }

        // cleanerror is called once to clear the multiabstract error field, which is filled more than once with the same text if an error occurs
	function clearError(errorfieldname) {
	  var theerrorfield = document.getElementById(errorfieldname);
	  theerrorfield.innerHTML = '';
        }

	function updateWords(fieldname, errorfieldname, wordcount, maxwordcount) {
	  var thefield = document.getElementById(fieldname);
	  var theerrorfield = document.getElementById(errorfieldname);
          if (wordcount > maxwordcount) {
	    thefield.innerHTML = '<span style="color:red;"><b>' + wordcount + '</b></span>';
            theerrorfield.innerHTML = '<span style="color:red;"><b>Maximum number of words exceeded!</b></span>';

          } else {
	    thefield.innerHTML = wordcount;
          }
	}

        function checkTitleWords(maxwordcount) {
	  document.getElementById('errorTitle').innerHTML = '';
          updateWords('titlewordCounter', 'errorTitle', countWords('title'), maxwordcount);
        }

	function checkMultiAbstractDynWC() {
          abstractAlertTriggered = 0;
	  abstract_word_count = 0;
	  
          
	  
          
	  return true;
	}
	
	// call it at the beginning to fill the MultiAbstract fields
        jQuery(function() {
          checkMultiAbstractDynWC()
        });


	function checkMissing(fieldname, fielddescr) {
	  var emailErrMsg = "Please enter a value for '" + fielddescr + "'.";
	  var thefield = document.getElementsByName(fieldname);
	  if (thefield[0].value=="" && 
	      (thefield.length==1 || 
               (thefield.length==2 && thefield[1].value =="")
              )
             ) {
	    alert(emailErrMsg);
	    return false;
	  }
	  return true;
	}

	function checkWords(fieldname, fielddescr, number) {
	  var word_count = countWords(fieldname);

          if (word_count == 1) {
            wordOrWords = " word";
          }
          else {
            wordOrWords = " words";
          }
// We are dealing with words and  not characters and char_count is not defined.  Should
// be deleted soon .... Oct 2013
//          if (char_count == 1) {
//            charOrChars = " character";
//          } else {
//            charOrChars = " characters";
//          }

	  var emailErrMsg = "No more than " + number + " words for '" 
              + fielddescr + "' (you entered " + word_count + " " + wordOrWords + ").";

	  if (word_count > number) {
	    alert(emailErrMsg);
	    return false;
	  }
	  return true;
	}

	function checkMultiAbstractTotalWords(fieldname, number) {
	  var emailErrMsg = "No more than " + number + " words for the complete abstract.";

          abstract_word_count += countWords(fieldname);

	  if (abstract_word_count > number) {
	    alert(emailErrMsg);
	    return false;
	  }
	  return true;
	}

	function checkMultiAbstractLocalWords(fieldname, number) {
	  var emailErrMsg = "No more than " + number + " words for the abstract box.";

	  if (countWords(fieldname) > number) {
	    alert(emailErrMsg);
	    return false;
	  }
	  return true;
	}

	function checkChars(fieldname, fielddescr, number) {
	  var thefield = document.getElementsByName(fieldname);
          if (thefield[0].value.length == 1) {
            charOrChars = " char";
          }
          else {
            charOrChars = " chars";
          }

	  var emailErrMsg = "No more than " + number + " characters for '" + 
                fielddescr + "' (you entered " + thefield[0].value.length + charOrChars + ").";
          
	  if (thefield[0].value.length > number) {
	    alert(emailErrMsg);
	    return false;
	  }
	  return true;
	}
	  
	function checkScrollbar(fieldname, fielddescr, fieldvalue) {
	  var emailErrMsg = "Please enter a value for '" + fielddescr + "'.";
	  var thefield = document.getElementsByName(fieldname);
	  if (thefield[0].value == fieldvalue) {
	    alert(emailErrMsg);
	    return false;
	  }
	  return true;
	}

	function checkEmail(fieldname, fielddescr) {
	  var Thisfilter=/^([\w-+]+(?:\.[\w-+]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	  var emailErrMsg = "Need to enter a valid email address for " + fielddescr + ".";

	  if (!(Thisfilter.test(document.getElementsByName(fieldname)[0].value))) {
	    alert(emailErrMsg);
	    return false;
	  }
	  return true;
	}

	function checkEmailNotMandatory(fieldname, fielddescr) {
	  var Thisfilter=/^([\w-+]+(?:\.[\w-+]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	  var emailErrMsg = "Need to enter a valid email address for " + fielddescr + ".";
	  
	  if (document.getElementsByName(fieldname)[0].value != "") {
	    if (!(Thisfilter.test(document.getElementsByName(fieldname)[0].value))) {
	      alert(emailErrMsg);
              return false;
	    }
	  }
	  return true;
	}

	function checkFileMissing(fieldname, fielddescr) {
	  var emailErrMsg = "Please submit a file for '" + fielddescr + "'.";
	  var thefield = document.getElementsByName(fieldname);
	  if (thefield[0].value=="") {
	    alert(emailErrMsg);
	    return false;
	  }
	  return true;
	}

	function checkMandatoryIfPresent(fieldname, fielddescr) {
	  var thefield = document.getElementsByName(fieldname);
	  if (thefield[0].value=="") {
	    mandatoryIfPresentEmpty++;
	  } else {
	    mandatoryIfPresentFull++;
	  }
	  return true;
	}

	function checkMissingIfPresent(fieldname, fielddescr) {
	  var emailErrMsg = "Please enter a value for '" + fielddescr + "'.";
	  var thefield = document.getElementsByName(fieldname);
	  if (thefield[0].value=="" && mandatoryIfPresentFull>0 && mandatoryIfPresentEmpty>0) {
	    alert(emailErrMsg);
	    return false;
	  }
	  return true;
	}

	function checkMissingIfPresentEmail(fieldname, fielddescr) {
	  var Thisfilter=/^([\w-+]+(?:\.[\w-+]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	  var emailErrMsg = "Need to enter a valid email address for " + fielddescr + ".";

	  if (!(Thisfilter.test(document.getElementsByName(fieldname)[0].value))
              && mandatoryIfPresentFull>0 && mandatoryIfPresentEmpty>0
              ) {
	    alert(emailErrMsg);
	    return false;
	  }
	  return true;
	}

	function checkCheckbox(fieldname, fielddescr) {
	  var emailErrMsg = "The checkbox '" + fielddescr + "' is Mandatory.";
	  var thefield = document.getElementsByName(fieldname);
	  if (!thefield[0].checked) {
	    alert(emailErrMsg);
	    return false;
	  }
	  return true;
	}

	// ------------------------------------
	      
	var keywordnumber;
	var abstract_word_count;

	function checkMissingId(fieldname, fieldid) {
          if (document.getElementById(fieldid) == null) {
            return true;
          }

	  var thefield = document.getElementsByName(fieldname);

	  if (thefield[0].value=="" && 
	      (thefield.length==1 || 
               (thefield.length==2 && thefield[1].value =="")
              )
             ) {
            document.getElementById(fieldid).style.display = "block";
	    return false;
	  } else {
	    document.getElementById(fieldid).style.display = "none";
            return true;
	  }
	}

	function checkCharsId(fieldname, fieldid, number) {
          if (document.getElementById(fieldid) == null) {
            return true;
          }

	  var thefield = document.getElementsByName(fieldname);

	  if (thefield[0].value.length > number) {
	    document.getElementById(fieldid).style.display = "block";
	    return false;
	  } else {
            document.getElementById(fieldid).style.display = "none";
            return true;
	  }
	}

	function checkWordsId(fieldname, fieldid, number) {
          if (document.getElementById(fieldid) == null) {
            return true;
          }

	  var thefield = document.getElementsByName(fieldname);
          var char_count = thefield[0].value.length;
          var fullStr = thefield[0].value + " ";
          var initial_whitespace_rExp = /^[^A-Za-z0-9]+/gi;
          var left_trimmedStr = fullStr.replace(initial_whitespace_rExp, "");
          var non_alphanumerics_rExp = rExp = /[^A-Za-z0-9]+/gi;
          var cleanedStr = left_trimmedStr.replace(non_alphanumerics_rExp, " ");
          var splitString = cleanedStr.split(" ");
          var word_count = splitString.length -1;

          // http://javascript.about.com/library/blcount.htm - use different method, compare use least.
	  var y= thefield[0].value + " ";
	  var r = 0;
	  a=y.replace(/\s/g,' ');
	  a=a.split(' ');
	  for (z=0; z<a.length; z++) {if (a[z].length > 0) r++;}
	  var word_count1 = r;

          word_count = (word_count < word_count1) ? word_count : word_count1;

          if (fullStr.length <2) {
            word_count = 0;
          }


	  if (word_count > number) {
	    document.getElementById(fieldid).style.display = "block";
	    return false;
	  } else {
	    document.getElementById(fieldid).style.display = "none";
            return true;
	  }
	}
	
	function checkScrollbarId(fieldname, fieldid, fieldvalue) {
          if (document.getElementById(fieldid) == null) {
            return true;
          }

	  var thefield = document.getElementsByName(fieldname);
	  if (thefield[0].value == fieldvalue) {
            document.getElementById(fieldid).style.display = "block";
	    return false;
	  } else {
            document.getElementById(fieldid).style.display = "none";
            return true;
	  }
	}
	  
	function checkEmailId(fieldname, fieldidformat, fieldidmissing) {
          if (document.getElementById(fieldidmissing) == null) {
            return true;
          }
          if (document.getElementById(fieldidformat) == null) {
            return true;
          }

	  var Thisfilter=/^([\w-+]+(?:\.[\w-+]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

	  if (document.getElementsByName(fieldname)[0].value != "") {
	    document.getElementById(fieldidmissing).style.display = "none";

	    if (!(Thisfilter.test(document.getElementsByName(fieldname)[0].value))) {
	      document.getElementById(fieldidformat).style.display = "block";
              return false;
	    } else {
	      document.getElementById(fieldidformat).style.display = "none";
	      return true;
	    }
	  } else {
	    document.getElementById(fieldidmissing).style.display = "block";
	    document.getElementById(fieldidformat).style.display = "none";
            return false;
	  }
	}

	function checkEmailIdNotMandatory(fieldname, fieldidformat) {
          if (document.getElementById(fieldidformat) == null) {
            return true;
          }

	  var Thisfilter=/^([\w-+]+(?:\.[\w-+]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	  
	  if (document.getElementsByName(fieldname)[0].value != "") {
	    if (!(Thisfilter.test(document.getElementsByName(fieldname)[0].value))) {
	      document.getElementById(fieldidformat).style.display = "block";
              return false;
	    } else {
	      document.getElementById(fieldidformat).style.display = "none";
	      return true;
	    }
	  } else {
	    document.getElementById(fieldidformat).style.display = "none";
            return true;
	  }
	}


	function checkCheckboxId(fieldname, fieldid) {
          if (document.getElementById(fieldid) == null) {
            return true;
          }

	  var thefield = document.getElementsByName(fieldname);

	  if (!thefield[0].checked) {
            document.getElementById(fieldid).style.display = "block";
	    return false;
	  } else {
	    document.getElementById(fieldid).style.display = "none";
            return true;
	  }
	}

	function checkFlag(thefieldid, fieldid) {
          if (document.getElementById(fieldid) == null) {
            return true;
          }

	  var thefield = document.getElementById(thefieldid);

	  if (thefield == null) {
            document.getElementById(fieldid).style.display = "block";
	    return false;
	  }

	  if (thefield.value == "1") {
	    document.getElementById(fieldid).style.display = "none";
            return true;
	  } else {
            document.getElementById(fieldid).style.display = "block";
	    return false;
	  }
	}




        var mandatoryIfPresentFull;
        var mandatoryIfPresentEmpty;

	function checkMandatoryIfPresentStart() {
          mandatoryIfPresentEmpty = 0;
          mandatoryIfPresentFull = 0;
	  return true;
	}

	function checkMandatoryIdIfPresent(fieldname) {
	  var thefield = document.getElementsByName(fieldname);
	  if (thefield[0].value=="") {
	    mandatoryIfPresentEmpty++;
	  } else {
	    mandatoryIfPresentFull++;
	  }
	  return true;
	}

	function checkMissingIdIfPresent(fieldname, fieldid) {
          if (document.getElementById(fieldid) == null) {
            return true;
          }

	  var thefield = document.getElementsByName(fieldname);
	  if (thefield[0].value=="" && mandatoryIfPresentFull>0 && mandatoryIfPresentEmpty>0) {
            document.getElementById(fieldid).style.display = "block";
	    return false;
	  } else {
	    document.getElementById(fieldid).style.display = "none";
            return true;
	  }
	}

	function checkMissingIdIfPresentEmail(fieldname, fieldidformat, fieldidmissing) {
          if (document.getElementById(fieldidmissing) == null) {
            return true;
          }
          if (document.getElementById(fieldidformat) == null) {
            return true;
          }

	  var Thisfilter=/^([\w-+]+(?:\.[\w-+]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

	  if (mandatoryIfPresentFull>0 && mandatoryIfPresentEmpty>0) {
	    if (document.getElementsByName(fieldname)[0].value != "") {
	      document.getElementById(fieldidmissing).style.display = "none";

	      if (!(Thisfilter.test(document.getElementsByName(fieldname)[0].value))) {
	        document.getElementById(fieldidformat).style.display = "block";
                return false;
	      } else {
	        document.getElementById(fieldidformat).style.display = "none";
	        return true;
	      }
	    } else {
	      document.getElementById(fieldidmissing).style.display = "block";
	      document.getElementById(fieldidformat).style.display = "none";
              return false;
	    }
          } else {
            document.getElementById(fieldidmissing).style.display = "none";
	    document.getElementById(fieldidformat).style.display = "none";
            return true;
          }
	}

	function checkKeywordStart() {
	  keywordnumber = 0;
	  return true;
	}

	function checkKeyword(fieldname) {
	  var thefield = document.getElementsByName(fieldname);
	  if (thefield[0].checked) {
	    keywordnumber++;
	  }
	  return true;
	}

	function checkKeywordEnd(number, exact, fieldid) {
          if (document.getElementById(fieldid) == null) {
            return true;
          }

	  if (exact) {
	    if (keywordnumber != number) {
	      document.getElementById(fieldid).style.display = "block";
	      return false;
	    } else {
	      document.getElementById(fieldid).style.display = "none";
              return true;
	    }
          } else {
	    if (keywordnumber < number) {
	      document.getElementById(fieldid).style.display = "block";
	      return false;
	    } else {
	      document.getElementById(fieldid).style.display = "none";
              return true;
	    }
          }
	}

	function disableForm() {
          sfield = document.getElementById("submitButton");

// Following line removed.  It's unnecessary, since the button is hidden anyway.
//        sfield.disabled = true; 
          return true;
        }

        function waitMsg() {
          area = document.getElementById("waitMsg");
          area.innerHTML = "<em><font color=red>Please wait while the submission is being uploaded.</font></em>";
          area = document.getElementById("submitButtonArea");
          area.style.display="none";
          area = document.getElementById("waitMsgArea");
          area.style.display="";
          return true;
        }

	function submitPage() {
	  var finalvalue = true;

	  abstract_word_count = 0;

	  
finalvalue = checkMissingId("title", "checkmissing_item_3") && finalvalue;
//finalvalue = item_spa_check_and_buildItemList() && finalvalue;
//finalvalue = item_spa_check_editing_mode() && finalvalue;
//finalvalue = item_sp_contacts_check() && finalvalue;

	  if (finalvalue) {
            disableForm();
            waitMsg();
            theaction = document.getElementById("theaction");
            theaction.value = 'submit';
	
            document.myForm.submit();
	  } else {
	    alert("The submission is incomplete.  Missing, incomplete or incorrect fields are highlighted with red text through the form. Please scroll up, fill in missing or incorrect fields, and then try again."); 
	  }
	}

	function enterPasscode() {
	  theaction = document.getElementById("theaction");
          theaction.value = 'enter';
	
          document.myForm.submit();
	}


	function backPage() {
	  document.location='https://www.softconf.com/lrec2016/main/manager/scmd.cgi?scmd=manager&ToPage=newSubmission&FromPage=Main';
//        theaction.value = 'back';
//        document.myForm.submit();
	}



    // ---------------------------
    // Portlets

    // load the schedule and initialize the portlets
    jQuery(function() {
	jQuery( ".portlet" )
	    .addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
	    .find( ".portlet-header" )
	    .addClass( "ui-widget-header ui-corner-all" )
	    .prepend( "<span class='ui-icon ui-icon-minusthick portlet-toggle'></span>");
	
	jQuery( ".portlet-toggle" ).click(function() {
	    var icon = jQuery( this );
	    icon.toggleClass( "ui-icon-minusthick ui-icon-plusthick" );
	    icon.closest( ".portlet" ).find( ".portlet-content" ).toggle();
	});
	
	jQuery( ".portlet" )
	    .find( ".portlet-content" ).toggle();
    });
	</script>




<div width="100%" id=submitButtonArea>

<table width="75%" align=center>
  <tr>



    <td align=center>
      <input type="button" id="submitButton" onClick="submitPage();" value="Submit">
    </td> 
  </tr>


</table>
</div>




<div width="100%" style="display: none" id=waitMsgArea>
<p>
<table width="100%" align=center>
<tr>
<td align=center width="100%" id=waitMsg>
</td>
</tr>
</table>
</div>

</form>


<p align=center>

<small>
Based on 
<a href="http://www.softconf.com" target="_blank">START</a> 
Conference Manager (V2.61.0 - Rev. 4391)
</small>
<p>&nbsp;
</div>
</body>
</html>