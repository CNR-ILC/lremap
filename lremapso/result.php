<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<?php
$paperId_1 = $_GET['paperId'];
$first = $_POST['first'];
//$paperId = $_POST['paperId'];
$paperId = (isset($_POST['paperId']) ? $_POST['paperId'] : $_GET['paperId']);
$the_conf = (isset($_POST['conf']) ? $_POST['conf'] : $_GET['conf']);
$the_conf="THE_CONF";
$numofres = (isset($_POST['numofres']) ? $_POST['numofres'] : $_GET['numofres']);
?>
<?php
include("db_api.php");
$connection;
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE>The LreMap Stand-Off Version</TITLE>
<STYLE>

<!--
  @import url(https://www.softconf.com/lrec2016/css/start.css); /* start CSS */
  @import url(https://www.softconf.com/lrec2016/css/topmenu.css); /* topmenu CSS */
  @import url(https://www.softconf.com/lrec2016/css/schedulemaker.css); /* schedulemaker CSS */
  @import url(https://www.softconf.com/lrec2016/css/submitPaper.css); /* submitpaper CSS */
  @import url(https://www.softconf.com/lrec2016/css/tab.css); /* tab CSS */

-->

</STYLE>


<style>
      .ui-autocomplete {
      max-height: 200px;
      overflow-y: auto;
      /* prevent horizontal scrollbar */
      overflow-x: hidden;
      /* add padding to account for vertical scrollbar */
      padding-right: 20px;
      }
      /* IE 6 doesn't support max-height
      * we use height instead, but this forces the menu to always be this tall
      */
      * html .ui-autocomplete {
      height: 100px;
      }

</style>



<script type="text/javascript" src="https://www.softconf.com/lrec2016/javascript/XulMenu.js"></script>
<script type="text/javascript" src="https://www.softconf.com/lrec2016/javascript/utils.js"></script>
<script type="text/javascript" src="https://www.softconf.com/lrec2016/javascript/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="https://www.softconf.com/lrec2016/javascript/jquery-migrate-1.2.1.js"></script>
<script type="text/javascript" src="https://www.softconf.com/lrec2016/javascript/jquery-ui.min.js"></script>
<script type="text/javascript" src="https://www.softconf.com/lrec2016/javascript/jquery.dialogextend.min.js"></script>
<script type="text/javascript" src="https://www.softconf.com/lrec2016/javascript/jquery.form.js"></script> 
<script type="text/javascript" src="https://www.softconf.com/lrec2016/javascript/jquery.jeditable.js"></script> 
<script type="text/javascript" src="https://www.softconf.com/lrec2016/javascript/jquery.startutils.js"></script> 
<link type="text/css" href="https://www.softconf.com/lrec2016/css/jquery-ui.css" rel="stylesheet" />

<script>
     jQuery.noConflict();
</script>
</HEAD>
<BODY BGCOLOR='#ffffff'>


<script>
  jQuery(function() {
    
    setToolTip();
  });
</script>
<?php
include("./lremapso_files/custom.php");
?>




<div>
<?php
include("./lremapso_files/addinfo.php");
?>
  
</div>



<div id="bar">
  <table cellspacing="0" cellpadding="0" id="menu1" class="XulMenu">
    <tr>
      <td id="topMenuAjax">


      </td>

      <td width="100%">
	&nbsp;
      </td>
      <td><a href="http://www.ilc.cnr.it" target="_blank">
		  <img src="./lremapso_files/logos/CNR-ILC_Logo_icon.png" border=0 title='Visit CNR-ILC' style="padding: 0px;">
		</a>
        </td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td><a href="http://www.elda.org" target="_blank">
		  <img src="./lremapso_files/logos/elra-elda-logo_icon.png" border=0 title='Visit ELRA/ELDA' style="padding: 0px;">
		</a>
        </td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>


    
	    </tr>
	  </table>
	</span>

	<div style="display:none">
	  <div id="warningdialog" title="Warnings">
	    <div style="overflow: auto; height:100%; width:100%; font-size: 12px;">
	      <div id='warningdialog_content'>Loading...
	      </div>
	    </div>
	  </div>
	</div>



      </td>


      <td nowrap>
	<span id="warningdialog_separator" style="display:none">
	  &nbsp;&nbsp;&nbsp;
	</span>
      </td> </tr>
  </table>
</div>




<!--
The following is not an error.  It's to outsmart IE7 to presenting the
dimensions as it should.
// -->

<div style="width=100%; COLOR: #000000; BACKGROUND-COLOR: #ffffff; padding-left: 10px; padding-right: 10px; padding-top: 15px; padding-bottom: 5px">

<form  ENCTYPE="multipart/form-data"

       ACTION="result.php" 

       METHOD=POST
       name='myForm'
       accept-charset='UTF-8'> 

<input type='hidden' id="theaction" name='page_theaction' value="">
<input type='hidden' id="pageid" name='pageid' value="0">
<input type='hidden' id="customsubmissionpage_id" name='customsubmissionpage_id' value="0">
<input type='hidden' id="paperId" name='paperId' value="<?php echo $paperId;?>">
<input type='hidden' id="conf" name='conf' value="<?php echo $the_conf;?>">
<input type='hidden' id="pageid" name='pageid' value="0">


<link rel="stylesheet" type="text/css" href="https://www.softconf.com/lrec2016/css/table.css" media="all">
<script type="text/javascript" src="https://www.softconf.com/lrec2016/javascript/table.js"></script>
<?php
include("./lremapso_files/confinfo.php");
?>
<style>
  .ui-autocomplete {
  max-height: 100px;
  overflow-y: auto;
  /* prevent horizontal scrollbar */
  overflow-x: hidden;
  /* add padding to account for vertical scrollbar */
  padding-right: 20px;
  }
  /* IE 6 doesn't support max-height
  * we use height instead, but this forces the menu to always be this tall
  */
  * html .ui-autocomplete {
  height: 100px;
  }
</style>
</br>
Resources described for submission <?php echo "$paperId:  $numofres";?> 
<hr>
<?php
$list = array();
for($i=1; $i<$numofres+1; $i++)
{
    $langdim=0;
    $langdimstr;
        $res = array();
        $final = array();
        $langs = array(); 
        $res[0] = $_POST['RMap{'.$i.'}{Name}'];
        $res[1] = $_POST['RMap{'.$i.'}{ISLRN}'];
        $res[2] = $_POST['RMap{'.$i.'}{Type}'];
        $res[3] = $_POST['RMap{'.$i.'}{TypeOther}'];
        $res[4] = $_POST['RMap{'.$i.'}{Size}'];
        $res[5] = $_POST['RMap{'.$i.'}{Unit}'];
        $res[6] = $_POST['RMap{'.$i.'}{Status}'];
        $res[7] = $_POST['RMap{'.$i.'}{StatusOther}'];
        $res[8] = $_POST['RMap{'.$i.'}{Language}']; // langselection
        $res[9] = $_POST['RMap{'.$i.'}{LanguageOther}'];
        $res[10] = $_POST['RMap{'.$i.'}{LanguageOther2}'];
        $res[11] = $_POST['RMap{'.$i.'}{LanguageOther3}'];
        $res[12] = $_POST['RMap{'.$i.'}{LanguageOther4}'];
        $res[13] = $_POST['RMap{'.$i.'}{LanguageOther5}'];
        $res[14] = $_POST['RMap{'.$i.'}{LanguageOtherN}'];
        $res[15] = $_POST['RMap{'.$i.'}{Modality}'];
        $res[16] = $_POST['RMap{'.$i.'}{ModalityOther}'];
        $res[17] = $_POST['RMap{'.$i.'}{Use}'];
        $res[18] = $_POST['RMap{'.$i.'}{UseOther}'];
        $res[19] = $_POST['RMap{'.$i.'}{Availability}'];
        $res[20] = $_POST['RMap{'.$i.'}{AvailabilityOther}'];
        $res[21] = $_POST['RMap{'.$i.'}{License}'];
        $res[22] = $_POST['RMap{'.$i.'}{URL}'];
        $res[23] = $_POST['RMap{'.$i.'}{Documentation}'];
        $res[24] = $_POST['RMap{'.$i.'}{Description}'];
        // langs
        $langs[0]=$res[9];
        $langs[1]=$res[10];
        $langs[2]=$res[11];
        $langs[3]=$res[12];
        $langs[4]=$res[13];
        $langs[5]=$res[14];

  /* prepare the stack */
  $final[0]=$the_conf; // conf
  $final[1]=date("Y"); // year
  $final[2]=$paperId; // pasccode
  $final[3]=$i; // resourceid
  //$final[4]=$res[2]; // type
  
  if(!empty($res[3])){ // type
    $final[4]=$res[3];
}  else{   
    $final[4]=$res[2];
}
  $final[5]=$res[0]; // name
  $final[6]=$res[4]; //size
  $final[7]=$res[5]; // unit
  
  if(!empty($res[7])){ // status
    $final[8]=$res[7];
}  else{   
    $final[8]=$res[6];
}

$final[9]=$res[8]; // langsel

foreach($langs as $val){
    //foreach($val as $elem){
        if(!empty($val))
            $langdim++;
      //  } 
    }
 if($langdim==0){
     $langdimstr="Zero";
 }elseif ($langdim==1)   {
     $langdimstr="Mono";
     } elseif($langdim==2){
         $langdimstr="Bi";
         }elseif($langdim==3){ $langdimstr="Tri";}
      else{
             $langdimstr="Multi";
         }
         
 $final[10]=$langdimstr; // langdim
 $final[11]= $res[9]; // lang1
 $final[12]= $res[10]; // lang2
 $final[13]= $res[11]; // lang3
 $final[14]= $res[12]; // lang4
 $final[15]= $res[13]; // lang5
 $final[16]= $res[14]; // langOther
 
  if(!empty($res[16])){ // modality
    $final[17]=$res[16];
}  else{   
    $final[17]=$res[15];
}

if(!empty($res[18])){ // use
    $final[18]=$res[18];
}  else{   
    $final[18]=$res[17];
}

if(!empty($res[20])){ // availability
    $final[19]=$res[20];
}  else{   
    $final[19]=$res[19];
}

$final[20]=$res[1]; // ISLRN
$final[21]=$res[21] ; // license
$final[22]=$res[22] ; // url 

$final[23]=$res[23] ; // license
$final[24]=$res[24] ; // url 
        $list[$i - 1] = $final;

}
/* 
table structure

`CONF` varchar(50) COLLATE utf8_bin NOT NULL,
  `YEAR` char(4) COLLATE utf8_bin NOT NULL,
  `passcode` varchar(20) COLLATE utf8_bin NOT NULL DEFAULT '',
  `resourceid` int(11) NOT NULL DEFAULT '0',
  `type` text COLLATE utf8_bin,
  `name` text COLLATE utf8_bin,
  `size` text COLLATE utf8_bin,
  `unit` text COLLATE utf8_bin,
  `prodstatus` text COLLATE utf8_bin,
  `langsel` text COLLATE utf8_bin,
  `langdimension` text COLLATE utf8_bin,
  `lang1` text COLLATE utf8_bin,
  `lang2` text COLLATE utf8_bin,
  `lang3` text COLLATE utf8_bin,
  `lang4` text COLLATE utf8_bin,
  `lang5` text COLLATE utf8_bin,
  `langother` text COLLATE utf8_bin,
  `modality` text COLLATE utf8_bin,
  `resourceusage` text COLLATE utf8_bin,
  `avail` text COLLATE utf8_bin,
  `ISLRN` text COLLATE utf8_bin,
  `license` text COLLATE utf8_bin,
  `url` text COLLATE utf8_bin,
  `doc` text COLLATE utf8_bin,
  `description` text COLLATE utf8_bin,
*/
$sql = array();
// connection
$connection=GetMyConnection();
$k=0;
foreach($list as $val){
    $j=0;
    $str=" VALUES (  ";
    foreach($val as $elem){
        //echo $j." ".$elem." </br>";
        //echo "\"".$elem."\", \"".$elem. "\", ";
        if($j<count($val)-1)
            $str=$str."\"".$elem."\", ";
        else 
            $str=$str."\"".$elem."\"); ";
        // create the insert string
        
        
        $j++;
}
    
    
    $sql[$k]=$str;
    //echo $sql[$k];
    $k++;
    
}

$ret=InsertIntoDb($connection, $sql);
//echo "XXXXXX ".$ret;
if($ret==1){
    include("./lremapso_files/footer_ok.php");
    
}
else{
    include("./lremapso_files/footer_ko.php");
}
CloseConnection();
?>
<hr>
<p align=center>

<small>
Based on 
<a href="http://www.softconf.com" target="_blank">START</a> 
Conference Manager (V2.61.0 - Rev. 4391)
</small>
<p>&nbsp;
</body>
</html>